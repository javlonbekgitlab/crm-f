import { action, observable } from "mobx"

export const globalVariables = observable({
    set: action(function(res){
        const arrayOfRes = Object.keys(res)
        arrayOfRes.map(item => {
            this[item] = res[item]
        })
    })
})

export const counterImg = observable({
    value: 0,
    setValue: function(value) {
        this.value = value
    }
})
