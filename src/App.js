import React, { useState } from 'react'
import './App.css'
// import { AdminPage } from './pages/admin-page-oldVersion'
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom"
import { NotFound } from './pages/not-found'
import { routes } from './store/routes'
import { ModuleMenu } from './pages/moduleMenu'
import { Property } from './pages/assets/property'
import { Retrieve } from './pages/assets/retrieve'
import { Release } from './pages/assets/release'
import { Defects } from './pages/assets/defects'
import { Technics } from './pages/assets/technics'
import { Orders } from './pages/sales/orders'
import { ApplicationTable } from './pages/sales/applications'
import { SupportPro } from './pages/sales/supportPro'
import { Storage } from './pages/assets/storage'
// import { Office } from './pages/assets/office'
import { Branch } from './pages/assets/branch'
import { inject, observer } from 'mobx-react'
import { FiscalModules } from './pages/assets/fiscalModules'
import { Devices } from './pages/assets/devices'
import { Interior } from './pages/assets/interior'
import { Others } from './pages/assets/others'
import { CustomSnackbar } from './components/notification/snackbar'
import { closeSnackBar } from './actions'
import { Tools } from './pages/adminstration/tools'
import { Contacts } from './pages/adminstration/contacts'
import { CalendarCustom } from './pages/adminstration/calendar'
import { SupportProApp } from './pages/sales/supportProApp'
import { Stores } from './pages/sales/stores'
import { HrTable } from './pages/adminstration/hr'
import { CashDrawer } from './pages/sales/cashDrawers'
import { Pdf } from './components/pdfRender'
import { SupportProRepair } from './pages/sales/supportProRepair'
import { PaymentsTable } from './pages/accounting/payments'
import { SupportProTerminalsTable } from './pages/sales/supportProTerminals'
import { Test } from './pages/temp'
import { SubscriberControlSystem } from './pages/accounting/subscriberControlSystem'
import { AdminstrativeContractsTable } from './pages/accounting/adminstrative-contracts'
import { InternationalContractsTable } from './pages/accounting/international-contracts'
import { AdminstrativeContractsAdditionalTable } from './pages/accounting/additional-adminstrative-contracts'
import { InternationalContractsAdditionalTable } from './pages/accounting/addit-international-contracts'
import { AcceptanceCertificate } from './pages/assets/acceptanceCertificate'
// import { accessModule } from './store/localStorage'

export const App = inject('snackbar', 'accessModule')(observer(({snackbar, accessModule}) => {
  const [render, reRender] = useState(true)
  return (
    <>
      <CustomSnackbar
        message = {snackbar.message}
        status = {snackbar.status}
        show = {snackbar.show}
        autoHideDuration = {snackbar.autoHideDuration}
        setShow = {closeSnackBar}
      />
      <BrowserRouter>
        <Switch>
          <Route exact path = {routes.menu} >
            <ModuleMenu render = {render} reRender = {reRender}/>
          </Route>
          <Route path = {routes.orders}>
            {accessModule('support_lite_mod') ? <Orders/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route path = {routes.applications}>
            {accessModule('support_lite_mod') ? <ApplicationTable/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route path = {routes.stores}>
            {accessModule('support_lite_mod') ? <Stores/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route path = {routes.cashDrawers}>
            {accessModule('support_lite_mod') ? <CashDrawer/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route exact path = {routes.supportPro}>
            {accessModule('support_pro_mod') ? <SupportPro/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route exact path = {routes.supportProTerminals}>
            {accessModule('support_pro_mod') ? <SupportProTerminalsTable/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route path = {routes.supportProApp}>
            {accessModule('support_repair_mod') ? <SupportProApp/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route path = {routes.supportProRepair}>
            {accessModule('support_repair_mod') ? <SupportProRepair/> : <Redirect to = {routes.menu}/>} 
          </Route>
          <Route exact path = {routes.property}>
            {accessModule('object_mod') ? <Property/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.retrieve}>
            {accessModule('assembly_mod') ? <Retrieve/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.release}>
            {accessModule('assembly_mod') ? <Release/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.defects}>
            {accessModule('assembly_mod') ? <Defects/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.technics}>
            {(accessModule('user_items_mod') || accessModule('items_mod')) ? <Technics/> : <Redirect to = {routes.menu}/>}
          </Route>
          {/* <Route path = {routes.office}>
            {accessModule('object_mod') ? <Office/> : <Redirect to = {routes.menu}/>}
          </Route> */}
          <Route path = {routes.branch}>
            {accessModule('object_mod') ? <Branch/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.storage}>
            {accessModule('object_mod') ? <Storage/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.fiscalModules}>
            {(accessModule('user_items_mod') || accessModule('items_mod')) ? <FiscalModules/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.devices}>
            {(accessModule('user_items_mod') || accessModule('items_mod')) ? <Devices/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.interior}>
            {(accessModule('user_items_mod') || accessModule('items_mod')) ? <Interior/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.others}>
            {(accessModule('user_items_mod') || accessModule('items_mod')) ? <Others/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.acceptanceCertificate}>
            {(accessModule('assets_mod') || accessModule('items_mod')) ? <AcceptanceCertificate/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.hr}>
            {accessModule('hr_mod') ? <HrTable/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.tools}>
            {accessModule('tools_mod') ? <Tools/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.contacts}>
            {accessModule('tools_mod') ? <Contacts/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.calendar}>
            {accessModule('tools_mod') ? <CalendarCustom/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.payments}>
            {accessModule('e-bank_mod') ? <PaymentsTable/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.subscriberControlSystem}>
            {accessModule('billing_mod') ? <SubscriberControlSystem/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.adminstrativeContracts}>
            {accessModule('contracts_mod') ? <AdminstrativeContractsTable/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.adminstrativeContractsAdditional}>
            {accessModule('contracts_mod') ? <AdminstrativeContractsAdditionalTable/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.internationalContracts}>
            {accessModule('contracts_mod') ? <InternationalContractsTable/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = {routes.internationalContractsAdditional}>
            {accessModule('contracts_mod') ? <InternationalContractsAdditionalTable/> : <Redirect to = {routes.menu}/>}
          </Route>
          <Route path = '/test'>
            <Test/>
          </Route>
          <Route path = '*'>
            <NotFound/>
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  )
}))
