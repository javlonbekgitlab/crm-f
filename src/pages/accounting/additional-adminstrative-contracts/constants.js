export const getTableTitles = lang => ([
    {
        name: '№',
        key: 'index'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.uniqueCode,
        key: 'uniqueCode'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.attachedEmployee,
        key: 'attachedEmployee'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.dateOfContract,
        key: 'dateOfContract'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.numberOfContract,
        key: 'numberOfContract'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.subjectOfContract,
        key: 'subjectOfContract'
    },
])

export const defaultColumns = ['attachedEmployee']