import React, { useCallback, useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultColumns, getTableTitles } from './constants'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import { Loading } from '../../../components/loading'
import { inject } from 'mobx-react'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
// import { getAllApplications } from '../../../store/getDataFromServer'
// import { ApplicationForm } from '../../../components/forms/sales/application'
import { ModalForm } from '../../../components/modalForm'
import { snackbarStatus } from '../../../actions'
import { logOut } from '../../../actions/logout'
import { AdditionalAdminstrativeContractsForm } from '../../../components/forms/accounting/additionalAdminstrativeContForm'
import { getAdminstrativeAdditionalContracts } from '../../../store/getDataFromServer'

export const AdminstrativeContractsAdditionalTable = 
inject('lang', 'snackbar', 'additionalAdminstrativeContractsStore')
(observer(( {lang, snackbar, additionalAdminstrativeContractsStore}) => {

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getTableTitles(lang))

    const [page, setPage] = useState(0)

    const [loading, setLoading] = useState(true)

    const [searchValue, setSearchValue] = useState('')

    const [uploadingForm, setUploadingForm] = useState(false)

    const [status, setStatus] = useState(0)

    const [showForm, setShowForm] = useState(false)

    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [adminstrativeContractData, setAdminstrativeContractData] = useState({
        _id: '',
        dateOfContract: '',
        numberOfContract: '',
        subjectOfContract: ''
    })

    const handleCloseForm = async statusArg => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (statusArg) {
                if (statusArg === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (statusArg.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                snackbar.setSnackbar('', snackbarStatus.error, 0)
                setShowForm(false)
            }
        }
        // repair
        console.log('page',page)
        await getAdminstrativeAdditionalContracts({mainContractId: adminstrativeContractData._id})
    }

    // const handleOpenAdditionalContractsList = ({_id}) => {
    //     // globalVariables.set({
    //     //     merMapId,
    //     //     tin
    //     // })
    //     // console.log(globalVariables)
    //     history.push(routes.adminstrativeContractsAdditional, {_id})
    // }

    // const handleChangeStatus = async ({target}) => {
    //     setStatus(parseInt(target.value))
    //     await getAllApplications({page, status: target.value, search: searchValue})
    // }

    // const handleChangePage = async page => {
    //     setPage(page)
    //     await getAllApplications({page, status, search: searchValue})
    // }

    // const handleSetSearchBar = ({target}) => setSearchValue(target.value)

    // const handleSubmitSearch = async value => {
    //     setSearchValue(value)
    //     await getAllApplications({page, status, search: value})
    // }
    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    // const tableRowClick = order => {
    //     setSelectedRecord(order)
    //     setShowForm(true)
    // } 

    // const handleDelete = async selectedItems => {
        // const { success, data } = await onDelete(selectedItems)

        // if (success) {
            // getMerchants()
            // setMessage(lang.notifications.success.deleted)
            // setMessageStatus('success')
        // } else {
        //     setMessage(lang.notifications.fail.deleted)
        //     setMessageStatus('error')
        // }
        // setMessageShow(true)

        // return success
    // }
    // useEffect(() => console.log('page changed'), [page, setPage])
    useEffect(() => {
        setTimeout(async () => {
            // const res = readLocalStorage('token') 
            setAdminstrativeContractData(history.location.state)
            await logOut({
                history: history, 
                getData: getAdminstrativeAdditionalContracts, 
                // keyForGetData: '_id',
                getDataParams: {
                    mainContractId: history.location.state._id
                },
                historyKeys: [
                    '_id',
                    'dateOfContract',
                    'numberOfContract',
                    'subjectOfContract',
                ]
            })
            setLoading(false)
        }, 0)
        
    }, [])

    useEffect(() => setTableTitles(getTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.additionalAdminstrativeContractsPage.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {additionalAdminstrativeContractsStore.data}
                    // total = {applicationList.total}
                    searchBar
                    // statusBar
                    // page = {page}
                    // setPage = {setPage}
                    // onChangePage = {handleChangePage}
                    tableCellTitles = {tableTitles}
                    // onChangeStatus = {handleChangeStatus}
                    // onRowClick = {tableRowClick}
                    // onClickSearchBarButton = {handleSubmitSearch}
                    // statusValue = {status}
                    // getData = {getAllApplications}
                    // actions = {actions}
                    onClickPlusButton = {handleOpenForm}
                    defaultHiddenColumns = {defaultColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                width = {'sm'}
                keepMounted = {keepMounted}
            >
                <AdditionalAdminstrativeContractsForm
                    onCloseForm = {handleCloseForm}
                    // onUploadingForm = {setUploadingForm}
                    data = {selectedRecord}
                    mainContract = {adminstrativeContractData}
                    // disabled
                />
            </ModalForm>
        </div>
    )
}
))

