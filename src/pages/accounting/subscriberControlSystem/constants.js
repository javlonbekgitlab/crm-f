export const getTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.merchant.tinOrPinfl,
        key: 'INN_PINFL'
    },
    {
        name: lang.table.columnsName.payments.clientName,
        key: 'CONTRAGENTNAME'
    },
    {
        name: lang.table.columnsName.shop.shopName,
        key: 'STORENAMES'
    },
    {
        name: lang.table.columnsName.cashDrawer.machineNo,
        key: 'MASHINENUMS'
    },
    {
        name: lang.forms.branch.agreementDate,
        key: 'CONTRACTDATE'
    },
    {
        name: lang.table.columnsName.payments.balance,
        key: 'BALANCE'
    }
])

export const defaultColumns = []