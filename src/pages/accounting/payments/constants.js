export const getTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.merchant.tinOrPinfl,
        key: 'tinOrPinfl'
    },
    {
        name: lang.table.columnsName.payments.accountNumber,
        key: 'nickName'
    },
    {
        name: lang.table.columnsName.payments.mfo,
        key: 'modelName'
    },
    {
        name: lang.table.columnsName.payments.quantity,
        key: 'machineNo'
    },
    {
        name: lang.table.columnsName.payments.description,
        key: 'fiskalMod'
    },
    {
        name: lang.table.columnsName.payments.payDay,
        key: 'userMobile'
    },
    {
        name: lang.table.columnsName.payments.payTime,
        key: 'contractNumber'
    },
    {
        name: lang.table.columnsName.payments.employee,
        key: 'date'
    }
])

export const defaultColumns = []