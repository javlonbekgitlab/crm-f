export const getTableTitles = lang => ([
    {
        name: '№',
        key: 'index'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.uniqueCode,
        key: 'uniqueCode'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.attachedEmployee,
        key: 'attachedEmployee'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.type,
        key: 'type'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.kindOfContract,
        key: 'kindOfContract'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.dateOfContract,
        key: 'dateOfContract'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.numberOfContract,
        key: 'numberOfContract'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.subjectOfContract,
        key: 'subjectOfContract'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.term,
        key: 'term'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.quantityOfSides,
        key: 'quantityOfSides'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.secondSide,
        key: 'secondSide'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.roleOfSecondSide,
        key: 'roleOfSecondSide'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.thirdSide,
        key: 'thirdSide'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.roleOfThirdSide,
        key: 'roleOfThirdSide'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.score,
        key: 'score'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.sum,
        key: 'sum'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.equivalent,
        key: 'equivalent'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.periodicity,
        key: 'periodicity'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.coefficientOfPrepayment,
        key: 'coefficientOfPrepayment'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.paymentOfPrepayment,
        key: 'paymentOfPrepayment'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.dateOfPrepayment,
        key: 'dateOfPrepayment'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.numberOfPrepaymentOrder,
        key: 'numberOfPrepaymentOrder'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.finalPayment,
        key: 'finalPayment'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.dateOfFinalPayment,
        key: 'dateOfFinalPayment'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.numberOfFinalPaymentOrder,
        key: 'numberOfFinalPaymentOrder'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.dateOfInvoice,
        key: 'dateOfInvoice'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.numberOfInvoice,
        key: 'numberOfInvoice'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.dateOfActCompletionAcceptanceTransfer,
        key: 'dateOfActCompletionAcceptanceTransfer'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.state,
        key: 'state'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.statusOfSecondSide,
        key: 'statusOfSecondSide'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.name,
        key: 'name'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.tin,
        key: 'tin'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.address,
        key: 'address'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.bank,
        key: 'bank'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.mfo,
        key: 'mfo'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.rc,
        key: 'rc'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.oked,
        key: 'oked'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.phoneNumber,
        key: 'phoneNumber'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.web,
        key: 'web'
    },
    {
        name: lang.table.columnsName.adminstrativeContracts.email,
        key: 'email'
    },
    // {
    //     name: lang.table.columnsName.adminstrativeContracts.additionalContract,
    //     key: 'additionalContract'
    // }
])

export const defaultColumns = [
    'attachedEmployee',
    'uniqueCode',
    'type',
    'kindOfContract',
    // 'dateOfContract',
    // 'numberOfContract',
    // 'subjectOfContract',
    'term',
    // 'quantityOfSides',
    'secondSide',
    'roleOfSecondSide',
    'thirdSide',
    'roleOfThirdSide',
    'score',
    // 'sum',
    'equivalent',
    'periodicity',
    'coefficientOfPrepayment',
    'paymentOfPrepayment',
    'dateOfPrepayment',
    'numberOfPrepaymentOrder',
    'finalPayment',
    'dateOfFinalPayment',
    'numberOfFinalPaymentOrder',
    // 'dateOfInvoice',
    // 'numberOfInvoice',
    'dateOfActCompletionAcceptanceTransfer',
    'state',
    'statusOfSecondSide',
    'web',
    'email'
]