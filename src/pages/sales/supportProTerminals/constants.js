export const getTableTitles = lang =>([
    {
        name: '№',
        key: 'index'
    },
    {
        name: lang.table.columnsName.terminals.id,
        key: 'id'
    },
    {
        name: lang.forms.order.serializeNumber,
        key: 'serialNo'
    },
    {
        name: lang.table.columnsName.terminals.resellerName,
        key: 'resellerName'
    },
    {
        name: lang.table.columnsName.terminals.modelName,
        key: 'modelName'
    },
    {
        name: lang.table.columnsName.terminals.firmware,
        key: 'firmware'
    }
])

export const defaultColumns = []

export const dataTmp = [
    {
        index: 1,
        id: 1000446881,
        _id: 'a',
        serialNo: '1170388902',
        resellerName: "Test_DevOps",
        modelName: 'A930',
        firmware: 'A930_PayDroid_7.1.1_Virgo_V04.3.24_20201214'
    },
    {
        index: 2,
        id: 1000446881,
        _id: 'b',
        serialNo: '1170391211',
        resellerName: "Test_DevOps",
        modelName: 'A930',
        firmware: 'A930_PayDroid_7.1.1_Virgo_V04.3.24_20201214'
    },
    {
        index: 3,
        id: 1000446881,
        // _id: 'c',
        serialNo: '1170391211',
        resellerName: "Test_DevOps",
        modelName: 'A930',
        firmware: 'A930_PayDroid_7.1.1_Virgo_V04.3.24_20201214'
    }
]