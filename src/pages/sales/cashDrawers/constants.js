export const getCashDrawersTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.cashDrawer.modelName,
        key: 'modelName'
    },
    {
        name: lang.table.columnsName.cashDrawer.machineNo,
        key: 'machineNo'
    },
    {
        name: lang.table.columnsName.cashDrawer.fiskalMod,
        key: 'fiskalMod'
    },
    {
        name: lang.forms.order.dateAgreement,
        key: 'date'
    },
    {
        name: lang.table.columnsName.partner.name,
        key: 'partnerName'
    },
    {
        name: lang.table.columnsName.partner.phone,
        key: 'partnerPhone'
    },
    // {
    //     name: lang.table.columnsName.partner.createdAt,
    //     key: 'createdAt'
    // }
])

export const cashDrawerDefaultHiddenColumns = []