import React, { useEffect, useState } from 'react'
import { inject, observer } from 'mobx-react'
import { CustomTable } from '../../../components/table'
import { Loading } from '../../../components/loading'
import { cashDrawerDefaultHiddenColumns, getCashDrawersTableTitles } from './constants'
import { useHistory } from 'react-router-dom'
import { AppBarCustom } from '../../../components/appBar'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import styles from '../../index.module.sass'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { ModalForm } from '../../../components/modalForm'
import { localStorageKeys } from '../../../constants'
import { routes } from '../../../store/routes'
import { deleteRequest, snackbarStatus } from '../../../actions'
import { getCashDrawers, getStores, getUsers } from '../../../store/getDataFromServer'
import { apiUrles } from '../../../api'
import { AddDevice } from '../../../components/forms/sales/device'
import { getOneStore } from '../../../components/forms/sales/orderForm/helperFunctions'
import { storeList } from '../../../store'
import { Collapse } from '@material-ui/core'
import { Tin } from '../../../components/forms/sales/orderForm/tin'

export const CashDrawer = inject('cashDrawerList', 'lang', 'snackbar', 'globalVariables')
    
    (observer(({ cashDrawerList, lang, snackbar, globalVariables }) => {

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getCashDrawersTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [showForm, setShowForm] = useState(false)

    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [storeData, setStoreData] = useState({})

    const [merchantData, setMerchantData] = useState({})

    const [activeStep , setActiveStep] = useState(0)

    // const { id, tin } = globalVariables
    const { 
        _id = undefined, 
        merchantId = undefined, 
        tinOrPinfl = undefined, 
        officeId = undefined,
        shopMapId = undefined
    } = history?.location?.state

    useEffect(() => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                await getCashDrawers(_id)
                const { success, data } = await getOneStore(_id)
                console.log('store maerhacasd',data)
                if (success) {
                    setMerchantData(data.merchant)
                    setStoreData(data)
                } else {
                    history.goBack()
                }
            }
            setLoading(false)
        }, 0)
    }, [])

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const handleSetActiveStep = step => {
        (step === 0) ? setActiveStep(step) : setActiveStep(activeStep + step)
    }

    // const handleRowClick = record => {
    //     console.log(record)
    //     setShowForm(true)
    //     setSelectedRecord(record)
    // }

    // const handleDelete = async selectedItems => {
        // const { success } = await deleteRequest({url: apiUrles.deleteUsers ,selectedItems})
        // if (success) {
        //     getUsers()
        //     snackbar.setSnackbar(lang.notifications.success.deleted, snackbarStatus.success)
        // } else {
        //     snackbar.setSnackbar(lang.notifications.fail.deleted, snackbarStatus.error)
        // }

        // return success
    // }

    const handleCloseForm = status => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (status) {
                if (status === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (status.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                setActiveStep(0)
                setShowForm(false)
            }
        }
        getCashDrawers(_id)
    }

    // const handleClickMore = () => history.push(routes.stores)

    // const actions = [
    //     {
    //         onClick: handleClickMore,
    //         icon: <MoreVertIcon color = 'action'/>
    //     }
    // ]

    useEffect(() => setTableTitles(getCashDrawersTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom 
                goHome
                title = {lang.cashDrawerPage.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {cashDrawerList.data}
                    tableCellTitles = {tableTitles}
                    defaultHiddenColumns = {cashDrawerDefaultHiddenColumns}
                    // actions = {actions}
                    onClickPlusButton = {handleOpenForm}
                    // onRowClick = {handleRowClick}
                />
                <ModalForm
                    open = {showForm}
                    close = {handleCloseForm}
                    width = 'sm'
                    keepMounted = {keepMounted}
                >
                    <Collapse in = {activeStep === 0}>
                        <Tin
                            exisistTinOrPinfl = {tinOrPinfl}
                            showComponent = {showForm}
                            // onChangeTin = {setTin}
                            onCloseForm = {handleCloseForm}
                            onSetActiveStep = {handleSetActiveStep}
                        />
                    </Collapse>
                    <Collapse in = {activeStep === 1}>
                        <AddDevice
                            data = {selectedRecord}
                            tinOrPinfl = {tinOrPinfl}
                            onCloseForm = {handleCloseForm}
                            shopId = {_id}
                            shopMapId = {shopMapId}
                            merchantId = {merchantId}
                            officeId = {officeId}
                            onUploadingForm = {setUploadingForm}
                            merchantData = {merchantData}
                            storeData = {storeData}
                            onSetActiveStep = {handleSetActiveStep}
                            phone = {merchantData?.userMobile}
                        />
                        {/* <AddDevice
                            tin = {tin}
                            onUploadingForm = {setUploadingForm}
                            onCloseForm = {handleCloseForm}
                            onSetActiveStep = {handleSetActiveStep}
                            storeData = {storeValues}
                            merchantData = {merchantData}
                            // submitValues = {{...storeValues, ...orderValues}}
                        /> */}
                    </Collapse>
                </ModalForm>
            </div>
        </div>
    )
}))