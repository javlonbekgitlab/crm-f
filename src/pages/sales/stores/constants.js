export const getStoresTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.shop.shopName,
        key: 'shopName'
    },
    {
        name: lang.table.columnsName.shop.region,
        key: 'regionName'
    },
    {
        name: lang.table.columnsName.shop.district,
        key: 'districtName'
    },
    {
        name: lang.table.columnsName.shop.address,
        key: 'cityAddress'
    },
    {
        name: lang.table.columnsName.shop.activityGroup,
        key: 'activityGroupName'
    },
    {
        name: lang.table.columnsName.shop.activityType,
        key: 'activityTypeName'
    },
    {
        name: lang.table.columnsName.partner.name,
        key: 'partnerName'
    },
    {
        name: lang.table.columnsName.partner.phone,
        key: 'partnerPhone'
    },
    {
        name: lang.table.columnsName.partner.createdAt,
        key: 'createdAt'
    }
])

export const defaultColumns = ['partnerPhone', 'activityTypeName']