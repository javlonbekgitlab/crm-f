import React, { useEffect, useState } from 'react'
import { inject, observer } from 'mobx-react'
import { CustomTable } from '../../../components/table'
import { Loading } from '../../../components/loading'
import { getStoresTableTitles, defaultColumns } from './constants'
import { CustomSnackbar } from '../../../components/notification/snackbar'
import { useHistory, withRouter } from 'react-router-dom'
import { AppBarCustom } from '../../../components/appBar'
import MoreVertIcon from '@material-ui/icons/MoreVert'  
import styles from '../../index.module.sass'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { ModalForm } from '../../../components/modalForm'
// import useLogOut from '../../../components/appBarActions/helperFunctions'
import { localStorageKeys } from '../../../constants'
import { routes } from '../../../store/routes'
import { deleteRequest, snackbarStatus } from '../../../actions'
import { getStores, getUsers } from '../../../store/getDataFromServer'
import { StoreOrderForm } from '../../../components/forms/sales/storeOrderForm'
import { logOut } from '../../../actions/logout'
import { Tin } from '../../../components/forms/sales/orderForm/tin'
import { Collapse } from '@material-ui/core'
import { AddDevice } from '../../../components/forms/sales/device'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'

export const Stores = inject(
    'storeList', 
    'lang', 
    'snackbar'
    )(observer(({ 
        storeList, 
        lang, 
        snackbar
    }) => {

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getStoresTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [showForm, setShowForm] = useState(false)

    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [storeValues, setStoreValues] = useState({})

    const [activeStep , setActiveStep] = useState(0)

    const [merchantData, setMerchantData] = useState({
        nickName: undefined,
        userMobile: undefined
    })

    // const [tin, setTin] = useState('')

    const [storeDataFromMerch, setStoreDataFromMerch] = useState({
        _id: null,
        tinOrPinfl: null,
        merMapId: null,
    })
    // history?.location?.state
    // const { id, tin } = globalVariables

    useEffect(() => {
        setTimeout(async () => {
            // const res = readLocalStorage('token') 
            await logOut({
                history: history, 
                getData: getStores, 
                keyForGetData: '_id',
                historyKeys: [
                    '_id',
                    'tinOrPinfl'
                ]
            })
            setStoreDataFromMerch({
                _id: history?.location?.state?._id,
                tinOrPinfl: history?.location?.state?.tinOrPinfl,
                merMapId: history?.location?.state?.merMapId
            })
            setMerchantData({
                nickName: history?.location?.state?.nickName,
                userMobile: history?.location?.state?.userMobile
            })
            // if (!res || !history?.location?.state?._id || !history?.location?.state?.tin) {
            //     // history.replace(routes.menu)
            //     // removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            // } else {
            //     await getStores(_id)
            // }
            setLoading(false)
        }, 0)
        
    }, [])
    
    const { _id, tinOrPinfl, merMapId } = storeDataFromMerch

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const handleSetStoreValues = storeValuesFromForm => {
        setStoreValues({...storeValuesFromForm, merchantId: _id, merMapId, tinOrPinfl})
    }

    const handleSetActiveStep = step => {
        (step === 0) ? setActiveStep(step) : setActiveStep(activeStep + step)
    }

    // const handleRowClick = record => {
    //     console.log(record)
    //     setShowForm(true)
    //     setSelectedRecord(record)
    // }

    // const handleDelete = async selectedItems => {
        // const { success } = await deleteRequest({url: apiUrles.deleteUsers ,selectedItems})
        // if (success) {
        //     getUsers()
        //     snackbar.setSnackbar(lang.notifications.success.deleted, snackbarStatus.success)
        // } else {
        //     snackbar.setSnackbar(lang.notifications.fail.deleted, snackbarStatus.error)
        // }

        // return success
    // }

    const handleCloseForm = status => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (status) {
                if (status === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (status.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                        setActiveStep(0)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                setActiveStep(0)
                setShowForm(false)
            }
        }
        getStores(_id)
    }

    const handleClickMore = ({_id, merchantId, tinOrPinfl, officeId, shopMapId}) => {
        // globalVariables.set({_id})
        history.push(routes.cashDrawers, {_id, merchantId, tinOrPinfl, officeId, shopMapId})
    }

    const actions = [
        {
            onClick: handleClickMore,
            icon: <MoreVertIcon color = 'action'/>
        }
    ]

    useEffect(() => setTableTitles(getStoresTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom
                goHome 
                title = {lang.storePage.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {storeList.data}
                    tableCellTitles = {tableTitles}
                    defaultHiddenColumns = {defaultColumns}
                    actions = {actions}
                    onClickPlusButton = {handleOpenForm}
                    // onRowClick = {handleRowClick}
                />
                <ModalForm
                    open = {showForm}
                    close = {handleCloseForm}
                    width = {activeStep === 1 ? 'lg' : 'sm'}
                    keepMounted = {keepMounted}
                >
                    <Collapse in = {activeStep === 0}>
                        <Tin
                            exisistTinOrPinfl = {tinOrPinfl}
                            showComponent = {showForm}
                            // onChangeTin = {setTin}
                            onCloseForm = {handleCloseForm}
                            onSetActiveStep = {handleSetActiveStep}
                        />
                    </Collapse>
                    <Collapse in = {activeStep === 1}>
                        <StoreOrderForm
                            data = {selectedRecord}
                            tinOrPinfl = {tinOrPinfl}
                            onSetActiveStep = {handleSetActiveStep}
                            // merchantId = {_id}
                            // merMapId = {merMapId}
                            onSetStoreValues = {handleSetStoreValues}
                            onCloseForm = {handleCloseForm}
                        />
                    </Collapse>
                    <Collapse in = {activeStep === 2}>
                        <AddDevice
                            tinOrPinfl = {tinOrPinfl}
                            onUploadingForm = {setUploadingForm}
                            onCloseForm = {handleCloseForm}
                            onSetActiveStep = {handleSetActiveStep}
                            storeData = {storeValues}
                            merchantData = {merchantData}
                            // submitValues = {{...storeValues, ...orderValues}}
                        />
                    </Collapse>
                </ModalForm>
            </div>
        </div>
    )
}))