export const getSupportProAppTableTitles = lang => ([
    {
      name: 'ID',
      key: 'index'
    },
    {
      name: lang.forms.order.companyName,
      key: 'name'
    },
    {
      name: lang.table.columnsName.application.package,
      key: 'package'
    },
    {
      name: lang.table.columnsName.application.version,
      key: 'version'
    },
    {
      name: lang.table.columnsName.application.date,
      key: 'date'
    }
])

export const supportProAppDetailDataColumns = [
    {
      name: 'IDtitle',
      key: 'index'
    },
    {
      name: 'nametitle',
      key: 'name'
    },
    {
      name: 'middleNametitle',
      key: 'middleName'
    }
  ]

export const defaultSupportProAppHiddenColumns = []
