// export const ordersTableTitles = [
//     {
//         name: 'ID',
//         key: 'index'
//     },
//     {
//         name: 'Контракт',
//         key: 'contract'
//     },
//     {
//         name: 'Название',
//         key: 'shopName'
//     },
//     {
//         name: 'ИНН',
//         key: 'inn'
//     },
//     {
//         name: 'Серийный номер',
//         key: 'serial_number'
//     },
//     {
//         name: 'Фискальный модуль',
//         key: 'fisk_number'
//     },
// ]

export const getOrdersTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.merchant.name,
        key: 'nickName'
    },
    {
        name: lang.table.columnsName.merchant.phone,
        key: 'userMobile'
    },
    {
        name: lang.table.columnsName.merchant.tinOrPinfl,
        key: 'tinOrPinfl'
    },
    {
        name: lang.table.columnsName.partner.name,
        key: 'partnerName'
    },
    {
        name: lang.table.columnsName.partner.phone,
        key: 'partnerPhone'
    }
])

export const defaultColumns = []