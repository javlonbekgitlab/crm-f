import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultColumns, getOrdersTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import MoreVertIcon from '@material-ui/icons/MoreVert'  
import { OrderForm } from '../../../components/forms/sales/orderForm/index.js'
import { getMerchants, getRegions } from '../../../store/getDataFromServer'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { StoreOrderForm } from '../../../components/forms/sales/storeOrderForm'
import { Loading } from '../../../components/loading'
import { inject } from 'mobx-react'
import { snackbarStatus } from '../../../actions'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'
import { Collapse } from '@material-ui/core'
import { AddDevice } from '../../../components/forms/sales/device'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { Tin } from '../../../components/forms/sales/orderForm/tin'

export const Orders = inject('merchantsList', 'lang', 'snackbar')
    
    (observer(( {merchantsList, lang, snackbar}) => {
    
    useEffect(async () => {
        await getRegions()
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                await getMerchants()
            }
            setLoading(false)
        }, 0)
    }, [])

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getOrdersTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)

    const [activeStep , setActiveStep] = useState(0)
    
    const [showForm, setShowForm] = useState(false)

    const [searchValue, setSearchValue] = useState('')
    
    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [orderValues, setOrderValues] = useState({})
    
    const [storeValues, setStoreValues] = useState({})

    const [tinOrPinfl, setTinOrPinfl] = useState('')

    const handleCloseForm = async status => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (status) {
                if (status === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (status.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                        setActiveStep(0)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                setActiveStep(0)
                setShowForm(false)
            }
        }
        await getMerchants()
    }

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const handleSetActiveStep = step => {
        (step === 0) ? setActiveStep(step) : setActiveStep(activeStep + step)
    }

    const handleSubmitSearch = value => {
        setSearchValue(value)
        console.log(value)
    }
    // const tableRowClick = order => {
    //     setSelectedRecord(order)
    //     setShowForm(true)
    //     setActiveStep(1)
    // } 

    // const handleDelete = async selectedItems => {
        // const { success, data } = await onDelete(selectedItems)

        // if (success) {
            // getMerchants()
            // setMessage(lang.notifications.success.deleted)
            // setMessageStatus('success')
        // } else {
        //     setMessage(lang.notifications.fail.deleted)
        //     setMessageStatus('error')
        // }
        // setMessageShow(true)

        // return success
    // }

    const handleOpenStoreList = ({_id, tinOrPinfl, merMapId, nickName, userMobile}) => {
        // globalVariables.set({
        //     merMapId,
        //     tin
        // })
        // console.log(globalVariables)
        history.push(routes.stores, {_id, tinOrPinfl, merMapId, nickName, userMobile})
    }

    // const handleChangeStatus = ({target}) => {
    //     setStatus(+target.value)
    //     console.log(target.value)
    // }
    
    const actions = [
        {
            onClick: handleOpenStoreList,
            icon: <MoreVertIcon color = 'action'/>
        }
    ]

    useEffect(() => setTableTitles(getOrdersTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.orderPage.title}>
                <UniversalAppBarActions/>
                {/* <OrdersAppBarActions
                    userName = {`${merchantsList?.data?.personalData?.lastName} ${merchantsList?.data?.personalData?.firstNameInitial}`} 
                    phone = {merchantsList?.data?.personalData?.phone}
                /> */}
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {merchantsList.data}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenForm}
                    onClickSearchBarButton = {handleSubmitSearch}
                    // onRowClick = {tableRowClick}
                    actions = {actions}
                    // defaultHiddenColumns = {defaultColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                width = {activeStep === 2 ? 'lg' : 'sm'}
                keepMounted = {keepMounted}
            >
                <Collapse in = {activeStep === 0}>
                    <Tin
                        showComponent = {showForm}
                        onChangeTinOrPinfl = {e => {console.log(e);setTinOrPinfl(e)}}
                        onCloseForm = {handleCloseForm}
                        onSetActiveStep = {handleSetActiveStep}
                    />
                </Collapse>
                <Collapse in = {activeStep === 1}>
                    <OrderForm
                        data = {selectedRecord}
                        onUploadingForm = {setUploadingForm}
                        closeForm = {handleCloseForm}
                        tinOrPinfl = {tinOrPinfl}
                        onSetActiveStep = {handleSetActiveStep}
                        onSetOrderValues = {setOrderValues}
                    />
                </Collapse>
                <Collapse in = {activeStep === 2}>
                    <StoreOrderForm
                        // data = {selectedRecord}
                        tinOrPinfl = {tinOrPinfl}
                        onSetActiveStep = {handleSetActiveStep}
                        onSetStoreValues = {setStoreValues}
                        onCloseForm = {handleCloseForm}
                    />
                </Collapse>
                <Collapse in = {activeStep === 3}>
                    <AddDevice
                        // data = {selectedRecord}
                        tinOrPinfl = {tinOrPinfl}
                        onUploadingForm = {setUploadingForm}
                        onCloseForm = {handleCloseForm}
                        onSetActiveStep = {handleSetActiveStep}
                        merchantData = {orderValues}
                        storeData = {storeValues}
                        fullSubmit
                        // submitValues = {{...storeValues, ...orderValues}}
                    />
                </Collapse>
            </ModalForm>
            {/* <ModalForm
                open = {shopForm}
                close = {handleCloseShopForm}
                width = 'xl'
            >
                <StoreOrderForm
                    data = {dataForShop}
                    onCloseForm = {handleCloseShopForm}
                />
            </ModalForm> */}
        </div>
    )
}
))

