export const ordersTableTitles = [
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.order.contractNumber,
        key: 'contract'
    },
    {
        name: lang.table.columnsName.shop.shopName,
        key: 'shopName'
    },
    {
        name: lang.forms.order.tin,
        key: 'inn'
    },
    {
        name: lang.forms.order.serializeNumber,
        key: 'serial_number'
    },
    {
        name: lang.forms.order.fiskNumber,
        key: 'fisk_number'
    },
]

export const defaultColumns = []