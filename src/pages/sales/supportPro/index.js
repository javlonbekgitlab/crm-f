import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { Link, useHistory } from 'react-router-dom'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import { Loading } from '../../../components/loading'
import { inject } from 'mobx-react'
import { Paper } from '@material-ui/core'
import { ButtonWithIcon } from '../../../components/button'
import stylesSecond from './index.module.sass'
import { Map } from '../../../components/map'
import { routes } from '../../../store/routes'
import { CustomMenu } from '../../../components/menu'
import { Confirmation } from '../../../components/menu/menuContent/confirmation'
import { SupportProForm } from '../../../components/forms/sales/supportPro'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { localStorageKeys } from '../../../constants'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { Marker } from 'react-leaflet'
import { logOut } from '../../../actions/logout'
import { onSearchTerminal } from '../../../components/forms/sales/supportPro/helperFunctions'

export const SupportPro = inject('lang', 'terminalStore')(observer(({lang, terminalStore}) => {
    
    console.log('data of terminal store', terminalStore)
    console.log('data of ', terminalStore.data)

    const getTerminalData = async searchValue => {
        const res = await onSearchTerminal(searchValue)
        console.log('res',res)
    }

    useEffect(() => {
        setTimeout(async () => {
            await logOut({
                history: history, 
                getData: getTerminalData, 
                keyForGetData: 'serialNo',
                historyKeys: [
                    'serialNo',
                    'firmware',
                    'id',
                    'modelName',
                    'resellerName',
                    '_id',
                    'index'
                ]
            })
            console.log(history.location.state)
            // const res = readLocalStorage('token') 
            // if (!res) {
            //     history.replace(routes.menu)
            //     removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            // } else {
            // }
            setLoading(false)
        }, 0)
    }, [])

    const history = useHistory()

    const [loading, setLoading] = useState(true)

    const [message, setMessage] = useState('')

    const [messageShow, setMessageShow] = useState(false)

    const [messageStatus, setMessageStatus] = useState('info')

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.supportProPage.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <div className = {stylesSecond.paperCont}>
                    <Paper className = {stylesSecond.paper} elevation = {3}>
                        <div className = {stylesSecond.header}>
                            <Link to = {routes.supportProApp}>
                                <ButtonWithIcon
                                    variant = 'contained'
                                >
                                    {lang.supportProPage.buttonTitles.app}
                                </ButtonWithIcon>
                            </Link>
                            <CustomMenu
                                render = {closeMenu => <Confirmation 
                                    onCloseMenu = {closeMenu}
                                    text = {lang.supportProPage.confirmation.text}
                                    title = {lang.supportProPage.confirmation.title}
                                />}
                                button = {
                                    <ButtonWithIcon
                                        variant = 'contained'
                                    >
                                        {lang.supportProPage.buttonTitles.uzcardPos}
                                    </ButtonWithIcon>
                                }
                            />
                            <CustomMenu
                                render = {closeMenu => <Confirmation 
                                    onCloseMenu = {closeMenu}
                                    text = {lang.supportProPage.confirmation.text}
                                    title = {lang.supportProPage.confirmation.title}
                                />}
                                button = {
                                    <ButtonWithIcon
                                        variant = 'contained'
                                    >
                                        {lang.supportProPage.buttonTitles.uzcardPosSa}
                                    </ButtonWithIcon>
                                }
                            />
                            <CustomMenu 
                                render = {closeMenu => <Confirmation 
                                    onCloseMenu = {closeMenu}
                                    text = {lang.supportProPage.confirmation.text}
                                    title = {lang.supportProPage.confirmation.title}
                                />}
                                button = {
                                    <ButtonWithIcon
                                        variant = 'contained'
                                    >
                                        {lang.supportProPage.buttonTitles.tinda}
                                    </ButtonWithIcon>
                                }
                            />
                            <CustomMenu
                                render = {closeMenu => <Confirmation 
                                    onCloseMenu = {closeMenu}
                                    text = {lang.supportProPage.confirmation.text}
                                    title = {lang.supportProPage.confirmation.title}
                                />}
                                button = {
                                    <ButtonWithIcon
                                        variant = 'contained'
                                    >
                                        {lang.supportProPage.buttonTitles.tindaFm}
                                    </ButtonWithIcon>
                                }
                            />
                            <ButtonWithIcon
                                variant = 'contained'
                            >
                                {lang.supportProPage.buttonTitles.connectPax}
                            </ButtonWithIcon>
                            <ButtonWithIcon
                                variant = 'contained'
                            >
                                {lang.supportProPage.buttonTitles.pushMessage}
                            </ButtonWithIcon>
                            <ButtonWithIcon
                                variant = 'contained'
                            >
                                {lang.supportProPage.buttonTitles.sync}
                            </ButtonWithIcon>
                        </div>
                        <div className = {stylesSecond.content}>
                            <div className = {stylesSecond.mapCont}>
                                <Map
                                    center = {terminalStore.data?.geoLocation?.lat && [
                                        terminalStore.data.geoLocation.lat, 
                                        terminalStore.data.geoLocation.lng
                                    ]}
                                >
                                    {terminalStore.data?.geoLocation?.lat && terminalStore.data?.geoLocation?.lng && 
                                        <Marker position = {[
                                            terminalStore.data.geoLocation.lat, 
                                            terminalStore.data.geoLocation.lng
                                        ]}/>
                                    }
                                </Map>    
                            </div> 
                            <SupportProForm/>
                        </div>
                    </Paper>
                </div>
            </div>
        </div>
    )
}
))

