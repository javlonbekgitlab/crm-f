import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultColumns, getTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import MoreVertIcon from '@material-ui/icons/MoreVert'  
// import { OrderForm } from '../../../components/forms/sales/orderForm'
import { getMerchants, getRegions } from '../../../store/getDataFromServer'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { StoreOrderForm } from '../../../components/forms/sales/storeOrderForm'
import { Loading } from '../../../components/loading'
import { inject } from 'mobx-react'
import { snackbarStatus } from '../../../actions'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'
import { Collapse } from '@material-ui/core'
import { AddDevice } from '../../../components/forms/sales/device'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { Tin } from '../../../components/forms/sales/orderForm/tin'
import { SupportProRepairForm } from '../../../components/forms/sales/supportProRepair'

export const SupportProRepair = inject('supportProRepairList', 'lang', 'snackbar')
    
    (observer(( {supportProRepairList, lang, snackbar}) => {
    
    useEffect(async () => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                // await getMerchants()
            }
            setLoading(false)
        }, 0)
    }, [])

    const history = useHistory()
    
    const [tableTitles, setTableTitles] = useState(getTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)

    const [showForm, setShowForm] = useState(false)

    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const handleCloseForm = async status => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (status) {
                if (status === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (status.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                setShowForm(false)
            }
        }
        // await getMerchants()
    }

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const tableRowClick = order => {
        setSelectedRecord(order)
        setShowForm(true)
    } 

    // const handleDelete = async selectedItems => {
        // const { success, data } = await onDelete(selectedItems)

        // if (success) {
            // getMerchants()
            // setMessage(lang.notifications.success.deleted)
            // setMessageStatus('success')
        // } else {
        //     setMessage(lang.notifications.fail.deleted)
        //     setMessageStatus('error')
        // }
        // setMessageShow(true)

        // return success
    // }

    useEffect(() => setTableTitles(getTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            {/* <Loading start = {loading}/> */}
            <AppBarCustom title = {lang.supportProRepair.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {supportProRepairList.data}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenForm}
                    onRowClick = {tableRowClick}
                    // actions = {actions}
                    defaultHiddenColumns = {defaultColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                width = {'sm'}
                keepMounted = {keepMounted}
            >
                <SupportProRepairForm
                    closeForm = {handleCloseForm}
                    data = {selectedRecord}
                />
            </ModalForm>
        </div>
    )
}
))

