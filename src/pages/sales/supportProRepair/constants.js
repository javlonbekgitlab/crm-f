export const getTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.cashDrawer.modelName,
        key: 'modelNo'
    },
    {
        name: lang.table.columnsName.supportProRepair.applicationNumber,
        key: 'applicationNumber'
    },
    {
        name: lang.table.columnsName.merchant.name,
        key: 'merchName'
    },
    {
        name: lang.table.columnsName.cashDrawer.machineNo,
        key: 'serial_number'
    },
    {
        name: lang.table.columnsName.supportProRepair.createDate,
        key: 'createDate'
    },
    {
        name: lang.table.columnsName.supportProRepair.breakdownReason,
        key: 'breakdownReason'
    },
    {
        name: lang.table.columnsName.supportProRepair.ctoEmployee,
        key: 'ctoEmployee'
    }
])

export const defaultColumns = []