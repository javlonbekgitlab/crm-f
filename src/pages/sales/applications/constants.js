export const getTableTitles = lang =>([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.table.columnsName.merchant.name,
        key: 'nickName'
    },
    {
        name: lang.table.columnsName.merchant.tinOrPinfl,
        key: 'tinOrPinfl'
    },
    {
        name: lang.table.columnsName.cashDrawer.modelName,
        key: 'modelName'
    },
    {
        name: lang.table.columnsName.cashDrawer.machineNo,
        key: 'machineNo'
    },
    {
        name: lang.table.columnsName.cashDrawer.fiskalMod,
        key: 'fiskalMod'
    },
    {
        name: lang.table.columnsName.merchant.phone,
        key: 'userMobile'
    },
    {
        name: lang.forms.order.contractNumber,
        key: 'contractNumber'
    },
    {
        name: lang.forms.order.dateAgreement,
        key: 'date'
    },
    {
        name: lang.table.columnsName.cashDrawer.applicationStatus,
        key: 'statusName'
    },
    {
        name: lang.table.columnsName.shop.activityGroup,
        key: 'activityGroupName'
    },
    {
        name: lang.table.columnsName.shop.activityType,
        key: 'activityTypeName'
    },
    {
        name: lang.table.columnsName.shop.region,
        key: 'regionName'
    },
    {
        name: lang.table.columnsName.shop.district,
        key: 'districtName'
    },
    {
        name: lang.table.columnsName.shop.shopName,
        key: 'shopName'
    },
    {
        name: lang.forms.order.bidType,
        key: 'typeOfApplicationName'
    },
    {
        name: lang.table.columnsName.partner.name,
        key: 'partnerName'
    },
    {
        name: lang.table.columnsName.partner.phone,
        key: 'partnerPhone'
    },
    {
        name: lang.table.columnsName.partner.createdAt,
        key: 'createdAt'
    }
    // {
    //     name: lang.table.columnsName.cashDrawer.contractFile,
    //     key: 'contractFile'
    // },
    // {
    //     name: lang.table.columnsName.cashDrawer.applicationFile,
    //     key: 'applicationFile'
    // },
    // {
    //     name: lang.table.columnsName.shop.cadastreFile,
    //     key: 'cadastreFile'
    // },
    // {
    //     name: lang.table.columnsName.shop.nomenclatureFile,
    //     key: 'nomenclatureFile'
    // }
])

export const defaultColumns = [
    'typeOfApplicationName', 
    'shopName', 
    'activityTypeName', 
    'activityGroupName',
    'userMobile',
    'contractNumber',
    'createdAt',
    
]