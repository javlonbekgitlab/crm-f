// class Test {
import { inject } from 'mobx-react';
import { observer } from 'mobx-react';
import { action, observable } from 'mobx';
//     set = function({target}){
//         this[target.name] = target.value
//         console.log(this[target.name])
//     }
// }

// const object = new Test()
// const tmp = {
//     target: {
//         value: 'tets'
//     }
// }
// object.set(tmp)
// String.prototype.tes = function(){
//     console.log(this)
// }

// 'asdas'.tes()

// const arr = [
//     {
//         key: 1,
//         keytwo: '123123'
//     },
//     {
//         key: 1,
//         keytwo: '123123'
//     },
//     {
//         key: 1,
//         keytwo: '123123'
//     },
//     {
//         key: 1,
//         keytwo: '123123'
//     }
// ]
// const arr = []
// for(let i = 0; i < 100000000; i++) {
//     arr[i] = {
//         key: 5,
//         keytwo: '123123'
//     }
// }
// console.time('test2')
// arr.map(item => {
//     item.key = 5
// })
// arr.map(item => {
//     item.keytwo = 'aasda'    
// })
// console.timeEnd('test2')
// console.time('test1')
// arr.map(item => {
//     item.key = 5
//     item.keytwo = 'aasda'    
// })
// console.timeEnd('test1')

console.log(process.argv)
export const test = observable({
    value: '123',
    setValue: action(function(){
        this.value = Math.random()
    })
})

export const Test = inject('test')(observer(({test}) => {
    return (
        <>
            <div>{test.value}</div>
            <input value = {test.value}/>
            <button onClick = {() => test.setValue()}>change</button>
        </>
    )
}))
