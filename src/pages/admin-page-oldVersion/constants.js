export const tableTitle = {
    partner: 'partners',
    merchant: 'merchants',
    store: 'stores'
}
export const partnerTableTitles = [
    {
        name: 'ID',
        key: 'thirdOpMapId'
    },
    {
        name: 'Имя партнера',
        key: 'opName'
    },
    {
        name: 'Адрес',
        key: 'cityAddress'
    },
    {
        name: 'Контактное лицо',
        key: 'contactManager'
    },
    {
        name: 'Логин',
        key: 'contactMob'
    },
    {
        name: 'Время создания',
        key: 'time'
    }
]

export const defaultHiddenColumns = ['opName']

export const storeTableTitles = [
    {
        name: 'ID',
        key: 'thirdOpMapId'
    },
    {
        name: 'Название магазина',
        key: 'shopName'
    },
    {
        name: 'Адрес магазина',
        key: 'cityAddress'
    },
    {
        name: 'Время создания',
        key: 'time'
    }
]


