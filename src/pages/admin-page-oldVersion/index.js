// import React, { useEffect, useState } from 'react'
// import { defaultHiddenColumns, partnerTableTitles, tableTitle, } from './constants'
// import { partList } from '../../store'
// import styles from './index.module.sass'
// import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos'
// import { CustomTable } from '../../components/table'
// import { ModalForm } from '../../components/modalForm'
// import { PartnerForm } from '../../components/forms/partnerForm'
// import { lang } from '../../languages/lang'
// import { observer } from 'mobx-react-lite'
// import MoreVertIcon from '@material-ui/icons/MoreVert'  
// import { MerchantTable } from '../../components/merchantTable'
// import { getPartners } from '../../store/getDataFromServer'

// export const AdminPage = observer(() => {

//     useEffect(() => {
//         getPartners()
//     },[])

//     const [partnerForm, setPartnerForm] = useState(false)

//     const [partnerEdit, setPartnerEdit] = useState(false)

//     const [selectedPartner, setSelectedPartner] = useState({})

//     const [openMerchant, setMerchant] = useState(false)

//     const [currentPartner, setCurrentPartner] = useState({})

//     const [arrowUpwardShow, setArrowUpwardShow] = useState(false)

//     const handleOpenMerchant = currentPartner => {
//         setMerchant(true)
//         setCurrentPartner(currentPartner)
//     }

//     const handleCloseMerchant = () => {
//         setMerchant(false)
//     }

//     const handleOpenPartnerForm = () => {
//         setPartnerForm (true)
//         setPartnerEdit(false)
//     }

//     const handleClosePartnerForm  = () => {
//         setPartnerForm (false)
//     }    

//     const handleRowClick = partnerId => {
//         setPartnerEdit(true)
//         setPartnerForm(true)
//         partList.data.map(
//             partner => {
//                 (partner.thirdOpMapId === partnerId.thirdOpMapId) && setSelectedPartner(partner)
//                 return null
//             }     
//         )
//     }

//     const handleContScroll = () => {
//         // problems with calling funtion
//         if (window.pageYOffset > 20) {
//             setArrowUpwardShow(true)
//         } 
//         if (window.pageYOffset < 20) {
//             setArrowUpwardShow(false)
//         }
//     }
//     window.addEventListener('scroll', handleContScroll)

//     const handleArrowClick = () => {
//         window.scrollTo({
//             top: 0,
//             left: 0,
//             behavior: 'smooth'
//         })
//     }

//     const actions = [{
//         onClick: handleOpenMerchant,
//         icon: <MoreVertIcon/>
//     }]

//     return (
//         <div className = {styles.cont}>
//             <CustomTable
//                 tableContent = {partList.data}
//                 tableTitle = {tableTitle.partner}
//                 tableCellTitles = {partnerTableTitles}
//                 defaultHiddenColumns = {defaultHiddenColumns}
//                 onClickPlusButton = {handleOpenPartnerForm}
//                 onRowClick = {handleRowClick}
//                 actions = {actions}
//             />
//             <MerchantTable
//                 open = {openMerchant}
//                 currentPartner = {currentPartner}
//                 closeMerchant = {handleCloseMerchant}
//             />
//             <ModalForm
//                 title = {partnerEdit ? lang.forms.partner.editPartner : lang.forms.partner.addPartner}
//                 open = {partnerForm}
//                 close = {handleClosePartnerForm}
//             >
//                 <PartnerForm
//                     edit = {partnerEdit}
//                     closeForm = {handleClosePartnerForm}
//                     data = {selectedPartner}
//                 />
//             </ModalForm>
//             <div 
//                 className = {arrowUpwardShow ? `${styles.arrowUpward} ${styles.arrowUpwardShow}` : `${styles.arrowUpward}`}
//                 onClick = {handleArrowClick}
//             >
//                 <ArrowForwardIosIcon fontSize = 'large'/>
//             </div>
//         </div>
//     )
// })