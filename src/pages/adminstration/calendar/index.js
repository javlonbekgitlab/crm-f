import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { useHistory } from 'react-router-dom'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import stylesSecond from './index.module.sass'
import { getMonth, getListOfYears } from './helperFunctions'
import { CustomMenu } from '../../../components/menu'
import { currentMonth, currentYear, listOfMonths } from './constants'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { localStorageKeys } from '../../../constants'
import { routes } from '../../../store/routes'

export const CalendarCustom = inject('lang')(observer(({lang}) => {

    const history = useHistory()

    useEffect(() => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {                
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                // await getProperties()
            }
            setLoading(false)
        }, 0)
    }, [])

    const [loading, setLoading] = useState(true)

    const [year, setYear] = useState(currentYear)

    const [month, setMonth] = useState({
        value: currentMonth,
        title: lang.calendar.months[currentMonth].title
    })

    const [selectedMonth] = ([getMonth(year, month.value)])

    const handleSelectYear = year => setYear(year)

    const handleSelectMonth = month => setMonth(month)

    useEffect(() => setMonth(prevState => ({...prevState, title: lang.calendar.months[currentMonth].title})), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.calendar.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <div className = {stylesSecond.content}>
                    <div className = {stylesSecond.dateCont}>
                        <CustomMenu
                            button = {
                                <div className = {stylesSecond.dateContent}>
                                    <div className = {stylesSecond.dateContentItem}>{month.title}</div>
                                    <div className = {stylesSecond.dateContentItem}>{year}</div>
                                </div>
                            }
                        >
                            <div className = {stylesSecond.datePickerCont}>
                                <div className = {stylesSecond.monthCont}>
                                    {lang.calendar.months.map((item, index) => (
                                        <div 
                                            className = {stylesSecond.monthTitle}
                                            onClick = {() => handleSelectMonth({
                                                value: listOfMonths[index].value,
                                                title: item.title
                                            })}
                                        >
                                            {item.title}
                                        </div>
                                    ))}
                                </div>
                                <div className = {stylesSecond.yearsCont}> 
                                    {getListOfYears().map(item => (
                                        <div 
                                            onClick = {() => handleSelectYear(item.value)}
                                            className = {item.currentYear ? `${stylesSecond.year} ${stylesSecond.currentYear}` : stylesSecond.year}
                                        >
                                            {item.title}
                                        </div>
                                    )) }
                                </div>
                            </div>
                        </CustomMenu>
                    </div>
                    <div className = {stylesSecond.daysTitleCont}>
                        {lang.calendar.daysInWeek.map(day => (
                            <div className = {stylesSecond.daysTitle}>{day}</div>
                        ))}
                    </div>
                    <div className = {stylesSecond.gridCont}>    
                        {selectedMonth.map(item => (
                            <div 
                                className = {
                                    item.notCurrentDays  
                                    ? `${stylesSecond.days} ${stylesSecond.notCurrentDays}` 
                                    : ((item.currentDay && (month.value === currentMonth) && (year === currentYear)) ? `${stylesSecond.currentDay} ${stylesSecond.days}` : stylesSecond.days)
                                }
                            >
                                <CustomMenu 
                                    button = {
                                        <div className = {stylesSecond.dayTitle}>{item.dateDay}</div>
                                    }
                                >
                                    <div className = {stylesSecond.actionCont}>
                                        <div className = {stylesSecond.actionTitle}>{lang.calendar.action.title}</div>
                                        <div className = {stylesSecond.actionType}>{lang.calendar.action.typeOfActions[0].title}</div>
                                        <div className = {stylesSecond.actionDateCont}>
                                            <div>{lang.calendar.action.start}: 10.10.2100</div>
                                            <div>{lang.calendar.action.end}: 10.10.2100</div>
                                        </div>
                                    </div>
                                </CustomMenu>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}
))
