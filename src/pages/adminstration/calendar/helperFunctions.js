import { Api, apiUrles } from "../../../api"
import { readLocalStorage } from "../../../store/localStorage"
import { currentDay, currentMonth, currentYear } from "./constants"

export const onDelete = async selectedItems => {
    
    const response = {}

    const token = readLocalStorage('token')

    const res = await Api(apiUrles.propertiesDelete + selectedItems.toString(), {}, 'delete', token)

    if (res.status === 200) {
        response.success = true
    } else {
        console.log(res)
        response.success = false
    }

    response.data = res.data

    return response
}

const daysInMonth = (year, month) => new Date(year, month, 0).getDate()

export const getDaysArray = (year, month) => {

    let numDaysInMonth = []

    for (let counter = 0; counter < 12; counter++) {
        numDaysInMonth.push(daysInMonth(year, counter + 1))
    }
    let daysInWeek = [ 
        {
            title: 'Sunday',
            index: 1
        }, 
        {
            title: 'Monday',
            index: 2
        }, 
        {
            title: 'Tuesday',
            index: 3
        }, 
        {
            title: 'Wednesday',
            index: 4
        }, 
        {
            title: 'Thursday',
            index: 5
        }, 
        {
            title: 'Friday',
            index: 6
        }, 
        {
            title: 'Saturday',
            index: 7
        },]
    let daysIndex = { 'Sun': 0, 'Mon': 1, 'Tue': 2, 'Wed': 3, 'Thu': 4, 'Fri': 5, 'Sat': 6 }
    let index = daysIndex[(new Date(year, month, 1)).toString().split(' ')[0]]
    let daysArray = []

    for (let i = 0, l = numDaysInMonth[month]; i < l; i++) {
        daysArray.push({
            dateDay: (i + 1), 
            weekDays: daysInWeek[index++]
        })
        if (index === 7) {
            index = 0
        }
    }

    return daysArray
}

const getNextMonthIndex = goToMonth => {
    if (goToMonth > 11) {
        return 1
    } else {
        return (goToMonth + 1)
    }
}

const getCurrentMonthIndex = goToMonth => {
    if ((goToMonth) > 11) {
        return 1
    }
    if ((goToMonth) < 0) {
        return 12
    }
    if (((goToMonth) < 12) && ((goToMonth) >= 0)) {
        return (goToMonth)
    }
}

const getPrevMonth = (year, goToMonth) => {
    let prevMonthIndex 
    if ((goToMonth - 1) <= -1) {
        prevMonthIndex = 11
        return getDaysArray(year - 1, prevMonthIndex)
    } else {
        prevMonthIndex = (goToMonth - 1)
        return getDaysArray(year, prevMonthIndex)
    }
}

const getNextMonth = (year, goToMonth) => {
    if (goToMonth > 10) {
        return getDaysArray(year + 1, 0)
    } else {
        return getDaysArray(year, (goToMonth + 1))
    }
}

export const getMonth = (year = currentYear, goToMonth = currentMonth) => {
    const prevMonth = getPrevMonth(year, goToMonth)
    const currentMonthForCalculate = getDaysArray(year, getCurrentMonthIndex(goToMonth))
    console.log(currentDay)
    const currentMonth = currentMonthForCalculate.map(item => {
        console.log(item.dateDay)
        if (item.dateDay === currentDay) {
            item.currentDay = true
        }
        return item
    })
    let prevMonthDaysQuantity = currentMonthForCalculate[0].weekDays.index - 2
    if (prevMonthDaysQuantity === -1) {
        prevMonthDaysQuantity = 6
    }
    let prevMonthDays = []
    if (prevMonthDaysQuantity > 0) {
        const days = prevMonth.slice(-prevMonthDaysQuantity)
        prevMonthDays = days.map(item => {
            item.notCurrentDays = true
            return item
        })
    } 
    const nextMonth = getNextMonth(year, goToMonth)
    const nextMonthDaysQuantity =  8 - currentMonthForCalculate[currentMonthForCalculate.length - 1].weekDays.index
    let nextMonthDays = []
    if (nextMonthDaysQuantity !== 7) {
        let days = nextMonth.slice(0, nextMonthDaysQuantity)
        nextMonthDays = days.map(item => {
            item.notCurrentDays = true
            return item
        })
    }

    const month = [...prevMonthDays, ...currentMonth, ...nextMonthDays]

    return month
}

export const getListOfYears = (past = 10, future = 10) => {
    let years = []
    const sumYears = past + future
    const startYear = currentYear - past
    for (let index = 0; index < sumYears; index++) {
        years.push({
            title: `${startYear + index}`,
            value: startYear + index,
            currentYear: (startYear + index === currentYear) ? true : false            
        })
    }

    return years

}