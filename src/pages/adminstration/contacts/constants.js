export const contactsTableTitles = [
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: 'test',
        key: 'test'
    }
]

export const contactsDetailDataColumns = [
    {
      name: 'IDtitle',
      key: 'index'
    },
    {
      name: 'nametitle',
      key: 'name'
    },
    {
      name: 'middleNametitle',
      key: 'middleName'
    }
  ]

export const defaultContactsHiddenColumns = []
