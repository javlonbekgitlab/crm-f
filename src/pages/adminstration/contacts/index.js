import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultContactsHiddenColumns, contactsTableTitles, contactsDetailDataColumns } from './constants'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { onDelete } from './helperFunctions'
import { CustomSnackbar } from '../../../components/notification/snackbar'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'

export const Contacts = inject('lang', 'contactsList')(observer(({lang, contactsList}) => {

    useEffect(() => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {            
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                // await getProperties()
            }
            setLoading(false)
        }, 0)
    }, [])

    const [loading, setLoading] = useState(true)

    const [message, setMessage] = useState('')

    const [messageShow, setMessageShow] = useState(false)

    const [messageStatus, setMessageStatus] = useState('info')

    const history = useHistory()

    const tableRowClick = property => {
        
    }
    

    // const handleDeleteProperty = async selectedItems => {
    //     const { success, data } = await onDelete(selectedItems)

    //     if (success) {
    //         getProperties()
    //         setMessage(lang.notifications.success.deleted)
    //         setMessageStatus('success')
    //     } else {
    //         setMessage(lang.notifications.fail.deleted)
    //         setMessageStatus('error')
    //     }
    //     setMessageShow(true)

    //     return success
    // }

    return (
        <div className = {styles.cont}>
            <CustomSnackbar
                message = {message}
                status = {messageStatus}
                show = {messageShow}
                setShow = {setMessageShow}
            />
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.contacts.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {contactsList.data}
                    tableCellTitles = {contactsTableTitles}
                    onRowClick = {tableRowClick}
                    // onClickDelete = {handleDeleteProperty}
                    detailTableColumns = {contactsDetailDataColumns}
                    detailPanelTitle = {lang.contacts.detailPanel.title}
                />
            </div>
        </div>
    )
}
))