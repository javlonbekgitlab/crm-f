import { Api, apiUrles } from "../../../api"
import { readLocalStorage } from "../../../store/localStorage"

export const onDelete = async selectedItems => {
    
    const response = {}

    const token = readLocalStorage('token')

    const res = await Api(apiUrles.propertiesDelete + selectedItems.toString(), {}, 'delete', token)

    if (res.status === 200) {
        response.success = true
    } else {
        console.log(res)
        response.success = false
    }

    response.data = res.data

    return response
}