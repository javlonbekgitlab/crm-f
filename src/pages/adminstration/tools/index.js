import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { Link, useHistory } from 'react-router-dom'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { Paper } from '@material-ui/core'
import stylesSecond from './index.module.sass'
import { ButtonWithIcon } from '../../../components/button'
import EventNoteIcon from '@material-ui/icons/EventNote'
import PeopleIcon from '@material-ui/icons/People'
import { routes } from '../../../store/routes'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { localStorageKeys } from '../../../constants'

export const Tools = inject('lang')(observer(({lang}) => {

    useEffect(() => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                // await getProperties()
            }
            setLoading(false)
        }, 0)
    }, [])

    const [loading, setLoading] = useState(true)

    const history = useHistory()

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.tools.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <div className = {stylesSecond.paperCont}>
                    <Paper className = {stylesSecond.paper} elevation = {3}>
                        <div className = {stylesSecond.header}>
                            <Link to = {routes.contacts}>
                                <ButtonWithIcon
                                    icon = {<PeopleIcon/>}
                                    variant = 'contained'
                                >
                                    {lang.forms.buttonTitles.contacts}
                                </ButtonWithIcon>
                            </Link>
                            <Link to = {routes.calendar}>
                                <ButtonWithIcon
                                    icon = {<EventNoteIcon/>}
                                    variant = 'contained'
                                >
                                    {lang.forms.buttonTitles.calendar}
                                </ButtonWithIcon>
                            </Link>
                        </div>
                        <div className = {stylesSecond.content}></div>
                    </Paper>
                </div>
            </div>
        </div>
    )
}
))