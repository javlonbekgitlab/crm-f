export const getUsersTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.hr.surName,
        key: 'surName'
    },
    {
        name: lang.forms.hr.name,
        key: 'username'
    },
    {
        name: lang.forms.hr.middleName,
        key: 'middleName'
    },
    {
        name: lang.forms.hr.series,
        key: 'series'
    },
    {
        name: lang.forms.hr.passportNumber,
        key: 'passportNumber'
    },
    {
        name: lang.forms.hr.givenPlace,
        key: 'givenPlace'
    },
    {
        name: lang.forms.hr.givenDate,
        key: 'givenDate'
    },
    {
        name: lang.forms.hr.birthDate,
        key: 'birthDate'
    },
    {
        name: lang.forms.hr.birthPlace,
        key: 'birthPlace'
    },
    {
        name: lang.forms.hr.inhabitationPlace,
        key: 'inhabitationPlace'
    },
    {
        name: lang.forms.hr.education,
        key: 'education'
    },
    {
        name: lang.forms.hr.numberOfDiploma,
        key: 'numberOfDiploma'
    },
    {
        name: lang.forms.hr.levelOfEducation,
        key: 'levelOfEducation'
    },
    {
        name: lang.forms.hr.corpTelNumber,
        key: 'corpTelNumber'
    },
    {
        name: lang.forms.hr.personalTelNumber,
        key: 'personalTelNumber'
    },
    {
        name: lang.forms.hr.social,
        key: 'social'
    },
    {
        name: lang.forms.hr.department,
        key: 'department'
    },
    {
        name: lang.forms.hr.post,
        key: 'position'
    },
    {
        name: lang.forms.hr.corpEmail,
        key: 'corpEmail'
    },
    {
        name: lang.forms.hr.gmail,
        key: 'gmail'
    },
    {
        name: lang.forms.hr.zoomId,
        key: 'zoomId'
    },
    {
        name: lang.forms.hr.weChatId,
        key: 'weChatId'
    },
    {
        name: lang.forms.hr.telegramId,
        key: 'telegramId'
    },
    {
        name: lang.forms.hr.post,
        key: 'positionName'
    },
    {
        name: lang.forms.hr.phoneNumber,
        key: 'phoneNumber'
    },
])

export const usersdefaultColumns = []