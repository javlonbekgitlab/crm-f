import React, { useEffect, useState } from 'react'
import { inject, observer } from 'mobx-react'
import { CustomTable } from '../../../components/table'
import { Loading } from '../../../components/loading'
import { usersdefaultColumns, getUsersTableTitles } from './constants'
import { CustomSnackbar } from '../../../components/notification/snackbar'
import { useHistory, withRouter } from 'react-router-dom'
import { AppBarCustom } from '../../../components/appBar'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import styles from '../../index.module.sass'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { ModalForm } from '../../../components/modalForm'
import { HrForm } from '../../../components/forms/adminstration/hrForm'
import useLogOut from '../../../components/appBarActions/helperFunctions'
import { localStorageKeys } from '../../../constants'
import { routes } from '../../../store/routes'
import { deleteRequest, snackbarStatus } from '../../../actions'
import { getUsers } from '../../../store/getDataFromServer'
import { apiUrles } from '../../../api'

export const HrTable = inject('usersList', 'lang', 'snackbar')(observer(({usersList, lang, snackbar}) => {
    
    useEffect(() => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                await getUsers()
            }
            setLoading(false)
        }, 0)
    }, [])

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getUsersTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [showForm, setShowForm] = useState(false)

    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const handleRowClick = record => {
        console.log(record)
        setShowForm(true)
        setSelectedRecord(record)
    }

    // const handleDelete = async selectedItems => {
    //     const { success } = await deleteRequest({url: apiUrles.deleteUsers ,selectedItems})
    //     if (success) {
    //         getUsers()
    //         snackbar.setSnackbar(lang.notifications.success.deleted, snackbarStatus.success)
    //     } else {
    //         snackbar.setSnackbar(lang.notifications.fail.deleted, snackbarStatus.error)
    //     }

    //     return success
    // }

     const handleCloseForm = async statusArg => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (statusArg) {
                if (statusArg === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (statusArg.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                snackbar.setSnackbar('', snackbarStatus.error, 0)
                setShowForm(false)
            }
        }
        // repair
        // await getAllApplications({page, status, search: searchValue})
        // await getDevice({})
    }

    useEffect(() => setTableTitles(getUsersTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.hrPage.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {usersList.data}
                    tableCellTitles = {tableTitles}
                    defaultHiddenColumns = {usersdefaultColumns}
                    detailPanelTitle = {lang.hrPage.role.title}
                    onClickPlusButton = {handleOpenForm}
                    onRowClick = {handleRowClick}
                    // onClickDelete = {handleDelete}
                />
                <ModalForm
                    open = {showForm}
                    close = {handleCloseForm}
                    width = 'lg'
                    keepMounted = {keepMounted}
                >
                    <HrForm
                        data = {selectedRecord}
                        onUploadingForm = {setUploadingForm}
                        closeForm = {handleCloseForm}
                    />
                </ModalForm>
            </div>
        </div>
    )
}))