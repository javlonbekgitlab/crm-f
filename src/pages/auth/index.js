import { inject, observer } from 'mobx-react'
import React from 'react'
import { Login } from '../../components/forms/authFom'
import styles from './index.module.sass'

export const Auth = inject('lang')(observer(({lang, onClose}) => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.titleCont}>
                    {lang.forms.signIn.title}
                </div>
                <Login closeForm = {onClose}/>
            </div>
        </div>
    )
}))
