export const officeTableTitles = [
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: 'Название офиса',
        key: 'officeName'
    },
    {
        name: 'Тип офиса',
        key: 'officeType'
    },
    {
        name: 'Республика',
        key: 'republic'
    },
    {
        name: 'Регион',
        key: 'district'
    },
    {
        name: 'Адрес',
        key: 'address'
    },
    {
        name: 'Номер кадастра',
        key: 'cadastre'
    },
    {
        name: 'Общая площадь',
        key: 'area'
    },
    {
        name: 'Количество комнат',
        key: 'roomNumber'
    },
    {
        name: 'Количество сотрудников',
        key: 'employeeNumber'
    },
    {
        name: 'Активы',
        key: 'assets'
    }
]

export const defaultOfficeHiddenColumns = []
