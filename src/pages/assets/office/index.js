import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
// import { readLocalStorage } from '../../store/localStorage'
import { CustomTable } from '../../../components/table'
// import { useHistory } from 'react-router-dom'
import { defaultOfficeHiddenColumns, officeTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'

export const Office = inject('lang')(observer(({lang}) => {

    
    useEffect(() => {
        // setTimeout(async () => {
        //     const res = readLocalStorage('token') 
        //     if (!res) {
        //         history.replace('/')
        //     } else {
        //         // await getOrders()
        //     }
            setLoading(false)
        // }, 0)
    }, [])

    // const history = useHistory()

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [officeForm, setOfficeForm] = useState(false)

    const [selectedOffice, setSelectedOffice] = useState({})

    const handleCloseOfficeForm = () => {
        if (!uploadingForm) {
            setOfficeForm(false)
            setSelectedOffice({})
        }
    }

    const handleOpenOfficeForm = () => setOfficeForm(true)

    const tableRowClick = office => {
        setSelectedOffice(office)
        setOfficeForm(true)
    } 

    const handleDeleteOffice = office => {
        console.log(office)
    }

    const actions = [
        {
            onClick: handleDeleteOffice,
            icon: <CloseIcon/>
        }
    ]

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.office.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {[]}
                    tableCellTitles = {officeTableTitles}
                    onClickPlusButton = {handleOpenOfficeForm}
                    onRowClick = {tableRowClick}
                    actions = {actions}
                    defaultHiddenColumns = {defaultOfficeHiddenColumns}
                />
            </div>
            <ModalForm
                open = {officeForm}
                close = {handleCloseOfficeForm}
                width = 'xl'
            >
                <div>form Property</div>
            </ModalForm>
        </div>
    )
})
)

