export const getFiscalModulesTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.retrieve.incomingType,
        key: 'typeOfComing'
    },
    {
        name: lang.forms.retrieve.deviceType,
        key: 'typeOfDevice'
    },
    {
        name: lang.forms.retrieve.year,
        key: 'dateOfRelease'
    },
    {
        name: lang.forms.retrieve.supplyNumber,
        key: 'numberOfBatch'
    },
    {
        name: lang.forms.retrieve.supplyDate,
        key: 'dateOfBatch'
    },
    {
        name: lang.forms.order.serializeNumber,
        key: 'serialNumber'
    },
    {
        name: lang.forms.retrieve.agreement,
        key: 'contractNumber'
    },
    {
        name: lang.forms.retrieve.agreementDate,
        key: 'dateOfContract'
    },
    {
        name: lang.forms.retrieve.act,
        key: 'numberOfAct'
    },
    {
        name: lang.forms.retrieve.actDate,
        key: 'dateOfAct'
    },
    // {
    //     name: lang.forms.retrieve.responsibleEmployee,
    //     key: 'respEmployee'
    // },
    // {
    //     name: lang.forms.retrieve.status,
    //     key: 'status'
    // },
])

export const defaultFiscalModulesHiddenColumns = []
