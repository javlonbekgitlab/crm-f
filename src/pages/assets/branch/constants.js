export const getBranchTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.branch.branchName,
        key: 'branchName'
    },
    {
        name: lang.forms.branch.branchType,
        key: 'branchType'
    },
    {
        name: lang.forms.branch.republic,
        key: 'republic'
    },
    {
        name: lang.forms.branch.region,
        key: 'region'
    },
    {
        name: lang.forms.branch.district,
        key: 'district'
    },
    {
        name: lang.forms.branch.address,
        key: 'address'
    },
    {
        name: lang.forms.branch.cadastre,
        key: 'cadastre'
    },
    {
        name: lang.forms.branch.agreement,
        key: 'agreement'
    },
    {
        name: lang.forms.branch.agreementDate,
        key: 'agreementDate'
    },
    {
        name: lang.forms.branch.agreementSum,
        key: 'agreementSum'
    },
    {
        name: lang.forms.branch.agreementTerm,
        key: 'agreementTerm'
    },
    {
        name: lang.forms.branch.area,
        key: 'area'
    },
    {
        name: lang.forms.branch.roomNumber,
        key: 'roomNumber'
    },
    {
        name: lang.forms.branch.employeeNumber,
        key: 'employeeNumber'
    },
    {
        name: lang.forms.branch.assets,
        key: 'assets'
    },
])

export const defaultBranchHiddenColumns = []
