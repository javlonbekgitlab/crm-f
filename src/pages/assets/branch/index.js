import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultBranchHiddenColumns, getBranchTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import MoreVertIcon from '@material-ui/icons/MoreVert'  
import { BranchesForm } from '../../../components/forms/assets/branchesForm'
import { getBranches } from '../../../store/getDataFromServer'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { branchesList } from '../../../store'
import { localStorageKeys } from '../../../constants'
import { routes } from '../../../store/routes'

export const Branch = inject('lang')(observer(({lang}) => {

    useEffect(() => {
        setTimeout(async () => {
            // const res = readLocalStorage('token') 
            // if (!res) {
            //     history.replace(routes.menu)
            //     removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            // } else {
            //     // await getBranches()
            // }
            setLoading(false)
        }, 0)
    }, [])

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getBranchTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [branchForm, setBranchForm] = useState(false)

    const [selectedBranch, setSelectedBranch] = useState({})

    const handleCloseBranchForm = () => {
        if (!uploadingForm) {
            setBranchForm(false)
            setSelectedBranch({})
        }
    }

    const handleOpenBranchForm = () => setBranchForm(true)

    const tableRowClick = branch => {
        setSelectedBranch(branch)
        setBranchForm(true)
    } 

    const handleDeleteBranch = branch => {
        console.log(branch)
    }

    const actions = [
        {
            onClick: () => console.log('object'),
            icon: <MoreVertIcon/>
        },
        {
            onClick: handleDeleteBranch,
            icon: <CloseIcon/>
        }
    ]

    useEffect(() => setTableTitles(getBranchTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.branch.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {branchesList.data}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenBranchForm}
                    onRowClick = {tableRowClick}
                    actions = {actions}
                    defaultHiddenColumns = {defaultBranchHiddenColumns}
                />
            </div>
            <ModalForm
                open = {branchForm}
                close = {handleCloseBranchForm}
                width = 'md'
            >
                <BranchesForm
                    data = {selectedBranch}
                    closeForm = {handleCloseBranchForm}
                    onUploadingForm = {setUploadingForm}
                />
            </ModalForm>
        </div>
    )
}
))

