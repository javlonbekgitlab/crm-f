import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
// import { readLocalStorage } from '../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultOthersHiddenColumns, getOthersTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { Loading } from '../../../components/loading'
import { OthersForm } from '../../../components/forms/assets/others'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { logOut } from '../../../actions/logout'
import { snackbarStatus } from '../../../actions'
import { getOthers } from '../../../store/getDataFromServer'

export const Others = 
inject('lang', 'snackbar', 'othersStore')
(observer(({lang, snackbar, othersStore}) => {

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getOthersTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [showForm, setShowForm] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [keepMounted, setKeepMounted] = useState(false)

    const handleCloseForm = async statusArg => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (statusArg) {
                if (statusArg === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (statusArg.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                snackbar.setSnackbar('', snackbarStatus.error, 0)
                setShowForm(false)
            }
        }
        // repair
        // await getAllApplications({page, status, search: searchValue})
        await getOthers({})
    }

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const tableRowClick = data => {
        setSelectedRecord(data)
        setShowForm(true)
    } 

    useEffect(() => {
        setTimeout(async () => {
            // const res = readLocalStorage('token') 
            await logOut({
                history: history, 
                getData: getOthers,
                getDataParams: {} 
                // keyForGetData: '_id',
                // historyKeys: [
                //     '_id',
                // ]
            })
            setLoading(false)
        }, 0)
        
    }, [])

    useEffect(() => setTableTitles(getOthersTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.others.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {othersStore.data}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenForm}
                    onRowClick = {tableRowClick}
                    defaultHiddenColumns = {defaultOthersHiddenColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                width = 'md'
                keepMounted = {keepMounted}
            >
                <OthersForm
                    data = {selectedRecord}
                    closeForm = {handleCloseForm}
                />
            </ModalForm>
        </div>
    )
}
))

