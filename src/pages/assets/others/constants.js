export const getOthersTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.others.mark,
        key: 'stamp'
    },
    {
        name: lang.forms.retrieve.model,
        key: 'model'
    },
    {
        name: lang.forms.others.year,
        key: 'yearOfProduction'
    },
    {
        name: lang.forms.others.techPassNumber,
        key: 'numberOfTechniquePassport'
    },
    {
        name: lang.forms.others.registrationNumber,
        key: 'registrationNumber'
    },
    {
        name: lang.forms.others.dateContractOfSale,
        key: 'dateOfContractPurchasesSales'
    },
    {
        name: lang.forms.others.contractOfSale,
        key: 'numberofContractPurchasesSales'
    },
    {
        name: lang.forms.others.agreementSum,
        key: 'amountOfContract'
    }
])

export const defaultOthersHiddenColumns = [
    'year',
    'dateContractOfSale',
    'contractOfSale',
    'mark'
]
