import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
// import { readLocalStorage } from '../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultDefectsHiddenColumns, getDefectsTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { onSubmitDefects, validateDefectsForm } from '../../../components/forms/assets/defectsForm/helperFunctions'
import { snackbarStatus } from '../../../actions'
import { logOut } from '../../../actions/logout'
import { getDefects } from '../../../store/getDataFromServer'
import { ReleaseForm } from '../../../components/forms/assets/releaseForm'

export const Defects = 
inject('lang', 'snackbar', 'defectsStore')
(observer(({lang, snackbar, defectsStore}) => {

    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getDefectsTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [showForm, setShowForm] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [keepMounted, setKeepMounted] = useState(false)

    const handleCloseForm = async statusArg => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (statusArg) {
                if (statusArg === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (statusArg.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                snackbar.setSnackbar('', snackbarStatus.error, 0)
                setShowForm(false)
            }
        }
        // repair
        // await getAllApplications({page, status, search: searchValue})
        await getDefects({})
    }

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const tableRowClick = data => {
        setSelectedRecord(data)
        setShowForm(true)
    } 

    useEffect(() => {
        setTimeout(async () => {
            // const res = readLocalStorage('token') 
            await logOut({
                history: history, 
                getData: getDefects,
                getDataParams: {} 
                // keyForGetData: '_id',
                // historyKeys: [
                //     '_id',
                // ]
            })
            setLoading(false)
        }, 0)
        
    }, [])

    useEffect(() => setTableTitles(getDefectsTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.defects.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {defectsStore.data}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenForm}
                    onRowClick = {tableRowClick}
                    defaultHiddenColumns = {defaultDefectsHiddenColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                keepMounted = {keepMounted}
                width = 'md'
            >
                <ReleaseForm
                    data = {selectedRecord}
                    closeForm = {handleCloseForm}
                    onSubmit = {onSubmitDefects}
                    onValidate = {validateDefectsForm}
                />
            </ModalForm>
        </div>
    )
}
))

