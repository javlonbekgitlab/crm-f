export const getInteriorTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.technics.title,
        key: 'naming'
    },
    {
        name: lang.forms.technics.type,
        key: 'type'
    },
    {
        name: lang.forms.technics.brand,
        key: 'brand'
    },
    {
        name: lang.forms.retrieve.model,
        key: 'model'
    },
    {
        name: lang.forms.retrieve.year,
        key: 'yearOfRelease'
    },
    {
        name: lang.forms.retrieve.agreementDate,
        key: 'dateOfContract'
    },
    {
        name: lang.forms.retrieve.agreement,
        key: 'numberOfContract'
    },
    {
        name: lang.forms.technics.price,
        key: 'price'
    },
    {
        name: lang.forms.retrieve.responsibleEmployee,
        key: 'respEmp'
    },
    {
        name: lang.forms.technics.inventoryNumber,
        key: 'inventoryNumber'
    },
    {
        name: lang.forms.technics.inventoryLast,
        key: 'lastInventory'
    },
    {
        name: lang.forms.technics.state,
        key: 'state'
    }
])

export const defaultInteriorHiddenColumns = []
