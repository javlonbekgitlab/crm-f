export const getStorageTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.storage.storageLocation,
        key: 'storageLocation'
    },
    {
        name: lang.forms.storage.storageName,
        key: 'storageName'
    },
    {
        name: lang.forms.storage.date,
        key: 'date'
    },
    {
        name: lang.forms.storage.orderNumber,
        key: 'orderNumber'
    },
    {
        name: lang.forms.storage.orderDate,
        key: 'orderDate'
    },
    {
        name: lang.forms.storage.storageType,
        key: 'storageType'
    },
    // {
    //     name: lang.forms.storage.responsibleEmploy,
    //     key: 'responsibleEmployee'
    // },
    // {
    //     name: lang.forms.storage.assets,
    //     key: 'assets'
    // },
])

export const defaultStorageHiddenColumns = []
