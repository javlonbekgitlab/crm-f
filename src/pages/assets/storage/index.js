import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
// import { readLocalStorage } from '../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultStorageHiddenColumns, getStorageTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import CloseIcon from '@material-ui/icons/Close'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { StorageForm } from '../../../components/forms/assets/storageForm'
import { snackbarStatus } from '../../../actions'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'

export const Storage = 
inject('lang', 'snackbar')
(observer(({lang, snackbar}) => {

    
    const history = useHistory()

    const [tableTitles, setTableTitles] = useState(getStorageTableTitles(lang))

    const [page, setPage] = useState(0)

    const [loading, setLoading] = useState(true)

    const [searchValue, setSearchValue] = useState('')

    const [uploadingForm, setUploadingForm] = useState(false)

    const [status, setStatus] = useState(0)

    const [showForm, setShowForm] = useState(false)

    const [keepMounted, setKeepMounted] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const handleCloseForm = async statusArg => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (statusArg) {
                if (statusArg === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (statusArg.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                snackbar.setSnackbar('', snackbarStatus.error, 0)
                setShowForm(false)
            }
        }
        // repair
        // console.log('page',page)
        // await getAdminstrativeContractsStoreData({})
        // await getAllApplications({page, status, search: searchValue})
        // await getMerchants()
    }

    // const handleOpenAdditionalContractsList = ({
    //     _id,
    //     dateOfContract,
    //     numberOfContract,
    //     subjectOfContract,
    // }) => {
    //     // globalVariables.set({
    //     //     merMapId,
    //     //     tin
    //     // })
    //     // console.log(globalVariables)
    //     history.push(routes.adminstrativeContractsAdditional, {
    //         _id,
    //         dateOfContract,
    //         numberOfContract,
    //         subjectOfContract,
    //     })
    // }

    // const handleChangeStatus = async ({target}) => {
    //     setStatus(parseInt(target.value))
    //     await getAllApplications({page, status: target.value, search: searchValue})
    // }

    // const handleChangePage = async page => {
    //     setPage(page)
    //     await getAllApplications({page, status, search: searchValue})
    // }

    // const handleSetSearchBar = ({target}) => setSearchValue(target.value)

    // const handleSubmitSearch = async value => {
    //     setSearchValue(value)
    //     await getAllApplications({page, status, search: value})
    // }
    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    // const tableRowClick = order => {
    //     setSelectedRecord(order)
    //     setShowForm(true)
    // } 

    // const handleDelete = async selectedItems => {
        // const { success, data } = await onDelete(selectedItems)

        // if (success) {
            // getMerchants()
            // setMessage(lang.notifications.success.deleted)
            // setMessageStatus('success')
        // } else {
        //     setMessage(lang.notifications.fail.deleted)
        //     setMessageStatus('error')
        // }
        // setMessageShow(true)

        // return success
    // }
    // useEffect(() => console.log('page changed'), [page, setPage])
    useEffect(async () => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                console.log('all pppp')
                // await getAdminstrativeContractsStoreData({})
                // await getAllApplications({page, search: searchValue})
            }
            setLoading(false)
        }, 0)
    }, [])

    useEffect(() => setTableTitles(getStorageTableTitles(lang)), [lang.activeLang])

    // const actions = [
    //     {
    //         onClick: handleDeleteStorage,
    //         icon: <CloseIcon/>
    //     }
    // ]

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.storage.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {[]}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenForm}
                    // onRowClick = {tableRowClick}
                    // actions = {actions}
                    defaultHiddenColumns = {defaultStorageHiddenColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                width = 'md'
            >
                <StorageForm
                    data = {selectedRecord}
                    closeForm = {handleCloseForm}
                />
            </ModalForm>
        </div>
    )
}
))

