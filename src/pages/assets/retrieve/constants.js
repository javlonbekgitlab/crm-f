export const getRetrieveTableTitles = lang =>([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.retrieve.incomingType,
        key: 'incomingType'
    },
    {
        name: lang.forms.retrieve.replacementPartType,
        key: 'sparePartType'
    },
    {
        name: lang.forms.retrieve.category,
        key: 'category'
    },
    {
        name: lang.forms.retrieve.model,
        key: 'model'
    },
    {
        name: lang.forms.retrieve.year,
        key: 'year'
    },
    {
        name: lang.forms.retrieve.supplyNumber,
        key: 'batchNum'
    },
    {
        name: lang.forms.retrieve.supplyDate,
        key: 'batchDate'
    },
    {
        name: lang.forms.retrieve.amount,
        key: 'quantity'
    },
    {
        name: lang.forms.retrieve.agreement,
        key: 'contractNum'
    },
    {
        name: lang.forms.retrieve.agreementDate,
        key: 'contractDate'
    },
    {
        name: lang.forms.retrieve.act,
        key: 'actNum'
    },
    {
        name: lang.forms.retrieve.actDate,
        key: 'actDate'
    },
    {
        name: lang.forms.retrieve.responsibleEmployee,
        key: 'respEmp'
    },
    {
        name: lang.forms.retrieve.status,
        key: 'status'
    },
])

export const defaultRetrieveHiddenColumns = []
