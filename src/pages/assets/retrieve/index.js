import React, { useEffect, useState } from 'react'
import styles from "../../index.module.sass"
// import { readLocalStorage } from '../../store/localStorage'
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultRetrieveHiddenColumns, getRetrieveTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import { snackbarStatus } from '../../../actions'
import CloseIcon from '@material-ui/icons/Close'
import { Loading } from '../../../components/loading'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { inject } from 'mobx-react'
import { RetrieveForm } from '../../../components/forms/assets/retrieveForm'
import { logOut } from '../../../actions/logout'
import { getRetrieve } from '../../../store/getDataFromServer'
import { onSubmit, validateForm } from '../../../components/forms/assets/retrieveForm/helperFunctions';

export const Retrieve = 
inject('lang', 'retrieveStore', 'snackbar')
(observer(({lang, retrieveStore, snackbar}) => {
    
    // useEffect(() => {
    //     // setTimeout(async () => {
    //     //     const res = readLocalStorage('token') 
    //     //     if (!res) {
    //     //         history.replace('/')
    //     //     } else {
    //     //         // await getOrders()
    //     //     }
    //         setLoading(false)
    //     // }, 0)
    // }, [])

    const history = useHistory()
    
    const [tableTitles, setTableTitles] = useState(getRetrieveTableTitles(lang))

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [showForm, setShowForm] = useState(false)

    const [selectedRecord, setSelectedRecord] = useState({})

    const [keepMounted, setKeepMounted] = useState(false)

    const handleCloseForm = async statusArg => {
        setSelectedRecord({})
        if (!uploadingForm) {
            if (statusArg) {
                if (statusArg === 'cancel') {
                    setKeepMounted(false)
                    setShowForm(false)
                } else {
                    if (statusArg.success) {
                        setSelectedRecord({})
                        snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                        setKeepMounted(false)
                        setShowForm(false)
                    } else {
                        snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                    }
                }
            } else {
                snackbar.setSnackbar('', snackbarStatus.error, 0)
                setShowForm(false)
            }
        }
        // repair
        // await getAllApplications({page, status, search: searchValue})
        await getRetrieve({})
    }

    const handleOpenForm = () => {
        setShowForm(true)
        setKeepMounted(true)
    }

    const tableRowClick = data => {
        setSelectedRecord(data)
        setShowForm(true)
    } 

    useEffect(() => {
        setTimeout(async () => {
            // const res = readLocalStorage('token') 
            await logOut({
                history: history, 
                getData: getRetrieve,
                getDataParams: {} 
                // keyForGetData: '_id',
                // historyKeys: [
                //     '_id',
                // ]
            })
            setLoading(false)
        }, 0)
        
    }, [])

    useEffect(() => setTableTitles(getRetrieveTableTitles(lang)), [lang.activeLang])

    return (
        <div className = {styles.cont}>
            <Loading
                start = {loading}
            />
            <AppBarCustom 
                title = {lang.retrieve.title}
            >
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {retrieveStore.data}
                    tableCellTitles = {tableTitles}
                    onClickPlusButton = {handleOpenForm}
                    onRowClick = {tableRowClick}
                    defaultHiddenColumns = {defaultRetrieveHiddenColumns}
                />
            </div>
            <ModalForm
                open = {showForm}
                close = {handleCloseForm}
                width = 'md'
            >
                <RetrieveForm
                    closeForm = {handleCloseForm}
                    data = {selectedRecord}
                    onSubmit = {onSubmit}
                    onValidate = {validateForm}
                />
            </ModalForm>
        </div>
    )
}
))

