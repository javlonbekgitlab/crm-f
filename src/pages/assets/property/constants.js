export const propertyTableTitles = [
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: 'Название недвижимости',
        key: 'propertyName'
    },
    {
        name: 'Тип недвижимости',
        key: 'propertyType'
    },
    {
        name: 'Дата договора купли продажи',
        key: 'dateContractOfSale'
    },
    {
        name: 'Номер договора купли продажи',
        key: 'contractOfSale'
    },
]

export const defaultPropertyHiddenColumns = []
