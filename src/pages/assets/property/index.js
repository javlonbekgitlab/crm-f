import React, { useCallback, useEffect, useState } from 'react'
import styles from "../../index.module.sass"
import { CustomTable } from '../../../components/table'
import { useHistory } from 'react-router-dom'
import { defaultPropertyHiddenColumns, propertyTableTitles } from './constants'
import { ModalForm } from '../../../components/modalForm'
import { AppBarCustom } from '../../../components/appBar'
import { observer } from 'mobx-react-lite'
import MoreVertIcon from '@material-ui/icons/MoreVert'  
import { Loading } from '../../../components/loading'
import { inject } from 'mobx-react'
import { UniversalAppBarActions } from '../../../components/appBarActions/orders'
import { BranchesForm } from '../../../components/forms/assets/branchesForm'
import { Collapse } from '@material-ui/core'
import { getProperties } from '../../../store/getDataFromServer'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { OfficeForm } from '../../../components/forms/assets/officeForm'
import { StorageForm } from '../../../components/forms/assets/storageForm'
import { deleteRequest, snackbarStatus } from '../../../actions'
import { reserveProperty } from '../../../store/reserve'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'
import { apiUrles } from '../../../api'
import { PropertyForm } from '../../../components/forms/assets/propertyForm'

export const Property = inject('lang', 'propertyList', 'snackbar')(observer(({lang, propertyList, snackbar}) => {

    useEffect(() => {
        setTimeout(async () => {
            const res = readLocalStorage('token') 
            if (!res) {
                
                history.replace(routes.menu)
                removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
            } else {
                await getProperties()
            }
            setLoading(false)
        }, 0)
    }, [])

    const [loading, setLoading] = useState(true)

    const [uploadingForm, setUploadingForm] = useState(false)
    
    const [propertyForm, setPropertyForm] = useState(false)

    const [selectedProperty, setSelectedProperty] = useState({})

    const [activeStep , setActiveStep] = useState(0)

    const [selectedType, setSelectedType] = useState(undefined)

    const [typeOfProperty, setTypeOfProperty] = useState(undefined)

    const [propertyValues, setPropertyValues] = useState({})

    const [subForm, setSubForm] = useState(false)

    const history = useHistory()

    const handleClosePropertyForm = useCallback(response => {
        console.log(response)
        if (!uploadingForm) {
            if (response && response !== 'cancel') {
                if (response.status) {
                    snackbar.setSnackbar(lang.notifications.success.added, snackbarStatus.success)
                    setPropertyForm(false)
                    setSelectedProperty({})
                    getProperties()
                    setActiveStep(0)
                } else {
                    snackbar.setSnackbar(lang.notifications.fail.added, snackbarStatus.error)
                }
                reserveProperty.clearReserveProperty()
            } 
            if (!response) {
                setPropertyForm(false)
                setSelectedProperty({})
                getProperties()
                setActiveStep(0)
            }
        }
    }, [])

    const handleCloseSubForm = useCallback(() => {
        setSubForm(false)
    }, [])

    const handleOpenPropertyForm = () => setPropertyForm(true)

    const tableRowClick = property => {
        setSelectedProperty(property)
        setPropertyForm(true)
    }
    
    const handleOpenSubForm = property => {
        setSubForm(true)
        setSelectedType(property.propertyType)
    }

    const handleDeleteProperty = async selectedItems => {
        const { success } = await deleteRequest({url: apiUrles.propertiesDelete, selectedItems})
        if (success) {
            getProperties()
            snackbar.setSnackbar(lang.notifications.success.deleted, snackbarStatus.success)
        } else {
            snackbar.setSnackbar(lang.notifications.fail.deleted, snackbarStatus.error)
        }

        return success
    }

    const actions = [
        {
            onClick: handleOpenSubForm,
            icon: <MoreVertIcon color = 'action'/>
        }
    ]

    return (
        <div className = {styles.cont}>
            <Loading start = {loading}/>
            <AppBarCustom title = {lang.property.title}>
                <UniversalAppBarActions/>
            </AppBarCustom>
            <div className = {styles.tableCont}>
                <CustomTable
                    tableContent = {propertyList.data}
                    tableCellTitles = {propertyTableTitles}
                    onClickPlusButton = {handleOpenPropertyForm}
                    onRowClick = {tableRowClick}
                    // onClickDelete = {handleDeleteProperty}
                    actions = {actions}
                    defaultHiddenColumns = {defaultPropertyHiddenColumns}
                />
            </div>
            {propertyForm && <ModalForm
                open = {propertyForm}
                close = {handleClosePropertyForm}
                width = {activeStep === 0 ? 'sm' : 'md'}
            >
                <Collapse in = {activeStep === 0}>
                    <PropertyForm
                        closeForm = {handleClosePropertyForm}
                        data = {selectedProperty}
                        onSetActiveStep = {setActiveStep}
                        onUploadingForm = {setUploadingForm}
                        onSetPropertyValues = {setPropertyValues}
                        onSetTypeOfProperty = {setTypeOfProperty}
                    />
                </Collapse>
                <Collapse in = {activeStep === 1}>
                    {typeOfProperty === 'branch' && <BranchesForm
                        closeForm = {handleClosePropertyForm}
                        parentValues = {propertyValues}
                        onUploadingForm = {setUploadingForm}
                        data = {{}}
                    />}
                    {typeOfProperty === 'office' && <OfficeForm
                        closeForm = {handleClosePropertyForm}
                        parentValues = {propertyValues}
                        onUploadingForm = {setUploadingForm}
                        data = {{}}
                    />}
                    {typeOfProperty === 'storage' && <StorageForm
                        closeForm = {handleClosePropertyForm}
                        parentValues = {propertyValues}
                        onUploadingForm = {setUploadingForm}
                        data = {{}}
                    />}
                </Collapse>
            </ModalForm>}
            {subForm && <ModalForm
                open = {subForm}
                close = {handleCloseSubForm}
                width = 'md'
            >
                {selectedType === 'branch' && <BranchesForm
                    closeForm = {handleCloseSubForm}
                    // parentId = {propertyId}
                    onUploadingForm = {setUploadingForm}
                    data = {{}}
                    disabled
                />}
                {selectedType === 'office' && <OfficeForm
                    closeForm = {handleCloseSubForm}
                    // parentId = {propertyId}
                    onUploadingForm = {setUploadingForm}
                    data = {{}}
                    disabled
                />}
                {selectedType === 'storage' && <StorageForm
                    closeForm = {handleCloseSubForm}
                    // parentId = {propertyId}
                    onUploadingForm = {setUploadingForm}
                    data = {{}}
                    disabled
                />}
            </ModalForm>}
        </div>
    )
}
))