export const getTechnicsTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.technics.title,
        key: 'title'
    },
    {
        name: lang.forms.technics.type,
        key: 'type'
    },
    {
        name: lang.forms.retrieve.year,
        key: 'yearOfRelease'
    },
    {
        name: lang.forms.retrieve.model,
        key: 'model'
    },
    {
        name: lang.forms.retrieve.brand,
        key: 'brand'
    },
    {
        name: lang.forms.technics.price,
        key: 'price'
    },
    {
        name: lang.forms.technics.specs,
        key: 'specifications'
    },
    {
        name: lang.forms.technics.cpu,
        key: 'cpu'
    },
    {
        name: lang.forms.retrieve.agreement,
        key: 'numberOfContract'
    },
    {
        name: lang.forms.retrieve.agreementDate,
        key: 'dateOfContract'
    },
    {
        name: lang.forms.technics.ram,
        key: 'ram'
    },
    {
        name: lang.forms.technics.os,
        key: 'os'
    },
    {
        name: lang.forms.technics.memory,
        key: 'memory'
    },
    {
        name: lang.forms.technics.name,
        key: 'name'
    },
    {
        name: lang.forms.technics.netAddress,
        key: 'networkAddress'
    },
    {
        name: lang.forms.technics.inventoryNumber,
        key: 'inventoryNumber'
    },
    {
        name: lang.forms.technics.inventoryLast,
        key: 'lastInventory'
    },
])

export const defaultTechnicsHiddenColumns = [
    'year', 
    'agreement',
    'agreementDate',
    'price',
    'spec',
    'cpu',
    'os',
    'ram',
    'memory',
    'name',
    'netAddress',
    'state'
]
