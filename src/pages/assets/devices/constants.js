export const getDevicesTableTitles = lang => ([
    {
        name: 'ID',
        key: 'index'
    },
    {
        name: lang.forms.retrieve.incomingType,
        key: 'typeOfComing'
    },
    {
        name: lang.forms.retrieve.deviceType,
        key: 'typeofDevice'
    },
    {
        name: lang.forms.retrieve.year,
        key: 'yearOfRelease'
    },
    {
        name: lang.forms.retrieve.model,
        key: 'model'
    },
    {
        name: lang.forms.retrieve.brand,
        key: 'brand'
    },
    {
        name: lang.forms.retrieve.supplyNumber,
        key: 'numberOfBatch'
    },
    {
        name: lang.forms.retrieve.supplyDate,
        key: 'dateOfBatch'
    },
    {
        name: lang.forms.order.serializeNumber,
        key: 'serialNumber'
    },
    {
        name: lang.forms.retrieve.agreement,
        key: 'contractNumber'
    },
    {
        name: lang.forms.retrieve.agreementDate,
        key: 'dateOfContract'
    },
    {
        name: lang.forms.retrieve.act,
        key: 'numberOfAct'
    },
    {
        name: lang.forms.retrieve.actDate,
        key: 'dateOfAct'
    },
    // {
    //     name: 'Ответственный сотрудник',
    //     key: 'responsibleEmployee'
    // },
    // {
    //     name: 'Статус',
    //     key: 'status'
    // },
])

export const defaultDevicesHiddenColumns = [
    'supplyNumber',
    'supplyDate',
    'agreementDate',
    'actDate',
    'status',
    'year',
    'act',
    'incomingType',
    'brand'
]
