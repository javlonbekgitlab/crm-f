import { action, observable } from "mobx"

export const activeModule = observable({
    value: undefined,
    setActiveModule: function(param) {
        this.value = param
    }
})

