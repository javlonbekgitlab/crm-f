import React, { useEffect, useRef, useState } from 'react'
import styles from './index.module.sass'
import { Loading } from '../../components/loading'
import { Link, useHistory } from 'react-router-dom'
import { routes } from '../../store/routes'
import assetsSrc from '../../assets/img/assets.svg'
import salesSrc from '../../assets/img/sales.svg'
import adminSrc from '../../assets/img/admin.svg'
import accountSrc from '../../assets/img/account.svg'
import { ModalForm } from '../../components/modalForm'
import { Auth } from '../auth'
import { accessModule, readArrayLocalStorage, readLocalStorage } from '../../store/localStorage'
import { Module } from '../../components/module'
import { inject, observer } from 'mobx-react'
import { AppBarCustom } from '../../components/appBar'
import { MenuAppBarActions } from '../../components/appBarActions/menu'
import { activeModule } from './helperFunctions'
import { localStorageKeys } from '../../constants'
import { UniversalAppBarActions } from '../../components/appBarActions/orders'
import { counterImg } from '../../globalVariables'

// let counterImg = 0

export const ModuleMenu = inject('lang')(observer(({lang, render, reRender}) => {
    
    const [loading, setLoading] = useState(true)

    const [authModal, setAuthModal] = useState(true)

    // const [counterImg, setCounterImg] = useState(1)

    const history = useHistory(null)
    const assetsRef = useRef(null)
    const adminRef = useRef(null)
    const salesRef = useRef(null)
    const accountRef = useRef(null)

    const assetsImg = new Image()
    const adminImg = new Image()
    const salesImg = new Image()
    const accountImg = new Image()
    assetsImg.src = assetsSrc
    adminImg.src = adminSrc
    salesImg.src = salesSrc
    accountImg.src = accountSrc
    assetsImg.alt = 'assetsSrc'
    adminImg.alt = 'adminSrc'
    salesImg.alt = 'salesSrc'
    accountImg.alt = 'accountSrc'

    const loadImg = () => {
        counterImg.setValue(counterImg.value + 1)
        console.log(counterImg.value)
        if (counterImg.value >= 4) {
            assetsRef.current.style.backgroundImage = 'url(' + assetsImg.src + ')'
            adminRef.current.style.backgroundImage = 'url(' + adminImg.src + ')'
            salesRef.current.style.backgroundImage = 'url(' + salesImg.src + ')'
            accountRef.current.style.backgroundImage = 'url(' + accountImg.src + ')'
            // setTimeout(() => {
                setLoading(false)
            // }, 0)
        }
    }    

    useEffect(() => {
        assetsImg.onload = () => loadImg()
        adminImg.onload = () => loadImg()
        salesImg.onload = () => loadImg()
        accountImg.onload = () => loadImg()
        return () => {
            counterImg.setValue(0)
            setLoading(false)
        }
    }, [])

    useEffect(() => {
        const token = readLocalStorage('token')
        if (token) {
            setAuthModal(false)
        } 
        const roles = readArrayLocalStorage(localStorageKeys.roles)
        if (!roles) {
            activeModule.setActiveModule(undefined)
        }
    })

    const handleSelectModule = ({ target: { id }}) => {
        if (id) {
            activeModule.setActiveModule(id)
        }
    }

    const handleCloseModule = () => activeModule.setActiveModule(undefined)

    const handleLogout = () => {
        setAuthModal(true)
        activeModule.setActiveModule(undefined)
    }

    const handleCloseAuth = () => {
        setAuthModal(false)
        reRender(!render)
    }

    return (
        <div 
            className = {styles.cont}
        >
            <Loading start = {loading}/>
            <ModalForm 
                open = {loading ? false : authModal}
            >
                <Auth onClose = {handleCloseAuth}/>
            </ModalForm>
            <AppBarCustom
                logoShow
                goBack = {handleCloseModule}
                hideGoBack = {!activeModule.value}
            >
                <UniversalAppBarActions
                    hide = {authModal}
                    onClickExit = {handleLogout}
                />
            </AppBarCustom>
            <div className = {authModal ? `${styles.content} ${styles.contentWithoutPadding}` : styles.content}>
                <div className = {authModal ? `${styles.gridContWithoutGap} ${styles.gridCont}` : styles.gridCont}>
                    <Module
                        id = '1'
                        ref = {adminRef}
                        notLocked = {accessModule('adminstration_mod')}
                        active = {activeModule.value}
                        showTitleModule = {authModal}
                        onOpen = {handleSelectModule}
                        title = {lang.menuPage.moduleTitles.admin}
                    >
                        {accessModule('hr_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.hr}</div>
                            <div className = {(activeModule.value === '1') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.hr}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.hr}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {accessModule('edm_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.edm}</div>
                            <div className = {(activeModule.value === '1') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.edm}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.controlEdm}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>
                        }
                        {accessModule('tools_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.tools}</div>
                            <div className = {(activeModule.value === '1') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.contacts}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.contacts}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.calendar}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.calendar}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                    </Module>
                    <Module
                        id = '2'
                        ref = {assetsRef}
                        active = {activeModule.value}
                        notLocked = {accessModule('assets_mod')}
                        showTitleModule = {authModal}
                        onOpen = {handleSelectModule}
                        title = {lang.menuPage.moduleTitles.assets}
                    >
                        {accessModule('object_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.objects}</div>
                            <div className = {(activeModule.value === '2') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.branch}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.branch}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.storage}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.storage}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {accessModule('assembly_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.assembly}</div>
                            <div className = {(activeModule.value === '2') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.retrieve}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.retrieve}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.release}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.release}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.defects}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.defects}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {(accessModule('user_items_mod') || accessModule('items_mod')) && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.items}</div>
                            <div className = {(activeModule.value === '2') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <div className = {styles.firstSubModuleItems}>
                                    <div className = {`${styles.itemsTitle}`}>{lang.menuPage.subSubModule.equipment}</div>
                                    <div className = {(activeModule.value === '2') ? `${styles.secondSubModule} ${styles.secondSubModuleShow}` : styles.secondSubModule}>
                                        <Link to = {routes.technics}>
                                            <div className = {styles.secondSubModuleItems}>{lang.menuPage.subSubSubModules.technics}</div>
                                        </Link>
                                        <Link to = {routes.fiscalModules}>
                                            <div className = {styles.secondSubModuleItems}>{lang.menuPage.subSubSubModules.fiscalModules}</div>
                                        </Link>
                                        <Link to = {routes.devices}>
                                            <div className = {styles.secondSubModuleItems}>{lang.menuPage.subSubSubModules.devices}</div>
                                        </Link>
                                    </div>
                                </div>
                                <div className = {styles.firstSubModuleItems}>
                                    <Link to = {routes.interior}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.interior}</div>
                                    </Link>
                                </div>
                                <div className = {styles.firstSubModuleItems}>
                                    <Link to = {routes.others}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.others}</div>
                                    </Link>
                                </div>
                                <div className = {styles.firstSubModuleItems}>
                                    <div className = {`${styles.itemsTitle}`}>{lang.menuPage.subModules.requests}</div>
                                    <div className = {(activeModule.value === '2') ? `${styles.secondSubModule} ${styles.secondSubModuleShow}` : styles.secondSubModule}>
                                        <Link to = {routes.acceptanceCertificate}>
                                            <div className = {styles.secondSubModuleItems}>{lang.menuPage.subSubSubModules.acceptanceCertificate}</div>
                                        </Link>
                                        <Link to = {routes.fiscalModules}>
                                            <div className = {styles.secondSubModuleItems}>{lang.menuPage.subSubSubModules.purchasePlan}</div>
                                        </Link>
                                        <Link to = {routes.devices}>
                                            <div className = {styles.secondSubModuleItems}>{lang.menuPage.subSubSubModules.marking}</div>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>}
                    </Module>
                    <Module
                        id = '3'
                        ref = {salesRef}
                        active = {activeModule.value}
                        showTitleModule = {authModal}
                        notLocked = {accessModule('sales_mod')}
                        onOpen = {handleSelectModule}
                        title = {lang.menuPage.moduleTitles.sales}
                    >
                        {accessModule('support_lite_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.supportLite}</div>
                            <div className = {(activeModule.value === '3') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.orders}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.merchants}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.applications}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.applications}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {accessModule('support_pro_mod') &&  <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.supportPro}</div>
                            <div className = {(activeModule.value === '3') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.supportProTerminals}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.interactingWithTerminal}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {accessModule('support_repair_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.supportRepair}</div>
                            <div className = {(activeModule.value === '3') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.supportProRepair}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.repairRequestList}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                    </Module>
                    <Module
                        id = '4'
                        ref = {accountRef}
                        active = {activeModule.value}
                        showTitleModule = {authModal}
                        notLocked = {accessModule('accounting_mod')}
                        onOpen = {handleSelectModule}
                        title = {lang.menuPage.moduleTitles.accounting}
                    >
                        {accessModule('e-bank_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.eBank}</div>
                            <div className = {(activeModule.value === '4') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.payments}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.payments}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {accessModule('billing_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.billing}</div>
                            <div className = {(activeModule.value === '4') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.subscriberControlSystem}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.subscriberControlSystem}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                        {accessModule('contracts_mod') && <div className = {styles.submenuItem}>
                            <div className = {styles.dot}></div>
                            <div className = {styles.submenuTitle}>{lang.menuPage.subModules.contracts}</div>
                            <div className = {(activeModule.value === '4') ? `${styles.firstSubModule} ${styles.firstSubModuleShow}` : styles.firstSubModule}>
                                <Link to = {routes.adminstrativeContracts}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.adminstrativeContracts}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.internationalContracts}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.internationalContracts}</div>
                                    </div>
                                </Link>
                                <Link to = {routes.otherContracts}>
                                    <div className = {styles.firstSubModuleItems}>
                                        <div className = {`${styles.itemsTitle} ${styles.press}`}>{lang.menuPage.subSubModule.otherContracts}</div>
                                    </div>
                                </Link>
                            </div>
                        </div>}
                    </Module>
                </div>
            </div>
        </div>
    )
}))

