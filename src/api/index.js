import { progressForm } from '../actions'
const axios = require('axios')

export const apiUrles = {
    partner: 'openapi/partner',
    merchant: 'openapi/merchant',
    store: 'openapi/store',
    partnerAdd: 'openapi/partner/add',
    partnerUpdate: 'openapi/partner/update/',
    merchantAdd: 'openapi/merchant/add',
    merchantUpdate: 'openapi/merchant/update/',
    storeAdd: 'openapi/store/add',
    storeUpdate: 'openapi/store/update/',
    registraion: 'signup',
    login: 'signin',
    allApplications: 'api/sales/support/lite/all-applications',
    updateApplication: 'api/sales/support/lite/update/application/',
    orders: 'orders',
    ordersAdd: 'orders/add',
    ordersUpdate: 'orders/update/',
    ordersStoreAdd: 'orders/store/add',
    branches: 'branches',
    branchesAdd: 'branches/add',
    branchesUpdate: 'branches/update/',
    properties: 'properties',
    propertiesAdd: 'properties/add',
    propertiesUpdate: 'properties/update/',
    propertiesDelete: 'properties/delete/', 
    offices: 'offices',
    officesAdd: 'offices/add',
    officesUpdate: 'offices/update/',
    storages: 'storages',
    storagesAdd: 'storages/add',
    storagesUpdate: 'storages/update/',
    signUp: 'api/administration/hr/add',
    signIn: 'api/auth/signin',
    getUsers: 'api/administration/hr',
    getRoles: 'all-roles',
    deleteUsers: 'api/administration/hr/delete/',
    updateUsers: 'api/administration/hr/update/',
    merchants: 'api/sales/support/lite/merchants',
    addMerchant: 'api/sales/support/lite/add-merchant',
    upDateMerchant: 'api/sales/support/lite/update-merchant/',
    stores: 'api/sales/support/lite/all-stores/',
    addStore: 'api/sales/support/lite/add-store',
    oneStore: 'api/sales/support/lite/one-store/',
    cashDrawers: 'api/sales/support/lite/all-cash-drawers/',
    addCashDrawer: 'api/sales/support/lite/add-cash-drawer',
    sendApplicationToNitsWithId: 'api/sales/support/lite/send-nits-application-from-bot',
    chinaHosting: 'http://ds.51zzd.com',
    // getUrban: '/control/api/mer/queryCity',
    checkBalance: 'info/subject/by-tin',
    infoRegions: 'info/regions',
    districtsByRegion: 'info/districts-by-region',
    activityGroup: 'info/activity-group',
    activityList: 'info/activity-list',
    searchTerminal: 'api/sales/support/pro/search-terminal',
    terminalPushCmd: 'api/sales/support/pro/block-unblock-restart-terminal',
    subscriberControlSystemGetContract: 'api/accounting/billing/get-request',
    adminstrativeContractsStore: 'api/accounting/contracts/administrative',
    adminstrativeContractsStoreAdd: 'api/accounting/contracts/administrative/add',
    adminstrativeAdditionalContracts: 'api/accounting/contracts/administrative-additional/',
    adminstrativeAdditionalContractsAdd: 'api/accounting/contracts/administrative-additional/add',
    internationalContracts: 'api/accounting/contracts/international',
    internationalContractsAdd: 'api/accounting/contracts/international/add',
    internationalAdditionalContracts: 'api/accounting/contracts/international-additional/',
    internationalAdditionalContractsAdd: 'api/accounting/contracts/international-additional/add',
    retrieve: 'api/assets/assembly/retrieve',
    retrieveAdd: 'api/assets/assembly/retrieve/add',
    release: 'api/assets/assembly/release',
    releaseAdd: 'api/assets/assembly/release/add',
    defects: 'api/assets/assembly/defects',
    defectsAdd: 'api/assets/assembly/defects/add',
    equipmentAndTech: 'api/assets/items/equipment/techAndEquip',
    equipmentAndTechAdd: 'api/assets/items/equipment/techAndEquip/add',
    fiskalMod: 'api/assets/items/equipment/fiskMod',
    fiskalModAdd: 'api/assets/items/equipment/fiskMod/add',
    device: 'api/assets/items/equipment/terminals',
    deviceAdd: 'api/assets/items/equipment/terminals/add',
    interior: 'api/assets/items/interior',
    interiorAdd: 'api/assets/items/interior/add',
    others: 'api/assets/items/others',
    othersAdd: 'api/assets/items/others/add',
}

export const Api = async(url, data = null, type = 'get', token = null, hosting = '', onUploadProgress = false, auth = '') => {
    var response = {}
    onUploadProgress && progressForm.setProgressForm(0)
    // const host = hosting ? hosting : 'http://10.0.2.202:8005/'
    const host = hosting ? hosting : 'http://10.0.70.14:8005/'
    const config = {
        headers: {
            'x-access-token': token,
            'Content-Type': 'application/json'
        },
        onUploadProgress
    }
    if (hosting) {
        delete config.headers['x-access-token']
    }
    if (auth && hosting) {
        config.headers['Authorization'] = auth
    }
    if (type === 'get') {
        try {
            console.log('try get')
            response = await axios.get(`${host}${url}`, config)
        } catch (error) {
            response = error
            console.log(error)
        }
    }
    if (type === 'post') {
        try {
            console.log('try post body', data)
            response = await axios.post(`${host}${url}`, data, config)
            console.log('postttttttttttttt', response)
        } catch (error) {
            console.log(error)
            response = error
        }
    }
    if (type === 'delete') {
        try {
            response = await axios.delete(`${host}${url}`, config)
        } catch (error) {
            console.log(error)
            response = error
        }
    }
    return response

}