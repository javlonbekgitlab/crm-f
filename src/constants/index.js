import { lang } from "../languages/lang"

export const maskUserMobile = '99 99 999 99 99'

export const maskMFO = '99999'

export const maskBankAccount = '9999 9999 9999 9999 9999'

export const regexSpace = /\s/g

export const regexOnlyNumbers = /^\d+$/

export const roless = [
    {
        id: 0,
        name: 'hr_mod'
    },
    {
        id: 1,
        name: 'edm_mod'
    },
    {
        id: 2,
        name: 'tools_mod'
    },
    {
        id: 3,
        name: 'object_mod'
    },
    {
        id: 4,
        name: 'assembly_mod'
    },
    {
        id: 5,
        name: 'billing_mod'
    },
    {
        id: 6,
        name: 'contracts_mod'
    },
    {
        id: 7,
        name: 'support_lite_mod'
    },
    {
        id: 8,
        name: 'support_pro_mod'
    },
    {
        id: 9,
        name: 'support_repair_mod'
    },
    {
        id: 10,
        name: 'user_assembly_mod'
    },
    {
        id: 11,
        name: 'user_items_mod'
    },
    {
        id: 12,
        name: 'e-bank_mod'
    },
    {
        id: 13,
        name: 'items_mod'
    },
]

export const localStorageKeys = {
    token: 'token',
    roles: 'roles',
    username: 'username'
}

export const getStatuses = lang =>([
    {
        id: 0,
        label: lang.table.status.all
    },
    {
        id: 1,
        label: lang.table.status.new
    },
    {
        id: 2,
        label: lang.table.status.proc
    },
    {
        id: 3,
        label: lang.table.status.accept
    },
    {
        id: 4,
        label: lang.table.status.rejected
    },
    {
        id: 5,
        label: lang.table.statusForRowTable.noFile
    },
    {
        id: 6,
        label: lang.table.statusForRowTable.notSent
    }
])

export const statusForRowTable = [
    {
        id: 1,
        label: lang.table.statusForRowTable.new
    },
    {
        id: 2,
        label: lang.table.statusForRowTable.proc
    },
    {
        id: 3,
        label: lang.table.statusForRowTable.accept
    },
    {
        id: 4,
        label: lang.table.statusForRowTable.rejected
    },
    {
        id: 5,
        label: lang.table.statusForRowTable.noFile
    },
    {
        id: 6,
        label: lang.table.statusForRowTable.notSent
    }
]

export const typeConnection = [
    {
        id: '1',
        label: lang.forms.order.buy
    },
    {
        id: '4',
        label: lang.forms.order.connection
    }
]
 
export const typeConnectionSub = [
    {
        id: '2',
        label: lang.forms.order.bank
    },
    {
        id: '3',
        label: lang.forms.order.other
    }
]

export const modelNoData = [
    {
        title: 'PAX A-930',
        id: 3
    }
]

export const apiParams = {
    page: 'page=',
    search: 'search=',
    status: 'status=',
    snNameTID: 'snNameTID='
}

export const maxRowsOnTable = 10