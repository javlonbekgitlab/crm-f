String.prototype.mapString = function(func) {
    let stringArray = this.split("")
    let newStringArray = stringArray.map((item, index) => {
        return func.call(window, item, index, this)
    })
    return newStringArray.join("")
}