import { localStorageKeys } from "../constants"
import { removeItemsFromLocalStorage } from "../store/localStorage"
import { routes } from "../store/routes"
import { browserHistory } from "./browserHistory"

export const forcedLogOut = () => {
    removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles, localStorageKeys.username])
    browserHistory.replace(routes.menu)
}