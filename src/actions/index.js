import { action, observable } from "mobx"
// import { readLocalStorage } from "../../../store/localStorage"
import { localStorageKeys } from "../constants"
import { Api } from "../api"
import { readLocalStorage } from "../store/localStorage"

export const progressForm = observable({
    value: 0,
    setProgressForm: action(function(value){ this.value = value })
})

export const uploadProgress = event => {
    const percentCompleted = Math.round((event.loaded * 100) / event.total)
    progressForm.setProgressForm(percentCompleted)
}

export const snackbar = observable({
    show: false,
    message: '',
    status: '',
    autoHideDuration: undefined,
    setSnackbar: action(function(message = '', status = '', autoHideDuration = undefined){
        this.show = true
        this.message = message
        this.status = status
        this.autoHideDuration = autoHideDuration
    }),
    closeSnackBar: action(function() {this.show = false})
})

export const snackbarStatus = {
    error: 'error',
    info: 'info',
    success: 'success'
}

export const closeSnackBar = () => snackbar.closeSnackBar()

// export const accessModule = observable({
//     setAccessModule: action(function(key, value){
//         this[key] = value
//     }),
//     clearAccessModule: action(function(){
//         for (var member in this) delete this[member]
//     })
// })

export const deleteRequest = async ({url, selectedItems}) => {
    
    const response = {}

    const token = readLocalStorage(localStorageKeys.token)

    const res = await Api(url + selectedItems.toString(), {}, 'delete', token)

    if (res.status === 200) {
        response.success = true
    } else {
        console.log(res)
        response.success = false
    }

    response.data = res.data

    return response
}

export const transliterate = word => {
    let answer = ""
    let i
    const a = {}

    a["Ё"] = "YO"; a["Й"] = "Y"; a["Ц"] = "TS"; a["У"] = "U"; a["К"] = "K"; a["Е"] = "E"; a["Н"] = "N"; a["Г"] = "G"; a["Ш"] = "Sh"; a["Щ"] = "SH"; a["З"] = "Z"; a["Х"] = "X"; a["Ъ"] = "'";
    a["ё"] = "yo"; a["й"] = "y"; a["ц"] = "ts"; a["у"] = "u"; a["к"] = "k"; a["е"] = "e"; a["н"] = "n"; a["г"] = "g"; a["ш"] = "sh"; a["щ"] = "sh"; a["з"] = "z"; a["х"] = "x"; a["ъ"] = "'";
    a["Ф"] = "F"; a["Ы"] = "I"; a["В"] = "V"; a["А"] = "A"; a["П"] = "P"; a["Р"] = "R"; a["О"] = "O"; a["Л"] = "L"; a["Д"] = "D"; a["Ж"] = "J"; a["Э"] = "E"; a["Ғ"] = "G'"; a["Қ"] = "Q";
    a["ф"] = "f"; a["ы"] = "i"; a["в"] = "v"; a["а"] = "a"; a["п"] = "p"; a["р"] = "r"; a["о"] = "o"; a["л"] = "l"; a["д"] = "d"; a["ж"] = "j"; a["э"] = "e"; a["ғ"] = "g'"; a["қ"] = "q";
    a["Я"] = "Ya"; a["Ч"] = "Ch"; a["С"] = "S"; a["М"] = "M"; a["И"] = "I"; a["Т"] = "T"; a["Ь"] = ""; a["Б"] = "B"; a["Ю"] = "YU"; a["Ҳ"] = "H"; a["Ў"] = "O'";
    a["я"] = "ya"; a["ч"] = "ch"; a["с"] = "s"; a["м"] = "m"; a["и"] = "i"; a["т"] = "t"; a["ь"] = ""; a["б"] = "b"; a["ю"] = "yu"; a["ҳ"] = "h"; a["ў"] = "o'";

    for (i in word) {
        if (word.hasOwnProperty(i)) {
            if (a[word[i]] === undefined) {
                answer += word[i]
            } else {
                answer += a[word[i]]
            }
        }
    }
    return answer
}

export const transliterateObject = data => {
    const transliterated = {}
    Object.keys(data).map(key => {
        transliterated[key] = transliterate(data[key])
    })
    return transliterated
}


export const autoSelectNames = ({name = String, data = Array, keyOfValue = String, value}) => {
    const selectedRegion = data.find(obj => {
        return obj.id === value
    })
    const event = {
        target: {
            name,
            value: selectedRegion[keyOfValue]
        }
    }
    return event
}


export const autoSelectNameForTable = ({data = Array, keyOfValue = String, value}) => {
    try {
        const selected = data.find(obj => {
            return obj.id === value
        })
        return selected[keyOfValue]
    } catch (e) {
        return 'problem with typeOfApplication'
    } 
}