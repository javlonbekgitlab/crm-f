import { localStorageKeys } from '../constants'
import { readLocalStorage, removeItemsFromLocalStorage } from '../store/localStorage'
import { routes } from '../store/routes'
import { forcedLogOut } from './forcedLogOut'

export const logOut = async({ history, getData, getDataParams = null, keyForGetData = undefined, historyKeys = null}) => {
    const res = await readLocalStorage(localStorageKeys.token) 
    console.log('getDataParams___+_+_+_+_+_', getDataParams)
    let someVariable = 0
    historyKeys && (
        historyKeys.map((key) => {
            if (history?.location && history?.location?.state && history?.location?.state[key]) {
                someVariable = someVariable + 1
            }
        })
    )
    /// need to optimize
    if (keyForGetData) {
        if (!res) {
            forcedLogOut()
        }
        if (res && (historyKeys && (historyKeys.length !== someVariable))) {
            history.goBack()
        } 
        if (res) {
            await getData(history.location.state[keyForGetData])
        }
    } else if (getDataParams) {
        if (!res) {
            forcedLogOut()
        }
        if (res && (historyKeys && (historyKeys.length !== someVariable))) {
            history.goBack()
        } 
        if (res) {
            await getData(getDataParams)
        }
    } else {
        if (!res) {
            forcedLogOut()
        }
        if (res && (historyKeys && (historyKeys.length !== someVariable))) {
            history.goBack()
        } 
        if (res) {
            await getData()
        }
    }
}