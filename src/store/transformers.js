import { autoSelectNameForTable } from "../actions"
import { posts } from "../components/forms/adminstration/hrForm/helperFunctions"
import { maxRowsOnTable, modelNoData, statusForRowTable, typeConnection, typeConnectionSub } from "../constants"
import { getRoles } from "./getDataFromServer"

export const transformDate = ({data, key}) => {
    const transFormedData = data.map((item) => {
        item[key] && (item[key] = item[key].slice(0, 10))
        return item
    })
    return transFormedData
}

export const transformOrderList = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            return item
        })
        return transFormedData
    } else {
        return data
    }
}
export const transformIndexTinPinfl = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.tinOrPinfl = item.tin ? item.tin : item.pinfl
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformId = (data, page = 0) => {
    console.log('id', data)
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = ((page * maxRowsOnTable) + index + 1)
            return item
        })
        return transFormedData
    } else {
        return data
    }
}

export const transformPersonalData = perData => {
    if (typeof perData === 'object' && perData !== null) {
        perData.firstNameInitial = perData.firstName.slice(0, 1) + '.'
        return perData
    } 
}

export const transformDateUnix = (data, key) => {
    console.log(key)
    if (Array.isArray(data)) {
        const transFormedData = data.map((item) => {
            item[key] && (item[key] = new Date(item[key] * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformRoles = data => {
    const transformedRoles = data.map(item => {
        return item.name
    })
    return transformedRoles
}

export const transformUsers = (data, allRoles) => {
    if (Array.isArray(data)) {
        const transformed = data.map(item => {
            item.detailPanel = {
                action: true,
                data: transFormRolesForTable(item.roles, allRoles)
            }
            item.positionName = item.position ? autoSelectNameForTable({
                data: posts,
                keyOfValue: 'title',
                value: parseInt(item.position)
            }) : 'noData'
            item.birthDate && (item.birthDate = new Date(item.birthDate * 1000).toISOString().slice(0, 10))
            item.givenDate && (item.givenDate = new Date(item.givenDate * 1000).toISOString().slice(0, 10))
            return item
        })
        return transformed
    } else {
        return data
    }
}

export const transFormRolesForTable = (roles, allRoles) => {
    const transformed = allRoles.map(role => {
        const res = roles.filter(item => item._id === role._id)
        return {
            _id: role._id,
            title: role.name,
            access: res.length > 0 ? true : false
        }
    })
    return transformed
}

export const transformRegions = (data, key) => {
    if (Array.isArray(data)) {
        const transFormedData = data.map(item => ({
            id: item.code,
            title: item[key]
        }))
        return transFormedData
    } else {
        return []
    }
}

export const transformDevices = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.modelName = modelNoData[0].title
            item.index = index + 1
            item.date && (item.date = new Date(item.date * 1000).toISOString().slice(0, 10))
            item.tinOrPinfl = item.tin ? item.tin : item.pinfl
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformApplications = data => {
    if (Array.isArray(data) && data.length > 0) {
        const transFormedData = data.map(item => {
            delete item.merchant._id
            delete item.store._id
            const transformedItem = {
                ...item,
                ...item.store,
                ...item.merchant,
                tinOrPinfl: item.merchant.tin ? item.merchant.tin : item.merchant.pinfl,
                modelName: modelNoData[0].title,
                statusName: item.status ? autoSelectNameForTable({
                    data: statusForRowTable,
                    keyOfValue: 'label',
                    value: item.status
                }) : 'noData',
                typeOfApplicationName: item.typeOfApplication ? autoSelectNameForTable({
                    data: [...typeConnection, ...typeConnectionSub],
                    keyOfValue: 'label',
                    value: item.typeOfApplication
                }) : 'debug version'
            }
            // delete transformedItem.store
            // delete transformedItem.merchant
            return transformedItem
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformSubscriberControlSystem = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.CONTRACTDATE && (item.CONTRACTDATE = new Date(parseInt(item.CONTRACTDATE) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformAdminstrativeContracts = ({data, page = 1}) => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = ((page * maxRowsOnTable) + index + 1)
            item.dateOfActCompletionAcceptanceTransfer && (item.dateOfActCompletionAcceptanceTransfer = new Date(parseInt(item.dateOfActCompletionAcceptanceTransfer) * 1000).toISOString().slice(0, 10))
            item.dateOfContract && (item.dateOfContract = new Date(parseInt(item.dateOfContract) * 1000).toISOString().slice(0, 10))
            item.dateOfFinalPayment && (item.dateOfFinalPayment = new Date(parseInt(item.dateOfFinalPayment) * 1000).toISOString().slice(0, 10))
            item.dateOfInvoice && (item.dateOfInvoice = new Date(parseInt(item.dateOfInvoice) * 1000).toISOString().slice(0, 10))
            item.dateOfPrepayment && (item.dateOfPrepayment = new Date(parseInt(item.dateOfPrepayment) * 1000).toISOString().slice(0, 10))
            item.term && (item.term = new Date(parseInt(item.term) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformAdditionalAdminstrativeContracts = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.dateOfContract && (item.dateOfContract = new Date(parseInt(item.dateOfContract) * 1000).toISOString().slice(0, 10))
            item.term && (item.term = new Date(parseInt(item.term) * 1000).toISOString().slice(0, 10))
            item.dateOfPrepayment && (item.dateOfPrepayment = new Date(parseInt(item.dateOfPrepayment) * 1000).toISOString().slice(0, 10))
            item.dateOfFinalPayment && (item.dateOfFinalPayment = new Date(parseInt(item.dateOfFinalPayment) * 1000).toISOString().slice(0, 10))
            item.dateOfInvoice && (item.dateOfInvoice = new Date(parseInt(item.dateOfInvoice) * 1000).toISOString().slice(0, 10))
            item.dateOfActCompletionAcceptanceTransfer && (item.dateOfActCompletionAcceptanceTransfer = new Date(parseInt(item.dateOfActCompletionAcceptanceTransfer) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

// export const transformRetrieve = data => {
//     if (Array.isArray(data)) {
//         const transFormedData = data.map((item, index) => {
//             item.index = index + 1
//             item.contractDate && (item.contractDate = new Date(parseInt(item.contractDate) * 1000).toISOString().slice(0, 10))
//             item.actDate && (item.actDate = new Date(parseInt(item.actDate) * 1000).toISOString().slice(0, 10))
//             return item
//         })
//         return transFormedData
//     } else {
//         return []
//     }
// }

export const transformRelease = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.year && (item.year = new Date(parseInt(item.year) * 1000).toISOString().slice(0, 10))
            item.batchDate && (item.batchDate = new Date(parseInt(item.batchDate) * 1000).toISOString().slice(0, 10))
            item.contractDate && (item.contractDate = new Date(parseInt(item.contractDate) * 1000).toISOString().slice(0, 10))
            item.actDate && (item.actDate = new Date(parseInt(item.actDate) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformTechAndEquip = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.yearOfRelease && (item.yearOfRelease = new Date(parseInt(item.yearOfRelease) * 1000).toISOString().slice(0, 10))
            item.dateOfContract && (item.dateOfContract = new Date(parseInt(item.dateOfContract) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformFiskalMod = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.dateOfRelease && (item.dateOfRelease = new Date(parseInt(item.dateOfRelease) * 1000).toISOString().slice(0, 10))
            item.dateOfContract && (item.dateOfContract = new Date(parseInt(item.dateOfContract) * 1000).toISOString().slice(0, 10))
            item.dateOfBatch && (item.dateOfBatch = new Date(parseInt(item.dateOfBatch) * 1000).toISOString().slice(0, 10))
            item.dateOfAct && (item.dateOfAct = new Date(parseInt(item.dateOfAct) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformDevice = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.yearOfRelease && (item.yearOfRelease = new Date(parseInt(item.yearOfRelease) * 1000).toISOString().slice(0, 10))
            item.dateOfContract && (item.dateOfContract = new Date(parseInt(item.dateOfContract) * 1000).toISOString().slice(0, 10))
            item.dateOfBatch && (item.dateOfBatch = new Date(parseInt(item.dateOfBatch) * 1000).toISOString().slice(0, 10))
            item.dateOfAct && (item.dateOfAct = new Date(parseInt(item.dateOfAct) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformInterior = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.yearOfRelease && (item.yearOfRelease = new Date(parseInt(item.yearOfRelease) * 1000).toISOString().slice(0, 10))
            item.dateOfContract && (item.dateOfContract = new Date(parseInt(item.dateOfContract) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}

export const transformOthers = data => {
    if (Array.isArray(data)) {
        const transFormedData = data.map((item, index) => {
            item.index = index + 1
            item.dateOfContractPurchasesSales && (item.dateOfContractPurchasesSales = new Date(parseInt(item.dateOfContractPurchasesSales) * 1000).toISOString().slice(0, 10))
            item.yearOfProduction && (item.yearOfProduction = new Date(parseInt(item.yearOfProduction) * 1000).toISOString().slice(0, 10))
            return item
        })
        return transFormedData
    } else {
        return []
    }
}