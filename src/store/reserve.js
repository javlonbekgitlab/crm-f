import { action, makeAutoObservable, makeObservable, observable } from "mobx"

export const reserveOrder = observable({
    setReserveOrder: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveOrder: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveProperty = observable({
    setReserveProperty: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveProperty: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveBranch = observable({
    setReserveBranch: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveBranch: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveOffice = observable({
    setReserveOffice: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveOffice: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveStorage = observable({
    setReserveStorage: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveStorage: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveRetrieve = observable({
    setReserveRetrieve: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveRetrieve: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveTechnics = observable({
    setReserveTechnics: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveTechnics: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveFiscalModules = observable({
    setReserveFiscalModules: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveFiscalModules: action(function(){
        for (var member in this) delete this[member]
    })
})

export const reserveDevices = observable({
    setReserveDevices: action(function({target}){
        this[target.name] = target.value
    }),
    clearReserveDevices: action(function(){
        for (var member in this) delete this[member]
    })
})

export class ReserveForm {
    // constructor() {
    //     makeAutoObservable(this, {
    //         // this: observable,
    //         setReserve: action,
    //         clearReservefunction: action
    //     })
    // }
    setReserve({target}){
        this[target.name] = target.value
    }
    clearReservefunction(){
        for (var member in this) delete this[member]
    }
} 

const reserve = new ReserveForm()

const ev = {
    target: {
        value: 'asdasdas'
    }
}
reserve.clearReservefunction()
reserve.setReserve(ev)