import { action, observable } from "mobx"
import { getRoles } from "./getDataFromServer"
import { transformRelease, transformAdditionalAdminstrativeContracts, transformAdminstrativeContracts, transformApplications, transformDate, transformDateUnix, transformDevices, transformId, transformIndexTinPinfl, transformSubscriberControlSystem, transformUsers, transformTechAndEquip, transformFiskalMod, transformDevice, transformInterior, transformOthers } from "./transformers"


// временное решение для мастер ветки
// export let partList = observable({
//     config: {},
//     data: [],
//     headers: {},
//     request: {},
//     status: 0
// })

// export const setPartList = action(data => {
//     partList.config = data.config
//     partList.data = transformDate(data.data)
//     partList.headers = data.headers
//     partList.request = data.request
//     partList.status = data.status
// })

// export let merchantList = observable({
//     config: {},
//     data: [],
//     headers: {},
//     request: {},
//     status: 0
// }) 

// export const setMerchantList = action(data => {
//     merchantList.config = data.config
//     merchantList.data = transformDate(data.data)
//     merchantList.headers = data.headers
//     merchantList.request = data.request
//     merchantList.status = data.status
// })

// export const storeList = observable({
//     config: {},
//     data: [],
//     headers: {},
//     request: {},
//     status: 0
// })

// export const setStoreList = action(data => {
//     storeList.config = data.config
//     storeList.data = transformDate(data.data)
//     storeList.headers = data.headers
//     storeList.request = data.request
//     storeList.status = data.status
// })

export const merchantsList = observable({
    data: [],
    setOrderList: action(function(res){
        // this.data = transformDate({
        //     data: transformId(res.data),
        //     key: 'createdAt'
        // })
        this.data = transformIndexTinPinfl(res.data)
    })
})

export const applicationList = observable({
    data: [],
    total: Number,
    setApplicationList: action(function({response, page}){
        console.log('response.data',response.data.data)
        this.data = transformDate({
            data: transformId(transformApplications(transformDateUnix(response.data.data, 'date')), page),
            key: 'createdAt'
        })
        this.total = response.data.total
    }),
    clearList: function() {
        this.data = []
        this.total = 0
    }
})

export const supportProRepairList = observable({
    data: [
        {
            index: 1,
            ctoEmployee: 'chuvak',
            breakdownReason: 'buzgan',
            modelNo: 'pax a 930',
            serial_number: 11214555,
            comment: 'commnet',
            applicationNumber: '115wq255',
            merchName: 'merchant name'
        }
    ],
    setSupportProRepairList: action(function(res){
        console.log(res)
        this.data = transformDateUnix(transformId(res.data), 'createDate')
    })
})

export const storeList = observable({
    data: [],
    setStoreList: action(function(res){
        // this.data = transformDate({
        //     data: transformDateUnix(transformId(res.data), 'time'),
        //     key: 'createdAt'
        // })
        this.data = transformIndexTinPinfl(res.data)
    })
})

export const cashDrawerList = observable({
    data: [],
    setCashDrawerList: action(function(res){
        this.data = transformDevices(res.data)
    })
})

export const supportProAppList = observable({
    data: [
        {   
            index: '1', 
            name: 'PAXSTORE',
            package: 'com.pax.market...',
            version: '7.4.1',
            date: '01.01.2103',
            detailPanel: {
                action: false,
                data: [
                    {
                        index: 1,
                        name: 'test',
                        middleName: 'asdas'
                    }
                ]
            }
        },
        {   
            index: '12', 
            name: 'PAXSTORE',
            package: 'com.pax.market...',
            version: '7.4.1',
            date: '01.01.2103',
            detailPanel: {
                action: false,
                data: [
                    {
                        index: 1,
                        name: 'test',
                        middleName: 'asdas'
                    }
                ]
            }
        },
    ],
    setContactsList: action(function(res){
        this.data = transformId(res.data)
    })
})

export const propertyList = observable({
    data: [],
    setPropertyList: action(function(res){
        this.data = transformDateUnix(transformId(res.data), 'dateContractOfSale')
    })
})

export const contactsList = observable({
    data: [
        {   
            index: '1', 
            test: 'test',
            detailPanel: {
                action: false,
                data: [
                    {
                        index: 1,
                        name: 'test',
                        middleName: 'asdas'
                    }
                ]
            }
        },
        {   
            index: '12', 
            test: 'test2',
            detailPanel: {
                action: false,
                data: [
                    {
                        index: 1,
                        name: 'test',
                        middleName: 'asdas'
                    }
                ]
            }
        },
    ],
    setContactsList: action(function(res){
        this.data = transformId(res.data)
    })
})

export const usersList = observable({
    data: [],
    setUsersList: action(async function(res){
        const response = await getRoles()
        this.data = transformUsers(transformId(res.data), response)
    })
})

export const rolesList = observable({
    data: [],
    setRolesList: action(function(res){
        this.data = res.data
    })
})

export const branchesList = observable({
    data: [],
    setBranchesList: action(function(res){
        this.data = transformId(res.data)
    })
})

export const terminalStore = observable({
    data: {},
    setTerminalStore: action(function(res){
        this.data = res
        console.log(this.data)
    })
})

export const subscriberControlSystemStore = observable({
    data: [],
    setSubscriberControlSystemStore: action(function(res){
        this.data = transformSubscriberControlSystem(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

// export const adminstrativeContractsStore = observable({
//     data: [],
//     setAdminstrativeContractsStore: action(function(res){
//         this.data = transformAdminstrativeContracts(res)
//         console.log('administrative',this.data)
//     }),
//     clearList: function() {
//         this.data = []
//     }
// })

export const adminstrativeContractsStore = observable({
    data: [],
    total: Number,
    setAdminstrativeContractsStore: action(function({response, page}){
        console.log('response.data',response)
        this.data = transformAdminstrativeContracts({data: response.data.data, page})
        this.total = response.data.total
    }),
    clearList: function() {
        this.data = []
        this.total = 0
    }
})

export const additionalAdminstrativeContractsStore = observable({
    data: [],
    setAdditionalAdminstrativeContractsStore: action(function(res){
        this.data = transformAdditionalAdminstrativeContracts(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const internationalContractsStore = observable({
    data: [],
    setInternationalContractsStore: action(function(res){
        this.data = transformAdminstrativeContracts(res)
        console.log('international',this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const additionalInternationalContractsStore = observable({
    data: [],
    setAdditionalInternationalContractsStore: action(function(res){
        this.data = transformAdditionalAdminstrativeContracts(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const retrieveStore = observable({
    data: [],
    setRetrieveStore: action(function(res) {
        console.log('retrieve', res)
        this.data = transformRelease(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const releaseStore = observable({
    data: [],
    setReleaseStore: action(function(res) {
        console.log('Release', res)
        this.data = transformRelease(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const defectsStore = observable({
    data: [],
    setDefectsStore: action(function(res) {
        console.log('Defects', res)
        this.data = transformRelease(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const techAndEquipStore = observable({
    data: [],
    setTechAndEquipStore: action(function(res) {
        console.log('store', res)
        this.data = transformTechAndEquip(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const fiskalModStore = observable({
    data: [],
    setFiskalModStore: action(function(res) {
        console.log('store', res)
        this.data = transformFiskalMod(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const deviceStore = observable({
    data: [],
    setDeviceStore: action(function(res) {
        console.log('store', res)
        this.data = transformDevice(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const interiorStore = observable({
    data: [],
    setInteriorStore: action(function(res) {
        console.log('store', res)
        this.data = transformInterior(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const othersStore = observable({
    data: [],
    setOthersStore: action(function(res) {
        console.log('store', res)
        this.data = transformOthers(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})

export const acceptanceCertificateStore = observable({
    data: [],
    setAcceptanceCertificateStore: action(function(res) {
        console.log('store', res)
        this.data = transformOthers(res)
        console.log(this.data)
    }),
    clearList: function() {
        this.data = []
    }
})
// export const serviceCenter = observable([
//     {
//         title: 'Tashkent',
//         id: '1'
//     },
//     {
//         title: 'Cairo',
//         id: '2'
//     },
//     {
//         title: 'Los Angeles',
//         id: '3'
//     }
// ])


// export const employees = observable([
//     {
//         title: 'T-800',
//         id: '1'
//     },
//     {
//         title: 'Serious Sam',
//         id: '2'
//     },
//     {
//         title: 'Carl Johnson',
//         id: '3'
//     }
// ])
