import { merchantsList, branchesList, propertyList, usersList, rolesList, storeList, cashDrawerList, applicationList, subscriberControlSystemStore, adminstrativeContractsStore, retrieveStore, releaseStore, defectsStore, techAndEquipStore, fiskalModStore, deviceStore, interiorStore, othersStore, additionalAdminstrativeContractsStore, internationalContractsStore, additionalInternationalContractsStore, acceptanceCertificateStore } from "."
import { Api, apiUrles } from "../api"
import { snackbar, snackbarStatus } from "../actions"
import { lang } from "../languages/lang"
import { readLocalStorage } from "./localStorage"
import sha1 from 'sha1'
import querystring from 'querystring'
import { apiParams } from "../constants"
import { forcedLogOut } from "../actions/forcedLogOut"
require('dotenv').config()

// export const getPartners = async() => {
//     const res = await Api(apiUrles.partner)
//     if (res.status === 200) {
//         setPartList(res)
//     } else {
//         console.log(res)
//     }
// }

// export const getMerchantList = async() => {
//     const resMerchant = await Api(apiUrles.merchant)
//     if (resMerchant.status === 200) {
//         setMerchantList(resMerchant)
//     } else {
//         alert(resMerchant.toString())
//     }
// }

// export const getStoreList = async() => {
//     const resStore = await Api(apiUrles.store)
//     if (resStore.status === 200) {
//         // setStoreList(resStore)
//     } else {
//         alert(resStore.toString())
//     }
// }
export const getMerchants = async() => {
    const token = readLocalStorage('token')
    const res = await Api(apiUrles.merchants, {}, 'get', token)
    console.log(res)
    if (res.status === 200) {
        merchantsList.setOrderList(res)
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getAllApplications = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    console.log('___________________________________________________________')
    const response = await Api(`${apiUrles.allApplications}?${apiParams.page}${page}&${apiParams.search}${search}&${apiParams.status}${status}`, {}, 'get', token)
    console.log('all applications',response)
    if (response.status === 200) {
        applicationList.setApplicationList({
            response,
            page
        })
    } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
        console.log('error forcelogout')
        forcedLogOut()
    } else {
        applicationList.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getSubscriberControlSystemStoreData = async ({search = ''}) => {
    console.log(search)
    const token = readLocalStorage('token')
    const response = await Api(
        `${apiUrles.subscriberControlSystemGetContract}`, 
        {contractnumber: search}, 
        'post',
        token
    )
    console.log('billing datataaa',response)
    if (response.status === 200) {
        if (response.data.code === 0) {
            subscriberControlSystemStore.setSubscriberControlSystemStore([response.data.params])
        } else {
            subscriberControlSystemStore.clearList()
            snackbar.setSnackbar(lang.notifications.helperText.notFound, snackbarStatus.error)
        }
    } else {
        // applicationList.clearList()
        subscriberControlSystemStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getAdminstrativeContractsStoreData = async ({page = 0, search = ''}) => {
    // console.log(search)
    const token = readLocalStorage('token')
    const response = await Api(
        `${apiUrles.adminstrativeContractsStore}?${apiParams.page}${page}`, 
        null, 
        'get',
        token
    )
    console.log(response)
    if (response.status === 200) {
        adminstrativeContractsStore.setAdminstrativeContractsStore({
            response,
            page
        })
    } else {
        // applicationList.clearList()
        adminstrativeContractsStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getAdminstrativeAdditionalContracts = async ({search = '', mainContractId = ''}) => {
    // console.log(search)
    console.log('main+_____',mainContractId)
    const token = readLocalStorage('token')
    const response = await Api(
        `${apiUrles.adminstrativeAdditionalContracts}${mainContractId}`, 
        null, 
        'get',
        token
    )
    console.log(response)
    if (response.status === 200) {
        additionalAdminstrativeContractsStore.setAdditionalAdminstrativeContractsStore(response.data)
    } else {
        // applicationList.clearList()
        additionalAdminstrativeContractsStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getInternationalContracts = async ({search = ''}) => {
    // console.log(search)
    const token = readLocalStorage('token')
    const response = await Api(
        `${apiUrles.internationalContracts}`, 
        null, 
        'get',
        token
    )
    console.log(response)
    if (response.status === 200) {
        internationalContractsStore.setInternationalContractsStore(response.data)
    } else {
        // applicationList.clearList()
        internationalContractsStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getInternationalAdditionalContracts = async ({search = '', mainContractId = ''}) => {
    // console.log(search)
    console.log('main+_____',mainContractId)
    const token = readLocalStorage('token')
    const response = await Api(
        `${apiUrles.internationalAdditionalContracts}${mainContractId}`, 
        null, 
        'get',
        token
    )
    console.log(response)
    if (response.status === 200) {
        additionalInternationalContractsStore.setAdditionalInternationalContractsStore(response.data)
    } else {
        // applicationList.clearList()
        additionalInternationalContractsStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getRetrieve = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.retrieve, {}, 'get', token)
    console.log('retrieve response', response)
    if (response.status === 200) {
        retrieveStore.setRetrieveStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        retrieveStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getRelease = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.release, {}, 'get', token)
    console.log('release response', response)
    if (response.status === 200) {
        releaseStore.setReleaseStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        releaseStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getDefects = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.defects, {}, 'get', token)
    console.log('release response', response)
    if (response.status === 200) {
        defectsStore.setDefectsStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        defectsStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getTechAndEquip = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.equipmentAndTech, {}, 'get', token)
    console.log('equipmentAndTech response', response)
    if (response.status === 200) {
        techAndEquipStore.setTechAndEquipStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        techAndEquipStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getFiskalMod = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.fiskalMod, {}, 'get', token)
    console.log('fisk response', response)
    if (response.status === 200) {
        fiskalModStore.setFiskalModStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        fiskalModStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getDevice = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.device, {}, 'get', token)
    console.log('device response', response)
    if (response.status === 200) {
        deviceStore.setDeviceStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        deviceStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getInterior = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.interior, {}, 'get', token)
    console.log('device response', response)
    if (response.status === 200) {
        interiorStore.setInteriorStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        interiorStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getOthers = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    const response = await Api(apiUrles.others, {}, 'get', token)
    console.log('device response', response)
    if (response.status === 200) {
        othersStore.setOthersStore(response.data)
    // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    //     console.log('error forcelogout')
    //     forcedLogOut()
    } else {
        othersStore.clearList()
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getAcceptanceCertificates = async ({page = 0, search = '', status = '0'}) => {
    const token = readLocalStorage('token')
    // const response = await Api(apiUrles.others, {}, 'get', token)
    // console.log('device response', response)
    // if (response.status === 200) {
    //     acceptanceCertificateStore.setAcceptanceCertificateStore(response.data)
    // // } else if (response.status === 401 || response.status === 404 || response.status === 403 ) {
    // //     console.log('error forcelogout')
    // //     forcedLogOut()
    // } else {
    //     acceptanceCertificateStore.clearList()
    //     snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    // }
}

export const getStores = async id => {
    const token = readLocalStorage('token')
    const res = await Api(`${apiUrles.stores}${id}`, {}, 'get', token)
    console.log(res)
    if (res.status === 200) {
        storeList.setStoreList(res)
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getCashDrawers = async id => {
    const token = readLocalStorage('token')
    const res = await Api(`${apiUrles.cashDrawers}${id}`, {}, 'get', token)
    console.log('cashdrawers',res)
    if (res.status === 200) {
        cashDrawerList.setCashDrawerList(res)
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
    }
}

export const getBranches = async() => {
    const token = readLocalStorage('token')
    const res = await Api(apiUrles.branches, {}, 'get', token)
    if (res.status === 200) {
        branchesList.setBranchesList(res)
    } else {
        console.log(res)
    }
}

export const getProperties = async() => {
    const token = readLocalStorage('token')
    const res = await Api(apiUrles.properties, {}, 'get', token)
    if (res.status === 200) {
        propertyList.setPropertyList(res)
        // snackbar.setSnackbar()
        // showStatusConnection(false)
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        // showStatusConnection(true)

    }
}

export const getUsers = async() => {
    const token = readLocalStorage('token')
    const res = await Api(apiUrles.getUsers, {}, 'get', token)
    if (res.status === 200) {
        console.log(res)            
        usersList.setUsersList(res)
        // snackbar.setSnackbar()
        // showStatusConnection(false)
    } else {
        console.log(res)            
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        // showStatusConnection(true)

    }
}

export const getRoles = async () => {
    // const token = readLocalStorage('token')
    const res = await Api(apiUrles.getRoles, {}, 'get')
    console.log('roles',res)
    if (res.status === 200) {
        rolesList.setRolesList(res)
        return res.data
        // snackbar.setSnackbar()
        // showStatusConnection(false)
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        // showStatusConnection(true)
        return []

    }
}

// export const getUrban = async cityCountyId => {
//     const appsecret = '&appsecret=0664a3cc2f08b057704024af64350333'
//     const data = {
//         opMapId: 10000001,
//         cityCountyId,
//         time: Math.floor(new Date().getTime() / 1000),
//     }
//     const ordered = Object.keys(data).sort().reduce((obj, key) => { 
//             obj[key] = data[key] 
//             return obj
//         }, 
//     {})
//     const sign = sha1(querystring.stringify(ordered,  null, null, { encodeURIComponent: querystring.unescape }) + appsecret)
//     data.sign = sign
//     const res = await Api(apiUrles.getUrban, data, 'post', null, apiUrles.chinaHosting)
//     return res
// }

export const getRegions = async () => {
    const res = await Api(`${apiUrles.infoRegions}?lang=${`uz`}`, null, 'get', null)
    if (res.status === 200) {
        return res.data.data
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        return false
    }
}

export const getDistricts = async code => {
    const res = await Api(`${apiUrles.districtsByRegion}?lang=${`uz`}&code=${code}`, null, 'get', null)
    if (res.status === 200) {
        return res.data.data
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        return false
    }
}

export const getGroupActivities = async () => {
    const res = await Api(`${apiUrles.activityGroup}?lang=${lang.activeLang}`, null, 'get', null)
    if (res.status === 200) {
        return res.data.data
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        return false
    }
}

export const getListActivities = async code => {
    const res = await Api(`${apiUrles.activityList}?lang=${lang.activeLang}&code=${code}`, null, 'get', null)
    if (res.status === 200) {
        return res.data.data
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        return false
    }
}