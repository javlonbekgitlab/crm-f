export const writeLocalStorage = (key, value) => localStorage.setItem(key, value)

export const readLocalStorage = key => localStorage.getItem(key)

export const removeItemsFromLocalStorage = keys => keys.map(key => localStorage.removeItem(key))

export const readArrayLocalStorage = key => JSON.parse(localStorage.getItem(key))

export const writeArrayLocalStorage = (key, value) => {
    let additionalRoles = []
    value.map(role => {
        if ((role !== 'sales_mod') && (role === 'support_lite_mod' || role === 'support_pro_mod' || role === 'support_repair_mod')) {
            additionalRoles.push('sales_mod')
        }
        if ((role !== 'assets_mod') && (role === 'object_mod' || role === 'assembly_mod' || role === 'user_assembly_mod' || role === 'user_items_mod' || role === 'items_mod')) {
            additionalRoles.push('assets_mod')
        }
        if ((role !== 'adminstration_mod') && (role === 'hr_mod' || role === 'edm_mod' || role === 'tools_mod' )) {
            additionalRoles.push('adminstration_mod')
        }
        if ((role !== 'accounting_mod') && (role === 'billing_mod' || role === 'contracts_mod' || role === 'e-bank_mod')) {
            additionalRoles.push('accounting_mod')
        }
    })
    localStorage.setItem(key, JSON.stringify([...additionalRoles, ...value]))
}

export const accessModule = module => {
    const roles = JSON.parse(localStorage.getItem('roles'))
    if (roles && roles.includes(module)) {
        return true
    } else {
        return false
    }
}

