import { observable, action } from "mobx"
// import { wordsEn } from "./en"
import { wordsRu } from "./ru"
import { wordsUz } from "./uz"

export let lang = observable({
    ...wordsRu, 
    activeLang: 'ru'
})

export const changeLanguage = action(function(setLang){
    console.log(setLang)
    switch(setLang) {
        case 'ru':
            lang.forms = wordsRu.forms
            lang.menuPage = wordsRu.menuPage
            lang.activeLang = 'ru'
            lang.actsOfreceivingPage = wordsRu.actsOfreceivingPage
            lang.additionalAdminstrativeContractsPage = wordsRu.additionalAdminstrativeContractsPage
            lang.additionalInternationalContractsPage = wordsRu.additionalInternationalContractsPage
            lang.adminstrativeContractsPage = wordsRu.adminstrativeContractsPage
            lang.appBar = wordsRu.appBar
            lang.applicationPage = wordsRu.applicationPage
            lang.branch = wordsRu.branch
            lang.calendar = wordsRu.calendar
            lang.cashDrawerPage = wordsRu.cashDrawerPage
            lang.contacts = wordsRu.contacts
            lang.defects = wordsRu.defects
            lang.devices = wordsRu.devices
            lang.fiscalModules = wordsRu.fiscalModules
            lang.forms = wordsRu.forms
            lang.hrPage = wordsRu.hrPage
            lang.interior = wordsRu.interior
            lang.internationalContractsPage = wordsRu.internationalContractsPage
            lang.markingPage = wordsRu.markingPage
            lang.menuPage = wordsRu.menuPage
            lang.notifications = wordsRu.notifications
            lang.office = wordsRu.office
            lang.orderPage = wordsRu.orderPage
            lang.otherContractsPage = wordsRu.otherContractsPage
            lang.others = wordsRu.others
            lang.paymentsPage = wordsRu.paymentsPage
            lang.procurementPlanPage = wordsRu.procurementPlanPage
            lang.property = wordsRu.property
            lang.release = wordsRu.release
            lang.retrieve = wordsRu.retrieve
            lang.storage = wordsRu.storage
            lang.storePage = wordsRu.storePage
            lang.subscriberControlSystemPage = wordsRu.subscriberControlSystemPage
            lang.supportProApp = wordsRu.supportProApp
            lang.supportProPage = wordsRu.supportProPage
            lang.supportProRepair = wordsRu.supportProRepair
            lang.table = wordsRu.table
            lang.technics = wordsRu.technics
            lang.terminalPage = wordsRu.terminalPage
            lang.tools = wordsRu.tools
            break;
        case 'uz': 
            lang.forms = wordsUz.forms
            lang.menuPage = wordsUz.menuPage
            lang.activeLang = 'uz'
            lang.actsOfreceivingPage = wordsUz.actsOfreceivingPage
            lang.additionalAdminstrativeContractsPage = wordsUz.additionalAdminstrativeContractsPage
            lang.additionalInternationalContractsPage = wordsUz.additionalInternationalContractsPage
            lang.adminstrativeContractsPage = wordsUz.adminstrativeContractsPage
            lang.appBar = wordsUz.appBar
            lang.applicationPage = wordsUz.applicationPage
            lang.branch = wordsUz.branch
            lang.calendar = wordsUz.calendar
            lang.cashDrawerPage = wordsUz.cashDrawerPage
            lang.contacts = wordsUz.contacts
            lang.defects = wordsUz.defects
            lang.devices = wordsUz.devices
            lang.fiscalModules = wordsUz.fiscalModules
            lang.forms = wordsUz.forms
            lang.hrPage = wordsUz.hrPage
            lang.interior = wordsUz.interior
            lang.internationalContractsPage = wordsUz.internationalContractsPage
            lang.markingPage = wordsUz.markingPage
            lang.menuPage = wordsUz.menuPage
            lang.notifications = wordsUz.notifications
            lang.office = wordsUz.office
            lang.orderPage = wordsUz.orderPage
            lang.otherContractsPage = wordsUz.otherContractsPage
            lang.others = wordsUz.others
            lang.paymentsPage = wordsUz.paymentsPage
            lang.procurementPlanPage = wordsUz.procurementPlanPage
            lang.property = wordsUz.property
            lang.release = wordsUz.release
            lang.retrieve = wordsUz.retrieve
            lang.storage = wordsUz.storage
            lang.storePage = wordsUz.storePage
            lang.subscriberControlSystemPage = wordsUz.subscriberControlSystemPage
            lang.supportProApp = wordsUz.supportProApp
            lang.supportProPage = wordsUz.supportProPage
            lang.supportProRepair = wordsUz.supportProRepair
            lang.table = wordsUz.table
            lang.technics = wordsUz.technics
            lang.terminalPage = wordsUz.terminalPage
            lang.tools = wordsUz.tools
            break;
    }
})