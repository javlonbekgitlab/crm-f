export const wordsRu = {
    forms: {
        validations: {
            onlyNumbers: 'только числа',
            lengthTinPinfl: 'length is equal to 9 or 14'
        },
        buttonTitles: {
            add: 'Добавить',
            cancel: 'Отменить',
            edit: 'Изменить',
            back: 'назад',
            next: 'Далее',
            filter: 'Фильтр',
            delete: 'Удалить',
            download: 'Cкачать',
            search: 'Поиск',
            contacts: 'Контакты',
            calendar: 'Календарь',
            status: 'Статус',
            closeApplication: 'Закрыть заявку',
            openApplication: 'ЗАЯВЛЕНИЕ',
            addCashier: 'ДОБАВИТЬ КАССИРА',
            sentToNits: 'Отправить Заявление',
            checkTinPinfl: 'Проверить',
            additionalConract: 'Доп. Соглашение'
            // next: 'След',
            // prev: 'Пред',
            // today: 'Сегодня',
            // month: 'Месяц',
            // week: 'Неделя',
            // day: 'День',
            // agenda: 'Повестка дня'
        },
        partner: {
            partnerName: 'имя партнера',
            address: 'адрес',
            firstMiddleLastName: 'ФИО менеджера',
            enterSystem: 'вход в систему',
            addPartner: 'addPartner',
            editPartner: 'editPartner',
        },
        merchant: {
            opName: 'имя партнера',
            userMobile: 'логин ID',
            nickName: 'имя босса',
            password: 'пароль',
            addMerchant: 'addMerchant',
            editMerchant: 'editMerchant',
        },
        store: {
            shopName: 'название магазина',
            cityAddress: 'адрес',
            addStore: 'addStore',
            editStore: 'editStore',
        },
        hr: {
            title: 'ДАННЫЕ О СОТРУДНИКЕ',
            passportData: 'ПАСПОРТНЫЕ ДАННЫЕ',
            educationData: 'ДАННЫЕ ОБ ОБРАЗОВАНИИ',
            taxData: 'НАЛОГОВЫЕ ДАННЫЕ',
            corporativeData: 'КОРПОРАТИВНЫЕ ДАННЫЕ',
            fullName: 'ФИО',
            post: 'Должность',
            department: 'Отдел',
            phoneNumber: 'Телефонный номер',
            roles: 'Роли',
            structure: 'Структура',
            email: 'Email',
            password: 'Пароль',
            surName: 'Фамилия',
            name: 'Имя',
            middleName: 'Отчество',
            series: 'Серия',
            passportNumber: 'Номер паспорта',
            givenPlace: 'Место выдачи',
            givenDate: 'Дата выдачи',
            birthDate: 'Дата рождения',
            birthPlace: 'Место рождения',
            inhabitationPlace: 'Место жительства',
            education: 'Образование',
            seriesOfDiploma: 'Серия диплома',
            levelOfEducation: 'Учебное заведение',
            numberOfDiploma: 'Серия и Номер диплома',
            cardNumber: 'Номер пластиковой карты',
            corpTelNumber: 'Корпоративный моб. номер',
            personalTelNumber: 'Личный моб. номер',
            social: 'Соц. сети',
            telegramId: 'Telegram ID',
            corpEmail: 'Корпоративный почтовый ящик',
            // gmail: 'Почтовый ящик Google',
            gmail: 'gmail',
            zoomId: 'Zoom ID',
            weChatId: 'WeChat ID'
        },
        signIn: {
            title: 'Вход в систему',
            login: 'Логин',
            password: 'Пароль',
            toReg: 'Регистрация',
            showLogin: 'Логин: '
        },
        signUp: {
            title: 'Регистрация',
            login: 'Логин',
            password: 'Пароль',
            name: 'Имя',
            surName: 'Фамилия',
            userName: 'userName',
            phoneNumber: 'Телефонный номер',
            address: 'Адрес',
            adminSystemLogin: 'adminSystemLogin',
            confirmPassword: 'Подтвердите пароль',
            toLog: 'Вход в систему'
        },
        storeOrder: {
            name: 'Название предприятия',
            regions: 'Регион',
            city: 'Район/город',
            address: 'Адрес',
            groupActivity: 'Группа видов деятельности',
            typeActivity: 'Вид деятельности',
            numberCadastre: 'Номер кадастра / Номер аренды',
            docCadastre: 'Документ кадастра / Аренды',
            latitude: 'Широта',
            longitude: 'Долгота',
            cashier: 'КАССИР'
        },
        terminal: {
            application: 'Заявление'
        },
        order: {
            tin: 'ИНН',
            pinfl: 'ПИНФЛ',
            tinOrPinfl: 'Введите ИНН или ПИНФЛ',
            newStationSelect: 'Выбор торговой точки',
            companyName: 'Наименование',
            region: 'регион',
            city: 'район / город',
            companyAddress: 'юридический адрес субъекта',
            contractNumber: 'Контракт №',
            contractDate: 'contractDate',
            deviceModel: 'Модель',
            serializeNumber: 'Серийный номер',
            fiskNumber: 'Фискальный модуль',
            brand: 'Бренд',
            serviceCenter: 'Сервисный центр',
            employee: 'Сотрудники',
            firstMiddleLastName: 'ФИО',
            telephone: 'Телефон',
            login: 'Логин',
            bidType: 'Тип заявления',
            dateAgreement: 'Дата договора',
            connection: 'Подключение',
            contractFile: 'Файл договора',
            buy: 'Покупка',
            bank: 'Банк',
            other: 'Прочее',
            titles: {
                inn: 'инн',
                device: 'устройства',
                personalData: 'личные данные',
                service: 'сервисные услуги',
                agreement: 'файлы',
                formCentTechService: 'форма цто',
                documentation: 'номенклатура',
                copyPassport: 'копия паспорта директора',
                registraionDoc: 'свидетельство о регистрации',
                others: 'прочее',
                identification: 'Идентификация'
            }
        },
        supportPro: {
            type: 'Тип',
            bank: 'Банк',
            deviceNumber: 'Номер аппарата',
            model: 'Модель',
            commercialNumber: 'Коммерческое название',
            idMerchant: 'ID продавца',
            idTerminal: 'ID терминала',
            transMenu: 'Транс меню',
            lockTerminal: 'Блокировать терминал',
            unlockTerminal: 'Разблокировать терминал'
        },
        supportProRepair: {
            ctoWorker: 'Сотрудник ЦТО',
            dateCreateApplication: 'Дата запроса',
            breakdownReason: 'Причина поломки',
            applicationFile: 'Форма запроса на починку',
            downloadApplication: 'СКАЧАТЬ ФОРМУ',
            comment: 'Комментарий'
        },
        property: {
            titles: {
                immovables: 'НЕДВИЖИМОСТЬ',
                agreement: 'ДОГОВОР'
            },
            nameImmovables: 'Название недвижимости',
            typeImmovables: 'Тип недвижимости',
            dateContractOfSale: 'Дата договора купли продажи',
            contractOfSale: 'Номер договора купли продажи'
        },
        branch: {
            titles: {
                agreement: 'ДОГОВОР',
                filial: 'ФИЛИАЛ',
                location: 'МЕСТОПОЛОЖЕНИЕ',
                others: 'ПРОЧЕЕ'
            },
            branchName: 'Название филиала',  
            branchType: 'Тип филиала',  
            republic: 'Республика',  
            region: 'Регион',  
            district: 'Район',  
            address: 'Адрес',  
            cadastre: 'Номер кадастра',  
            agreement: 'Номер договора',  
            agreementDate: 'Дата договора',  
            agreementSum: 'Сумма договора',  
            agreementTerm: 'Срок договора',  
            area: 'Общая площадь',  
            roomNumber: 'Количество комнат',  
            employeeNumber: 'Количество сотрудников',  
            assets: 'Активы'  
        },
        office: {
            titles: {
               office: 'ОФИС' 
            },
            officeName: 'Название офиса',
            officeType: 'Тип офиса'
        },
        storage: {
            titles: {
                storage: 'СКЛАД',
                document: 'ДОКУМЕНТ'
            },
            storageLocation: 'Расположение склада',
            storageName: 'Наименование склада',
            storageType: 'Тип склада',
            date: 'Дата создания',
            orderNumber: 'Номер приказа',
            orderDate: 'Дата приказа',
            responsibleEmployee: 'Ответственный сотрудник'
        },
        retrieve: {
            titles: {
                spec: 'СПЕЦИФИКАЦИЯ',
                device: 'УСТРОЙСТВО',
                others: 'ПРОЧЕЕ',
                agreement: 'ДОГОВОР'
            },
            incomingType: 'Тип прихода',
            replacementPartType: 'Тип запчасти',
            category: 'Категория',
            model: 'Модель',
            year: 'Год выпуска',
            supplyNumber: 'Номер партии',
            supplyDate: 'Дата партии',
            amount: 'Количество',
            agreement: 'Номер договора',
            agreementDate: 'Дата договора',
            act: 'Номер акта',
            actDate: 'Дата акта',
            responsibleEmployee: 'Ответственный сотрудник',
            status: 'Статус',
            file_name: 'Файл',
            brand: 'Бренд',
            deviceType: 'Тип устройства'
        },
        technics: {
            titles: {
                spec: 'ХАРАКТЕРИСТИКИ'
            },
            name: 'Имя',
            type: 'Тип',
            brand: 'Бренд',
            price: 'Стоимость',
            specs: 'Характеристики',
            cpu: 'ЦП',
            ram: 'ОЗУ',
            os: 'ОС',
            memory: 'Память',
            title: 'Наименование',
            netAddress: 'Сетевой адрес',
            inventoryNumber: 'Инвентаризационный номер',
            inventoryLast: 'Последняя инвентаризация',
            state: 'Состояние',
            serialNumber: 'Серийный номер'
        },
        interior: {
            titles: {
                object: 'Объект'
            }
        },
        others: {
            mark: 'Марка',
            year: 'Год производства',
            techPassNumber: 'Номер технического паспорта',
            registrationNumber: 'Регистрационный номер',
            dateContractOfSale: 'Дата договора купли продажи',
            contractOfSale: 'Номер договора купли продажи',
            agreementSum: 'Сумма договора'
        }
    },
    orderPage: {
        title: 'Данные предпринимателя'
    },
    storePage: {
        title: 'Магазин'
    },
    cashDrawerPage: {
        title: 'Аппарат'
    },
    supportProRepair: {
        title: 'Регистрация на ремонт'
    },
    applicationPage: {
        title: 'Список заявлений'
    },
    paymentsPage: {
        title: 'Оплата'
    },
    adminstrativeContractsPage: {
        title: 'Административные Договора'
    },
    additionalAdminstrativeContractsPage: {
        title: 'Административные Договора Доп.Сог'
    },
    internationalContractsPage: {
        title: 'Интернациональные Договора'
    },
    additionalInternationalContractsPage: {
        title: 'Интернациональные Договора Доп.Сог'
    },
    otherContractsPage: {
        title: 'Остальные Договора'
    },
    subscriberControlSystemPage: {
        title: 'Система контроля абонента'
    },
    actsOfreceivingPage: {
        title: 'Акты приёма-передачи'
    },
    procurementPlanPage: {
        title: 'План закупа'
    },
    markingPage: {
        title: 'Маркировка'
    },
    terminalPage: {
        title: 'Терминалы'
    },
    supportProPage: {
        title: 'Pro',
        buttonTitles: {
            app: 'ПРИЛОЖЕНИЯ',
            uzcardPos: 'UZCARD POS',
            uzcardPosSa: 'UZCARD POS SA',
            tinda: 'TINDA',
            tindaFm: 'TINDA FM',
            connectPax: 'СОЕДИНИТЬ С PAX',
            pushMessage: 'PUSH СООБЩЕНИЯ',
            sync: 'СИНХРОНИЗАЦИЯ'
        },
        confirmation: {
            title: 'ПРИЛОЖЕНИЯ',
            text: 'Вы действительно хотите установить данное приложение?',
        }
    },
    supportProApp: {
        title: 'App',
        detailPanel: {
            title: 'push params'
        }
    },
    menuPage: {
        moduleTitles: {
            admin: 'Администрация',
            assets: 'Ресурсы',
            sales: 'Продажи',
            accounting: 'Бухгалтерия'
        },
        subModules: {
            objects: 'Объекты',
            assembly: 'Сборка',
            items: 'Предметы',
            requests: 'Заявки',
            hr: 'HR',
            edm: 'ЭДО',
            tools: 'Органайзер',
            supportLite: 'Регистрация договора',
            supportPro: 'Управление',
            supportRepair: 'Регистрация на ремонт',
            eBank: 'Электронный банк',
            billing: 'Биллинг',
            contracts: 'Контракты'
        },
        subSubModule: {
            retrieve: 'Cобранные',
            release: 'Реализованные',
            defects: 'Деффективные',
            equipment: 'Оборудование',
            interior: 'Интерьер',
            others: 'Прочее',
            property: 'Свойства',
            branch: 'Филиалы',
            storage: 'Склады',
            hr: 'Управление сотрудниками',
            contacts: 'Контакты партнёров',
            applications: 'Список заявлений',
            merchants: 'Данные предпринимателя',
            calendar: 'Календарь',
            payments: 'Оплаты',
            interactingWithTerminal: 'Взаимодействие с терминалом',
            repairRequestList: 'Cписок запросов',
            controlEdm: 'Контроль документооборота',
            subscriberControlSystem: 'Система контроля абонента',
            contractsList: 'Список договоров',
            adminstrativeContracts: 'Административные Договора',
            internationalContracts: 'Интернациональные Договора',
            otherContracts: 'Остальные Договора'
        },
        subSubSubModules: {
            technics: 'Техника',
            fiscalModules: 'Фискальные модули',
            devices: 'Устройства',
            acceptanceCertificate: 'Акт приёма-передачи',
            purchasePlan: 'План закупа',
            marking: 'Маркировка'
        }
    },
    retrieve: {
        title: 'Извлечённые'
    },
    property: {
        title: 'Недвижимость'
    },
    release: {
        title: 'Реализованные'
    },
    defects: {
        title: 'Деффективные'
    },
    technics: {
        title: 'Техника'
    },
    office: {
        title: 'Офис'
    },
    storage: {
        title: 'Склад'
    },
    branch: {
        title: 'Филиал'
    },
    fiscalModules: {
        title: 'Фискальные модули'
    },
    devices: {
        title: 'Устройства'
    },
    interior: {
        title: 'Интерьер'
    },
    others: {
        title: 'Прочее'
    },
    contacts: {
        title: 'Контакты',
        detailPanel: {
            title: 'Адрес:  Мемориал Святой Софии'
        }
    },
    tools: {
        title: 'Управление'
    },
    hrPage: {
        title: 'Hr',
        role: {
            title: 'Роли'
        }
    },
    calendar: {
        title: 'Календарь',
        daysInWeek: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        months: [
            {
                title: 'Январь'
            },
            {
                title: 'Февраль'
            },
            {
                title: 'Март'
            },
            {
                title: 'Апрель'
            },
            {
                title: 'Май'
            },
            {
                title: 'Июнь'
            },
            {
                title: 'Июль'
            },
            {
                title: 'Август'
            },
            {
                title: 'Сентябрь'
            },
            {
                title: 'Октябрь'
            },
            {
                title: 'Ноябрь'
            },
            {
                title: 'Декабрь'
            },
        ],
        action: {
            title: 'СОБЫТИЕ',
            start: 'Начало',
            end: 'Конец',
            typeOfActions: [
                {
                    title: 'Встреча с клиентами'
                }
            ]
        }
    },
    notifications: {
        success: {
            deleted: 'Удалено',
            added: 'Добавлено'
        },
        fail: {
            deleted: 'Не удалось удалить',
            added: 'Не удалось добавить',
            noConnection: 'Нет интернета'
        },
        helperText: {
            store: 'Магазин: ',
            merchant: 'Merchant: ',
            device: 'Кассовый аппарат ',
            tariff: 'Оплата по тарифу: ',
            balance: 'Баланс: ',
            notFound: 'Не удалось найти',
            statusDescription: 'Причина отказа: '
        }
    },
    table: {
        status: {
            all: 'Все',
            new: 'Новые',
            proc: 'Просматриваются',
            accept: 'Принятые',
            rejected: 'Отмененные' 
        },
        statusForRowTable: {
            new: 'Новый',
            proc: 'Просматриваются',
            accept: 'Принято',
            rejected: 'Отказано',
            noFile: 'Отсутствует договор',
            notSent: 'Еще не отправлено' 
        },
        columnsName: {
            merchant: {
                name: 'Предприниматель',
                phone: 'Телефон',
                tin: 'ИНН',
                tinOrPinfl: 'ИНН / ПИНФЛ',
                passportFile: 'Паспорт',
                date: 'Дата'
            },
            shop: {
                shopName: 'Наименование магазина',
                address: 'Адрес магазина',
                region: 'Регион',
                district: 'Район',
                activityGroup: 'Группа видов деятельности',
                activityType: 'Виды деятельности',
                cadastreFile: 'Документы кадастра/аренда',
                nomenclatureFile: 'Номенклатура'
            },
            cashDrawer: {
                fiskalMod: 'Фискальный модуль',
                machineNo: 'Серийный номер Терминала',
                modelName: 'Модель',
                applicationFile: 'Заявления',
                contractFile: 'Файл договора',
                applicationStatus: 'Статус'
            },
            partner: {
                phone: 'Тел. номер оператора',
                name: 'Оператор',
                createdAt: 'Дата создания'
            },
            payments: {
                accountNumber: 'Номер счёта',
                mfo: 'МФО',
                bankAccount: 'Расчетный счет',
                name: 'Название',
                quantity: 'Количество',
                description: 'Описание',
                payDay: 'Дата оплаты',
                payTime: 'Время оплаты',
                employee: 'Работник',
                balance: 'Баланс',
                clientName: 'Имя Контрагента'
            },
            terminals: {
                modelName: 'Модель терминала',
                id: 'ID',
                firmware: 'Версия прошивки',
                resellerName: 'resellerName'
            },
            adminstrativeContracts: {
                id: 'ID',
                uniqueCode: 'Уникальный код',
                attachedEmployee: 'Прикрепленный сотрудник',
                type: 'Тип',
                kindOfContract: 'Вид',
                dateOfContract: 'Дата договора',
                numberOfContract: 'Номер договора',
                subjectOfContract: 'Предмет договора',
                term: 'Срок',
                quantityOfSides: 'Количество сторон',
                secondSide: '2ая Сторона',
                roleOfSecondSide: 'Роль 2ой Стороны',
                thirdSide: '3я Сторона',
                roleOfThirdSide: 'Роль 3ей Стороны',
                score: 'Счет',
                sum: 'Сумма',
                equivalent: 'Эквивалент',
                periodicity: 'Периодичность',
                coefficientOfPrepayment: 'Коэффицент Аванса (%)',
                paymentOfPrepayment: 'Оплата Аванса',
                dateOfPrepayment: 'Дата оплаты',
                numberOfPrepaymentOrder: 'Номер Платежного поручения',
                finalPayment: 'Конечная оплата',
                dateOfFinalPayment: 'Дата конечный оплаты',
                numberOfFinalPaymentOrder: 'Номер конечного Платежного поручения',
                dateOfInvoice: 'Дата Счет-Фактуры',
                numberOfInvoice: 'Номер Счет-Фактуры',
                dateOfActCompletionAcceptanceTransfer: 'Дата акта выполненных работ',
                // dateOfActCompletionAcceptanceTransfer: 'Дата акта выполненных работ (приемапередачи)',
                state: 'Состояние',
                statusOfSecondSide: 'Статус 2-ой стороны',
                name: 'Наименование',
                tin: 'ИНН',
                address: 'Адрес',
                bank: 'Банк',
                mfo: 'МФО',
                rc: 'Р/С',
                oked: 'ОКЭД',
                phoneNumber: 'Телефон',
                web: 'Web',
                email: 'E-mail',
                additionalContract: 'Доп Соглашение',
            },
            adminstrativeAdditionalContracts: {
                uniqueCode: 'Уникальный код',
                attachedEmployee: 'Прикрепленный сотрудник',
                dateOfContract: 'Дата договора',
                numberOfContract: 'Номер договора',
                subjectOfContract: 'Предмет договора',
                dateOfAdditionalContract: 'Дата договора доп. согл.',
                numberOfAdditionalContract: 'Номер договора доп. согл',
                subjectOfAdditionalContract: 'Предмет договора доп. согл',
            },
            application: {
                package: 'Пакет',
                version: 'Версия',
                date: 'Число'
            },
            supportProRepair: {
                applicationNumber: 'Номер заявки',
                createDate: 'Дата запроса на починку',
                breakdownReason: 'Причина поломки',
                ctoEmployee: 'Сотрудник ЦТО'
            }
        },
        pagination: {
            rowsPerPage: 'Строк на странице',
            of: 'из'
        },
        notifications: {
            noData: 'Таблица пуста'
        }
    },
    appBar: {
        logOut: 'Выйти из аккаунта'
    }
}