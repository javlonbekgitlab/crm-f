export const wordsUz = {
    forms: {
        validations: {
            onlyNumbers: "faqat raqamlar",
            lengthTinPinfl: "uzunlik 9 yoki 14 ga teng"
        },
        buttonTitles: {
            add: "Qo'shish",
            cancel: "Bekor qilish",
            edit: "O'zgartirish",
            back: "orqaga",
            next: "Keyingi",
            filter: 'Filtr',
            delete: "O'chirish",
            download: "Yuklab olish",
            search: 'Qidirmoq',
            contacts: "Kontaktlar",
            calendar: "Taqvim",
            status: 'Holat',
            closeApplication: "Ilovani yopish",
            openApplication: 'BAYORAT',
            addCashier: "Kassir qo'shish",
            sentToNits: "Ilova yuborish",
            checkTinPinfl: "Tasdiqlash",
            additionalConract: "Qo'shish. Shartnoma"
            // next: "Tiz",
            // prev: "Oldingi",
            // today: 'Bugun',
            // month: "Oy",
            // week: 'Bir hafta',
            // day: "Kun",
            // agenda: 'Kun tartibi'
        },
        partner: {
            partnerName: "sherik nomi",
            address: "manzil",
            firstMiddleLastName: "Menejerning to'liq ismi",
            enterSystem: "tizimga kirish",
            addPartner: 'addPartner',
            editPartner: 'editPartner',
        },
        merchant: {
            opName: "sherik nomi",
            userMobile: "login ID",
            nickName: "boshliqning ismi",
            password: "parol",
            addMerchant: 'addMerchant',
            editMerchant: "tahrirlash Merchant",
        },
        store: {
            shopName: "do'kon nomi",
            cityAddress: "manzil",
            addStore: 'addStore',
            editStore: 'editStore',
        },
        hr: {
            title: "Xodimlar haqida ma'lumot",
            passportData: "Passport ma'lumotlari",
            educationData: "Ta'lim ma'lumotlari",
            taxData: "Soliq ma'lumotlari",
            corporativeData: "Korporativ ma'lumotlari",
            fullName: "To'liq ismi sharifi",
            post: "Pozitsiya",
            department: "Bo'lim",
            phoneNumber: 'Telefon raqami',
            roles: "Rollar",
            structure: "Tuzilishi",
            email: "Elektron pochta",
            password: "Parol",
            surName: 'Familiya',
            name: 'Ism',
            middleName: 'Otasining ism',
            series: 'Seriya',
            passportNumber: 'Passport raqami',
            givenPlace: 'Berilgan joyi',
            givenDate: 'Berilgan sanasi',
            birthDate: "Tug'ilgan sana",
            birthPlace: "Tug'ilgan joyi",
            inhabitationPlace: 'Yashash joyi',
            education: "Ta'lim",
            seriesOfDiploma: 'Diplom seriyasi',
            levelOfEducation: "Ta'lim dargohi",
            numberOfDiploma: 'Diplom seriyasi va raqami',
            cardNumber: 'Plastik karta raqami',
            corpTelNumber: 'Korporativ telefon raqami',
            personalTelNumber: 'Shaxsiy telefon raqami',
            social: 'Ijtimoiy tarmoq',
            telegramId: 'Telegram ID',
            corpEmail: 'Korporativ pochta qutisi',
            // gmail: 'Google pochta qutisi',
            gmail: 'gmail',
            zoomId: 'Zoom ID',
            weChatId: 'WeChat ID'            
        },
        signIn: {
            title: "Tizimga kirish",
            login: 'Kirish',
            password: "Parol",
            toReg: "Ro'yxatdan o'tish",
            showLogin: 'Kirish:'
        },
        signUp: {
            title: "Ro'yxatdan o'tish",
            login: 'Kirish',
            password: "Parol",
            name: "Ism",
            surName: 'Familiya',
            userName: "foydalanuvchi nomi",
            phoneNumber: 'Telefon raqami',
            address: "Manzil",
            adminSystemLogin: 'adminSystemLogin',
            confirmPassword: 'Parolni tasdiqlang',
            toLog: "Tizimga kirish"
        },
        storeOrder: {
            name: 'Kopmaniya nomi',
            regions: "mintaqa",
            city: "Tuman / shahar",
            address: "Manzil",
            groupActivity: "Faoliyatlar guruhi",
            typeActivity: "Faoliyat turi",
            numberCadastre: "Kadastr raqami / Ijara raqami",
            docCadastre: "Inventarizatsiya / ijara hujjati",
            latitude: "kenglik",
            longitude: "uzunlik",
            cashier: 'KASSIR'
        },
        terminal: {
            application: "Bayonot"
        },
        order: {
            tin: 'KARVONSAROY',
            pinfl: 'PINFL',
            tinOrPinfl: 'TIN yoki PINFL kiriting',
            newStationSelect: "Chizish joyini tanlash",
            companyName: "Nomi",
            region: "mintaqa",
            city: "tuman / shahar",
            companyAddress: "sub'ektning yuridik manzili",
            contractNumber: 'Shartnoma raqami',
            contractDate: "shartnoma sanasi",
            deviceModel: "Model",
            serializeNumber: 'Ishlab chiqarish raqami',
            fiskNumber: "Fiskal modul",
            brand: "Tovar",
            serviceCenter: "Xizmat ko'rsatish markazi",
            employee: "Xodimlar",
            firstMiddleLastName: "TO'LIQ ISMI SHARIF",
            telephone: "Telefon",
            login: 'Kirish',
            bidType: "Ilova turi",
            dateAgreement: "Shartnoma sanasi",
            connection: 'Ulanmoq',
            contractFile: "Shartnoma fayli",
            buy: "Sotib olish",
            bank: "Bank",
            other: "Boshqa",
            titles: {
                inn: 'karvonsaroy',
                device: "qurilmalar",
                personalData: "Shaxsiy ma'lumotlar",
                service: "xizmatlar",
                agreement: 'fayllar',
                formCentTechService: "tto shakli",
                documentation: "nomenklatura",
                copyPassport: "direktorning pasporti nusxasi",
                registraionDoc: "ro'yxatga olish guvohnomasi",
                others: "boshqa",
                identification: "Identifikatsiya"
            }
        },
        supportPro: {
            type: "Tiri",
            bank: "Bank",
            deviceNumber: "Mashina raqami",
            model: "Model",
            commercialNumber: "Tijorat nomi",
            idMerchant: "Sotuvchi identifikatori",
            idTerminal: "Terminal identifikatori",
            transMenu: "Trans menyu",
            lockTerminal: "Terminalni bloklash",
            unlockTerminal: "Terminalni blokdan chiqarish"
        },
        supportProRepair: {
            ctoWorker: "Markaziy xizmat ko‘rsatish markazi xodimi",
            dateCreateApplication: "So'rov sanasi",
            breakdownReason: "Buzilish sababi",
            applicationFile: "Ta'mirlash so'rovi shakli",
            downloadApplication: 'FORMASINI YUKLAB OLISH',
            comment: "Izoh"
        },
        property: {
            titles: {
                immovables: 'KO `CHMAS MULK',
                agreement: 'SHARTNOMA'
            },
            nameImmovables: "Mulk nomi",
            typeImmovables: "Mulk turi",
            dateContractOfSale: "Sotib olish shartnomasi sanasi",
            contractOfSale: "Sotish shartnomasi raqami"
        },
        branch: {
            titles: {
                agreement: 'SHARTNOMA',
                filial: 'FILIAL',
                location: 'MANZIL',
                others: 'BOSHQA'
            },
            branchName: "Filial nomi",
            branchType: "Filial turi",
            republic: "Respublika",
            region: "mintaqa",
            district: "Tuman",
            address: "Manzil",
            cadastre: "Kadastr raqami",
            agreement: "Shartnoma raqami",
            agreementDate: "Shartnoma sanasi",
            agreementSum: "Kartnoma summasi",
            agreementTerm: "Shartnoma muddati",
            area: "Umumiy maydon",
            roomNumber: "Xonalar soni",
            employeeNumber: "Xodimlar soni",
            assets: "Aktivlar"
        },
        office: {
            titles: {
               office: 'IDORA'
            },
            officeName: "Ofis nomi",
            officeType: "Ofis turi"
        },
        storage: {
            titles: {
                storage: "ombor",
                document: 'HUJJAT'
            },
            storageLocation: "Omborning joylashuvi",
            storageName: "Ombor nomi",
            storageType: "Ombor turi",
            date: "Yaratilgan sana",
            orderNumber: 'Buyurtma raqami',
            orderDate: "Buyurtma sanasi",
            responsibleEmployee: "Ma'sul xodim"
        },
        retrieve: {
            titles: {
                spec: 'SPESİFİKALAR',
                device: 'QURILMA',
                others: 'BOSHQA',
                agreement: 'SHARTNOMA'
            },
            incomingType: "Kerish turi",
            replacementPartType: "Ehtiyot qismlar turi",
            category: "Kategoriya",
            model: "Model",
            year: "Chorilgan yili",
            supplyNumber: "To'plam raqami",
            supplyDate: "Bayram sanasi",
            amount: "Miqdor",
            agreement: "Shartnoma raqami",
            agreementDate: "Shartnoma sanasi",
            act: "Aksiya raqami",
            actDate: "Harakat sanasi",
            responsibleEmployee: "Ma'sul xodim",
            status: 'Holat',
            file_name: "Fayl",
            brand: "Tovar",
            deviceType: "Qurilma turi"
        },
        technics: {
            titles: {
                spec: 'XUSUSIYATLAR'
            },
            name: "Ism",
            type: "Turi",
            brand: "Tovar",
            price: "Narx",
            specs: "Xususiyatlar",
            cpu: 'MARKAZIY PROTSESSOR',
            ram: 'RAM',
            os: "OS",
            memory: "Xotira",
            title: "Nomlanishi",
            netAddress: "Tarmoq manzili",
            inventoryNumber: "Inventar raqami",
            inventoryLast: "Oxirgi inventarizatsiya",
            state: "davlat",
            serialNumber: 'Ishlab chiqarish raqami'
        },
        interior: {
            titles: {
                object: "Ob'ekt"
            }
        },
        others: {
            mark: "Tovar",
            year: "ishlab chiqarish yili",
            techPassNumber: "Texnik pasport raqami",
            registrationNumber: "Ro'yxatga olish raqami",
            dateContractOfSale: "Sotib olish shartnomasi sanasi",
            contractOfSale: "Sotish shartnomasi raqami",
            agreementSum: "Kartnoma summasi"
        }
    },
    orderPage: {
        title: "Tadbirkor ma'lumotlari"
    },
    storePage: {
        title: "Do'kon"
    },
    cashDrawerPage: {
        title: "Asbob"
    },
    supportProRepair: {
        title: "Ta'mirlash uchun ro'yxatdan o'tish"
    },
    applicationPage: {
        title: "Bayonotlar ro'yxati"
    },
    paymentsPage: {
        title: "To'lov"
    },
    adminstrativeContractsPage: {
        title: "Ma'muriy shartnomalar"
    },
    additionalAdminstrativeContractsPage: {
        title: "Ma'muriy shartnomalar bo'yicha qo'shimcha So'g'"
    },
    internationalContractsPage: {
        title: "Xalqaro shartnomalar"
    },
    additionalInternationalContractsPage: {
        title: "Xalqaro Shartnomalar So'g'ga qo'shimcha"
    },
    otherContractsPage: {
        title: "Shartnomaning qolgan qismi"
    },
    subscriberControlSystemPage: {
        title: "Abonentni boshqarish tizimi"
    },
    actsOfreceivingPage: {
        title: "Qabul qilish va topshirish aktlari"
    },
    procurementPlanPage: {
        title: "Sotib olish rejasi"
    },
    markingPage: {
        title: "belgilash"
    },
    terminalPage: {
        title: "Terminallar"
    },
    supportProPage: {
        title: 'Pro',
        buttonTitles: {
            app: 'ILOVALAR',
            uzcardPos: 'UZCARD POS',
            uzcardPosSa: 'UZCARD POS SA',
            tinda: "TINDA",
            tindaFm: "TINDA FM",
            connectPax: "PAX BILAN ulaning",
            pushMessage: "Push xabarlar",
            sync: "SINXRONIZASYON"
        },
        confirmation: {
            title: 'ILOVALAR',
            text: "Ushbu ilovani o'rnatmoqchimisiz?",
        }
    },
    supportProApp: {
        title: "Ilova",
        detailPanel: {
            title: "Push params"
        }
    },
    menuPage: {
        moduleTitles: {
            admin: "Ma'muriyat",
            assets: "Resurslar",
            sales: "Sotish",
            accounting: "Buxgalteriya hisobi"
        },
        subModules: {
            objects: "Ob'ektlar",
            assembly: "Asambleya",
            items: "Elementlar",
            hr: 'HR',
            requests: 'Arizalar',
            edm: 'EDO',
            tools: "Tashkilotchi",
            supportLite: "Shartnomani ro'yxatdan o'tkazish",
            supportPro: 'Boshqaruv',
            supportRepair: "Ta'mirlash uchun ro'yxatdan o'tish",
            eBank: "Elektron bank",
            billing: "Hisob-kitob",
            contracts: "Shartnomalar"
        },
        subSubModule: {
            retrieve: "Yig'ilgan",
            release: "Amalga kiritilgan",
            defects: "Buzuq",
            equipment: "Uskunalar",
            interior: "Ichki",
            others: "Boshqa",
            property: "Xususiyatlar",
            branch: 'Filiallar',
            storage: 'Omborlar',
            hr: "Xodimlarni boshqarish",
            contacts: "Hamkorlarning aloqalari",
            applications: "Bayonotlar ro'yxati",
            merchants: "Tadbirkor ma'lumotlari",
            calendar: "Taqvim",
            payments: "To'lovlar",
            interactingWithTerminal: "Terminal bilan o'zaro aloqa qilish",
            repairRequestList: "So'rovlar ro'yxati",
            controlEdm: "Hujjatlar aylanishini nazorat qilish",
            subscriberControlSystem: "Abonentni boshqarish tizimi",
            contractsList: "Shartnomalar ro'yxati",
            adminstrativeContracts: "Ma'muriy shartnomalar",
            internationalContracts: "Xalqaro shartnomalar",
            otherContracts: "Shartnomaning qolgan qismi"
        },
        subSubSubModules: {
            technics: "Texnika",
            fiscalModules: "Fiskal modullar",
            devices: "Qurilmalar",
            acceptanceCertificate: 'Qabul qilish sertifikati',
            purchasePlan: 'Xarid rejasi',
            marking: 'Belgilash'
        }
    },
    retrieve: {
        title: 'Chiqildi'
    },
    property: {
        title: 'Ko `chmas mulk'
    },
    release: {
        title: "Amalga kiritilgan"
    },
    defects: {
        title: "Buzuq"
    },
    technics: {
        title: "texnika"
    },
    office: {
        title: 'Idora'
    },
    storage: {
        title: "ombor"
    },
    branch: {
        title: "filial"
    },
    fiscalModules: {
        title: "Fiskal modullar"
    },
    devices: {
        title: "Qurilmalar"
    },
    interior: {
        title: "Ichki"
    },
    others: {
        title: "Boshqa"
    },
    contacts: {
        title: "Kontaktlar",
        detailPanel: {
            title: "Manzil: Ayasofya yodgorligi"
        }
    },
    tools: {
        title: 'Boshqaruv'
    },
    hrPage: {
        title: 'Hr',
        role: {
            title: "rollar"
        }
    },
    calendar: {
        title: "Taqvim",
        daysInWeek: ['Du', 'Se', 'Chor', 'Pay', 'Ju', 'Shan', 'Yak'],
        months: [
            {
                title: "yanvar"
            },
            {
                title: 'Fevral'
            },
            {
                title: "mart"
            },
            {
                title: "aprel"
            },
            {
                title: "may"
            },
            {
                title: 'iyun'
            },
            {
                title: 'iyul'
            },
            {
                title: "avgust"
            },
            {
                title: 'sentyabr'
            },
            {
                title: "oktyabr"
            },
            {
                title: "noyabr"
            },
            {
                title: 'dekabr'
            },
        ],
        action: {
            title: 'VOQEA',
            start: "Boshlash",
            end: 'Oxiri',
            typeOfActions: [
                {
                    title: "Mijozlar bilan uchrashuv"
                }
            ]
        }
    },
    notifications: {
        success: {
            deleted: "O'chirildi",
            added: "Qo'shilgan"
        },
        fail: {
            deleted: "O'chirib bo'lmadi",
            added: "Qo'shib bo'lmadi",
            noConnection: "Internet yo'q"
        },
        helperText: {
            store: "Do'kon:",
            merchant: "Savdogar: ",
            device: 'Kassa apparati ',
            tariff: "Tarif bo'yicha to'lov:",
            balance: 'Balans:',
            notFound: "Topa olmadi",
            statusDescription: 'Rad etish sababi:'
        }
    },
    table: {
        status: {
            all: "Hammasi",
            new: "Yangi",
            proc: "Ko'rildi",
            accept: "Qabul qilingan",
            rejected: "Bekor qilingan"
        },
        statusForRowTable: {
            new: "Yangi",
            proc: "Ko'rildi",
            accept: "Qabul qilingan",
            rejected: "Rad etilgan",
            noFile: "Hech qanday kelishuv yo'q",
            notSent: "Hali yuborilmagan"
        },
        columnsName: {
            merchant: {
                name: 'Tadbirkor',
                phone: "Telefon",
                tin: 'KARVONSAROY',
                tinOrPinfl: "INN / PINFL",
                passportFile: "Pasport",
                date: "Sana"
            },
            shop: {
                shopName: "Do'kon nomi",
                address: "Do'kon manzili",
                region: "Viloyat",
                district: "Tuman",
                activityGroup: "Faoliyat guruhi",
                activityType: "Faoliyat turi",
                cadastreFile: "Kadastr hujjatlari / ijara",
                nomenclatureFile: "nomenklatura"
            },
            cashDrawer: {
                fiskalMod: "Fiskal modul",
                machineNo: "Terminal seriya raqami",
                modelName: "Model",
                applicationFile: "Bayonotlar",
                contractFile: "Shartnoma fayli",
                applicationStatus: 'Holat'
            },
            partner: {
                phone: "Tel. operator raqami",
                name: "Operator",
                createdAt: "Yaratilgan sana"
            },
            payments: {
                accountNumber: 'Hisob raqami',
                mfo: 'MFO',
                bankAccount: 'Hisob raqami',
                name: "Ism",
                quantity: "Miqdor",
                description: "Ta'rif",
                payDay: "To'lov sanasi",
                payTime: "To'lov vaqti",
                employee: "Xodim",
                balance: "Balans",
                clientName: "Qarshi tomonning nomi"
            },
            terminals: {
                modelName: "Terminal modeli",
                id: 'ID',
                firmware: "Proshivka versiyasi",
                resellerName: "resellerName"
            },
            adminstrativeContracts: {
                id: 'ID',
                uniqueCode: "Noyob kod",
                attachedEmployee: "Biriktirilgan xodim",
                type: "Tiri",
                kindOfContract: "Ko'rinish",
                dateOfContract: "Shartnoma sanasi",
                numberOfContract: "Shartnoma raqami",
                subjectOfContract: "Shartnomaning predmeti",
                term: "Muddati",
                quantityOfSides: "Tomonlar soni",
                secondSide: "Ikkinchi tomon",
                roleOfSecondSide: "Ikkinchi tomonning roli",
                thirdSide: "3-shaxs",
                roleOfThirdSide: "Uchinchi tomonning roli",
                score: "Tekshirish",
                sum: "sum",
                equivalent: "Ekvivalent",
                periodicity: "Davriylik",
                coefficientOfPrepayment: 'Oldin nisbati (%)',
                paymentOfPrepayment: "Avans to'lash",
                dateOfPrepayment: "To'lov sanasi",
                numberOfPrepaymentOrder: "To'lov buyurtmasi raqami",
                finalPayment: "Yakuniy to'lov",
                dateOfFinalPayment: "Yakuniy to'lov sanasi",
                numberOfFinalPaymentOrder: "Yakuniy to'lov topshirig'ining raqami",
                dateOfInvoice: "Hisob-faktura sanasi",
                numberOfInvoice: "Hisob-faktura raqami",
                dateOfActCompletionAcceptanceTransfer: 'Tugallash sertifikati sanasi',
                // dateOfActCompletionAcceptanceTransfer: "To'ldirish sertifikati (qabul qilish va topshirish) sanasi",
                state: "davlat",
                statusOfSecondSide: "Ikkinchi tomon maqomi",
                name: "Ism",
                tin: 'KARVONSAROY',
                address: "Manzil",
                bank: "Bank",
                mfo: 'MFO',
                rc: 'P / S',
                oked: 'OKED',
                phoneNumber: "Telefon",
                web: "Veb",
                email: "Elektron pochta",
                additionalContract: "Qo'shimcha kelishuv",
            },
            adminstrativeAdditionalContracts: {
                uniqueCode: "Noyob kod",
                attachedEmployee: "Biriktirilgan xodim",
                dateOfContract: "Shartnoma sanasi",
                numberOfContract: "Shartnoma raqami",
                subjectOfContract: "Shartnomaning predmeti",
                dateOfAdditionalContract: "Qo'shimcha shartnoma sanasi",
                numberOfAdditionalContract: "Qo'shimcha shartnoma raqami",
                subjectOfAdditionalContract: "Qo'shimcha shartnoma mavzusi",
            },
            application: {
                package: "To'plam",
                version: 'Versiya',
                date: 'Sana'
            },
            supportProRepair: {
                applicationNumber: 'Ariza raqami',
                createDate: "Ta'mirlashga ariza berilgan sana",
                breakdownReason: 'Ishdan chiqish sababi',
                ctoEmployee: 'XKM xodimi'
            }
        },
        pagination: {
            rowsPerPage: "Har bir sahifaga satr",
            of: 'dan',
        },
        notifications: {
            noData: "Jadval bo'sh",
        }
    },
    appBar: {
        logOut: "Akkauntdan chiqish"
    }
}