export const wordsEn = {
    forms: {
        buttonTitles: {
            add: 'Добавить',
            cancel: 'Отменить',
            edit: 'Изменить',
            back: 'назад',
            next: 'Далее',
            filter: 'Фильтр',
            delete: 'Удалить',
            download: 'Cкачать',
            search: 'Поиск',
            contacts: 'Контакты',
            calendar: 'Календарь',
            status: 'Статус',
            closeApplication: 'Закрыть заявку',
            // trace: 'След',
            // prev: 'Пред',
            // today: 'Сегодня',
            // month: 'Месяц',
            // week: 'Неделя',
            // day: 'День',
            // agenda: 'Повестка дня'
        },
        partner: {
            partnerName: 'имя партнера',
            address: 'адрес',
            firstMiddleLastName: 'ФИО менеджера',
            enterSystem: 'вход в систему',
            addPartner: 'addPartner',
            editPartner: 'editPartner',
        },
        merchant: {
            opName: 'имя партнера',
            userMobile: 'логин ID',
            nickName: 'имя босса',
            password: 'пароль',
            addMerchant: 'addMerchant',
            editMerchant: 'editMerchant',
        },
        store: {
            shopName: 'название магазина',
            cityAddress: 'адрес',
            addStore: 'addStore',
            editStore: 'editStore',
        },
        hr: {
            title: 'ДАННЫЕ О СОТРУДНИКЕ',
            fullName: 'ФИО',
            post: 'Должность',
            phoneNumber: 'Телефонный номер',
            roles: 'Роли',
            structure: 'Структура',
            email: 'Email',
            password: 'Пароль'
            
        },
        signIn: {
            title: 'Вход в систему',
            login: 'Логин',
            password: 'Пароль',
            toReg: 'Регистрация',
            showLogin: 'Логин: '
        },
        signUp: {
            title: 'Регистрация',
            login: 'Логин',
            password: 'Пароль',
            name: 'Имя',
            surName: 'Фамилия',
            userName: 'userName',
            phoneNumber: 'Номер',
            address: 'Адрес',
            adminSystemLogin: 'adminSystemLogin',
            confirmPassword: 'Подтвердите пароль',
            toLog: 'Вход в систему'
        },
        storeOrder: {
            name: 'Название предприятия',
            regions: 'Регион',
            city: 'Район/город',
            address: 'Адрес',
            groupActivity: 'Группа видов деятельности',
            typeActivity: 'Вид деятельности',
            numberCadastre: 'Номер кадастра / Номер аренды',
            docCadastre: 'Документ кадастра / Аренды'
        },
        order: {
            tin: 'ИНН',
            newStationSelect: 'Выбор торговой точки',
            companyName: 'Наименование',
            region: 'регион',
            city: 'район / город',
            companyAddress: 'юридический адрес субъекта',
            contractNumber: 'Контракт №',
            contractDate: 'contractDate',
            deviceModel: 'Модель',
            serializeNumber: 'Серийный номер',
            fiskNumber: 'Фискальный модуль',
            brand: 'Бренд',
            serviceCenter: 'Сервисный центр',
            employee: 'Сотрудники',
            firstMiddleLastName: 'ФИО',
            telephone: 'Телефон',
            login: 'Логин',
            bidType: 'Тип заявления',
            dateAgreement: 'Дата договора',
            connection: 'Подключение',
            buy: 'Покупка',
            titles: {
                inn: 'инн',
                device: 'устройства',
                personalData: 'личные данные',
                service: 'сервисные услуги',
                agreement: 'файл договора',
                formCentTechService: 'форма цто',
                documentation: 'номенклатура',
                copyPassport: 'копия паспорта директора',
                registraionDoc: 'свидетельство о регистрации',
                others: 'прочее',
                identification: 'Идентификация'
            }
        },
        supportPro: {
            type: 'Тип',
            bank: 'Банк',
            deviceNumber: 'Номер аппарата',
            model: 'Модель',
            commercialNumber: 'Коммерческое название',
            idMerchant: 'ID продавца',
            idTerminal: 'ID терминала',
            transMenu: 'Транс меню'
        },
        supportProRepair: {
            ctoWorker: 'Сотрудник ЦТО',
            dateCreateApplication: 'Дата запроса',
            breakdownReason: 'Причина поломки',
            applicationFile: 'Форма запроса на починку',
            downloadApplication: 'СКАЧАТЬ ФОРМУ',
            comment: 'Комментарий'
        },
        property: {
            titles: {
                immovables: 'НЕДВИЖИМОСТЬ',
                agreement: 'ДОГОВОР'
            },
            nameImmovables: 'Название недвижимости',
            typeImmovables: 'Тип недвижимости',
            dateContractOfSale: 'Дата договора купли продажи',
            contractOfSale: 'Номер договора купли продажи'
        },
        branch: {
            titles: {
                agreement: 'ДОГОВОР',
                filial: 'ФИЛИАЛ',
                location: 'МЕСТОПОЛОЖЕНИЕ',
                others: 'ПРОЧЕЕ'
            },
            branchName: 'Название филиала',  
            branchType: 'Тип филиала',  
            republic: 'Республика',  
            region: 'Регион',  
            district: 'Район',  
            address: 'Адрес',  
            cadastre: 'Номер кадастра',  
            agreement: 'Номер договора',  
            agreementDate: 'Дата договора',  
            agreementSum: 'Сумма договора',  
            agreementTerm: 'Срок договора',  
            area: 'Общая площадь',  
            roomNumber: 'Количество комнат',  
            employeeNumber: 'Количество сотрудников',  
            assets: 'Активы'  
        },
        office: {
            titles: {
               office: 'ОФИС' 
            },
            officeName: 'Название офиса',
            officeType: 'Тип офиса'
        },
        storage: {
            titles: {
                storage: 'СКЛАД',
                document: 'ДОКУМЕНТ'
            },
            storageLocation: 'Расположение склада',
            storageName: 'Наименование склада',
            storageType: 'Тип склада',
            date: 'Дата создания',
            orderNumber: 'Номер приказа',
            orderDate: 'Дата приказа',
            responsibleEmployee: 'Ответственный сотрудник'
        },
        retrieve: {
            titles: {
                spec: 'СПЕЦИФИКАЦИЯ',
                device: 'УСТРОЙСТВО',
                others: 'ПРОЧЕЕ',
                agreement: 'ДОГОВОР'
            },
            incomingType: 'Тип прихода',
            replacementPartType: 'Тип запчасти',
            category: 'Категория',
            model: 'Модель',
            year: 'Год выпуска',
            supplyNumber: 'Номер партии',
            supplyDate: 'Дата партии',
            amount: 'Количество',
            agreement: 'Номер договора',
            agreementDate: 'Дата договора',
            act: 'Номер акта',
            actDate: 'Дата акта',
            responsibleEmployee: 'Ответственный сотрудник',
            status: 'Статус',
            file_name: 'Файл'
        },
        technics: {
            titles: {
                spec: 'ХАРАКТЕРИСТИКИ'
            },
            name: 'Имя',
            type: 'Тип',
            brand: 'Бренд',
            price: 'Стоимость',
            specs: 'Характеристики',
            cpu: 'ЦП',
            ram: 'ОЗУ',
            os: 'ОС',
            memory: 'Память',
            title: 'Наименование',
            netAddress: 'Сетевой адрес',
            inventoryNumber: 'Инвентаризационный номер',
            inventoryLast: 'Последняя инвентаризация',
            state: 'Состояние',
            serialNumber: 'Серийный номер'
        },
        interior: {
            titles: {
                object: 'Объект'
            }
        },
        others: {
            mark: 'Марка',
            year: 'Год производства',
            techPassNumber: 'Номер технического паспорта',
            registrationNumber: 'Регистрационный номер',
        }
    },
    orderPage: {
        header: {
            profile: 'профиль',
            useLogOut: 'выйти'
        },
        title: 'заказы'
    },
    storePage: {
        title: 'Store'
    },
    cashDrawerPage: {
        title: 'Аппарат'
    },
    supportProRepair: {
        title: 'Регистрация на ремонт'
    },
    supportProPage: {
        title: 'Pro',
        buttonTitles: {
            app: 'ПРИЛОЖЕНИЯ',
            uzcardPos: 'UZCARD POS',
            uzcardPosSa: 'UZCARD POS SA',
            tinda: 'TINDA',
            tindaFm: 'TINDA FM',
            connectPax: 'СОЕДИНИТЬ С PAX',
            pushMessage: 'PUSH СООБЩЕНИЯ',
            sync: 'СИНХРОНИЗАЦИЯ'
        },
        confirmation: {
            title: 'ПРИЛОЖЕНИЯ',
            text: 'Вы действительно хотите установить данное приложение?',
        }
    },
    supportProApp: {
        title: 'App',
        detailPanel: {
            title: 'push params'
        }
    },
    menuPage: {
        moduleTitles: {
            admin: 'Adminstration',
            assets: 'Assets',
            sales: 'Sales',
            accounting: 'Accounting'
        },
        subModules: {
            objects: 'objects',
            assembly: 'assembly',
            items: 'items',
            hr: 'hr',
            edm: 'edm',
            tools: 'tools',
            supportLite: 'supportLite',
            supportPro: 'supportPro',
            supportRepair: 'supportRepair',
            eBank: 'eBank',
            billing: 'billing',
            contracts: 'contracts'
        },
        subSubModule: {
            retrieve: 'retrieve',
            release: 'release',
            defects: 'defects',
            equipment: 'equipment',
            interior: 'interior',
            others: 'others',
            property: 'property',
            hr: 'employee management',
            contacts: 'contacts',
            applications: 'Заявление',
            merchants: 'Предприятие',
            calendar: 'calendar'
        },
        subSubSubModules: {
            technics: 'technics',
            fiscalModules: 'fiscalModules',
            devices: 'devices',
        }
    },
    retrieve: {
        title: 'Retrieve'
    },
    property: {
        title: 'Недвижимость'
    },
    release: {
        title: 'release'
    },
    defects: {
        title: 'defects'
    },
    technics: {
        title: 'technics'
    },
    office: {
        title: 'office'
    },
    storage: {
        title: 'storage'
    },
    branch: {
        title: 'branch'
    },
    fiscalModules: {
        title: 'fiscalModules'
    },
    devices: {
        title: 'devices'
    },
    interior: {
        title: 'interior'
    },
    others: {
        title: 'others'
    },
    contacts: {
        title: 'Контакты',
        detailPanel: {
            title: 'Адрес:  Мемориал Святой Софии'
        }
    },
    tools: {
        title: 'Tools'
    },
    hrPage: {
        title: 'Hr',
        role: {
            title: 'Роли'
        }
    },
    calendar: {
        title: 'Календарь',
        daysInWeek: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
        months: [
            {
                title: 'Январь'
            },
            {
                title: 'Февраль'
            },
            {
                title: 'Март'
            },
            {
                title: 'Апрель'
            },
            {
                title: 'Май'
            },
            {
                title: 'Июнь'
            },
            {
                title: 'Июль'
            },
            {
                title: 'Август'
            },
            {
                title: 'Сентябрь'
            },
            {
                title: 'Октябрь'
            },
            {
                title: 'Ноябрь'
            },
            {
                title: 'Декабрь'
            },
        ],
        action: {
            title: 'СОБЫТИЕ',
            start: 'Начало',
            end: 'Конец',
            typeOfActions: [
                {
                    title: 'Встреча с клиентами'
                }
            ]
        }
    },
    notifications: {
        success: {
            deleted: 'Удалено',
            added: 'Добавлено'
        },
        fail: {
            deleted: 'Не удалось удалить',
            added: 'Не удалось добавить',
            noConnection: 'Нет интернета'
        },
        helperText: {
            store: 'Магазин: ',
            merchant: 'Merchant: ',
            device: 'Кассовый аппарат ',
            tariff: 'Оплата по тарифу: ',
            balance: 'Баланс:',
            notFound: 'notFound'
        }
    },
    table: {
        status: {
            all: 'all',
            new: 'new',
            proc: 'proc',
            accept: 'accept',
            rejected: 'rejected' 
        }
    }
}