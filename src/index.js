import React from 'react'
import { Provider } from 'mobx-react'
import ReactDOM from 'react-dom'
import { App } from './App'
import reportWebVitals from './reportWebVitals'
import { techAndEquipStore, deviceStore, othersStore, fiskalModStore,interiorStore, contactsList, merchantsList, propertyList, rolesList, storeList, supportProAppList, usersList, cashDrawerList, supportProRepairList, applicationList, terminalStore, subscriberControlSystemStore, adminstrativeContractsStore, retrieveStore, releaseStore, defectsStore, additionalAdminstrativeContractsStore, additionalInternationalContractsStore, internationalContractsStore, acceptanceCertificateStore } from './store'
import { lang } from './languages/lang'
import { accessModule } from './store/localStorage'
import { snackbar } from './actions'
import { globalVariables } from './globalVariables'
import { test } from './pages/temp'
// "browserslist": {
//   "production": [
//     ">0.2%",
//     "not dead",
//     "not op_mini all"
//   ],
//   "development": [
//     "last 1 chrome version",
//     "last 1 firefox version",
//     "last 1 safari version"
//   ]
// }
ReactDOM.render(
  <React.StrictMode>
    <Provider 
      merchantsList = {merchantsList}
      storeList = {storeList}
      lang = {lang}
      propertyList = {propertyList}
      snackbar = {snackbar}
      contactsList = {contactsList}
      usersList = {usersList}
      supportProAppList = {supportProAppList}
      rolesList = {rolesList}
      cashDrawerList = {cashDrawerList}
      globalVariables = {globalVariables}
      accessModule = {accessModule}
      supportProRepairList = {supportProRepairList}
      applicationList = {applicationList}
      terminalStore = {terminalStore}
      test = {test}
      subscriberControlSystemStore = {subscriberControlSystemStore}
      adminstrativeContractsStore = {adminstrativeContractsStore}
      additionalAdminstrativeContractsStore = {additionalAdminstrativeContractsStore}
      internationalContractsStore = {internationalContractsStore}
      additionalInternationalContractsStore = {additionalInternationalContractsStore}
      retrieveStore = {retrieveStore}
      releaseStore = {releaseStore}
      defectsStore = {defectsStore}
      techAndEquipStore = {techAndEquipStore}
      fiskalModStore = {fiskalModStore}
      deviceStore = {deviceStore}
      interiorStore = {interiorStore}
      othersStore = {othersStore}
      acceptanceCertificateStore = {acceptanceCertificateStore}
    >
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
