import { useEffect } from "react"
import { useHistory } from "react-router-dom"
import { localStorageKeys } from "../../constants"
import { removeItemsFromLocalStorage } from "../../store/localStorage"
import { routes } from "../../store/routes"

export default () => {
    // const history = useHistory()
    // window.history.replaceState(null, 'redirect', '/')
    // history.replace(routes.menu)
    removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
}