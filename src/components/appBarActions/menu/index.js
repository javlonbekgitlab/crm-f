import React from 'react'
import styles from '../index.module.sass'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import useLogOut from '../helperFunctions'
import { useHistory } from 'react-router'
import { removeItemsFromLocalStorage } from '../../../store/localStorage'
import { localStorageKeys } from '../../../constants'
import { routes } from '../../../store/routes'
import { inject } from 'mobx-react'

export const MenuAppBarActions = inject('accessModule')(({accessModule, onClickExit, hide }) => {
    
    const history = useHistory()
    
    const handleLogOut = () => {
        history.replace(routes.menu)
        removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
        // accessModule.clearAccessModule()
        onClickExit()
    }
   
    return (
        <div className = {styles.cont}>
            <ExitToAppIcon
                className = {hide ? `${styles.icon} ${styles.hide}` : styles.icon}
                onClick = {handleLogOut}
            />
        </div>
    )
})
