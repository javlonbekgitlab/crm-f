import React from 'react'
import styles from '../index.module.sass'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import { removeItemsFromLocalStorage } from '../../../store/localStorage'
import { routes } from '../../../store/routes'
import { localStorageKeys } from '../../../constants'
import { useHistory } from 'react-router'
import { inject } from 'mobx-react'
// import useLogOut from '../helperFunctions'

export const PropertyAppBarActions = inject('accessModule')(({accessModule}) => {

    const history = useHistory()
    
    const handleLogOut = () => {
        history.replace(routes.menu)
        removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles])
        // accessModule.clearAccessModule()
    }

    return (
        <div className = {styles.cont}>
            <ExitToAppIcon
                className = {styles.icon}
                onClick = {handleLogOut}
            />
        </div>
    )
})
