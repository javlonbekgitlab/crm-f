import React, { useEffect, useState } from 'react'
import styles from '../index.module.sass'
import PersonIcon from '@material-ui/icons/Person'
import PhoneIcon from '@material-ui/icons/Phone'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import EditIcon from '@material-ui/icons/Edit'
import { inject } from 'mobx-react'
import { CustomMenu } from '../../menu'
import useLogOut from '../helperFunctions'
import { useHistory } from 'react-router'
import { routes } from '../../../store/routes'
import { readLocalStorage, removeItemsFromLocalStorage } from '../../../store/localStorage'
import { localStorageKeys } from '../../../constants'
import { changeLanguage } from '../../../languages/lang'
import { Button } from '../../button'

export const UniversalAppBarActions = inject('lang', 'accessModule')(({accessModule, lang, onClickExit, hide}) => {

    const [userName, setUserName] = useState('')

    const history = useHistory()
    
    const handleLogOut = () => {
        history.replace(routes.menu)
        removeItemsFromLocalStorage([localStorageKeys.token, localStorageKeys.roles, localStorageKeys.username])
        // accessModule.clearAccessModule()
        setUserName('')
        onClickExit && onClickExit()
    }

    useEffect(() => {
        setUserName(readLocalStorage(localStorageKeys.username))
    }, [hide])

    return (
        <div className = {styles.cont}>
            <div className = {styles.absoluteCont}>
                <div className = {styles.buttonCont}>
                    {lang.activeLang === 'ru' && <Button onClick = {() => changeLanguage('uz')}>{'uz'}</Button>}
                    {lang.activeLang === 'uz' && <Button onClick = {() => changeLanguage('ru')}>{'ru'}</Button>}
                </div>
                {userName && <CustomMenu
                    button = {<AccountCircleIcon className = {styles.icon}/>}
                >
                    <div className = {styles.item}>
                        <div 
                            className = {styles.text}
                            // onClick = {handleLogOut}    
                        >
                            {userName}
                        </div>
                    </div>
                    <div className = {styles.item}>
                        <div 
                            className = {styles.text}
                            onClick = {handleLogOut}    
                        >
                            {lang.appBar.logOut}
                        </div>
                    </div>
                </CustomMenu>}
            </div>
        </div>
    )
})