/* eslint-disable no-use-before-define */
import React, { memo } from 'react'
import Autocomplete from '@material-ui/lab/Autocomplete'
import { makeStyles } from '@material-ui/core/styles'
import Checkbox from '@material-ui/core/Checkbox'
import CircularProgress from '@material-ui/core/CircularProgress'
import TextField from '@material-ui/core/TextField'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank'
import CheckBoxIcon from '@material-ui/icons/CheckBox'
import Chip from '@material-ui/core/Chip'
import PropTypes from 'prop-types'
import { roles } from '../../../constants'
import { Input } from '../../input'


const icon = <CheckBoxOutlineBlankIcon fontSize="small" />
const checkedIcon = <CheckBoxIcon fontSize="small" />

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(3),
    },
  },
}))

export const MultiSelect = memo(({options = [], onChange, value, label, error, name, loading}) => {

    const classes = useStyles()
    console.log(options)

    const handleChange = (e, value) => {
        const event = {
            target: {
                value,
                name
            }
        }
        onChange && onChange(event)
    }

    return (
        <div className={classes.root}>
            <Autocomplete
                multiple
                id="tags-standard"
                options={options}
                getOptionLabel={(option) => option.name}
                disableCloseOnSelect
                value = {value && value}
                onChange = {handleChange}
                freeSolo
                renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                        <Chip 
                            color = 'primary' 
                            label={option.name} 
                            {...getTagProps({ index })} 
                        />
                    ))
                }
                renderOption={(option, { selected }) => (
                    <React.Fragment>
                        <Checkbox
                            icon={icon}
                            checkedIcon={checkedIcon}
                            style={{ marginRight: 8 }}
                            checked={selected}
                            color = 'primary'
                        />
                        {option.name}
                    </React.Fragment>
                )}    
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label={label}
                        margin = 'dense' 
                        size = 'small'
                        fullWidth
                        variant='standard'
                        helperText = {error}
                        error = {error ? true : false}
                        // inputProps = {{
                        //     step: 100
                        // }}
                        InputLabelProps={{
                            style: {
                            textOverflow: 'ellipsis',
                            whiteSpace: 'nowrap',
                            overflow: 'hidden',
                            width: '100%',
                            fontSize: '0.9em',
                            color: '#696969',
                            } }}
                        // variant="outlined"
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                                </React.Fragment>
                            ),
                        }}
                    /> 
                // />
                // <Input
                //     {...params}
                //     standart
                //     label={label}
                //     error = {error}
                // >
                //     {/* <div>
                //         <h1>asdasdasd</h1>
                //     </div> */}
                //     <>
                //     <CircularProgress color="inherit" size={20} />
                //     {params.InputProps.endAdornment}
                //     </>
                // </Input>
                )}
            />
        </div>
    )
})

MultiSelect.propTypes = {
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.object,
    label: PropTypes.string
}

