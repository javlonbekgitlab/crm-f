import React, { memo, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import PropTypes from 'prop-types'
import { FormHelperText } from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress'


const useStyles = makeStyles((theme) => ({
    cont: {
        width: '100%',
        marginTop: '0.479em',
        // maxWidth: 400,
        position: 'relative',
        top: '-7px'
    },
    formControl: {
        minWidth: 260,
        width: '100%',
    },
    label: {
        color: '#696969',
        fontSize: '0.9em',
        width: 'max-content',
        maxWidth: 240,
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        height: '1.2em'
    },
    progress: {
        display: 'flex',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: '0.3em'
    }
}))

export const SelectCustom = memo(({title, value, onChange, data, error, onOpen, ...props}) => {

    const classes = useStyles()

    const [open, setOpen] = useState(false)

    const handleClose = () => setOpen(false)

    const handleOpen = () => {
        onOpen && onOpen()
        setOpen(true)
    }

    console.log('select')

    return (
        <div className = {classes.cont}>
            <FormControl className={classes.formControl} error = {error}>
                <InputLabel className = {classes.label} id="demo-controlled-open-select-label">{title}</InputLabel>
                <Select
                    labelId="demo-controlled-open-select-label"
                    id="demo-controlled-open-select"
                    open={open}
                    onClose={handleClose}
                    onOpen={handleOpen}
                    value={value}
                    defaultValue = ""
                    // fullWidth
                    style = {{marginBottom: -6,
                    }}
                    // autoWidth
                    onChange={onChange}
                    {...props}
                >
                    {data.map(item => 
                        <MenuItem key = {item.id} value = {item.id}>
                            {item.title}
                        </MenuItem>
                    )}
                    {(data.length < 1) && <div className = {classes.progress}>
                        <CircularProgress color="inherit" size={40} />
                    </div>}
                </Select>
                <FormHelperText>{error}</FormHelperText>
            </FormControl>
        </div>
    )
})

Select.propTypes = {
    title: PropTypes.string,
    data: PropTypes.array,
    onChange: PropTypes.func
}
