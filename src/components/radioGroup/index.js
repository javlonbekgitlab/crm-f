import React, { memo } from 'react'
import styles from './index.module.sass'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import PropTypes from 'prop-types'
import { MenuItem } from '@material-ui/core'

export const RadioButtonGroup = memo(({onChange, value, name, error, data, onClose}) => {
    return (
        <FormControl component="fieldset">
            <RadioGroup 
                className = {styles.radioGroup} 
                aria-label="bidType" 
                name = {name}
                value={value} 
                onChange={onChange}
            >
                {data.map((item, index) => {
                    return onClose ? <MenuItem>
                        <FormControlLabel 
                            key = {index}
                            value = {item.id}
                            control = {<Radio size = 'small' color="primary"/>} 
                            label = {<div className = {styles.label}>{item.label}</div>} 
                        /> 
                    </MenuItem>
                    : <FormControlLabel 
                        key = {index}
                        value = {item.id}
                        control = {<Radio size = 'small' color="primary"/>} 
                        label = {<div className = {styles.label}>{item.label}</div>} 
                    /> 
                })}
            </RadioGroup>
            {error && <div className = {styles.error}>{error}</div>}
        </FormControl>
    )
})

RadioButtonGroup.propTypes = {
    name: PropTypes.string,
    value: PropTypes.string,
    onChange: PropTypes.func,
    error: PropTypes.bool,
    data: PropTypes.array
}

