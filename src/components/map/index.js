import React, { memo, useEffect, useState } from 'react'
import GoogleMapReact from 'google-map-react'
import styles from './index.module.sass'
import ReactMapGl from 'react-map-gl'
import { YMaps, Map as YandexMap, Placemark, ZoomControl, TypeSelector, FullscreenControl, SearchControl, GeolocationControl } from 'react-yandex-maps'
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined'
import locationIcon from '../../assets/icons/location.png'
import 'leaflet/dist/leaflet.css'
import { MapContainer, Marker, Popup, TileLayer, useMap, useMapEvents } from 'react-leaflet'
import 'leaflet/dist/leaflet.css';
import 'leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.webpack.css'; // Re-uses images from ~leaflet package
import * as L from 'leaflet';
import 'leaflet-defaulticon-compatibility';

import { GeoSearchControl, OpenStreetMapProvider } from 'leaflet-geosearch';
import { inject, observer } from 'mobx-react'

const Search = (props) => {
    const map = useMap() // access to leaflet map
    const { provider, lang } = props

    useEffect(() => {
        const searchControl = new GeoSearchControl({
            provider,
            marker: {
                // optional: L.Marker    - default L.Icon.Default
                draggable: false,
            },
            searchLabel: lang,
            showMarker: true,
            // resultFormat: ({ result }) => result.label
        })
        map.addControl(searchControl) // this is how you add a control in vanilla leaflet
        return () => map.removeControl(searchControl)
    }, [props])

    return null // don't want anything to show up from this comp
}


export const Map = inject('lang')(observer(({lang, children, getCoords, disabled, error, center = [41.311081,69.240562]}) => {
    // const [viewport, setViewport] = useState({
    //     width: 400,
    //     height: 400,
    //     latitude: 37.7577,
    //     longitude: -122.4376,
    //     zoom: 8
    // })
    return (
        <div className = {error ? `${styles.cont} ${styles.errCont}` : styles.cont}>
            {/* <ReactMapGl
                width = '100vh'
                height = '100%'
                mapboxApiAccessToken = 'pk.eyJ1IjoiamF2bG9uYmVrLSIsImEiOiJja3AzdnZ0d3gwZnI3Mm5sZWw5b2xqcDNhIn0.3wBXxEO_yQK5gTjwUj_Pcw'
                {...viewport}
                onViewportChange={nextViewport => setViewport(nextViewport)}
                // className = {styles.map}
                // style = {{top: 0, right: 0}}
            >
                <AttributionControl compact={true} style={{width: '100%', right: 0, top: 0}} />
            </ReactMapGl> */}
            {/* <YMaps>
                <YandexMap 
                    defaultState={{ center: [41.311081, 69.240562]}}
                    className = {styles.map}
                    width = '100%'
                    height = '100%'
                    
                >
                    <TypeSelector/>
                    <FullscreenControl />
                    <SearchControl />
                    <GeolocationControl />
                    <LocationOnOutlinedIcon color = 'primary'/>
                </YandexMap>
            </YMaps> */}
            {/* <GoogleMapReact
                bootstrapURLKeys={{ key: 'AIzaSyAs8FcePD4bUCdZBjkeCfDRnmvTRlEdNHA' }}
                defaultCenter={{lat: 41.311081,lng: 69.240562}}
                defaultZoom={zoom}
                yesIWantToUseGoogleMapApiInternals = {true}
                onGoogleApiLoaded={({ map, maps }) => console.log(map, maps)}
                {...props}
                options = {{draggableCursor: 'crosshair'}}
            >
                {children}
            </GoogleMapReact> */}
             <MapContainer 
                style ={{width:'100%',height: '100%'}}
                center={center} 
                zoom={13}
                attributionControl
            >
                {!disabled && <Search
                    provider={new OpenStreetMapProvider()}
                    lang = {lang.forms.buttonTitles.search} 
                />}
                <TileLayer
                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                />
                {children}
                {!disabled && <Events getCoords = {getCoords}/>}
            </MapContainer>
        </div>
    )
}))

const Events = ({getCoords}) => {
    const map = useMapEvents({
        moveend() {
            const { lat, lng } = map.getCenter()
            getCoords && getCoords({lat, lng})
        }   
    })
    return null
}