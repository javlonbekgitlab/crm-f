import React from 'react'
import { transliterateObject } from '../../../actions'
import styles from './index.module.sass'

export const ApplicationNits = ({data}) => {

    const date = new Date(Date.now()).toISOString().slice(0, 10)

    const {
        regionName,
        districtName,
        cityAddress,
        compName,
        activityGroupName,
        activityTypeName,
        modelName,
        machineNo,
        fiskalMod,
        nickName,
    } = transliterateObject(data)
    // {
    //     regionName: 'Тошкент вилояти',//bytin
    //     districtName: 'Zangiota tumani',//bytin
    //     cityAddress: 'КАТАРТАЛ, МЧЖ ``BEK BARAKA IXTISOSLASHGAN SAVDO KOMPLEKSI`` 362-D',//bytin
    //     compName: '\"SAIDABONU FASHION\" MChJ',//bytin
    //     tin: '306545471',
    //     activityGroupName: 'savdo faloliyati',
    //     activityTypeName: 'kiyim-kechaklar',
    //     modelName: 'PAX A-930 ',
    //     machineNo: '1170780550',
    //     fiskalMod: 'UZ201125149088',
    //     nickName: 'YAKUBOV ODIL KUTLIMURATOVICH'
    // }
    return (
        <div className={styles.cont}>
            <div className={styles.arca}>{`"ARCA GROUP" MchJ`}</div>
            <div className={styles.texnik}>{`texnik xizmat ko'rsatish markaziga`}</div>
            <div className={styles.ariza}>{`Ariza`}</div>
            <div className={styles.onlayn}>{`(Onlayn – NKM yoki virtual kassani turg'un shaxobcha (ko'chma savdo obekti) joylashgan joy bo'ycha davlat soliq xizmati organlarida ro'yxatdan o'tkazish to'g'risida)`}</div>
            <div className={styles.textCont}>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{`Sizdan ,`.split(' ').map(word =>
                <div className={styles.text}>{word}&nbsp;</div>
            )}
                {`${regionName}, `.split(' ').map(word =>
                    <div className={styles.bold}>&nbsp;{word.toLocaleUpperCase()}&nbsp;</div>
                )}
                {`${districtName}`.split(' ').map(word =>
                    <div className={styles.bold}>&nbsp;{word.toLocaleUpperCase()}&nbsp;</div>
                )}
                {`${cityAddress}`.split(' ').map(word =>
                    <div className={styles.bold}>&nbsp;{word}&nbsp;</div>
                )}
                {`manzilida joylashgan,`.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${compName}`.split(' ').map(word =>
                    <div className={styles.bold}>&nbsp;{word}&nbsp;</div>
                )}
                {(data.tinOrPinfl.length === 9 ? 'stir:': 'pinfl:').split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${data.tinOrPinfl}`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {')ga'}&nbsp;
                {`qarashli, `.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${activityGroupName}`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {`${activityTypeName}`.split(' ').map(word =>
                    <div className={styles.bold}>&nbsp;{word}&nbsp;</div>
                )}
                {`savdo shaxobchasida foydalanish uchun,`.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${modelName}`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {`rusumli, zavod raqami (versiyasi)`.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${machineNo}`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {`bo'lgan onlayn-NKM (virtual kassa)`.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${fiskalMod}`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {`seriya raqamli fiskal modulni,`.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
                {`${districtName}`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {`DSI`.split(' ').map(word =>
                    <div className={styles.bold}>{word}&nbsp;</div>
                )}
                {`davlat soliq inspeksiyasida ro'yxatga qo'yishda amaliy yordam berishingizni so'rayman.`.split(' ').map(word =>
                    <div className={styles.text}>{word}&nbsp;</div>
                )}
            </div>
            <div className={styles.quyidagi}>
                {`Quyidagi hujjatlar ilova qilinadi:`}
            </div>
            <div className={styles.docs}>
                {`1.Turg'un shaxobcha (ko'chma savdo obekti) mavjudligini tasdiqlaydigan hujjatlaring nusxalari (bino, inshootning yoki uning qismining ijara shartnomasi yoki kadastr hujjatlari).`}
            </div>
            <div className={styles.docs}>
                {`2.Turg'un savdo (xizmat ko'rsatish) shaxobchasi (ko'chma savdo obekti) manzilining xaritadagi koordinatasi.`}
            </div>
            <div className={styles.docs}>
                {`3. Onlayn-NKM va (yoki) virtual kassaga texnik xizmat ko'rsatish va uni ta'mirlashga txkm bilan tuzatilgan shartnomaning nusxasi.`}
            </div>
            <div className={styles.fullName}>
                <div className={styles.fullNameItems}>
                    {`Tadbirkorlik subyekti rahbarining F.I.O.`.split(' ').map(word =>
                        <div>{word}&nbsp;</div>
                    )}
                </div>
                <div className={styles.fullNameItems}>
                    {`${nickName}`.split(' ').map(word =>
                        <div>&nbsp;{word}&nbsp;</div>
                    )}
                </div>
            </div>
            <div className={styles.phoneCont}>
                <div className={styles.phone}>{`Telefon raqami: 9${data.userMobile}`}</div>
                <div className={styles.dateCont}>
                    <div className={styles.item}>
                        <div className={styles.dash}></div>
                        <div className={styles.dateTitle}>{`Imzo`}</div>
                    </div>
                    <div className={styles.item}>
                        <div>{date}</div>
                        <div className={styles.dateTitle}>{`Sana`}</div>
                    </div>
                </div>
            </div>
        </div>
    )
}
