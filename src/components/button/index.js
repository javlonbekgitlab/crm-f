import React, { memo } from 'react'
import { Button as ButtonMaterial, CircularProgress, IconButton as IconButtonMaterail} from '@material-ui/core'
import styles from './index.module.sass'
import { makeStyles } from '@material-ui/core/styles'
import PropTypes from 'prop-types'

export const Button = memo(({
    withoutStyle,
    color = 'primary', 
    variant, 
    loading, 
    disabled,
    children,
    ...props
    }) => {
    return (
        <div className = {styles.buttonCont}>
            <ButtonMaterial
                size = 'small'
                variant={withoutStyle ? 'text' : 'contained'}
                color = {color}
                className = {withoutStyle ? styles.button : styles.simpleButton}
                disabled = {loading || disabled}
                {...props} 
            >
                <div className = {styles.textCont}>
                    {children}
                    {loading && <div className = {styles.progress}>
                        <CircularProgress color="inherit" size={20} />
                    </div>}
                </div>
            </ButtonMaterial>
        </div>
    )
    }
)

export const IconButton = memo(({children, ...props}) => {

    const classes = useStyles()

    return (
        <IconButtonMaterail className = {classes.button} {...props}>
            {children}
        </IconButtonMaterail>
    )
})

const useStyles = makeStyles((theme) => ({
    button: {
        display: 'flex',
        alignItems: 'center',
        color: '#00000050',
        width: 'max-content',  
        justifyContent: 'center',
        transition: 'all 0.2s',
        '&:active': {
          transform: 'scale(0.8)'
        },
    },
    buttonIcon: {
        color: '#ffffff',
        display: 'flex',
        justifyContent: 'space-between',
    },
    children: {
        display: 'flex',
        // padding: '0 0.2em',
        alignItems: 'center',
        width: 'max-content'
    },
    icon: {
        display: 'flex',
        alignItems: 'center',
        paddingRight: '0.7em',
    },
    progress: {
        display: 'flex',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: '0.5em'
        
    }
  }))

export const ButtonWithIcon = memo(({ icon, children, disabled, onClick, loading}) => {
    console.log('buttonwithIcon',children)
    const classes = useStyles()

    return (
        <div className = {disabled ? styles.disabled : styles.customButton}>
            <ButtonMaterial
                variant = 'contained'
                onClick = {onClick}
                disabled = {disabled}
                color = 'primary'
                className = {classes.buttonIcon}
                // {...props}
            >
                {icon && <div className = {classes.icon}>
                    {icon}
                </div>}
                <div className = {classes.children}>
                    {children}
                    {loading && <div className = {classes.progress}>
                        <CircularProgress color="inherit" size={20} />
                    </div>}
                </div>
            </ButtonMaterial>
        </div>
    )
})

ButtonWithIcon.propTypes = {
    color: PropTypes.string,
    icon: PropTypes.element
}
