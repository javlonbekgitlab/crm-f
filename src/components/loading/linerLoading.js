import React from 'react'
import { makeStyles, withStyles } from '@material-ui/core/styles'
import LinearProgress from '@material-ui/core/LinearProgress'
import { observer } from 'mobx-react'
import { progressForm } from '../../actions'

const useStyles = makeStyles({
    cont: {
        display: 'flex',
        position: 'absolute',
        top: '0',
        left: '0',
        alignItems: 'flex-end',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: '#ffffff90',
        zIndex: '25',
        transition: 'all 0.2s'
    },
    hide: {
        display: 'flex',
        position: 'absolute',
        top: '0',
        left: '0',
        alignItems: 'flex-end',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        transition: 'all 0.2s',
        backgroundColor: 'transparent',
        zIndex: -1,
    },
    root: {
        padding: '0.2em 1.5em 0.5em 1.5em',
        backgroundColor: '#fff',
        width: '100%',
    },
})

const BorderLinearProgress = withStyles((theme) => ({
    root: {
        borderRadius: 5,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    colorPrimary: {
        padding: '0.5em 0 0.1em 0',
        display: 'flex',
        backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
    },
    bar: {
        borderRadius: 5,
        backgroundColor: '#1a90ff',
    },
  }))(LinearProgress)

export const LinearLoading = observer(({start}) => {

    const classes = useStyles()

    return (
        <div className = {start ? classes.cont : classes.hide}>
            <div className={classes.root}>
                <BorderLinearProgress variant="buffer" value={progressForm.value} valueBuffer={progressForm.value}/>
            </div>
        </div>
        
    )
})