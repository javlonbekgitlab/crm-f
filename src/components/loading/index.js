import React, { useEffect, useState } from 'react'
import './index.css'

export const Loading = ({start}) => {

    const [loading, setLoading] = useState(start)

    useEffect(() => {
        setTimeout(() => {
            if (!start) {
                setLoading(false)
            }
        }, 0);
    }, [start])

    return (
        <div className = {loading ? 'loadingCont' : 'loadingClose'}>
            <div className="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
        </div>
    )
}