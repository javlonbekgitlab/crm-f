import { CircularProgress } from '@material-ui/core'
import React from 'react'
import styles from './index.module.sass'

export const TableLoader = ({start}) => {
    return (
        <div className = {start ? styles.cont : `${styles.cont} ${styles.hide}`}>
            {start && <CircularProgress size = {60}/>}
        </div>
    )
}