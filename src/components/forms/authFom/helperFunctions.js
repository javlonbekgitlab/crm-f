import { snackbar, snackbarStatus } from "../../../actions"
import { Api, apiUrles } from "../../../api"
import { localStorageKeys } from "../../../constants"
import { writeArrayLocalStorage, writeLocalStorage } from "../../../store/localStorage"

export const validateLogin = ({email, password}) => {

    const errors = {}

    if (email.length < 1) {
        errors.emailError = 'required'
    }

    if (password.length < 1) {
        errors.passwordError = 'required'
    }

    return errors
}

export const onSubmitLogin = async values => {

    const response = {}

    const {email, password} = values

    const newData = {
        email,
        password
    }

    const resLog = await Api(apiUrles.signIn, newData, 'post')
    if (resLog.status === 200) {
        response.success = true
        console.log(resLog)
        writeArrayLocalStorage(localStorageKeys.roles ,resLog.data.roles)
        writeLocalStorage(localStorageKeys.token, resLog.data.accessToken)
        writeLocalStorage(localStorageKeys.username, resLog.data.username)
    } else {
        response.success = false
        snackbar.setSnackbar(resLog.toString(), snackbarStatus.error)
    }

    return response
}