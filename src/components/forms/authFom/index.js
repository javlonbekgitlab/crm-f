import React, { useCallback, useEffect } from 'react'
import styles from './index.module.sass'
import { useFormik } from 'formik'
import { validateLogin, onSubmitReg, onSubmitLogin } from './helperFunctions'
import DoneIcon from '@material-ui/icons/Done'
// import { useHistory } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { Input } from '../../input'
import { Button } from '../../button'
import { writeLocalStorage } from '../../../store/localStorage'

export const Login = inject('lang')(observer(({closeForm, lang}) => {

    useEffect(() => {
        return () => {
            formik.resetForm()
        }
    }, [])

    const handleLogin = async values => {

        const { success } = await onSubmitLogin(values)

        if (success) {
            closeForm()
            formik.resetForm()
        }

    }

    const handleSubmit = ({charCode}) => (charCode === 13 && formik.handleSubmit())
    
    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
            validateOnChange: false,
            validateOnBlur: false
        },
        validate: validateLogin,
        onSubmit: handleLogin,
        validateOnChange: false,
        validateOnBlur: false
    })

    const {
        values: { email, password},
        errors: { emailError, passwordError},
    } = formik

    return (
        <div className = {styles.inputCont}>
            <Input
                onChange = {formik.handleChange}
                standart
                name = 'email'
                error = {emailError}
                onKeyPress = {handleSubmit}
                value = {email}
                label = {lang.forms.signIn.login}
            />
            <Input
                onChange = {formik.handleChange}
                standart
                name = 'password'
                type = 'password'
                error = {passwordError}
                value = {password}
                onKeyPress = {handleSubmit}
                label = {lang.forms.signIn.password}
            />
            <div className = {styles.signInCont}>
                <Button
                    withoutStyle
                    onClick = {formik.handleSubmit}
                >
                    <DoneIcon/>
                </Button>
            </div>
        </div>
    )
}))
