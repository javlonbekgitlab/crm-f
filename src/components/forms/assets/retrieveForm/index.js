import React, { useCallback } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import { regexSpace } from '../../../../constants'
// import { getRetrieve } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'
import { inject } from 'mobx-react'


const propertyTypes = [
    {
        title: 'Office',
        id: '1'
    },
    {
        title: 'Storage',
        id: '2'
    },
    {
        title: 'fiscal modules',
        id: '3'
    }
]

export const RetrieveForm = inject('lang')(({closeForm, data, onUploadingForm, onSubmit, onValidate, lang, disabled}) => {

    const handleCloseForm = () => {
        closeForm('cancel')
    }

    const handleSubmitRetrieve = async values => {

        // onUploadingForm(true)

        const { success } = await onSubmit(values)
        // if (success) {
            // onUploadingForm(false)
            // getRetrieve()
            closeForm({success})
        // } else {
            // closeForm({success})
        // }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (typeof target.value === 'string') {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
        // }
    }, [])


    const formik = useFormik({
        initialValues: {
            incomingType: data.incomingType ? data.incomingType : '',
            year: data.year ? data.year : new Date(),
            sparePartType: data.sparePartType ? data.sparePartType : '',
            quantity: data.quantity ? data.quantity : '',
            category: data.category ? data.category : '',
            model: data.model ? data.model : '' ,
            batchNum: data.batchNum ? data.batchNum : '',
            batchDate: data.batchDate ? data.batchDate : new Date() ,
            contractNum: data.contractNum ? data.contractNum : '' ,
            contractDate: data.contractDate ? new Date(data.contractDate * 1000) : new Date(),
            actNum: data.actNum ? data.actNum : '' ,
            actDate: data.actDate ? new Date(data.actDate * 1000) : new Date(),
            respEmp: data.respEmp ? data.respEmp : '' ,
            status: data.status ? data.status : '' ,
            id: data.id ? data.id : '',
            // property_id:Values
        },
        onSubmit: handleSubmitRetrieve,
        validate: onValidate,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            quantity,
            category,
            sparePartType,
            incomingType,
            year,
            model,
            batchNum,
            batchDate,
            contractNum,
            contractDate,
            actNum,
            actDate,
            respEmp,
            file_name,
        },
        errors: {
            sparePartTypeErr,
            quantityErr,
            categoryErr,
            incomingTypeErr,
            modelErr,
            batchNumErr,
            contractNumErr,
            actNumErr,
            actDateErr,
            respEmpErr,
            file_nameErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.device}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'incomingType'
                                        onChange = {primaryOnChange}
                                        error =  {incomingTypeErr}
                                        disabled = {disabled}
                                        value = {incomingType}
                                        standart
                                        label = {lang.forms.retrieve.incomingType}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.retrieve.replacementPartType}
                                        name = 'sparePartType'
                                        onChange = {primaryOnChange}
                                        error = {sparePartTypeErr}
                                        disabled = {disabled}
                                        value = {sparePartType}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'category'
                                        onChange = {primaryOnChange}
                                        error =  {categoryErr}
                                        disabled = {disabled}
                                        value = {category}
                                        standart
                                        label = {lang.forms.retrieve.category}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        label = {lang.forms.retrieve.model}
                                        name = 'model'
                                        onChange = {primaryOnChange}
                                        error = {modelErr}
                                        disabled = {disabled}
                                        value = {model}
                                        standart
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.spec}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        label = {lang.forms.retrieve.year}
                                        name = 'year'
                                        onChange = {primaryOnChange}
                                        disabled = {disabled}
                                        value = {year}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'batchNum'
                                        onChange = {primaryOnChange}
                                        error = {batchNumErr}
                                        disabled = {disabled}
                                        value = {batchNum}
                                        standart
                                        label = {lang.forms.retrieve.supplyNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'batchDate'
                                        onChange = {primaryOnChange}
                                        value = {batchDate}
                                        label = {lang.forms.retrieve.supplyDate}
                                        disabled = {disabled}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'quantity'
                                        onChange = {primaryOnChange}
                                        error =  {quantityErr}
                                        disabled = {disabled}
                                        value = {quantity}
                                        standart
                                        label = {lang.forms.retrieve.amount}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'contractNum'
                                        onChange = {primaryOnChange}
                                        error =  {contractNumErr}
                                        value = {contractNum}
                                        disabled = {disabled}
                                        standart
                                        label = {lang.forms.retrieve.agreement}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'contractDate'
                                        onChange = {primaryOnChange}
                                        value = {contractDate}
                                        label = {lang.forms.retrieve.agreementDate}
                                        disabled = {disabled}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'actNum'
                                        onChange = {primaryOnChange}
                                        error =  {actNumErr}
                                        disabled = {disabled}
                                        value = {actNum}
                                        standart
                                        label = {lang.forms.retrieve.act}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'actDate'
                                        onChange = {primaryOnChange}
                                        value = {actDate}
                                        label = {lang.forms.retrieve.actDate}
                                        disabled = {disabled}
                                    />
                                    {/* <Input
                                        name = 'actDate'
                                        onChange = {primaryOnChange}
                                        error =  {actDateErr}
                                        disabled = {disabled}
                                        value = {actDate}
                                        standart
                                        label = {lang.forms.retrieve.actDate}
                                    /> */}
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                {/* <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'status'
                                        onChange = {primaryOnChange}
                                        error = {statusErr}
                                        disabled = {disabled}
                                        value = {status}
                                        standart
                                        label = {lang.forms.retrieve.status}
                                    />
                                </div> */}
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'respEmp'
                                        onChange = {primaryOnChange}
                                        error = {respEmpErr}
                                        disabled = {disabled}
                                        value = {respEmp}
                                        standart
                                        label = {lang.forms.retrieve.responsibleEmployee}
                                    />
                                </div>
                            </div>
                        </Paper>
                        {/* <Paper
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'file_name'
                                        onChange = {primaryOnChange}
                                        value = {file_name}
                                        error = {file_nameErr}
                                        disabled
                                        oldFile = {data.file_url ? {
                                            name: data.file_name,
                                            url: data.file_url
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper> */}
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

RetrieveForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}