import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import EditIcon from '@material-ui/icons/Edit'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
// import { getRetrieve } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'
import { reserveTechnics } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { onSubmitTechnics, validateFormTechnics } from './helperFunctions'


const propertyTypes = [
    {
        title: 'Ip telephone',
        id: '1'
    },
    {
        title: 'Notebook',
        id: '2'
    },
    {
        title: 'PC',
        id: '3'
    }
]

export const TechnicsForm = inject('lang')(({closeForm, data, onUploadingForm, lang, disabled}) => {

    const [disableSpec, setDisableSpec] = useState(true)

    const handleCloseForm = () => {
        closeForm('cancel')
    }

    const handleSubmitTechnics = async values => {

        // onUploadingForm(true)

        const { success } = await onSubmitTechnics(values)
        // if (success) {
        //     onUploadingForm(false)
        //     // getRetrieve()
        closeForm({success})
        // } else {
        //     closeForm(true)
        // }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (typeof target.value === 'string') {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }

        if (target.name === 'type') {
            if ((value === '2') || (value === '3')) {
                setDisableSpec(false)
            } else {
                setDisableSpec(true)
            }
            console.log(disableSpec)
            // const event = {
            //     target: {
            //         name: 'disableSpec',
            //         value: disableSpec,
            //     }
            // }
            // console.log(event.target.value)
            // formik.handleChange(event)
        }

        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
        // }
    }, [])


    // // // values with condition reserve
    // const valuesFromPropAndState = {
    //     name: data.name ? data.name : '',
    //     title: data.title ? data.title : '',
    //     year: data.year ? data.year : new Date(),
    //     type: data.type ? data.type : '',
    //     netAddress: data.netAddress ? data.netAddress : '',
    //     brand: data.brand ? data.brand : '',
    //     model: data.model ? data.model : '' ,
    //     serialNumber: data.serialNumber ? data.serialNumber : '',
    //     supplyDate: data.supplyDate ? data.supplyDate : new Date() ,
    //     agreement: data.agreement ? data.agreement : '' ,
    //     agreementDate: data.agreementDate ? new Date(data.agreementDate * 1000) : new Date(),
    //     inventoryNumber: data.inventoryNumber ? data.inventoryNumber : '' ,
    //     inventoryLast: data.inventoryLast ? data.inventoryLast : '' ,
    //     responsibleEmployee: data.responsibleEmployee ? data.responsibleEmployee : '' ,
    //     state: data.state ? data.state : '' ,
    //     price: data.price ? data.price : '' ,
    //     specs: data.specs ? data.specs : '' ,
    //     cpu: data.cpu ? data.cpu : '' ,
    //     ram: data.ram ? data.ram : '' ,
    //     os: data.os ? data.os : '' ,
    //     memory: data.memory ? data.memory : '' ,
    //     id: data.id ? data.id : '',
    // }

    const formik = useFormik({
        initialValues: {
            name: data.name ? data.name : '',
            title: data.title ? data.title : '',
            yearOfRelease: data.yearOfRelease ? data.yearOfRelease : new Date(),
            type: data.type ? data.type : '',
            networkAddress: data.networkAddress ? data.networkAddress : '',
            brand: data.brand ? data.brand : '',
            model: data.model ? data.model : '' ,
            // serialNumber: data.serialNumber ? data.serialNumber : '',
            // supplyDate: data.supplyDate ? data.supplyDate : new Date() ,
            agreement: data.agreement ? data.agreement : '' ,
            dateOfContract: data.dateOfContract ? new Date(data.dateOfContract * 1000) : new Date(),
            inventoryNumber: data.inventoryNumber ? data.inventoryNumber : '' ,
            lastInventory: data.lastInventory ? data.lastInventory : '' ,
            responsibleEmployee: data.responsibleEmployee ? data.responsibleEmployee : '' ,
            state: data.state ? data.state : '' ,
            price: data.price ? data.price : '' ,
            specifications: data.specifications ? data.specifications : '' ,
            cpu: data.cpu ? data.cpu : '' ,
            ram: data.ram ? data.ram : '' ,
            os: data.os ? data.os : '' ,
            memory: data.memory ? data.memory : '' ,
            // id: data.id ? data.id : '',
            // file_name: {
            //     name: lang.forms.retrieve.file_name
            // }
            // property_id:Values
        },
        onSubmit: handleSubmitTechnics,
        validate: validateFormTechnics ,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            networkAddress,
            brand,
            type,
            name,
            yearOfRelease,
            model,
            // serialNumber,
            numberOfContract,
            dateOfContract,
            inventoryNumber,
            lastInventory,
            // responsibleEmployee,
            state,
            price,
            specifications,
            cpu,
            ram,
            os,
            memory,
            title,
            // file_name,
        },
        errors: {
            typeErr,
            networkAddressErr,
            brandErr,
            nameErr,
            modelErr,
            // serialNumberErr,
            numberOfContractErr,
            inventoryNumberErr,
            lastInventoryErr,
            // responsibleEmployeeErr,
            stateErr,
            priceErr,
            specificationsErr,
            cpuErr,
            osErr,
            ramErr,
            memoryErr,
            titleErr,
            // file_nameErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.device}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'title'
                                        id = 'title'
                                        onChange = {primaryOnChange}
                                        error =  {titleErr}
                                        disabled = {disabled}
                                        value = {title}
                                        standart
                                        label = {lang.forms.technics.title}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.technics.type}
                                        name = 'type'
                                        onChange = {primaryOnChange}
                                        error = {typeErr}
                                        disabled = {disabled}
                                        value = {type}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'brand'
                                        id = 'brand'
                                        onChange = {primaryOnChange}
                                        error =  {brandErr}
                                        disabled = {disabled}
                                        value = {brand}
                                        standart
                                        label = {lang.forms.technics.brand}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        label = {lang.forms.retrieve.model}
                                        name = 'model'
                                        id = 'model'
                                        onChange = {primaryOnChange}
                                        error = {modelErr}
                                        disabled = {disabled}
                                        value = {model}
                                        standart
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        label = {lang.forms.retrieve.year}
                                        name = 'yearOfRelease'
                                        onChange = {primaryOnChange}
                                        disabled = {disabled}
                                        value = {yearOfRelease}
                                    />
                                </div>
                                {/* <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'serialNumber'
                                        id = 'serialNumber'
                                        onChange = {primaryOnChange}
                                        error = {serialNumberErr}
                                        disabled = {disabled}
                                        value = {serialNumber}
                                        standart
                                        label = {lang.forms.technics.serialNumber}
                                    />
                                </div> */}
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                {/* <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'responsibleEmployee'
                                        id = 'responsibleEmployee'
                                        onChange = {primaryOnChange}
                                        error = {responsibleEmployeeErr}
                                        disabled = {disabled}
                                        value = {responsibleEmployee}
                                        standart
                                        label = {lang.forms.retrieve.responsibleEmployee}
                                    />
                                </div> */}
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'inventoryNumber'
                                        id = 'inventoryNumber'
                                        onChange = {primaryOnChange}
                                        error =  {inventoryNumberErr}
                                        disabled = {disabled}
                                        value = {inventoryNumber}
                                        standart
                                        label = {lang.forms.technics.inventoryNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'lastInventory'
                                        id = 'lastInventory'
                                        onChange = {primaryOnChange}
                                        error =  {lastInventoryErr}
                                        disabled = {disabled}
                                        value = {lastInventory}
                                        standart
                                        label = {lang.forms.technics.inventoryLast}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'state'
                                        id = 'state'
                                        onChange = {primaryOnChange}
                                        error = {stateErr}
                                        disabled = {disabled}
                                        value = {state}
                                        standart
                                        label = {lang.forms.technics.state}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.technics.titles.spec}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'price'
                                        id = 'price'
                                        onChange = {primaryOnChange}
                                        error =  {priceErr}
                                        disabled = {disableSpec}
                                        value = {price}
                                        standart
                                        label = {lang.forms.technics.price}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'specifications'
                                        onChange = {primaryOnChange}
                                        error =  {specificationsErr}
                                        disabled = {disableSpec}
                                        value = {specifications}
                                        standart
                                        label = {lang.forms.technics.specs}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <div className = {styles.inputCont}>
                                        <Input
                                            name = 'cpu'
                                            id = 'cpu'
                                            onChange = {primaryOnChange}
                                            error =  {cpuErr}
                                            disabled = {disableSpec}
                                            value = {cpu}
                                            standart
                                            label = {lang.forms.technics.cpu}
                                        />
                                    </div>
                                    <div className = {styles.inputCont}>
                                        <Input
                                            name = 'ram'
                                            id = 'ram'
                                            onChange = {primaryOnChange}
                                            error =  {ramErr}
                                            disabled = {disableSpec}
                                            value = {ram}
                                            standart
                                            label = {lang.forms.technics.ram}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.itemChilds}>
                                    <div className = {styles.inputCont}>
                                        <Input
                                            name = 'os'
                                            id = 'os'
                                            onChange = {primaryOnChange}
                                            error =  {osErr}
                                            disabled = {disableSpec}
                                            value = {os}
                                            standart
                                            label = {lang.forms.technics.os}
                                        />
                                    </div>
                                    <div className = {styles.inputCont}>
                                        <Input
                                            name = 'memory'
                                            id = 'memory'
                                            onChange = {primaryOnChange}
                                            error =  {memoryErr}
                                            disabled = {disableSpec}
                                            value = {memory}
                                            standart
                                            label = {lang.forms.technics.memory}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'name'
                                        id = 'name'
                                        onChange = {primaryOnChange}
                                        error =  {nameErr}
                                        disabled = {disableSpec}
                                        value = {name}
                                        standart
                                        label = {lang.forms.technics.name}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'networkAddress'
                                        onChange = {primaryOnChange}
                                        error =  {networkAddressErr}
                                        disabled = {disableSpec}
                                        value = {networkAddress}
                                        standart
                                        label = {lang.forms.technics.netAddress}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfContract'
                                        onChange = {primaryOnChange}
                                        error =  {numberOfContractErr}
                                        value = {numberOfContract}
                                        disabled = {disabled}
                                        standart
                                        label = {lang.forms.retrieve.agreement}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfContract'
                                        onChange = {primaryOnChange}
                                        value = {dateOfContract}
                                        label = {lang.forms.retrieve.agreementDate}
                                        disabled = {disabled}
                                    />
                                </div>
                                
                            </div>
                        </Paper>
                        {/* <Paper
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'file_name'
                                        onChange = {primaryOnChange}
                                        value = {file_name}
                                        error = {file_nameErr}
                                        disabled
                                        oldFile = {data.file_url ? {
                                            name: data.file_name,
                                            url: data.file_url
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper> */}
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

TechnicsForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}