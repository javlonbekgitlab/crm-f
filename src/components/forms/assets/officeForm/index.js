import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { onSubmit, validateOfficeForm } from './helperFunctions'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { LinearLoading } from '../../../loading/linerLoading'
import { reserveOffice, reserveProperty } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { onSubmitProperty } from '../propertyForm/helperFunctions'


const propertyTypes = [
    {
        title: 'Office',
        id: '1'
    },
    {
        title: 'Storage',
        id: '2'
    },
    {
        title: 'fiscal modules',
        id: '3'
    }
]

export const OfficeForm = inject('lang')(({closeForm, data, onUploadingForm, lang, parentValues, disabled}) => {
    
    const handleCloseForm = () => {
        closeForm()
        reserveOffice.clearReserveOffice()
        reserveProperty.clearReserveProperty()
    }

    const handleSubmitOffice = async values => {

        onUploadingForm(true)

        const { success, data } = await onSubmitProperty(parentValues)

        if (success) {
            const { success: successSecond } = await onSubmit(values, data.id)
            if (success) {
                onUploadingForm(false)
                reserveOffice.clearReserveOffice()
                // getOfficees()
                closeForm({status: successSecond})
            } else {
                closeForm({status: successSecond})
            }
        } else {
            closeForm({status: success})
        }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        
        if (target.value) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
            reserveOffice.setReserveOffice(event)
        // }
    }, [])


    // // values with condition reserve
    const valuesFromPropAndState = {
        officeName: data.officeName ? data.officeName : (reserveOffice.officeName ? reserveOffice.officeName : ''),
        officeType: data.officeType ? data.officeType : (reserveOffice.officeType ? reserveOffice.officeType : ''),
        republic: data.republic ? data.republic : (reserveOffice.republic ? reserveOffice.republic : ''),
        region: data.region ? data.region : (reserveOffice.region ? reserveOffice.region : ''),
        district: data.district ? data.district : (reserveOffice.district ? reserveOffice.district : '') ,
        address: data.address ? data.address : (reserveOffice.address ? reserveOffice.address : ''),
        cadastre: data.cadastre ? data.cadastre : (reserveOffice.cadastre ? reserveOffice.cadastre : '') ,
        area: data.area ? data.area : (reserveOffice.area ? reserveOffice.area : '') ,
        roomNumber: data.roomNumber ? data.roomNumber : (reserveOffice.roomNumber ? reserveOffice.roomNumber : '') ,
        agreement: data.agreement ? data.agreement : (reserveOffice.agreement ? reserveOffice.agreement : '') ,
        assets: data.assets ? data.assets : (reserveOffice.assets ? reserveOffice.assets : ''),
        employeeNumber: data.employeeNumber ? data.employeeNumber : (reserveOffice.employeeNumber ? reserveOffice.employeeNumber : '') ,
        id: data.id ? data.id : '',
    }

    const formik = useFormik({
        initialValues: {
            officeName: valuesFromPropAndState.officeName ,
            officeType: valuesFromPropAndState.officeType ,
            republic: valuesFromPropAndState.republic ,
            region: valuesFromPropAndState.region ,
            district: valuesFromPropAndState.district ,
            address: valuesFromPropAndState.address,
            cadastre: valuesFromPropAndState.cadastre ,
            area: valuesFromPropAndState.area ,
            roomNumber: valuesFromPropAndState.roomNumber ,
            employeeNumber: valuesFromPropAndState.employeeNumber ,
            assets: valuesFromPropAndState.assets,
            id: valuesFromPropAndState.id,
            // property_id: parentId
        },
        onSubmit: handleSubmitOffice,
        validate: validateOfficeForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            officeType,
            republic,
            officeName,
            region,
            district,
            address,
            cadastre,
            area,
            roomNumber,
            employeeNumber,
            assets,
        },
        errors: {
            republicErr,
            officeTypeErr,
            officeNameErr,
            regionErr,
            districtErr,
            addressErr,
            cadastreErr,
            areaErr,
            roomNumberErr,
            employeeNumberErr,
            assetsErr,
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                    <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.office.titles.office}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'officeName'
                                        id = 'officeName'
                                        onChange = {primaryOnChange}
                                        error =  {officeNameErr}
                                        disabled = {disabled}
                                        value = {officeName}
                                        standart
                                        label = {lang.forms.office.officeName}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'officeType'
                                        id = 'officeType'
                                        onChange = {primaryOnChange}
                                        error =  {officeTypeErr}
                                        disabled = {disabled}
                                        value = {officeType}
                                        standart
                                        label = {lang.forms.office.officeType}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.location}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.republic}
                                        name = 'republic'
                                        onChange = {primaryOnChange}
                                        error = {republicErr}
                                        disabled = {disabled}
                                        value = {republic}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.region}
                                        name = 'region'
                                        onChange = {primaryOnChange}
                                        error = {regionErr}
                                        disabled = {disabled}
                                        value = {region}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.district}
                                        name = 'district'
                                        onChange = {primaryOnChange}
                                        error = {districtErr}
                                        disabled = {disabled}
                                        value = {district}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'address'
                                        id = 'address'
                                        onChange = {primaryOnChange}
                                        error = {addressErr}
                                        disabled = {disabled}
                                        value = {address}
                                        standart
                                        label = {lang.forms.branch.address}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'cadastre'
                                        id = 'cadastre'
                                        onChange = {primaryOnChange}
                                        error =  {cadastreErr}
                                        value = {cadastre}
                                        standart
                                        label = {lang.forms.branch.cadastre}
                                        disabled = {disabled}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'roomNumber'
                                        id = 'roomNumber'
                                        onChange = {primaryOnChange}
                                        error = {roomNumberErr}
                                        disabled = {disabled}
                                        value = {roomNumber}
                                        standart
                                        label = {lang.forms.branch.roomNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'area'
                                        id = 'area'
                                        onChange = {primaryOnChange}
                                        error = {areaErr}
                                        disabled = {disabled}
                                        value = {area}
                                        standart
                                        label = {lang.forms.branch.area}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'employeeNumber'
                                        id = 'employeeNumber'
                                        onChange = {primaryOnChange}
                                        error = {employeeNumberErr}
                                        disabled = {disabled}
                                        value = {employeeNumber}
                                        standart
                                        label = {lang.forms.branch.employeeNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'assets'
                                        id = 'assets'
                                        onChange = {primaryOnChange}
                                        error = {assetsErr}
                                        disabled = {disabled}
                                        value = {assets}
                                        standart
                                        label = {lang.forms.branch.assets}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

OfficeForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}

// const OrderProgress = observer(({start}) => {
//     return (
//         <LinearLoading start = {start} progress = {progressOderForm.value}/>
//     )
// })