import React, { useCallback } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import { regexSpace } from '../../../../constants'
// import { getRetrieve } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'
import { inject } from 'mobx-react'
import { onSubmitInterior, validateFormInterior } from './helperFunctions'


const propertyTypes = [
    {
        title: 'Ip telephone',
        id: '1'
    },
    {
        title: 'Notebook',
        id: '2'
    },
    {
        title: 'PC',
        id: '3'
    }
]

export const InteriorForm = inject('lang')(({closeForm, data, onUploadingForm, lang, disabled}) => {


    const handleCloseForm = () => {
        closeForm('cancel')
    }

    const handleSubmitInterior = async values => {

        // onUploadingForm(true)

        const { success } = await onSubmitInterior(values)
        // if (successSecond) {
        //     onUploadingForm(false)
        //     reserveDevices.clearReserveDevices()
        //     // getRetrieve()
            closeForm({success})
        // } else {
        //     closeForm({state: successSecond})
        // }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (typeof target.value === 'string') {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }

        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
        // }
    }, [])

    const formik = useFormik({
        initialValues: {
            naming: data.naming ? data.naming :  '',
            yearOfRelease: data.yearOfRelease ? data.yearOfRelease :  new Date(),
            type: data.type ? data.type :  '',
            brand: data.brand ? data.brand :  '',
            model: data.model ? data.model :  '' ,
            supplyNumber: data.supplyNumber ? data.supplyNumber :  '',
            price: data.price ? data.price :  '',
            supplyDate: data.supplyDate ? data.supplyDate :  new Date() ,
            numberOfContract: data.numberOfContract ? data.numberOfContract :  '' ,
            dateOfContract: data.dateOfContract ? new Date(data.dateOfContract * 1000) :  new Date(),
            inventoryNumber: data.inventoryNumber ? data.inventoryNumber :  '' ,
            lastInventory: data.lastInventory ? data.lastInventory :  '' ,
            responsibleEmployee: data.responsibleEmployee ? data.responsibleEmployee :  '' ,
            state: data.state ? data.state :  '' ,
            id: data.id ? data.id : '',
            file_name: {
                name: lang.forms.retrieve.file_name
            }
            // property_id:Values
        },
        onSubmit: handleSubmitInterior,
        validate: validateFormInterior ,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            brand,
            type,
            yearOfRelease,
            model,
            price,
            numberOfContract,
            dateOfContract,
            inventoryNumber,
            lastInventory,
            responsibleEmployee,
            state,
            naming,
            file_name,
        },
        errors: {
            typeErr,
            brandErr,
            modelErr,
            priceErr,
            numberOfContractErr,
            inventoryNumberErr,
            lastInventoryErr,
            responsibleEmployeeErr,
            stateErr,
            namingErr,
            file_nameErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.interior.titles.object}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'naming'
                                        onChange = {primaryOnChange}
                                        error =  {namingErr}
                                        disabled = {disabled}
                                        value = {naming}
                                        standart
                                        label = {lang.forms.technics.title}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.technics.type}
                                        name = 'type'
                                        onChange = {primaryOnChange}
                                        error = {typeErr}
                                        disabled = {disabled}
                                        value = {type}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'brand'
                                        id = 'brand'
                                        onChange = {primaryOnChange}
                                        error =  {brandErr}
                                        disabled = {disabled}
                                        value = {brand}
                                        standart
                                        label = {lang.forms.technics.brand}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        label = {lang.forms.retrieve.model}
                                        name = 'model'
                                        id = 'model'
                                        onChange = {primaryOnChange}
                                        error = {modelErr}
                                        disabled = {disabled}
                                        value = {model}
                                        standart
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'price'
                                        id = 'price'
                                        onChange = {primaryOnChange}
                                        error = {priceErr}
                                        disabled = {disabled}
                                        value = {price}
                                        standart
                                        label = {lang.forms.technics.price}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        label = {lang.forms.retrieve.year}
                                        name = 'yearOfRelease'
                                        onChange = {primaryOnChange}
                                        disabled = {disabled}
                                        value = {yearOfRelease}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfContract'
                                        onChange = {primaryOnChange}
                                        error =  {numberOfContractErr}
                                        value = {numberOfContract}
                                        disabled = {disabled}
                                        standart
                                        label = {lang.forms.retrieve.agreement}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfContract'
                                        onChange = {primaryOnChange}
                                        value = {dateOfContract}
                                        label = {lang.forms.retrieve.agreementDate}
                                        disabled = {disabled}
                                    />
                                </div>
                                
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                {/* <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'responsibleEmployee'
                                        id = 'responsibleEmployee'
                                        onChange = {primaryOnChange}
                                        error = {responsibleEmployeeErr}
                                        disabled = {disabled}
                                        value = {responsibleEmployee}
                                        standart
                                        label = {lang.forms.retrieve.responsibleEmployee}
                                    />
                                </div> */}
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'inventoryNumber'
                                        id = 'inventoryNumber'
                                        onChange = {primaryOnChange}
                                        error =  {inventoryNumberErr}
                                        disabled = {disabled}
                                        value = {inventoryNumber}
                                        standart
                                        label = {lang.forms.technics.inventoryNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'lastInventory'
                                        onChange = {primaryOnChange}
                                        error =  {lastInventoryErr}
                                        disabled = {disabled}
                                        value = {lastInventory}
                                        standart
                                        label = {lang.forms.technics.inventoryLast}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'state'
                                        id = 'state'
                                        onChange = {primaryOnChange}
                                        error = {stateErr}
                                        disabled = {disabled}
                                        value = {state}
                                        standart
                                        label = {lang.forms.technics.state}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'file_name'
                                        onChange = {primaryOnChange}
                                        value = {file_name}
                                        error = {file_nameErr}
                                        disabled
                                        oldFile = {data.file_url ? {
                                            name: data.file_name,
                                            url: data.file_url
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

InteriorForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}