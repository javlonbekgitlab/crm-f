import React, { useCallback } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import { regexSpace } from '../../../../constants'
// import { getRetrieve } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'
import { inject } from 'mobx-react'
import { onSubmitOthers, validateFormDevices } from './helperFunctions'


// const propertyTypes = [
//     {
//         title: 'Ip telephone',
//         id: '1'
//     },
//     {
//         title: 'Notebook',
//         id: '2'
//     },
//     {
//         title: 'PC',
//         id: '3'
//     }
// ]

export const OthersForm = inject('lang')(({closeForm, data, onUploadingForm, lang, disabled}) => {


    const handleCloseForm = () => {
        closeForm('cancel')
    }

    const handleSubmitDevices = async values => {

        // onUploadingForm(true)

        const { success } = await onSubmitOthers(values)
        // if (successSecond) {
        //     onUploadingForm(false)
        //     reserveDevices.clearReserveDevices()
        //     // getRetrieve()
            closeForm({success})
        // } else {
        //     closeForm({state: successSecond})
        // }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (typeof target.value === 'string') {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }

        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
        // }
    }, [])

    const formik = useFormik({
        initialValues: {
            yearOfProduction: data.yearOfProduction ? data.yearOfProduction :  new Date(),
            stamp: data.stamp ? data.stamp :  '',
            model: data.model ? data.model :  '' ,
            numberOfTechniquePassport: data.numberOfTechniquePassport ? data.numberOfTechniquePassport :  '',
            serialNumber: data.serialNumber ? data.serialNumber :  '',
            contractOfSale: data.contractOfSale ? data.contractOfSale :  '' ,
            dateOfContractPurchasesSales: data.dateOfContractPurchasesSales ? new Date(data.dateOfContractPurchasesSales * 1000) :  new Date(),
            registrationNumber: data.registrationNumber ? data.registrationNumber :  '' ,
            amountOfContract: data.amountOfContract ? data.amountOfContract :  '' ,
            responsibleEmployee: data.responsibleEmployee ? data.responsibleEmployee :  '' ,
            file_name: {
                name: lang.forms.retrieve.file_name
            }
            // property_id:Values
        },
        onSubmit: handleSubmitDevices,
        validate: validateFormDevices ,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            stamp,
            yearOfProduction,
            model,
            serialNumber,
            numberofContractPurchasesSales,
            dateOfContractPurchasesSales,
            registrationNumber,
            amountOfContract,
            responsibleEmployee,
            numberOfTechniquePassport,
            file_name,
        },
        errors: {
            stampErr,
            modelErr,
            serialNumberErr,
            numberofContractPurchasesSalesErr,
            numberOfTechniquePassportErr,
            registrationNumberErr,
            amountOfContractErr,
            responsibleEmployeeErr,
            file_nameErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.device}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'stamp'
                                        onChange = {primaryOnChange}
                                        error =  {stampErr}
                                        disabled = {disabled}
                                        value = {stamp}
                                        standart
                                        label = {lang.forms.others.mark}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        label = {lang.forms.retrieve.model}
                                        name = 'model'
                                        onChange = {primaryOnChange}
                                        error = {modelErr}
                                        disabled = {disabled}
                                        value = {model}
                                        standart
                                    />
                                </div>
                                {/* <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'serialNumber'
                                        onChange = {primaryOnChange}
                                        error = {serialNumberErr}
                                        disabled = {disabled}
                                        value = {serialNumber}
                                        standart
                                        label = {lang.forms.technics.serialNumber}
                                    />
                                </div> */}
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.spec}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        label = {lang.forms.others.year}
                                        name = 'yearOfProduction'
                                        onChange = {primaryOnChange}
                                        disabled = {disabled}
                                        value = {yearOfProduction}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfTechniquePassport'
                                        onChange = {primaryOnChange}
                                        error = {numberOfTechniquePassportErr}
                                        disabled = {disabled}
                                        value = {numberOfTechniquePassport}
                                        standart
                                        label = {lang.forms.others.techPassNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'registrationNumber'
                                        id = 'registrationNumber'
                                        onChange = {primaryOnChange}
                                        error =  {registrationNumberErr}
                                        disabled = {disabled}
                                        value = {registrationNumber}
                                        standart
                                        label = {lang.forms.others.registrationNumber}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberofContractPurchasesSales'
                                        onChange = {primaryOnChange}
                                        error =  {numberofContractPurchasesSalesErr}
                                        value = {numberofContractPurchasesSales}
                                        disabled = {disabled}
                                        standart
                                        label = {lang.forms.property.contractOfSale}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfContractPurchasesSales'
                                        onChange = {primaryOnChange}
                                        value = {dateOfContractPurchasesSales}
                                        label = {lang.forms.property.dateContractOfSale}
                                        disabled = {disabled}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'amountOfContract'
                                        onChange = {primaryOnChange}
                                        error =  {amountOfContractErr}
                                        disabled = {disabled}
                                        value = {amountOfContract}
                                        standart
                                        label = {lang.forms.branch.agreementSum}
                                    />
                                </div>                                
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'responsibleEmployee'
                                        id = 'responsibleEmployee'
                                        onChange = {primaryOnChange}
                                        error = {responsibleEmployeeErr}
                                        disabled = {disabled}
                                        value = {responsibleEmployee}
                                        standart
                                        label = {lang.forms.retrieve.responsibleEmployee}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'file_name'
                                        onChange = {primaryOnChange}
                                        value = {file_name}
                                        error = {file_nameErr}
                                        disabled
                                        oldFile = {data.file_url ? {
                                            name: data.file_name,
                                            url: data.file_url
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

OthersForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}