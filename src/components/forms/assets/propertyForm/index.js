import React, { useCallback } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { validatePropertyForm } from './helperFunctions'
import { regexSpace } from '../../../../constants'
import { DatePicker } from '../../../datePicker'
import { reserveProperty } from '../../../../store/reserve'
import { inject } from 'mobx-react'


const propertyTypes = [
    {
        title: 'Office',
        id: 'office'
    },
    {
        title: 'Storage',
        id: 'storage'
    },
    {
        title: 'Branch',
        id: 'branch'
    }
]

export const PropertyForm = inject('lang')(({
    closeForm, 
    data, 
    lang, 
    onSetActiveStep, 
    onSetTypeOfProperty,
    onSetPropertyValues
}) => {
    
    const handleCloseForm = () => {
        closeForm()
        reserveProperty.clearReserveProperty()
    }

    const handleSubmitProperty = async values => {
        onSetActiveStep(1)
        onSetPropertyValues(values)
        // reserveProperty.clearReserveProperty()
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (target.name !== 'dateContractOfSale' && target.value) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }

        if (target.name === 'propertyType') {
            onSetTypeOfProperty(value)
        }
        
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
            reserveProperty.setReserveProperty(event)
        // }
    }, [])


    // // values with condition reserve
    const valuesFromPropAndState = {
        propertyType: data.propertyType ? data.propertyType : (reserveProperty.propertyType ? reserveProperty.propertyType : ''),
        contractOfSale: data.contractOfSale ? data.contractOfSale : (reserveProperty.contractOfSale ? reserveProperty.contractOfSale : ''),
        propertyName: data.propertyName ? data.propertyName : (reserveProperty.propertyName ? reserveProperty.propertyName : ''),
        dev_propertyTypeNumber: data.dev_propertyTypeNumber ? data.dev_propertyTypeNumber : (reserveProperty.dev_propertyTypeNumber ? reserveProperty.dev_propertyTypeNumber : ''),
        dateContractOfSale: data.dateContractOfSale ? new Date(data.dateContractOfSale * 1000) : (reserveProperty.dateContractOfSale ? reserveProperty.dateContractOfSale : new Date()),
    }

    const formik = useFormik({
        initialValues: {
            propertyType: valuesFromPropAndState.propertyType ,
            dateContractOfSale: valuesFromPropAndState.dateContractOfSale ,
            propertyName: valuesFromPropAndState.propertyName ,
            contractOfSale: valuesFromPropAndState.contractOfSale ,
            dev_propertyTypeNumber: valuesFromPropAndState.dev_propertyTypeNumber
        },
        onSubmit: handleSubmitProperty,
        validate: validatePropertyForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            dateContractOfSale,
            propertyName,
            propertyType,
            contractOfSale
        },
        errors: {
            propertyNameErr,
            propertyTypeErr,
            contractOfSaleErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.property.titles.immovables}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'propertyName'
                                        id = 'propertyName'
                                        onChange = {primaryOnChange}
                                        error =  {propertyNameErr}
                                        value = {propertyName}
                                        standart
                                        label = {lang.forms.property.nameImmovables}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.property.typeImmovables}
                                        name = 'propertyType'
                                        onChange = {primaryOnChange}
                                        error = {propertyTypeErr}
                                        value = {propertyType}
                                        data = {propertyTypes}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.property.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateContractOfSale'
                                        onChange = {primaryOnChange}
                                        value = {dateContractOfSale}
                                        label = {lang.forms.property.dateContractOfSale}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'contractOfSale'
                                        id = 'contractOfSale'
                                        onChange = {primaryOnChange}
                                        error = {contractOfSaleErr}
                                        value = {contractOfSale}
                                        standart
                                        label = {lang.forms.property.contractOfSale}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonCont}>
                            <Button 
                                onClick = {handleSubmit}
                            >
                                {lang.forms.buttonTitles.next}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

PropertyForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}

// const OrderProgress = observer(({start}) => {
//     return (
//         <LinearLoading start = {start} progress = {progressOderForm.value}/>
//     )
// })