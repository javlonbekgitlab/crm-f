import { Api, apiUrles } from "../../../../api"
import { progressForm, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"


export const onSubmitProperty = async values => {

    const response = {}

    const {
        dateContractOfSale,
        propertyName,
        propertyType,
        contractOfSale,
        id
    } = values

    const newData = {
        dateContractOfSale: new Date(dateContractOfSale).getTime() / 1000,
        propertyName,
        propertyType,
        contractOfSale,
    }
    console.log(newData)

    const token = readLocalStorage('token')

    let url = ''

    if (id) {
        url = apiUrles.propertiesUpdate + id
    } else {
        url = apiUrles.propertiesAdd
    }

    const res = await Api(url, newData, 'post', token, uploadProgress)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    progressForm.setProgressForm(0)
    return response
}

export const validatePropertyForm = ({
    propertyName,
    propertyType,
    contractOfSale
}) => {

    const errors = {}

    if (propertyName.length < 1) {
        errors.propertyNameErr = 'required'
    }
    if (propertyType.length < 1) {
        errors.propertyTypeErr = 'required'
    }
    if (contractOfSale.length < 1) {
        errors.contractOfSaleErr = 'required'
    }

    return errors
}
