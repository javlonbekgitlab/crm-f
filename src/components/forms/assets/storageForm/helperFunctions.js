import { Api, apiUrles } from "../../../../api"
import { progressForm, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmit = async (values, property_id) => {

    const response = {}

    const {
        storageLocation,
        storageType,
        storageName,
        orderNumber,
        responsibleEmployee,
        republic,
        region,
        district,
        address,
        cadastre,
        assets,
        orderDate,
        date,
        id
    } = values
    
    console.log(JSON.stringify(values))

    const newData = {
        date: new Date(date).getTime() / 1000,
        orderDate: new Date(orderDate).getTime() / 1000,
        storageLocation,
        storageType,
        storageName,
        orderNumber,
        responsibleEmployee,
        republic,
        region,
        district,
        address,
        cadastre,
        assets,
    }

    const token = readLocalStorage('token')

    let url = ''

    if (id) {
        url = apiUrles.storagesUpdate + id
    } else {
        newData.property_id = property_id
        url = apiUrles.storagesAdd
    }

    const res = await Api(url, newData, 'post', token, uploadProgress)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    progressForm.setProgressForm(0)

    return response
}

export const validateOrderForm = ({
    storageLocation,
    storageType,
    republic,
    storageName,
    region,
    district,
    address,
    cadastre,
    date,
    orderNumber,
    orderDate,
    responsibleEmployee,
    assets,
    id
}) => {

    const errors = {}

    // if (contract.length < 1) {
    //     errors.contractErr = 'required'
    // }
    // if (date_of_contract.length < 1) {
    //     errors.date_of_contractErr = 'required'
    // }
    // if (type_of_application.length < 1) {
    //     errors.type_of_applicationErr = 'required'
    // }
    // if (model.length < 1) {
    //     errors.modelErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(serial_number))) {
    //     errors.serial_numberErr = 'onlyNumbers'
    // }
    // if (serial_number.length < 1) {
    //     errors.serial_numberErr = 'required'
    // }
    // if (fisk_number.length < 1) {
    //     errors.fisk_numberErr = 'required'
    // }
    // if (!txkmFile.size && !id) {
    //     errors.txkmFileErr = 'required'
    // }
    // if (shopName.length < 1) {
    //     errors.shopNameErr = 'required'
    // }
    // if (service_center.length < 1) {
    //     errors.service_centerErr = 'required'
    // }
    // if (service_employee.length < 1) {
    //     errors.service_employeeErr = 'required'
    // }
    // if (merchantName.length < 1) {
    //     errors.merchantNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(password))) {
    //     errors.passwordErr = 'onlyNumbers'
    // }
    // if (password.length < 1) {
    //     errors.passwordErr = 'required'
    // }

    return errors
}
