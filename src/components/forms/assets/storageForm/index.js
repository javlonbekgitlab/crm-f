import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { onSubmit, validateOrderForm } from './helperFunctions'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { DatePicker } from '../../../datePicker'
import { LinearLoading } from '../../../loading/linerLoading'
import { reserveProperty, reserveStorage } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { onSubmitProperty } from '../propertyForm/helperFunctions'


const propertyTypes = [
    {
        title: 'Office',
        id: '1'
    },
    {
        title: 'Storage',
        id: '2'
    },
    {
        title: 'fiscal modules',
        id: '3'
    }
]

export const StorageForm = inject('lang')(({closeForm, data, onUploadingForm, lang, parentValues}) => {
    
    const handleCloseForm = () => {
        closeForm()
        reserveStorage.clearReserveStorage()
        reserveProperty.clearReserveProperty()
    }

    const handleSubmitStorage = async values => {

        onUploadingForm(true)

        const { success, data } = await onSubmitProperty(parentValues)

        if (success) {
            const { success: successSecond } = await onSubmit(values, data.id)
            if (success) {
                onUploadingForm(false)
                reserveStorage.clearReserveStorage()
                closeForm({status: successSecond})
            } else {
                closeForm({status: successSecond})
            }
        } else {
            closeForm({status: success})
        }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (target.name !== 'orderDate' && target.name !== 'date' && target.value) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
    
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
            reserveStorage.setReserveStorage(event)
        // }
    }, [])


    // // values with condition reserve
    const valuesFromPropAndState = {
        storageName: data.storageName ? data.storageName : (reserveStorage.storageName ? reserveStorage.storageName : ''),
        storageLocation: data.storageLocation ? data.storageLocation : (reserveStorage.storageLocation ? reserveStorage.storageLocation : ''),
        storageType: data.storageType ? data.storageType : (reserveStorage.storageType ? reserveStorage.storageType : ''),
        region: data.region ? data.region : (reserveStorage.region ? reserveStorage.region : ''),
        republic: data.republic ? data.republic : (reserveStorage.republic ? reserveStorage.republic : ''),
        district: data.district ? data.district : (reserveStorage.district ? reserveStorage.district : '') ,
        address: data.address ? data.address : (reserveStorage.address ? reserveStorage.address : ''),
        cadastre: data.cadastre ? data.cadastre : (reserveStorage.cadastre ? reserveStorage.cadastre : '') ,
        date: data.date ? new Date(data.date * 1000) : (reserveStorage.date ? reserveStorage.date : new Date()),
        orderDate: data.orderDate ? new Date(data.orderDate * 1000) : (reserveStorage.orderDate ? reserveStorage.orderDate : new Date()),
        orderNumber: data.orderNumber ? data.orderNumber : (reserveStorage.orderNumber ? reserveStorage.orderNumber : '') ,
        responsibleEmployee: data.responsibleEmployee ? data.responsibleEmployee : (reserveStorage.responsibleEmployee ? reserveStorage.responsibleEmployee : '') ,
        assets: data.assets ? data.assets : (reserveStorage.assets ? reserveStorage.assets : ''),
        id: data.id ? data.id : ''
    }

    const formik = useFormik({
        initialValues: {
            storageName: valuesFromPropAndState.storageName ,
            storageLocation: valuesFromPropAndState.storageLocation ,
            storageType: valuesFromPropAndState.storageType ,
            republic: valuesFromPropAndState.republic ,
            region: valuesFromPropAndState.region ,
            district: valuesFromPropAndState.district ,
            address: valuesFromPropAndState.address,
            cadastre: valuesFromPropAndState.cadastre ,
            date: valuesFromPropAndState.date ,
            orderNumber: valuesFromPropAndState.orderNumber ,
            orderDate: valuesFromPropAndState.orderDate ,
            responsibleEmployee: valuesFromPropAndState.responsibleEmployee ,
            assets: valuesFromPropAndState.assets,
            id: valuesFromPropAndState.id
        },
        onSubmit: handleSubmitStorage,
        validate: validateOrderForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            storageLocation,
            storageType,
            republic,
            storageName,
            region,
            district,
            address,
            cadastre,
            date,
            orderNumber,
            orderDate,
            responsibleEmployee,
            assets,
        },
        errors: {
            republicErr,
            storageLocationErr,
            storageTypeErr,
            storageNameErr,
            regionErr,
            districtErr,
            addressErr,
            cadastreErr,
            dateErr,
            orderNumberErr,
            orderDateErr,
            responsibleEmployeeErr,
            assetsErr,
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.storage.titles.storage}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'storageLocation'
                                        id = 'storageLocation'
                                        onChange = {primaryOnChange}
                                        error =  {storageLocationErr}
                                        value = {storageLocation}
                                        standart
                                        label = {lang.forms.storage.storageLocation}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'storageName'
                                        id = 'storageName'
                                        onChange = {primaryOnChange}
                                        error =  {storageNameErr}
                                        value = {storageName}
                                        standart
                                        label = {lang.forms.storage.storageName}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'storageType'
                                        id = 'storageType'
                                        onChange = {primaryOnChange}
                                        error =  {storageTypeErr}
                                        value = {storageType}
                                        standart
                                        label = {lang.forms.storage.storageType}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.location}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.republic}
                                        name = 'republic'
                                        onChange = {primaryOnChange}
                                        error = {republicErr}
                                        value = {republic}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.region}
                                        name = 'region'
                                        onChange = {primaryOnChange}
                                        error = {regionErr}
                                        value = {region}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.district}
                                        name = 'district'
                                        onChange = {primaryOnChange}
                                        error = {districtErr}
                                        value = {district}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'address'
                                        id = 'address'
                                        onChange = {primaryOnChange}
                                        error = {addressErr}
                                        value = {address}
                                        standart
                                        label = {lang.forms.branch.address}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.storage.titles.document}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'date'
                                        onChange = {primaryOnChange}
                                        value = {date}
                                        label = {lang.forms.storage.date}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'orderNumber'
                                        id = 'orderNumber'
                                        onChange = {primaryOnChange}
                                        error =  {orderNumberErr}
                                        value = {orderNumber}
                                        standart
                                        label = {lang.forms.storage.orderNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'orderDate'
                                        onChange = {primaryOnChange}
                                        value = {orderDate}
                                        label = {lang.forms.storage.orderDate}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'cadastre'
                                        id = 'cadastre'
                                        onChange = {primaryOnChange}
                                        error =  {cadastreErr}
                                        value = {cadastre}
                                        standart
                                        label = {lang.forms.branch.cadastre}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'responsibleEmployee'
                                        id = 'responsibleEmployee'
                                        onChange = {primaryOnChange}
                                        error = {responsibleEmployeeErr}
                                        value = {responsibleEmployee}
                                        standart
                                        label = {lang.forms.storage.responsibleEmployee}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'assets'
                                        id = 'assets'
                                        onChange = {primaryOnChange}
                                        error = {assetsErr}
                                        value = {assets}
                                        standart
                                        label = {lang.forms.branch.assets}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

StorageForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}

// const OrderProgress = observer(({start}) => {
//     return (
//         <LinearLoading start = {start} progress = {progressOderForm.value}/>
//     )
// })