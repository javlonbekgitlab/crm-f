import React, { useCallback } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import { regexSpace } from '../../../../constants'
// import { getRetrieve } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'
import { inject } from 'mobx-react'
import { validateFiscalModulesForm, onSubmitFiscalModules } from './helperFunctions.js'


const propertyTypes = [
    {
        title: 'Office',
        id: '1'
    },
    {
        title: 'Storage',
        id: '2'
    },
    {
        title: 'fiscal modules',
        id: '3'
    }
]

export const FiscalModulesForm = inject('lang')(({closeForm, data, onUploadingForm, lang, disabled}) => {

    const handleCloseForm = () => {
        closeForm('cancel')
    }

    const handleSubmitFiscalModules = async values => {

        // onUploadingForm(true)

        const { success } = await onSubmitFiscalModules(values)
        // if (successSecond) {
        //     onUploadingForm(false)
        //     reserveFiscalModules.clearReserveFiscalModules()
        //     // getFiscalModules()
        closeForm({success})
        // } else {
        //     closeForm({status: successSecond})
        // }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (typeof target.value === 'string') {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
        // }
    }, [])


    // // values with condition reserve
    // const valuesFromPropAndState = {
    // }

    const formik = useFormik({
        initialValues: {
            typeOfComing: data.typeOfComing ? data.typeOfComing : '',
            dateOfRelease: data.dateOfRelease ? data.dateOfRelease : new Date(),
            typeOfDevice: data.typeOfDevice ? data.typeOfDevice : '',
            serialNumber: data.serialNumber ? data.serialNumber : '',
            numberOfBatch: data.numberOfBatch ? data.numberOfBatch : '',
            dateOfBatch: data.dateOfBatch ? data.dateOfBatch : new Date() ,
            contractNumber: data.contractNumber ? data.contractNumber : '' ,
            dateOfContract: data.dateOfContract ? new Date(data.dateOfContract * 1000) : new Date(),
            numberOfAct: data.numberOfAct ? data.numberOfAct : '' ,
            dateOfAct: data.dateOfAct ? data.dateOfAct : new Date() ,
            responsibleEmployee: data.responsibleEmployee ? data.responsibleEmployee : '' ,
            id: data.id ? data.id : '',
            file_name: {
                name: lang.forms.retrieve.file_name
            }
            // property_id:Values
        },
        onSubmit: handleSubmitFiscalModules,
        validate: validateFiscalModulesForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            serialNumber,
            typeOfDevice,
            typeOfComing,
            dateOfRelease,
            numberOfBatch,
            dateOfBatch,
            contractNumber,
            dateOfContract,
            numberOfAct,
            dateOfAct,
            responsibleEmployee,
            file_name,
        },
        errors: {
            typeOfDeviceErr,
            serialNumberErr,
            typeOfComingErr,
            numberOfBatchErr,
            contractNumberErr,
            numberOfActErr,
            dateOfActErr,
            responsibleEmployeeErr,
            file_nameErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.device}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'typeOfComing'
                                        onChange = {primaryOnChange}
                                        error =  {typeOfComingErr}
                                        disabled = {disabled}
                                        value = {typeOfComing}
                                        standart
                                        label = {lang.forms.retrieve.incomingType}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.retrieve.deviceType}
                                        name = 'typeOfDevice'
                                        onChange = {primaryOnChange}
                                        error = {typeOfDeviceErr}
                                        disabled = {disabled}
                                        value = {typeOfDevice}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'serialNumber'
                                        id = 'serialNumber'
                                        onChange = {primaryOnChange}
                                        error =  {serialNumberErr}
                                        disabled = {disabled}
                                        value = {serialNumber}
                                        standart
                                        label = {lang.forms.technics.serialNumber}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'contractNumber'
                                        id = 'contractNumber'
                                        onChange = {primaryOnChange}
                                        error =  {contractNumberErr}
                                        value = {contractNumber}
                                        disabled = {disabled}
                                        standart
                                        label = {lang.forms.retrieve.agreement}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfContract'
                                        onChange = {primaryOnChange}
                                        value = {dateOfContract}
                                        label = {lang.forms.retrieve.agreementDate}
                                        disabled = {disabled}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfAct'
                                        id = 'numberOfAct'
                                        onChange = {primaryOnChange}
                                        error =  {numberOfActErr}
                                        disabled = {disabled}
                                        value = {numberOfAct}
                                        standart
                                        label = {lang.forms.retrieve.act}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfAct'
                                        onChange = {primaryOnChange}
                                        disabled = {disabled}
                                        value = {dateOfAct}
                                        label = {lang.forms.retrieve.actDate}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.spec}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        label = {lang.forms.retrieve.year}
                                        name = 'dateOfRelease'
                                        onChange = {primaryOnChange}
                                        disabled = {disabled}
                                        value = {dateOfRelease}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfBatch'
                                        onChange = {primaryOnChange}
                                        error = {numberOfBatchErr}
                                        disabled = {disabled}
                                        value = {numberOfBatch}
                                        standart
                                        label = {lang.forms.retrieve.supplyNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfBatch'
                                        onChange = {primaryOnChange}
                                        value = {dateOfBatch}
                                        label = {lang.forms.retrieve.supplyDate}
                                        disabled = {disabled}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.retrieve.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                {/* <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'status'
                                        id = 'status'
                                        onChange = {primaryOnChange}
                                        error = {statusErr}
                                        disabled = {disabled}
                                        value = {status}
                                        standart
                                        label = {lang.forms.retrieve.status}
                                    />
                                </div> */}
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'responsibleEmployee'
                                        id = 'responsibleEmployee'
                                        onChange = {primaryOnChange}
                                        error = {responsibleEmployeeErr}
                                        disabled = {disabled}
                                        value = {responsibleEmployee}
                                        standart
                                        label = {lang.forms.retrieve.responsibleEmployee}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'file_name'
                                        onChange = {primaryOnChange}
                                        value = {file_name}
                                        error = {file_nameErr}
                                        disabled
                                        oldFile = {data.file_url ? {
                                            name: data.file_name,
                                            url: data.file_url
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

FiscalModulesForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}