import { Api, apiUrles } from "../../../../api"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmitFiscalModules = async values => {

    const response = {}

    const {
        serialNumber,
        typeOfDevice,
        typeOfComing,
        dateOfRelease,
        numberOfBatch,
        dateOfBatch,
        contractNumber,
        dateOfContract,
        numberOfAct,
        dateOfAct,
        // responsibleEmployee
    } = values
    
    console.log(JSON.stringify(values))

    const newData = {
        serialNumber,
        typeOfDevice,
        typeOfComing,
        dateOfRelease: new Date(dateOfRelease).getTime() / 1000,
        numberOfBatch,
        dateOfBatch: new Date(dateOfBatch).getTime() / 1000,
        contractNumber,
        dateOfContract: new Date(dateOfContract).getTime() / 1000,
        numberOfAct,
        dateOfAct: new Date(dateOfAct).getTime() / 1000,
        // responsibleEmployee,
        // status,
    }
    
    const token = readLocalStorage('token')

    const res = await Api(apiUrles.fiskalModAdd, newData, 'post', token)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }

    return response
}

export const validateFiscalModulesForm = ({
    contract,
    date_of_contract,
    type_of_application,
    model,
    serial_number,
    fisk_number,
    txkmFile,
    shopName,
    service_center,
    service_employee,
    merchantName,
    userMobile,
    nickName,
    password,
    id
}) => {

    const errors = {}

    // if (contract.length < 1) {
    //     errors.contractErr = 'required'
    // }
    // if (date_of_contract.length < 1) {
    //     errors.date_of_contractErr = 'required'
    // }
    // if (type_of_application.length < 1) {
    //     errors.type_of_applicationErr = 'required'
    // }
    // if (model.length < 1) {
    //     errors.modelErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(serial_number))) {
    //     errors.serial_numberErr = 'onlyNumbers'
    // }
    // if (serial_number.length < 1) {
    //     errors.serial_numberErr = 'required'
    // }
    // if (fisk_number.length < 1) {
    //     errors.fisk_numberErr = 'required'
    // }
    // if (!txkmFile.size && !id) {
    //     errors.txkmFileErr = 'required'
    // }
    // if (shopName.length < 1) {
    //     errors.shopNameErr = 'required'
    // }
    // if (service_center.length < 1) {
    //     errors.service_centerErr = 'required'
    // }
    // if (service_employee.length < 1) {
    //     errors.service_employeeErr = 'required'
    // }
    // if (merchantName.length < 1) {
    //     errors.merchantNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(password))) {
    //     errors.passwordErr = 'onlyNumbers'
    // }
    // if (password.length < 1) {
    //     errors.passwordErr = 'required'
    // }

    return errors
}
