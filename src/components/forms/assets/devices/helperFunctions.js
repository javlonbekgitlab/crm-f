import { Api, apiUrles } from "../../../../api"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmitDevices = async values => {

    const response = {}

    const {
        brand,
        typeofDevice,
        yearOfRelease,
        model,
        serialNumber,
        contractNumber,
        dateOfContract,
        numberOfAct,
        dateOfAct,
        responsibleEmployee,
        state,
        numberOfBatch,
        dateOfBatch,
        typeOfComing,
        id
    } = values
    
    console.log(JSON.stringify(values))

    const newData = {
        brand,
        typeofDevice,
        yearOfRelease: new Date(yearOfRelease).getTime() / 1000,
        model,
        serialNumber,
        contractNumber,
        dateOfContract: new Date(dateOfContract).getTime() / 1000,
        numberOfAct,
        dateOfAct: new Date(dateOfAct).getTime() / 1000,
        responsibleEmployee,
        state,
        numberOfBatch,
        dateOfBatch: new Date(dateOfBatch).getTime() / 1000,
        typeOfComing,
    }
    
    const token = readLocalStorage('token')

    const res = await Api(apiUrles.deviceAdd, newData, 'post', token)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }

    return response
}

export const validateFormDevices = ({
    netAddress,
    brand,
    type,
    name,
    year,
    model,
    serialNumber,
    agreement,
    agreementDate,
    inventoryNumber,
    inventoryLast,
    responsibleEmployee,
    state,
    price,
    specs,
    cpu,
    ram,
    os,
    memory,
    title,
    file_name,
    disableSpec,
}) => {

    console.log(disableSpec)
    const errors = {}

    // if (contract.length < 1) {
    //     errors.contractErr = 'required'
    // }
    // if (date_of_contract.length < 1) {
    //     errors.date_of_contractErr = 'required'
    // }
    // if (type_of_application.length < 1) {
    //     errors.type_of_applicationErr = 'required'
    // }
    // if (model.length < 1) {
    //     errors.modelErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(serial_number))) {
    //     errors.serial_numberErr = 'onlyNumbers'
    // }
    // if (serial_number.length < 1) {
    //     errors.serial_numberErr = 'required'
    // }
    // if (fisk_number.length < 1) {
    //     errors.fisk_numberErr = 'required'
    // }
    // if (!txkmFile.size && !id) {
    //     errors.txkmFileErr = 'required'
    // }
    // if (shopName.length < 1) {
    //     errors.shopNameErr = 'required'
    // }
    // if (service_center.length < 1) {
    //     errors.service_centerErr = 'required'
    // }
    // if (service_employee.length < 1) {
    //     errors.service_employeeErr = 'required'
    // }
    // if (merchantName.length < 1) {
    //     errors.merchantNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(password))) {
    //     errors.passwordErr = 'onlyNumbers'
    // }
    // if (password.length < 1) {
    //     errors.passwordErr = 'required'
    // }

    return errors
}
