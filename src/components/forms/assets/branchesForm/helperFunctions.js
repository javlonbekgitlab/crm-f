import { Api, apiUrles } from "../../../../api"
import { progressForm, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmit = async (values, property_id) => {

    const response = {}

    const {
        republic,
        branchType,
        branchName,
        region,
        district,
        address,
        cadastre,
        agreement,
        agreementDate,
        agreementSum,
        agreementTerm,
        area,
        roomNumber,
        employeeNumber,
        assets,
        id
    } = values
    
    console.log(JSON.stringify(values))

    const newData = {
        agreementDate: new Date(agreementDate).getTime() / 1000,
        republic,
        branchType,
        branchName,
        region,
        district,
        address,
        cadastre,
        agreement,
        agreementSum,
        agreementTerm,
        area,
        roomNumber,
        employeeNumber,
        assets,
    }
    
    const token = readLocalStorage('token')

    let url = ''

    if (id) {
        url = apiUrles.branchesUpdate + id
    } else {
        newData.property_id = property_id
        url = apiUrles.branchesAdd
    }

    const res = await Api(url, newData, 'post', token, uploadProgress)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    progressForm.setProgressForm(0)

    return response
}

export const validateOrderForm = ({
    contract,
    date_of_contract,
    type_of_application,
    model,
    serial_number,
    fisk_number,
    txkmFile,
    shopName,
    service_center,
    service_employee,
    merchantName,
    userMobile,
    nickName,
    password,
    id
}) => {

    const errors = {}

    // if (contract.length < 1) {
    //     errors.contractErr = 'required'
    // }
    // if (date_of_contract.length < 1) {
    //     errors.date_of_contractErr = 'required'
    // }
    // if (type_of_application.length < 1) {
    //     errors.type_of_applicationErr = 'required'
    // }
    // if (model.length < 1) {
    //     errors.modelErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(serial_number))) {
    //     errors.serial_numberErr = 'onlyNumbers'
    // }
    // if (serial_number.length < 1) {
    //     errors.serial_numberErr = 'required'
    // }
    // if (fisk_number.length < 1) {
    //     errors.fisk_numberErr = 'required'
    // }
    // if (!txkmFile.size && !id) {
    //     errors.txkmFileErr = 'required'
    // }
    // if (shopName.length < 1) {
    //     errors.shopNameErr = 'required'
    // }
    // if (service_center.length < 1) {
    //     errors.service_centerErr = 'required'
    // }
    // if (service_employee.length < 1) {
    //     errors.service_employeeErr = 'required'
    // }
    // if (merchantName.length < 1) {
    //     errors.merchantNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(password))) {
    //     errors.passwordErr = 'onlyNumbers'
    // }
    // if (password.length < 1) {
    //     errors.passwordErr = 'required'
    // }

    return errors
}
