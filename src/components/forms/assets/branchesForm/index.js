import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import EditIcon from '@material-ui/icons/Edit'
import { onSubmit, validateOrderForm } from './helperFunctions'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { RadioButtonGroup } from '../../../radioGroup'
import { getBranches } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'
import { LinearLoading } from '../../../loading/linerLoading'
import { reserveBranch, reserveProperty } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { onSubmitProperty } from '../propertyForm/helperFunctions'


const propertyTypes = [
    {
        title: 'Office',
        id: '1'
    },
    {
        title: 'Storage',
        id: '2'
    },
    {
        title: 'fiscal modules',
        id: '3'
    }
]

export const BranchesForm = inject('lang')(({closeForm, data, onUploadingForm, lang, parentValues, disabled}) => {

    const handleCloseForm = () => {
        closeForm()
        reserveBranch.clearReserveBranch()
        reserveProperty.clearReserveProperty()
    }

    const handleSubmitBranch = async values => {

        onUploadingForm(true)

        const { success, data } = await onSubmitProperty(parentValues)

        if (success) {
            const { success: successSecond } = await onSubmit(values, data.id)
            if (successSecond) {
                onUploadingForm(false)
                reserveBranch.clearReserveBranch()
                getBranches()
                closeForm({status: successSecond})
            } else {
                closeForm({status: successSecond})
            }
        } else {
            closeForm({status: success})
        }
    }

    const primaryOnChange = useCallback(({ target }) => {

        let value = ''

        if (target.name !== 'agreementDate' && target.value) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if (value) {
            formik.handleChange(event)
            reserveBranch.setReserveBranch(event)
        // }
    }, [])


    // // values with condition reserve
    const valuesFromPropAndState = {
        branchName: data.branchName ? data.branchName : (reserveBranch.branchName ? reserveBranch.branchName : ''),
        region: data.region ? data.region : (reserveBranch.region ? reserveBranch.region : ''),
        republic: data.republic ? data.republic : (reserveBranch.republic ? reserveBranch.republic : ''),
        branchType: data.branchType ? data.branchType : (reserveBranch.branchType ? reserveBranch.branchType : ''),
        district: data.district ? data.district : (reserveBranch.district ? reserveBranch.district : '') ,
        address: data.address ? data.address : (reserveBranch.address ? reserveBranch.address : ''),
        cadastre: data.cadastre ? data.cadastre : (reserveBranch.cadastre ? reserveBranch.cadastre : '') ,
        agreement: data.agreement ? data.agreement : (reserveBranch.agreement ? reserveBranch.agreement : '') ,
        agreementDate: data.agreementDate ? new Date(data.agreementDate * 1000) : (reserveBranch.agreementDate ? reserveBranch.agreementDate : new Date()),
        agreementSum: data.agreementSum ? data.agreementSum : (reserveBranch.agreementSum ? reserveBranch.agreementSum : '') ,
        agreementTerm: data.agreementTerm ? data.agreementTerm : (reserveBranch.agreementTerm ? reserveBranch.agreementTerm : '') ,
        area: data.area ? data.area : (reserveBranch.area ? reserveBranch.area : '') ,
        roomNumber: data.roomNumber ? data.roomNumber : (reserveBranch.roomNumber ? reserveBranch.roomNumber : '') ,
        employeeNumber: data.employeeNumber ? data.employeeNumber : (reserveBranch.employeeNumber ? reserveBranch.employeeNumber : '') ,
        assets: data.assets ? data.assets : (reserveBranch.assets ? reserveBranch.assets : ''),
        id: data.id ? data.id : '',
    }

    const formik = useFormik({
        initialValues: {
            branchName: valuesFromPropAndState.branchName ,
            branchType: valuesFromPropAndState.branchType ,
            republic: valuesFromPropAndState.republic ,
            region: valuesFromPropAndState.region ,
            district: valuesFromPropAndState.district ,
            address: valuesFromPropAndState.address,
            cadastre: valuesFromPropAndState.cadastre ,
            agreement: valuesFromPropAndState.agreement ,
            agreementDate: valuesFromPropAndState.agreementDate ,
            agreementSum: valuesFromPropAndState.agreementSum ,
            agreementTerm: valuesFromPropAndState.agreementTerm ,
            area: valuesFromPropAndState.area ,
            roomNumber: valuesFromPropAndState.roomNumber ,
            employeeNumber: valuesFromPropAndState.employeeNumber ,
            assets: valuesFromPropAndState.assets,
            id: valuesFromPropAndState.id,
            // property_id:Values
        },
        onSubmit: handleSubmitBranch,
        validate: validateOrderForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            branchType,
            republic,
            branchName,
            region,
            district,
            address,
            cadastre,
            agreement,
            agreementDate,
            agreementSum,
            agreementTerm,
            area,
            roomNumber,
            employeeNumber,
            assets,
        },
        errors: {
            republicErr,
            branchTypeErr,
            branchNameErr,
            regionErr,
            districtErr,
            addressErr,
            cadastreErr,
            agreementErr,
            agreementDateErr,
            agreementSumErr,
            agreementTermErr,
            areaErr,
            roomNumberErr,
            employeeNumberErr,
            assetsErr,
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'cadastre'
                                        id = 'cadastre'
                                        onChange = {primaryOnChange}
                                        error =  {cadastreErr}
                                        value = {cadastre}
                                        standart
                                        label = {lang.forms.branch.cadastre}
                                        disabled = {disabled}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'agreement'
                                        id = 'agreement'
                                        onChange = {primaryOnChange}
                                        error =  {agreementErr}
                                        value = {agreement}
                                        disabled = {disabled}
                                        standart
                                        label = {lang.forms.branch.agreement}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'agreementDate'
                                        onChange = {primaryOnChange}
                                        value = {agreementDate}
                                        label = {lang.forms.branch.agreementDate}
                                        disabled = {disabled}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'agreementSum'
                                        id = 'agreementSum'
                                        onChange = {primaryOnChange}
                                        error =  {agreementSumErr}
                                        disabled = {disabled}
                                        value = {agreementSum}
                                        standart
                                        label = {lang.forms.branch.agreementSum}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'agreementTerm'
                                        id = 'agreementTerm'
                                        onChange = {primaryOnChange}
                                        error =  {agreementTermErr}
                                        disabled = {disabled}
                                        value = {agreementTerm}
                                        standart
                                        label = {lang.forms.branch.agreementTerm}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.location}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.republic}
                                        name = 'republic'
                                        onChange = {primaryOnChange}
                                        error = {republicErr}
                                        disabled = {disabled}
                                        value = {republic}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.region}
                                        name = 'region'
                                        onChange = {primaryOnChange}
                                        error = {regionErr}
                                        disabled = {disabled}
                                        value = {region}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.branch.district}
                                        name = 'district'
                                        onChange = {primaryOnChange}
                                        error = {districtErr}
                                        disabled = {disabled}
                                        value = {district}
                                        data = {propertyTypes}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'address'
                                        id = 'address'
                                        onChange = {primaryOnChange}
                                        error = {addressErr}
                                        disabled = {disabled}
                                        value = {address}
                                        standart
                                        label = {lang.forms.branch.address}
                                    />
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.filial}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'branchName'
                                        id = 'branchName'
                                        onChange = {primaryOnChange}
                                        error =  {branchNameErr}
                                        disabled = {disabled}
                                        value = {branchName}
                                        standart
                                        label = {lang.forms.branch.branchName}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'branchType'
                                        id = 'branchType'
                                        onChange = {primaryOnChange}
                                        error =  {branchTypeErr}
                                        disabled = {disabled}
                                        value = {branchType}
                                        standart
                                        label = {lang.forms.branch.branchType}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.branch.titles.others}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'roomNumber'
                                        id = 'roomNumber'
                                        onChange = {primaryOnChange}
                                        error = {roomNumberErr}
                                        disabled = {disabled}
                                        value = {roomNumber}
                                        standart
                                        label = {lang.forms.branch.roomNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'area'
                                        id = 'area'
                                        onChange = {primaryOnChange}
                                        error = {areaErr}
                                        disabled = {disabled}
                                        value = {area}
                                        standart
                                        label = {lang.forms.branch.area}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'employeeNumber'
                                        id = 'employeeNumber'
                                        onChange = {primaryOnChange}
                                        error = {employeeNumberErr}
                                        disabled = {disabled}
                                        value = {employeeNumber}
                                        standart
                                        label = {lang.forms.branch.employeeNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'assets'
                                        id = 'assets'
                                        onChange = {primaryOnChange}
                                        error = {assetsErr}
                                        disabled = {disabled}
                                        value = {assets}
                                        standart
                                        label = {lang.forms.branch.assets}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonMainCont}>
                            <div className = {styles.buttonCont}>
                                <Button 
                                    onClick = {handleSubmit}
                                >
                                    {lang.forms.buttonTitles.add}
                                </Button>
                                <Button 
                                    onClick={handleCloseForm}
                                >
                                    {lang.forms.buttonTitles.cancel}
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

BranchesForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}