export const getRandomNumber = () => {

    const randomNumber = parseInt(Math.random() * (900000 - 800000) + 800000)

    return randomNumber
}

export const validate = values => {
    
    const {opName, cityAddress, contactManager} = values

    const erros = {}

    if (opName.length < 1) {
        erros.opNameError = 'required'
    }

    if (cityAddress.length < 1) {
        erros.cityAddressError = 'required'
    }

    if (contactManager.length < 1) {
        erros.contactManagerError = 'required'
    }

    return erros
}