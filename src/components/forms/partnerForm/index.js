// import React, { useState } from 'react'
// import styles from '../index.module.sass'
// import { Button } from '../../button'
// import PropTypes from 'prop-types'
// import { getRandomNumber, validate } from './helperFunctions'
// import { useFormik } from 'formik'
// import { Api, apiUrles } from '../../../api'
// import { getPartners } from '../../../store/getDataFromServer'
// import { partList } from '../../../store'
// import jwt from 'jsonwebtoken'
// import { Input } from '../../input'
// import { inject } from 'mobx-react'

// const TOKEN_SECRET = 'kN2aURVcNr_YWd&Y*fg8#?z5&gSh$z2h+L$h52zNu7W7Egz@TTe#x!_KDH5-LNV^'

// export const PartnerForm = inject('lang')(({edit, closeForm, data, lang}) => {

//     const [generatorNumber, setGeneratorNumber] = useState(getRandomNumber())

//     const onSubmit = values => {
//         if (edit) {
//             updatePartner(values)
//         } else {
//             addPartner(values)
//         }
//     }

//     const addPartner = async values => {

//         const {opName, cityAddress, contactManager} = values

//         let checkContactMob = partList.data.filter(item => item.contactMob === generatorNumber)

//         if (checkContactMob.length > 0) {
//             setGeneratorNumber(getRandomNumber())
//         } else {
//             const newData = {
//                 opName,
//                 cityAddress,
//                 contactManager,
//                 contactMob: generatorNumber
//             } 
//             const res = await Api(apiUrles.partnerAdd, newData, 'post')
//             if (res.status === 200) {
//                 closeForm()
//                 getPartners()
//             } else {
//                 console.log(res) 
//             }

//         }
//     }

//     const updatePartner = async values => {
        
//         const {opName, cityAddress, contactManager} = values

//         const {thirdOpMapId, contactMob} = data

//         const updatedPartner = {
//             opName,
//             cityAddress,
//             contactManager,
//             thirdOpMapId,
//         }

//         const authtoken = jwt.sign({
//             opName,
//             cityAddress,
//             contactManager,
//             thirdOpMapId,
//             contactMob
//         }, TOKEN_SECRET)

//         const res = await Api(
//             `${apiUrles.partnerUpdate}${thirdOpMapId}`, 
//             updatedPartner, 'post', 
//             authtoken
//         )
//         if (res.status === 200) {
//             closeForm()
//             getPartners()
//         } else {
//             console.log(res) 
//         }
//     }

//     const formik = useFormik({
//         initialValues: {
//             opName: edit ? data.opName : '',
//             cityAddress: edit ? data.cityAddress : '',
//             contactManager: edit ? data.contactManager : ''
//         },
//         validate,
//         onSubmit
//     })

//     const {
//         errors: {opNameError, cityAddressError, contactManagerError},
//         values : {opName, cityAddress, contactManager},
//         handleChange,
//         handleSubmit
//     } = formik

//     return (
//         <div className = {styles.cont}>
//             <Input 
//                 name = 'opName'
//                 label={lang.forms.partner.partnerName} 
//                 helperText = {opNameError}
//                 error = {opNameError}
//                 onChange = {handleChange}
//                 value = {opName}
//             />
//             <Input 
//                 label={lang.forms.partner.address} 
//                 name = 'cityAddress'
//                 helperText = {cityAddressError}
//                 error = {cityAddressError}
//                 onChange = {handleChange}
//                 value = {cityAddress} 
//             />
//             <Input  
//                 name = 'contactManager'
//                 label={lang.forms.partner.firstMiddleLastName} 
//                 helperText = {contactManagerError}
//                 error = {contactManagerError}
//                 onChange = {formik.handleChange}
//                 value = {contactManager}
//             />
//             {!edit && <Input  
//                 disabled
//                 label={lang.forms.partner.enterSystem} 
//                 value = {generatorNumber} 
//             />}
//             <div className = {styles.buttonCont}>
//                 <Button  
//                     onClick = {handleSubmit}
//                 >
//                     {edit ? lang.forms.buttonTitles.edit : lang.forms.buttonTitles.add}
//                 </Button>
//                 <Button 
//                     onClick={closeForm}
//                 >
//                     {lang.forms.buttonTitles.cancel}
//                 </Button>
//             </div>
//         </div>
//     )
// })

// PartnerForm.propTypes = {
//     edit: PropTypes.bool,
//     closeForm: PropTypes.func
// }