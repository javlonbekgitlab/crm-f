import { Api, apiUrles } from "../../../../api"
import { roless } from "../../../../constants"
import { progressForm, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"
import { transformRoles } from "../../../../store/transformers"
import { getRoles } from "../../../../store/getDataFromServer"

export const onSubmit = async values => {

    const response = {}

    const {
        roles,
        surName,
        name,
        middleName,
        series,
        passportNumber,
        givenPlace,
        givenDate,
        birthDate,
        birthPlace,
        inhabitationPlace,
        education,
        // seriesOfDiploma,
        levelOfEducation,
        numberOfDiploma,
        cardNumber,
        tin,
        pinfl,
        corpTelNumber,
        personalTelNumber,
        social,
        telegramId,
        position,
        corpEmail,
        gmail,
        zoomId,
        weChatId,
        department,
        password
    } = values
    
    
    const newData = {
        surName,
        position: position + '',
        roles: transformRoles(roles), 
        username: name.trim(),
        phoneNumber: parseInt(personalTelNumber),
        department,
        email: corpEmail,
        middleName,
        series,
        passportNumber: parseInt(passportNumber),
        givenPlace,
        givenDate: new Date(givenDate).getTime() / 1000,
        birthDate: new Date(birthDate).getTime() / 1000,
        birthPlace,
        inhabitationPlace,
        education,
        // seriesOfDiploma,
        levelOfEducation,
        numberOfDiploma,
        cardNumber: parseInt(cardNumber),
        tin: parseInt(tin),
        pinfl,
        corpTelNumber: parseInt(corpTelNumber),
        social,
        telegramId,
        gmail,
        zoomId,
        weChatId,
        password
    }
    console.log(newData)
    
    const token = readLocalStorage('token')

    const res = await Api(apiUrles.signUp, newData, 'post', token)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
        console.log(res, 'res')    
    } else {
        console.log(res, 'errr')
        response.success = false
    }

    return response
}

export const validateForm = () => {

    const errors = {}

    // if (username.length < 1) {
    //     errors.usernameErr = 'required'
    // }
    // if (!position) {
    //     errors.positionErr = 'required'
    // }
    // if (roles.length < 1) {
    //     errors.rolesErr = 'required'
    // }
    // if (phoneNumber.length < 1) {
    //     errors.phoneNumberErr = 'required'
    // }
    // if (email.length < 1) {
    //     errors.emailErr = 'required'
    // }
    // if (password.length < 1) {
    //     errors.passwordErr = 'required'
    // }

    return errors
}

export const autoSelectRole = (postId, rolesFromDb = []) => {
    let selectedRolesNames = []
        posts.map(item => {
        if (parseInt(item.id) === parseInt(postId)) {
            selectedRolesNames = item.roles
        }
    })
    const selectedRoles = selectedRolesNames.flatMap(name => rolesFromDb.filter(role => role.name === name)) 
    return selectedRoles
}

export const posts = [
    {
        id: 1,
        title: 'Директор',
        roles: ['hr_mod', 'edm_mod', 'tools_mod', 'object_mod', 'assembly_mod', 'billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod', 'user_assembly_mod', 'user_items_mod', 'cto_mod', 'items_mod', 'e-bank_mod']
    },
    {
        id: 2,
        title: 'Советник директора',
        roles: ['hr_mod', 'edm_mod', 'tools_mod', 'object_mod', 'assembly_mod', 'billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod', 'user_assembly_mod', 'user_items_mod', 'cto_mod', 'items_mod', 'e-bank_mod']
    },
    {
        id: 3,
        title: 'Начальник отдела по работе с персоналом',
        roles: ['hr_mod']
    },
    {
        id: 4,
        title: 'Менеджер по кадром',
        // title: 'Менеджер по работе с персоналом',
        roles: ['hr_mod']
    },
    {
        id: 5,
        title: 'Инспектор по кадром',
        roles: ['hr_mod']
    },
    {
        id: 6,
        title: 'Начальник Юридического отдела',
        roles: ['edm_mod', 'tools_mod']
    },
    {
        id: 7,
        title: 'Юрист консульт',
        roles: ['edm_mod', 'tools_mod']
    },
    {
        id: 8,
        title: 'Главный бухгалтер',
        roles: ['object_mod', 'assembly_mod', 'user_assembly_mod', 'user_items_mod', 'accounting_mod', 'assets_mod']
    },
    {
        id: 9,
        title: 'Материальный бухгалтер',
        roles: ['object_mod', 'assembly_mod', 'user_assembly_mod', 'user_items_mod', 'accounting_mod', 'assets_mod']
    },
    {
        id: 10,
        title: 'Помощник бухгалтера',
        roles: ['object_mod', 'assembly_mod', 'user_assembly_mod', 'user_items_mod', 'accounting_mod', 'assets_mod']
    },
    {
        id: 11,
        title: 'Менеджер по договорам',
        roles: ['contracts_mod']
    },
    {
        id: 12,
        title: 'Заведующий канцелярией',
        roles: ['edm_mod']
    },
    {
        id: 13,
        title: 'Менеджер по документообороту',
        roles: ['edm_mod']
    },
    {
        id: 14,
        title: 'Начальник отдела безопасности',
        roles: ['hr_mod']
    },
    {
        id: 15,
        title: 'Сотрудник по внутреннему контролю',
        roles: ['hr_mod']
    },
    {
        id: 16,
        title: 'Начальник Хозяйственнего отдела',
        roles: ['object_mod']
    },
    {
        id: 17,
        title: 'Заведующий складом',
        roles: ['object_mod']
    },
    {
        id: 18,
        title: 'Специалист по складированию',
        roles: ['object_mod']
    },
    {
        id: 19,
        title: 'Специалист по ВЭД',
        roles: ['object_mod']
    },
    {
        id: 20,
        title: 'Специалист по снабжению',
        roles: ['object_mod']
    },
    {
        id: 21,
        title: 'Заместитель директора  по техническим вопросам',
        roles: ['hr_mod', 'edm_mod', 'tools_mod', 'object_mod', 'assembly_mod', 'billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod', 'user_assembly_mod', 'user_items_mod', 'cto_mod', 'items_mod', 'e-bank_mod']
    },
    {
        id: 22,
        title: 'Начальник отдела по разработке и внедрению ПО и развитию инфраструктуры ',
        roles: ['hr_mod', 'edm_mod', 'tools_mod', 'object_mod', 'assembly_mod', 'billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod', 'user_assembly_mod', 'user_items_mod', 'cto_mod', 'items_mod', 'e-bank_mod']
    },
    {
        id: 23,
        title: 'Адиминистратор безопасности сетей',
        roles: ['user_items_mod']
    },
    {
        id: 24,
        title: 'Администратор системы',
        roles: ['hr_mod', 'edm_mod', 'tools_mod', 'object_mod', 'assembly_mod', 'billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod', 'user_assembly_mod', 'user_items_mod', 'cto_mod', 'items_mod', 'e-bank_mod']
    },
    {
        id: 25,
        title: 'Начальник отдела',
        roles: []
    },
    {
        id: 26,
        title: 'Главный специалист по ремонту',
        roles: ['assembly_mod', 'support_repair_mod', 'user_assembly_mod']
    },
    {
        id: 27,
        title: 'Специалист отдела сервисного обслуживания',
        roles: ['assembly_mod', 'support_repair_mod', 'user_assembly_mod']
    },
    {
        id: 28,
        title: 'Заместитель директора по коммерческим вопросам',
        roles: ['object_mod', 'assembly_mod', 'user_assembly_mod', 'user_items_mod']
    },
    {
        id: 29,
        title: 'Начальник отдела по продвижению продаж',
        roles: ['billing_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod']
    },
    {
        id: 30,
        title: 'Начальник отдела по организации продаж',
        roles: ['billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod']
    },
    {
        id: 31,
        title: 'Начальник отдела сопровождения клиентов',
        roles: ['support_lite_mod', 'support_pro_mod', 'support_repair_mod']
    },
    {
        id: 32,
        title: 'Начальник отдела производства',
        roles: ['assembly_mod', 'user_assembly_mod']
    },
    {
        id: 33,
        title: 'Главный инженер-механик сектора по сборке',
        roles: ['assembly_mod', 'user_assembly_mod']
    },
    {
        id: 34,
        title: 'инженер-механик сектора по сборке',
        roles: ['assembly_mod', 'user_assembly_mod']
    },
    {
        id: 35,
        title: 'Главный инженер-механик сектора заливки ПО',
        roles: ['support_pro_mod']
    },
    {
        id: 36,
        title: 'Инженер-механик сектора заливки ПО',
        roles: ['support_pro_mod']
    },
    {
        id: 37,
        title: 'инженер-механик сектор тестерования',
        roles: ['support_lite_mod','support_pro_mod']
    },
    {
        id: 38,
        title: 'Специалист сектора упаковки',
        roles: ['user_assembly_mod']
    },
    {
        id: 39,
        title: 'Специалист по общим вопросам',
        roles: ['hr_mod', 'edm_mod', 'tools_mod', 'object_mod', 'assembly_mod', 'billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod', 'user_assembly_mod', 'user_items_mod', 'cto_mod', 'items_mod', 'e-bank_mod']
    },
    // {
    //     id: 39,
    //     title: 'Маркетолог',
    //     roles: []
    // },
    // {
    //     id: 40,
    //     title: 'Аналитик',
    //     roles: ['billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod']
    // },
    {
        id: 41,
        title: 'Специалист сектора массовых продаж',
        roles: ['contracts_mod', 'support_lite_mod',]
    },
    {
        id: 42,
        title: 'Главный специалист сектора розничных продаж',
        roles: ['contracts_mod', 'support_lite_mod', 'support_pro_mod']
    },
    {
        id: 43,
        title: 'инженер-механик',
        roles: ['support_lite_mod', 'support_pro_mod']
    },
    {
        id: 44,
        title: 'Главный специалист сектора сопровождения ПО',
        roles: ['billing_mod', 'contracts_mod', 'support_lite_mod', 'support_pro_mod', 'support_repair_mod']
    },
    {
        id: 45,
        title: 'Специалист сектора сопровождения ПО',
        roles: ['support_pro_mod']
    },
    {
        id: 46,
        title: 'Главный специалист сектора по работе с учетными записями',
        roles: ['support_pro_mod']
    },
    {
        id: 47,
        title: 'Специалист сектора по работе с учетными записями',
        roles: ['support_pro_mod']
    },
    {
        id: 48,
        title: 'Главный специалист сектора регистрации и учета устройств в системах Гос. Органов',
        roles: ['billing_mod', 'contracts_mod', 'support_pro_mod']
    },
    {
        id: 49,
        title: 'Специалист сектора регистрации и учета устройств в системах Гос. органов',
        roles: ['billing_mod', 'contracts_mod', 'support_pro_mod']
    },
    {
        id: 50,
        title: 'ЦТО',
        roles: ['support_lite_mod', 'cto_mod']
    }, 
]
