import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { SelectCustom } from '../../../select'
import { onSubmit, validateForm, autoSelectRole, posts } from './helperFunctions'
import { maskUserMobile, regexSpace, roless } from '../../../../constants'
import { inject, observer } from 'mobx-react'
import { Input } from '../../../input'
import { MultiSelect } from '../../../select/multiSelect'
import { Paper } from '@material-ui/core'
import { getRoles, getUsers } from '../../../../store/getDataFromServer'
import { DatePicker } from '../../../datePicker'


export const HrForm = inject('lang', 'rolesList')(observer(({closeForm, data, onUploadingForm, lang, rolesList}) => {

    useEffect(() => getRoles(), [])

    const [loading, setLoading] = useState(false)

    const handleCloseForm = () => closeForm('cancel')

    const handleSubmitHr = async values => {
        onUploadingForm(true)
        console.log(values)
        const { success } = await onSubmit(values)
        if (success) {
            onUploadingForm(false)
            closeForm({success})
        } else {
            onUploadingForm(false)
            closeForm({success})
        }
        getUsers()
    }

    const primaryOnChange = useCallback((nativeEvent) => {

        const { target } = nativeEvent
        let value = undefined
        if (typeof target.value === 'string' && (target.name !== 'username')) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        if (target?.name === 'position') {
            setLoading(true)
            const selectedRoles = autoSelectRole(target.value, rolesList.data)
            const event = { 
                target: { 
                    name: 'roles',
                    value: selectedRoles
                }
            }
            formik.handleChange(event)
            setLoading(false)
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [])


    // // values with condition reserve
    

    const formik = useFormik({
        initialValues: {
            surName: data?.surName ? data.surName : '',
            name: data?.name ? data.name : '',
            middleName: data?.middleName ? data.middleName : '',
            series: data?.series ? data.series : '',
            passportNumber: data?.passportNumber ? data.passportNumber : '',
            givenPlace: data?.givenPlace ? data.givenPlace : '',
            givenDate: data?.givenDate ? new Date(data.givenDate * 1000) : new Date(),
            birthDate: data?.birthDate ? new Date(data.birthDate * 1000) : new Date(),
            birthPlace: data?.birthPlace ? data.birthPlace : '',
            inhabitationPlace: data?.inhabitationPlace ? data.inhabitationPlace : '',
            education: data?.education ? data.education : '',
            // seriesOfDiploma: data?.seriesOfDiploma ? data.seriesOfDiploma : '',
            levelOfEducation: data?.levelOfEducation ? data.levelOfEducation : '',
            numberOfDiploma: data?.numberOfDiploma ? data.numberOfDiploma : '',
            tin: data?.tin ? data.tin : '',
            pinfl: data?.pinfl ? data.pinfl : '',
            cardNumber: data?.cardNumber ? data.cardNumber : '',
            corpTelNumber: data?.corpTelNumber ? data.corpTelNumber : '',
            personalTelNumber: data?.personalTelNumber ? data.personalTelNumber : '',
            social: data?.social ? data.social : '',
            telegramId: data?.telegramId ? data.telegramId : '',
            position: data?.position ? data.position : '',
            corpEmail: data?.corpEmail ? data.corpEmail : '',
            gmail: data?.gmail ? data.gmail : '',
            zoomId: data?.zoomId ? data.zoomId : '',
            weChatId: data?.weChatId ? data.weChatId : '',
            department: data?.department ? data.department : '',
            password: data?.password ? data.password : '',
            roles: data?.roles ? data.roles : []
        },
        onSubmit: handleSubmitHr,
        validate: validateForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            roles,
            surName,
            name,
            middleName,
            series,
            passportNumber,
            givenPlace,
            givenDate,
            birthDate,
            birthPlace,
            inhabitationPlace,
            education,
            // seriesOfDiploma,
            levelOfEducation,
            numberOfDiploma,
            cardNumber,
            tin,
            pinfl,
            corpTelNumber,
            personalTelNumber,
            social,
            telegramId,
            position,
            corpEmail,
            gmail,
            zoomId,
            weChatId,
            department,
            password
        },
        errors: {
            rolesErr,
            surNameErr,
            nameErr,
            middleNameErr,
            seriesErr,
            passportNumberErr,
            givenPlaceErr,
            givenDateErr,
            birthDateErr,
            birthPlaceErr,
            inhabitationPlaceErr,
            educationErr,
            // seriesOfDiplomaErr,
            levelOfEducationErr,
            numberOfDiplomaErr,
            cardNumberErr,
            tinErr,
            pinflErr,
            corpTelNumberErr,
            personalTelNumberErr,
            socialErr,
            telegramIdErr,
            positionErr,
            corpEmailErr,
            gmailErr,
            zoomIdErr,
            weChatIdErr,
            departmentErr,
            passwordErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.column}>
                    <div className = {styles.item}>
                        <Paper 
                            className = {styles.paper}
                            elevation = {3}
                        >
                            <div className = {styles.title}>{lang.forms.hr.passportData}</div>
                            <div className = {styles.inputCont}>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'surName'
                                            error = {surNameErr}
                                            value = {surName}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.surName}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'name'
                                            error = {nameErr}
                                            value = {name}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.name}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'middleName'
                                            error = {middleNameErr}
                                            value = {middleName}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.middleName}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'series'
                                            error = {seriesErr}
                                            value = {series}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.series}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'passportNumber'
                                            error = {passportNumberErr}
                                            value = {passportNumber}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.passportNumber}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'givenPlace'
                                            error = {givenPlaceErr}
                                            value = {givenPlace}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.givenPlace}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'givenDate'
                                            value = {givenDate}
                                            onChange = {primaryOnChange}
                                            label = {lang.forms.hr.givenDate}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'birthDate'
                                            value = {birthDate}
                                            onChange = {primaryOnChange}
                                            label = {lang.forms.hr.birthDate}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'birthPlace'
                                            error = {birthPlaceErr}
                                            value = {birthPlace}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.birthPlace}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'inhabitationPlace'
                                            error = {inhabitationPlaceErr}
                                            value = {inhabitationPlace}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.inhabitationPlace}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.item}>
                        <Paper 
                            className = {styles.paper}
                            elevation = {3}
                        >
                            <div className = {styles.title}>{lang.forms.hr.educationData}</div>
                            <div className = {styles.inputCont}>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'education'
                                            error = {educationErr}
                                            value = {education}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.education}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'numberOfDiploma'
                                            error = {numberOfDiplomaErr}
                                            value = {numberOfDiploma}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.numberOfDiploma}
                                        />
                                    </div>
                                    {/* <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'seriesOfDiploma'
                                            error = {seriesOfDiplomaErr}
                                            value = {seriesOfDiploma}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.seriesOfDiploma}
                                        />
                                    </div> */}
                                </div>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'levelOfEducation'
                                            error = {levelOfEducationErr}
                                            value = {levelOfEducation}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.levelOfEducation}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Paper>
                    </div>
                </div>
                <div className = {styles.column}>
                    <div className = {styles.item}>
                        <Paper 
                            className = {styles.paper}
                            elevation = {3}
                        >
                            <div className = {styles.title}>{lang.forms.hr.corporativeData}</div>
                            <div className = {styles.inputCont}>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <InputMask
                                            name = 'corpTelNumber'
                                            value = {corpTelNumber}
                                            onChange = {primaryOnChange}
                                            standart
                                            mask = {maskUserMobile}
                                        >
                                            {inputProps => <Input 
                                                label = {lang.forms.hr.corpTelNumber}
                                                {...inputProps}
                                                error = {corpTelNumberErr}
                                            />}
                                        </InputMask>
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <InputMask
                                            name = 'personalTelNumber'
                                            value = {personalTelNumber}
                                            onChange = {primaryOnChange}
                                            standart
                                            mask = {maskUserMobile}
                                        >
                                            {inputProps => <Input 
                                                label = {lang.forms.hr.personalTelNumber}
                                                {...inputProps}
                                                error = {personalTelNumberErr}
                                            />}
                                        </InputMask>
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'social'
                                            error = {socialErr}
                                            value = {social}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.social}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'department'
                                            error = {departmentErr}
                                            value = {department}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.department}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <SelectCustom
                                            data = {posts}
                                            title = {lang.forms.hr.post}
                                            name = 'position'
                                            error = {positionErr}
                                            value = {position}
                                            onChange = {primaryOnChange}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'password'
                                            error = {passwordErr}
                                            value = {password}
                                            onChange = {primaryOnChange}
                                            standart
                                            type = 'password'
                                            label = {lang.forms.hr.password}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'corpEmail'
                                            error = {corpEmailErr}
                                            value = {corpEmail}
                                            onChange = {primaryOnChange}
                                            type = 'email'
                                            standart
                                            label = {lang.forms.hr.corpEmail}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'gmail'
                                            error = {gmailErr}
                                            value = {gmail}
                                            onChange = {primaryOnChange}
                                            type = 'email'
                                            standart
                                            label = {lang.forms.hr.gmail}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'zoomId'
                                            error = {zoomIdErr}
                                            value = {zoomId}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.zoomId}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'weChatId'
                                            error = {weChatIdErr}
                                            value = {weChatId}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.weChatId}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'telegramId'
                                            error = {telegramIdErr}
                                            value = {telegramId}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.telegramId}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Paper>
                    </div>
                    <div className = {styles.item}>
                        <Paper 
                            className = {styles.paper}
                            elevation = {3}
                        >
                            <div className = {styles.title}>{lang.forms.hr.taxData}</div>
                            <div className = {styles.inputCont}>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'tin'
                                            error = {tinErr}
                                            value = {tin}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.order.tin}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'pinfl'
                                            error = {pinflErr}
                                            value = {pinfl}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.order.pinfl}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.column}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'cardNumber'
                                            error = {cardNumberErr}
                                            value = {cardNumber}
                                            onChange = {primaryOnChange}
                                            standart
                                            label = {lang.forms.hr.cardNumber}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Paper>
                    </div>
                </div>
            </div>
            <div className = {styles.buttonCont}>
                <Button
                    onClick = {handleSubmit}
                >
                    {lang.forms.buttonTitles.add}
                </Button>
                <Button
                    onClick={handleCloseForm}
                >
                    {lang.forms.buttonTitles.cancel}
                </Button>
            </div>
        </div>
    )
}))

HrForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}