import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { SelectCustom } from '../../../select'
import { onSubmit, validateForm, autoSelectRole, posts } from './helperFunctions'
import { maskUserMobile, regexSpace, roless } from '../../../../constants'
import { inject, observer } from 'mobx-react'
import { Input } from '../../../input'
import { MultiSelect } from '../../../select/multiSelect'
import { Paper } from '@material-ui/core'
import { getRoles, getUsers } from '../../../../store/getDataFromServer'


export const HrForm = inject('lang', 'rolesList')(observer(({closeForm, data, onUploadingForm, lang, rolesList}) => {

    useEffect(() => getRoles(), [])

    const [loading, setLoading] = useState(false)

    const handleCloseForm = () => closeForm('cancel')

    const handleSubmitHr = async values => {
        onUploadingForm(true)
        const { success } = await onSubmit(values)
        if (success) {
            onUploadingForm(false)
            closeForm({success})
        } else {
            onUploadingForm(false)
            closeForm({success})
        }
        getUsers()
    }

    const primaryOnChange = useCallback((nativeEvent) => {

        const { target } = nativeEvent
        let value = undefined
        if (typeof target.value === 'string' && (target.name !== 'username')) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        if (target?.name === 'position') {
            setLoading(true)
            const selectedRoles = autoSelectRole(target.value, rolesList.data)
            const event = { 
                target: { 
                    name: 'roles',
                    value: selectedRoles
                }
            }
            formik.handleChange(event)
            setLoading(false)
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [])


    // // values with condition reserve
    

    const formik = useFormik({
        initialValues: {
            username: data.username ? data.username : '',
            phoneNumber: data.phoneNumber ? data.phoneNumber : '',
            roles: [],
            position: data.position ? data.position : '',
            department: data.department ? data.department : '',
            email: data.email ? data.email : '',
            password: '',
            id: data._id ? data._id : ''
        },
        onSubmit: handleSubmitHr,
        validate: validateForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            position,
            roles,
            username,
            phoneNumber,
            department,
            email,
            password
        },
        errors: {
            positionErr,
            rolesErr,
            usernameErr,
            phoneNumberErr,
            departmentErr,
            emailErr,
            passwordErr
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <Paper 
                    className = {styles.paper}
                    elevation = {3}
                >
                    <div className = {styles.title}>{lang.forms.hr.title}</div>
                    <div className = {styles.inputCont}>
                        <div className = {styles.column}>
                            <div className = {styles.itemChilds}>
                                <Input
                                    name = 'username'
                                    error = {usernameErr}
                                    value = {username}
                                    onChange = {primaryOnChange}
                                    standart
                                    label = {lang.forms.hr.fullName}
                                />
                            </div>
                        </div>
                        <div className = {styles.column}>
                            <div className = {styles.itemChilds}>
                                <SelectCustom
                                    data = {posts}
                                    title = {lang.forms.hr.post}
                                    name = 'position'
                                    error = {positionErr}
                                    value = {position}
                                    onChange = {primaryOnChange}
                                />
                            </div>
                        </div>
                    </div>
                    <div className = {styles.inputCont}>
                        <div className = {styles.column}>
                            <div className = {styles.itemChilds}>
                                <InputMask
                                    name = 'phoneNumber'
                                    value = {phoneNumber}
                                    onChange = {primaryOnChange}
                                    standart
                                    mask = {maskUserMobile}
                                    id = 'phoneNumber'
                                >
                                    {inputProps => <Input 
                                        label = {lang.forms.hr.phoneNumber}
                                        {...inputProps}
                                        error = {phoneNumberErr}
                                    />}
                                </InputMask>
                            </div>
                            <div className = {styles.itemChilds}>
                                <Input
                                    name = 'department'
                                    error = {departmentErr}
                                    value = {department}
                                    onChange = {primaryOnChange}
                                    standart
                                    label = {lang.forms.hr.structure}
                                />
                            </div>
                        </div>
                    </div>
                    <div className = {styles.inputCont}>
                        <div className = {styles.column}>
                            <div className = {styles.itemChilds}>
                                <Input
                                    name = 'email'
                                    error = {emailErr}
                                    value = {email}
                                    onChange = {primaryOnChange}
                                    standart
                                    label = {lang.forms.hr.email}
                                />
                            </div>
                            <div className = {styles.itemChilds}>
                                <Input
                                    name = 'password'
                                    error = {passwordErr}
                                    value = {password}
                                    onChange = {primaryOnChange}
                                    standart
                                    type = 'password'
                                    label = {lang.forms.hr.password}
                                />
                            </div>
                        </div>
                    </div>  
                    <div className = {styles.inputCont}>
                        <div className = {styles.column}>
                            <div className = {styles.itemChilds}>
                                <MultiSelect
                                    label = {lang.forms.hr.roles}
                                    options = {rolesList.data}
                                    onChange = {primaryOnChange}
                                    error = {rolesErr}
                                    value = {roles}
                                    loading = {loading}
                                    name = 'roles'
                                />
                            </div>
                        </div>
                    </div>
                </Paper>
                <div className = {styles.buttonCont}>
                    <Button
                        onClick = {handleSubmit}
                    >
                        {lang.forms.buttonTitles.add}
                    </Button>
                    <Button
                        onClick={handleCloseForm}
                    >
                        {lang.forms.buttonTitles.cancel}
                    </Button>
                </div>
            </div>
        </div>
    )
}))

HrForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}