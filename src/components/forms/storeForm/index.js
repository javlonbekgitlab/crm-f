// import React from 'react'
// import styles from '../index.module.sass'
// import { Button } from '../../button'
// import PropTypes from 'prop-types'
// import { useFormik } from 'formik'
// import { validate } from './helperFunctions'
// import { Api, apiUrles } from '../../../api'
// import { getStoreList } from '../../../store/getDataFromServer'
// import { Input } from '../../input'
// import { inject } from 'mobx-react'

// export const StoreForm = inject('lang')(({edit, closeForm, data, parentId, lang}) => {

//     const onSubmit = values => {
//         if (edit) {
//             updateStore(values)
//         } else {
//             addStore(values)
//         }
//     }

//     const addStore = async values => {

//         const {shopName, cityAddress} = values

//         const newData = {
//             shopName,
//             cityAddress,
//             thirdMerMapId: parentId,
//         }
//         const resStore = await Api(apiUrles.storeAdd, newData, 'post')

//         if (resStore.status === 200) {
//             closeForm()
//             getStoreList()
//         } else {
//             alert(resStore.toString()) 
//         }
//     }

//     const updateStore = async values => {

//         const {shopName, cityAddress} = values

//         const newData = {
//             shopName,
//             cityAddress
//         }

//         const resStore = await Api(`${apiUrles.storeUpdate}${data.thirdShopMapId}`, newData, 'post')

//         if (resStore.status === 200) {
//             closeForm()
//             getStoreList()
//         } else {
//             alert(resStore.toString()) 
//         }
//     }

//     const formik = useFormik({
//         initialValues: {
//             shopName: edit ? data.shopName : '',
//             cityAddress: edit ? data.cityAddress : ''
//         },
//         validate,
//         onSubmit
//     })

//     const { 
//         errors: {shopNameError, cityAddressError},
//         values: {shopName, cityAddress},
//         handleChange,
//         handleSubmit
//     } = formik

//     return (
//         <div className = {styles.cont}>
//             <Input 
//                 label={lang.forms.store.shopName} 
//                 name = 'shopName'
//                 helperText = {shopNameError}
//                 error = {shopNameError}
//                 onChange = {handleChange}
//                 value = {shopName}
//             />
//             <Input  
//                 label={lang.forms.store.cityAddress} 
//                 name = 'cityAddress'
//                 helperText = {cityAddressError}
//                 error = {cityAddressError}
//                 onChange = {handleChange}
//                 value = {cityAddress}
//             />
//             <div className = {styles.buttonCont}>
//                 <Button
//                     onClick = {handleSubmit}
//                 >
//                     {edit ? lang.forms.buttonTitles.edit : lang.forms.buttonTitles.add}
//                 </Button>
//                 <Button 
//                     onClick={closeForm}
//                 >
//                     {lang.forms.buttonTitles.cancel}
//                 </Button>
//             </div>
//         </div>
//     )
// })

// StoreForm.propTypes = {
//     edit: PropTypes.bool,
//     closeForm: PropTypes.func,
//     parentId: PropTypes.string
// }