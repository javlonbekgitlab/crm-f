export const validate = values => {
    
    const {shopName, cityAddress } = values

    const erros = {}

    if (shopName.length <= 2) {
        erros.shopNameError = 'min 2 symb required'
    }

    if (cityAddress.length <= 2) {
        erros.cityAddressError = 'min 2 symb required'
    }

    return erros
}