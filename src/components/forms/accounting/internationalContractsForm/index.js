import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import EditIcon from '@material-ui/icons/Edit'
import { onSubmit, validateForm } from './helperFunctions'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { reserveOrder } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { Api } from '../../../../api'
import { DatePicker } from '../../../datePicker'
import { ModalForm } from '../../../modalForm'

export const InternationalContractsForm = inject('lang')(
    ({
        onCloseForm, 
        data, 
        lang 
    }) => {

    const handleCloseForm = () => onCloseForm('cancel')

    const handleSubmitAdmin = async values => {
        const { success } = await onSubmit(values)
        onCloseForm({success})
    }

    const primaryOnChange = useCallback(({ target }) => {
        let value = ''
        if (target.files) {
            if (target.files.length > 0) {
                value = target.files[0]
            }
        } else {
            if (target.name === 'phoneNumber') {
                value = `${parseInt(target.value.replace(regexSpace, ''))}`
            } else {
                value = target.value
            }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [])

    const formik = useFormik({
        initialValues:  {
            uniqueCode: '',
            attachedEmployee: '',
            type: '',
            kindOfContract: '',
            dateOfContract: data?.dateOfContract ? new Date(data.dateOfContract * 1000) : new Date(),
            numberOfContract: '',
            subjectOfContract: '',
            term: data?.term ? new Date(data.term * 1000) : new Date(),
            quantityOfSides: '',
            secondSide: '',
            roleOfSecondSide: '',
            thirdSide: '',
            roleOfThirdSide: '',
            score: '',
            sum: '',
            equivalent: '',
            periodicity: '',
            coefficientOfPrepayment: '',
            paymentOfPrepayment: '',
            dateOfPrepayment: data?.dateOfPrepayment ? new Date(data.dateOfPrepayment * 1000) : new Date(),
            numberOfPrepaymentOrder: '',
            finalPayment: '',
            dateOfFinalPayment: data?.dateOfFinalPayment ? new Date(data.dateOfFinalPayment * 1000) : new Date(),
            numberOfFinalPaymentOrder: '',
            dateOfInvoice: data?.dateOfInvoice ? new Date(data.dateOfInvoice * 1000) : new Date(),
            numberOfInvoice: '',
            dateOfActCompletionAcceptanceTransfer: data?.dateOfActCompletionAcceptanceTransfer ? new Date(data.dateOfActCompletionAcceptanceTransfer * 1000) : new Date(),
            state: '',
            statusOfSecondSide: '',
            name: '',
            tin: '',
            address: '',
            bank: '',
            mfo: '',
            rc: '',
            oked: '',
            phoneNumber: '',
            web: '',
            email: ''
        },
        onSubmit: handleSubmitAdmin,
        validate: validateForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            uniqueCode,
            attachedEmployee,
            type,
            kindOfContract,
            dateOfContract,
            numberOfContract,
            subjectOfContract,
            term,
            quantityOfSides,
            secondSide,
            roleOfSecondSide,
            thirdSide,
            roleOfThirdSide,
            score,
            sum,
            equivalent,
            periodicity,
            coefficientOfPrepayment,
            paymentOfPrepayment,
            dateOfPrepayment,
            numberOfPrepaymentOrder,
            finalPayment,
            dateOfFinalPayment,
            numberOfFinalPaymentOrder,
            dateOfInvoice,
            numberOfInvoice,
            dateOfActCompletionAcceptanceTransfer,
            state,
            statusOfSecondSide,
            name,
            tin,
            address,
            bank,
            mfo,
            rc,
            oked,
            phoneNumber,
            web,
            email,
        },
        errors: {
            uniqueCodeErr,
            attachedEmployeeErr,
            typeErr,
            kindOfContractErr,
            dateOfContractErr,
            numberOfContractErr,
            subjectOfContractErr,
            termErr,
            quantityOfSidesErr,
            secondSideErr,
            roleOfSecondSideErr,
            thirdSideErr,
            roleOfThirdSideErr,
            scoreErr,
            sumErr,
            equivalentErr,
            periodicityErr,
            coefficientOfPrepaymentErr,
            paymentOfPrepaymentErr,
            dateOfPrepaymentErr,
            numberOfPrepaymentOrderErr,
            finalPaymentErr,
            dateOfFinalPaymentErr,
            numberOfFinalPaymentOrderErr,
            dateOfInvoiceErr,
            numberOfInvoiceErr,
            dateOfActCompletionAcceptanceTransferErr,
            stateErr,
            statusOfSecondSideErr,
            nameErr,
            tinErr,
            addressErr,
            bankErr,
            mfoErr,
            rcErr,
            okedErr,
            phoneNumberErr,
            webErr,
            emailErr,
        },
        handleSubmit
    } = formik

    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnRow}>
                                <div className = {styles.topColumnItem}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'uniqueCode'
                                            onChange = {primaryOnChange}
                                            error = {uniqueCodeErr}
                                            value = {uniqueCode}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.uniqueCode}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <SelectCustom
                                            name = 'attachedEmployee'
                                            error = {attachedEmployeeErr}
                                            value = {attachedEmployee}
                                            onChange = {primaryOnChange}
                                            title = {lang.table.columnsName.adminstrativeContracts.attachedEmployee}
                                            data = {[{id:1, title: 'test'}]}
                                            // onOpen = {handleOpenRegions}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <SelectCustom
                                            name = 'type'
                                            error = {typeErr}
                                            value = {type}
                                            onChange = {primaryOnChange}
                                            title = {lang.table.columnsName.adminstrativeContracts.type}
                                            data = {[{id:1, title: 'test'}]}
                                            // onOpen = {handleOpenRegions}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <SelectCustom
                                            name = 'kindOfContract'
                                            error = {kindOfContractErr}
                                            value = {kindOfContract}
                                            onChange = {primaryOnChange}
                                            title = {lang.table.columnsName.adminstrativeContracts.kindOfContract}
                                            data = {[{id:1, title: 'test'}]}
                                            // onOpen = {handleOpenRegions}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'dateOfContract'
                                            onChange = {primaryOnChange}
                                            value = {dateOfContract}
                                            label = {lang.table.columnsName.adminstrativeContracts.dateOfContract}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'numberOfContract'
                                            onChange = {primaryOnChange}
                                            error = {numberOfContractErr}
                                            value = {numberOfContract}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.numberOfContract}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'subjectOfContract'
                                            onChange = {primaryOnChange}
                                            error = {subjectOfContractErr}
                                            value = {subjectOfContract}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.subjectOfContract}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'term'
                                            onChange = {primaryOnChange}
                                            value = {term}
                                            label = {lang.table.columnsName.adminstrativeContracts.term}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'quantityOfSides'
                                            onChange = {primaryOnChange}
                                            error = {quantityOfSidesErr}
                                            value = {quantityOfSides}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.quantityOfSides}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'secondSide'
                                            onChange = {primaryOnChange}
                                            error = {secondSideErr}
                                            value = {secondSide}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.secondSide}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'roleOfSecondSide'
                                            onChange = {primaryOnChange}
                                            error = {roleOfSecondSideErr}
                                            value = {roleOfSecondSide}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.roleOfSecondSide}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'thirdSide'
                                            onChange = {primaryOnChange}
                                            error = {thirdSideErr}
                                            value = {thirdSide}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.thirdSide}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'roleOfThirdSide'
                                            onChange = {primaryOnChange}
                                            error = {roleOfThirdSideErr}
                                            value = {roleOfThirdSide}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.roleOfThirdSide}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.topColumnItem}>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'score'
                                            onChange = {primaryOnChange}
                                            error = {scoreErr}
                                            value = {score}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.score}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'sum'
                                            onChange = {primaryOnChange}
                                            error = {sumErr}
                                            value = {sum}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.sum}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'equivalent'
                                            onChange = {primaryOnChange}
                                            error = {equivalentErr}
                                            value = {equivalent}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.equivalent}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'periodicity'
                                            onChange = {primaryOnChange}
                                            error = {periodicityErr}
                                            value = {periodicity}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.periodicity}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'coefficientOfPrepayment'
                                            onChange = {primaryOnChange}
                                            error = {coefficientOfPrepaymentErr}
                                            value = {coefficientOfPrepayment}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.coefficientOfPrepayment}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'paymentOfPrepayment'
                                            onChange = {primaryOnChange}
                                            error = {paymentOfPrepaymentErr}
                                            value = {paymentOfPrepayment}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.paymentOfPrepayment}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'dateOfPrepayment'
                                            onChange = {primaryOnChange}
                                            value = {dateOfPrepayment}
                                            label = {lang.table.columnsName.adminstrativeContracts.dateOfPrepayment}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'numberOfPrepaymentOrder'
                                            onChange = {primaryOnChange}
                                            error = {numberOfPrepaymentOrderErr}
                                            value = {numberOfPrepaymentOrder}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.numberOfPrepaymentOrder}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'finalPayment'
                                            onChange = {primaryOnChange}
                                            error = {finalPaymentErr}
                                            value = {finalPayment}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.finalPayment}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'dateOfFinalPayment'
                                            onChange = {primaryOnChange}
                                            value = {dateOfFinalPayment}
                                            label = {lang.table.columnsName.adminstrativeContracts.dateOfFinalPayment}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'numberOfFinalPaymentOrder'
                                            onChange = {primaryOnChange}
                                            error = {numberOfFinalPaymentOrderErr}
                                            value = {numberOfFinalPaymentOrder}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.numberOfFinalPaymentOrder}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'dateOfInvoice'
                                            onChange = {primaryOnChange}
                                            value = {dateOfInvoice}
                                            label = {lang.table.columnsName.adminstrativeContracts.dateOfInvoice}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'numberOfInvoice'
                                            onChange = {primaryOnChange}
                                            error = {numberOfInvoiceErr}
                                            value = {numberOfInvoice}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.numberOfInvoice}
                                        />
                                    </div>
                                </div>
                                <div className = {styles.topColumnItem}>
                                    <div className = {styles.itemChilds}>
                                        <DatePicker
                                            name = 'dateOfActCompletionAcceptanceTransfer'
                                            onChange = {primaryOnChange}
                                            value = {dateOfActCompletionAcceptanceTransfer}
                                            label = {lang.table.columnsName.adminstrativeContracts.dateOfActCompletionAcceptanceTransfer}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'state'
                                            onChange = {primaryOnChange}
                                            error = {stateErr}
                                            value = {state}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.state}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'statusOfSecondSide'
                                            onChange = {primaryOnChange}
                                            error = {statusOfSecondSideErr}
                                            value = {statusOfSecondSide}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.statusOfSecondSide}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'name'
                                            onChange = {primaryOnChange}
                                            error = {nameErr}
                                            value = {name}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.name}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'bank'
                                            onChange = {primaryOnChange}
                                            error = {bankErr}
                                            value = {bank}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.bank}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'address'
                                            onChange = {primaryOnChange}
                                            error = {addressErr}
                                            value = {address}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.address}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'tin'
                                            onChange = {primaryOnChange}
                                            error = {tinErr}
                                            value = {tin}
                                            standart
                                            label = {lang.forms.order.tinOrPinfl}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'mfo'
                                            onChange = {primaryOnChange}
                                            error = {mfoErr}
                                            value = {mfo}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.mfo}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'rc'
                                            onChange = {primaryOnChange}
                                            error = {rcErr}
                                            value = {rc}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.rc}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'oked'
                                            onChange = {primaryOnChange}
                                            error = {okedErr}
                                            value = {oked}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.oked}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <InputMask
                                            onChange = {primaryOnChange}
                                            standart
                                            mask = {maskUserMobile}
                                            name = 'phoneNumber'
                                            value = {phoneNumber}
                                        >
                                            {inputProps => <Input 
                                                label = {lang.forms.order.telephone}
                                                {...inputProps}
                                                error = {phoneNumberErr}
                                            />}
                                        </InputMask>
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'web'
                                            onChange = {primaryOnChange}
                                            error = {webErr}
                                            value = {web}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.web}
                                        />
                                    </div>
                                    <div className = {styles.itemChilds}>
                                        <Input
                                            name = 'email'
                                            onChange = {primaryOnChange}
                                            error = {emailErr}
                                            value = {email}
                                            standart
                                            label = {lang.table.columnsName.adminstrativeContracts.email}
                                        />
                                    </div>
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonCont}>
                            <Button 
                                onClick = {handleSubmit}
                            >
                                {lang.forms.buttonTitles.add}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

InternationalContractsForm.propTypes = {
    onCloseForm: PropTypes.func,
    data: PropTypes.object,
    onUploadingForm: PropTypes.func
}
