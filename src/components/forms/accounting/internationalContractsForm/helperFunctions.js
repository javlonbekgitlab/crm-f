import { Api, apiUrles } from "../../../../api"
import { localStorageKeys, regexOnlyNumbers } from "../../../../constants"
import { progressForm, snackbar, snackbarStatus, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"
import { lang } from "../../../../languages/lang"

export const onSubmit = async values => {

    const response = {}

    const {
        uniqueCode,
        attachedEmployee,
        type,
        kindOfContract,
        dateOfContract,
        numberOfContract,
        subjectOfContract,
        term,
        quantityOfSides,
        secondSide,
        roleOfSecondSide,
        thirdSide,
        roleOfThirdSide,
        score,
        sum,
        equivalent,
        periodicity,
        coefficientOfPrepayment,
        paymentOfPrepayment,
        dateOfPrepayment,
        numberOfPrepaymentOrder,
        finalPayment,
        dateOfFinalPayment,
        numberOfFinalPaymentOrder,
        dateOfInvoice,
        numberOfInvoice,
        dateOfActCompletionAcceptanceTransfer,
        state,
        statusOfSecondSide,
        name,
        tin,
        address,
        bank,
        mfo,
        rc,
        oked,
        phoneNumber,
        web,
        email,
    } = values

    const newData = {

        uniqueCode,
        attachedEmployee,
        type,
        kindOfContract,
        dateOfContract: new Date(dateOfContract).getTime() / 1000,
        numberOfContract,
        subjectOfContract,
        term: new Date(term).getTime() / 1000,
        quantityOfSides,
        secondSide,
        roleOfSecondSide,
        thirdSide,
        roleOfThirdSide,
        score,
        sum,
        equivalent,
        periodicity,
        coefficientOfPrepayment,
        paymentOfPrepayment,
        dateOfPrepayment: new Date(dateOfPrepayment).getTime() / 1000,
        numberOfPrepaymentOrder,
        finalPayment,
        dateOfFinalPayment: new Date(dateOfFinalPayment).getTime() / 1000,
        numberOfFinalPaymentOrder,
        dateOfInvoice: new Date(dateOfInvoice).getTime() / 1000,
        numberOfInvoice,
        dateOfActCompletionAcceptanceTransfer: new Date(dateOfActCompletionAcceptanceTransfer).getTime() / 1000,
        state,
        statusOfSecondSide,
        name,
        tin,
        address,
        bank,
        mfo,
        rc,
        oked,
        phoneNumber: phoneNumber,
        web,
        email,
    } 
    const token = readLocalStorage('token')

    const res = await Api(apiUrles.internationalContractsAdd, newData, 'post', token)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    // progressForm.setProgressForm(0)

    return response
}

export const validateForm = ({
    userMobile,
    nickName,
    userPasswd,
    file_name_pass,
    id
}) => {

    const errors = {}

    // if (!file_name_pass.size) {
    //     errors.file_name_passErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(userPasswd))) {
    //     errors.userPasswdErr = 'onlyNumbers'
    // }
    // if (userPasswd.length < 6 || userPasswd.length > 6) {
    //     errors.userPasswdErr = 'required 6 symb'
    // }

    return errors
}



