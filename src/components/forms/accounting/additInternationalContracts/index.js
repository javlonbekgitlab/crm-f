import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import EditIcon from '@material-ui/icons/Edit'
import { onSubmit, validateForm } from './helperFunctions'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { reserveOrder } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { Api } from '../../../../api'
import { DatePicker } from '../../../datePicker'
import { ModalForm } from '../../../modalForm'

export const AdditionalInternationalContractsForm = inject('lang')(
    ({
        onCloseForm, 
        data, 
        lang,
        mainContract 
    }) => {

    const handleCloseForm = () => onCloseForm('cancel')

    const handleSubmitAdmin = async values => {
        console.log(values)
        const { success } = await onSubmit(values)
        onCloseForm({success})
    }

    const primaryOnChange = useCallback(({ target }) => {
        let value = ''
        if (target.files) {
            if (target.files.length > 0) {
                value = target.files[0]
            }
        } else {
            if (target.name === 'userMobile') {
                value = target.value.replace(regexSpace, '')
            } else {
                value = target.value
            }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [])

    const formik = useFormik({
        initialValues:  {
            uniqueCode: '',
            attachedEmployee: '',
            dateOfContract: mainContract?.dateOfContract ? new Date(mainContract.dateOfContract * 1000) : new Date(),
            numberOfContract: mainContract?.numberOfContract ? mainContract.numberOfContract : '',
            subjectOfContract: mainContract?.subjectOfContract ? mainContract.subjectOfContract : '',
            dateOfAdditionalContract: data?.dateOfContract ? new Date(data.dateOfContract * 1000) : new Date(),
            numberOfAdditionalContract: '',
            subjectOfAdditionalContract: '',
            mainContractId: mainContract._id
        },
        onSubmit: handleSubmitAdmin,
        validate: validateForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            uniqueCode,
            attachedEmployee,
            dateOfContract,
            numberOfContract,
            subjectOfContract,
            dateOfAdditionalContract,
            numberOfAdditionalContract,
            subjectOfAdditionalContract,
        },
        errors: {
            uniqueCodeErr,
            attachedEmployeeErr,
            dateOfContractErr,
            numberOfContractErr,
            subjectOfContractErr,
            dateOfAdditionalContractErr,
            numberOfAdditionalContractErr,
            subjectOfAdditionalContractErr,
        },
        handleSubmit
    } = formik

    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'uniqueCode'
                                        onChange = {primaryOnChange}
                                        error = {uniqueCodeErr}
                                        value = {uniqueCode}
                                        standart
                                        label = {lang.table.columnsName.adminstrativeContracts.uniqueCode}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        name = 'attachedEmployee'
                                        error = {attachedEmployeeErr}
                                        value = {attachedEmployee}
                                        onChange = {primaryOnChange}
                                        title = {lang.table.columnsName.adminstrativeContracts.attachedEmployee}
                                        data = {[{id:1, title: 'test'}]}
                                        // onOpen = {handleOpenRegions}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfContract'
                                        onChange = {primaryOnChange}
                                        value = {dateOfContract}
                                        label = {lang.table.columnsName.adminstrativeContracts.dateOfContract}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfContract'
                                        onChange = {primaryOnChange}
                                        error = {numberOfContractErr}
                                        value = {numberOfContract}
                                        standart
                                        label = {lang.table.columnsName.adminstrativeContracts.numberOfContract}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'subjectOfContract'
                                        onChange = {primaryOnChange}
                                        error = {subjectOfContractErr}
                                        value = {subjectOfContract}
                                        standart
                                        label = {lang.table.columnsName.adminstrativeContracts.subjectOfContract}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'dateOfAdditionalContract'
                                        onChange = {primaryOnChange}
                                        value = {dateOfAdditionalContract}
                                        label = {lang.table.columnsName.adminstrativeAdditionalContracts.dateOfAdditionalContract}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'numberOfAdditionalContract'
                                        onChange = {primaryOnChange}
                                        error = {numberOfAdditionalContractErr}
                                        value = {numberOfAdditionalContract}
                                        standart
                                        label = {lang.table.columnsName.adminstrativeAdditionalContracts.numberOfAdditionalContract}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'subjectOfAdditionalContract'
                                        onChange = {primaryOnChange}
                                        error = {subjectOfAdditionalContractErr}
                                        value = {subjectOfAdditionalContract}
                                        standart
                                        label = {lang.table.columnsName.adminstrativeAdditionalContracts.subjectOfAdditionalContract}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonCont}>
                            <Button 
                                onClick = {handleSubmit}
                            >
                                {lang.forms.buttonTitles.add}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

AdditionalInternationalContractsForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func,
    mainContract: PropTypes.object
}
