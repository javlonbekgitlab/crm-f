import { Api, apiUrles } from "../../../../api"
import { localStorageKeys, regexOnlyNumbers } from "../../../../constants"
import { progressForm, snackbar, snackbarStatus, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"
import { lang } from "../../../../languages/lang"

export const onSubmit = async values => {

    const response = {}

    const {
        uniqueCode,
        attachedEmployee,
        // dateOfContract,
        // numberOfContract,
        // subjectOfContract,
        dateOfAdditionalContract,
        numberOfAdditionalContract,
        subjectOfAdditionalContract,
        mainContractId
    } = values

    let newData = {
        uniqueCode,
        // attachedEmployee,
        // dateOfContract: new Date(dateOfContract).getTime() / 1000,
        // numberOfContract,
        // subjectOfContract,
        dateOfAdditionalContract: new Date(dateOfAdditionalContract).getTime() / 1000,
        numberOfAdditionalContract,
        subjectOfAdditionalContract,
        mainContractId
    }

    const token = readLocalStorage('token')

    const res = await Api(apiUrles.adminstrativeAdditionalContractsAdd, newData, 'post', token)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    // progressForm.setProgressForm(0)

    return response
}

export const validateForm = ({
    uniqueCode,
    attachedEmployee,
    dateOfContract,
    numberOfContract,
    subjectOfContract,
    dateOfAdditionalContract,
    numberOfAdditionalContract,
    subjectOfAdditionalContract,
}) => {

    const errors = {}

    // if (!uniqueCode.size) {
    //     errors.uniqueCodeErr = 'required'
    // }
    if (attachedEmployee.length < 1) {
        errors.attachedEmployeeErr = 'required'
    }
    if (numberOfContract.length < 1) {
        errors.numberOfContractErr = 'required'
    }
    // if (!(regexOnlyNumbers.test(userPasswd))) {
    //     errors.userPasswdErr = 'onlyNumbers'
    // }
    // if (userPasswd.length < 6 || userPasswd.length > 6) {
    //     errors.userPasswdErr = 'required 6 symb'
    // }

    return errors
}



