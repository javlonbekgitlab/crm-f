import React, { useCallback, useState, useEffect } from 'react'
import { inject, observer } from 'mobx-react'
import SearchIcon from '@material-ui/icons/Search'
import styles from './index.module.sass'
import { Button, ButtonWithIcon } from '../../../button'
import { Input } from '../../../input'
import { useFormik } from 'formik'
import { regexSpace } from '../../../../constants'
import { onBlockUnblockTerminal, onSearchTerminal } from './helperFunctions'

export const SupportProForm = inject('lang','terminalStore')(observer(({lang, terminalStore}) => {

    const [disableBlocker, setDisableBlocker] = useState(false)

    const [disableUnBlocker, setDisableUnBlocker] = useState(true)

    const [blockerLoading, setBlockerLoading] = useState(false)

    const [unBlockerLoading, setUnBlockerLoading] = useState(false)

    const handlePressEnter = ({charCode}) => (charCode === 13 && handleSearchTerminal())

    useEffect(() => {
        primaryOnChange({target: {
            value: terminalStore.data.location,
            name: 'address'
        }})
        primaryOnChange({target: {
            value: terminalStore.data.tid,
            name: 'tid'
        }})
        primaryOnChange({target: {
            value: terminalStore.data.modelName,
            name: 'modelName'
        }})
        primaryOnChange({target: {
            value: terminalStore.data.name,
            name: 'name'
        }})
        primaryOnChange({target: {
            value: terminalStore.data.serialNo,
            name: 'serialNo'
        }})
    }, [terminalStore.data])

    const primaryOnChange = useCallback(({ target }) => {
        let value = ''
        if (typeof target.value === 'string' || target.value instanceof String) {
            value = target.value.replace(regexSpace, '')
        } else {
            value = target.value
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [])

    const formik = useFormik({
        initialValues: {
            searchValue: '',
            address: '',
            tid: '',
            modelName: '',
            name: '',
            serialNo: ''
        },

    })

    const {
        values: {
            searchValue,
            address,
            tid,
            modelName,
            name,
            serialNo
        }
    } = formik

    const handleSearchTerminal = async () => {
        const res = await onSearchTerminal(searchValue)
        if (!res) {
            formik.resetForm()
            terminalStore.setTerminalStore({})
        }
    }

    const handleBlockUnblockTerminal = param => {
        if (param === 1) {
            setDisableBlocker(!disableBlocker)
            setBlockerLoading(true)
            setTimeout(() => {
                setBlockerLoading(false)
                setDisableUnBlocker(!disableUnBlocker)
            }, 10000)
        } else {
            setUnBlockerLoading(true)
            setDisableUnBlocker(!disableUnBlocker)
            setTimeout(() => {
                setUnBlockerLoading(false)
                setDisableBlocker(!disableBlocker)
            }, 10000)
        }
        onBlockUnblockTerminal({
            terminalID: terminalStore.data.id,
            terminalPushCmd: param
        })
    }

    return (
        <div className = {styles.formCont}>
            <div className = {styles.formItem}>
                <div className = {styles.itemColumn}>
                    <Input
                        standart
                        label = {lang.forms.buttonTitles.search}
                        id = 'search'
                        name = 'searchValue'
                        value = {searchValue}
                        onChange = {primaryOnChange}
                        onKeyPress = {handlePressEnter}
                    >
                        <div className = {styles.buttonCont}>
                            <Button
                                withoutStyle
                                onClick = {handleSearchTerminal}
                            >
                                <SearchIcon/>
                            </Button>
                        </div>
                    </Input>
                    <Input
                        name = 'propertyName'
                        id = 'propertyName'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        // value = {propertyName}
                        standart
                        label = {lang.forms.supportPro.type}
                    />
                    <Input
                        name = 'propertyName'
                        id = 'propertyName'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        // value = {propertyName}
                        standart
                        label = {lang.forms.supportPro.bank}
                    />
                </div>
                <div className = {styles.itemColumn}>
                    <Input
                        name = 'serialNo'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        value = {serialNo}
                        standart
                        label = {lang.forms.supportPro.deviceNumber}
                    />
                    <Input
                        name = 'modelName'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        value = {modelName}
                        standart
                        label = {lang.forms.supportPro.model}
                    />
                    <Input
                        name = 'name'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        value = {name}
                        standart
                        label = {lang.forms.supportPro.commercialNumber}
                    />
                </div>
            </div>
            <div className = {styles.formItem}>
                <Input
                    name = 'propertyName'
                    id = 'propertyName'
                    // onChange = {primaryOnChange}
                    // error =  {propertyNameErr}
                    value = {address}
                    standart
                    label = {lang.forms.storeOrder.address}
                />
            </div>
            <div className = {styles.formItem}>
                <div className = {styles.itemColumn}>
                    <Input
                        name = 'propertyName'
                        id = 'propertyName'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        // value = {propertyName}
                        standart
                        label = {lang.forms.supportPro.idMerchant}
                    />
                    <Input
                        name = 'propertyName'
                        id = 'propertyName'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        // value = {propertyName}
                        standart
                        label = {lang.forms.order.tin}
                    />
                    {Object.keys(terminalStore.data).length > 0 && <div className = {styles.buttonContLocker}>
                        <ButtonWithIcon
                            loading = {blockerLoading}
                            disabled = {disableBlocker}
                            onClick = {() => handleBlockUnblockTerminal(1)}
                        >
                            {lang.forms.supportPro.lockTerminal}
                        </ButtonWithIcon>
                    </div>}
                </div>
                <div className = {styles.itemColumn}>
                    <Input
                        name = 'tid'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        value = {tid}
                        standart
                        label = {lang.forms.supportPro.idTerminal}
                    />
                    <Input
                        name = 'propertyName'
                        id = 'propertyName'
                        // onChange = {primaryOnChange}
                        // error =  {propertyNameErr}
                        // value = {propertyName}
                        standart
                        label = {lang.forms.supportPro.transMenu}
                    />
                    {Object.keys(terminalStore.data).length > 0 && <div className = {styles.buttonContLocker}>
                        <ButtonWithIcon
                            loading = {unBlockerLoading}
                            disabled = {disableUnBlocker}
                            onClick = {() => handleBlockUnblockTerminal(2)}
                        >
                            {lang.forms.supportPro.unlockTerminal}
                        </ButtonWithIcon>
                    </div>}
                </div>
            </div>
        </div>
    )
}))