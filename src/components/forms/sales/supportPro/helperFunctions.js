import { snackbar, snackbarStatus } from "../../../../actions"
import { Api, apiUrles } from "../../../../api"
import { apiParams, localStorageKeys } from "../../../../constants"
import { lang } from "../../../../languages/lang"
import { terminalStore } from "../../../../store"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSearchTerminal = async snNameTID => {
    console.log(snNameTID)
    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(`${apiUrles.searchTerminal}?${apiParams.snNameTID}${snNameTID}`, null, 'get', token)
    if (res.status === 200) {
        console.log(res.data)
        if (res.data[0]) {
            terminalStore.setTerminalStore(res.data[0])
            return true
        } else {
            snackbar.setSnackbar(lang.notifications.helperText.notFound, snackbarStatus.error)
            return false
        }
    } else {
        console.log(res)
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        return false
    }
}

export const onBlockUnblockTerminal = async value => {
    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(`${apiUrles.terminalPushCmd}`, value, 'post', token)
    if (res.status === 200) {
        return { success: true }
    } else {
        console.log(res)
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        return { success: false }
    }
}