import React, { useCallback, useEffect, useState } from 'react'
import { Paper } from '@material-ui/core'
import { useFormik } from 'formik'
import { inject } from 'mobx-react'
import { Button, IconButton } from '../../../button'
import SearchIcon from '@material-ui/icons/Search'
import { Input } from '../../../input'
import styles from './index.module.sass'
import { regexOnlyNumbers, regexSpace } from '../../../../constants'
import { snackbarStatus } from '../../../../actions'
import { onGetInfoByTin } from './helperFunctions'

const names = ['regionName', 'districtName', 'address']

export const Tin = inject('lang', 'snackbar')(({
    lang,
    showComponent,
    snackbar, 
    exisistTinOrPinfl, 
    onChangeTinOrPinfl, 
    onCloseForm, 
    onSetActiveStep
}) => {

    // const { nextIsNotAvailable, setNextIsNotAvailable } = next
    const [nextIsNotAvailable, setNextIsNotAvailable] = useState(false)

    const [tinOrPinfl, setTinOrPinfl] = useState(exisistTinOrPinfl ? exisistTinOrPinfl.toString() : '')

    const [dataOfMerchant, setDataOfMerchant] = useState({
        address: '',
        regionName: '',
        districtName: ''
    })

    const [error, setError] = useState('')

    const [balanceData, setBalanceData] = useState({
        text: '',
        status: 0
    })

    const { address, regionName, districtName } = dataOfMerchant 

    const handleSetTinOrPinfl = ({ target: { value } }) => {
        if (value) {
            if (regexOnlyNumbers.test(value)) {
                setError('')
                setTinOrPinfl(value)
            } else {
                setError(lang.forms.validations.onlyNumbers)
            } 
        }
    }

    const handlePressEnter = ({charCode}) => (charCode === 13 && handleValidate())

    const handleCloseForm = () => {
        onCloseForm('cancel')
        onSetActiveStep && onSetActiveStep(0)
    }

    const handleValidate = () => {
        console.log(tinOrPinfl)
        if (tinOrPinfl.length === 9 || tinOrPinfl.length === 14) {
            setError('')
            handleGetInfo()
        } else {
            setError(lang.forms.validations.lengthTinPinfl)
        }
    }

    const handleGetInfo = async () => {
        setDataOfMerchant({
            address: '',
            regionName: '',
            districtName: ''
        })
        const { success, data } = await onGetInfoByTin(tinOrPinfl) 
        console.log('data is',data)
        if (success && data.address && data.regionCode && data.districtCode) {
            onChangeTinOrPinfl && onChangeTinOrPinfl(tinOrPinfl)
            const {
                address = '', 
                regionName = '', 
                districtName = '', 
                terminalCost, 
                overpaymentSum
            } = data
            setDataOfMerchant({
                address,
                regionName,
                districtName
            })
            const debt = overpaymentSum - terminalCost
            if (debt < 0) {
                setNextIsNotAvailable(true)
                setBalanceData({
                    text: `${lang.notifications.helperText.tariff} ${terminalCost} ${lang.notifications.helperText.balance}${overpaymentSum}`,
                    status: 0
                })
                snackbar.setSnackbar(`${lang.notifications.helperText.tariff} ${terminalCost} ${lang.notifications.helperText.balance}${overpaymentSum}`, snackbarStatus.error)
            } 
            if (debt >= 0) {
                setNextIsNotAvailable(false)
                setBalanceData({
                    text: `${lang.notifications.helperText.tariff} ${terminalCost} ${lang.notifications.helperText.balance}${overpaymentSum}`,
                    status: 1
                })
                snackbar.setSnackbar(`OK`, snackbarStatus.success)
            }
            // if (!terminalCost && !overpaymentSum) {
            //     setNextIsNotAvailable(true)
            //     snackbar.setSnackbar(lang.notifications.helperText.notFound, snackbarStatus.error)
            // }
        } 
        if (success && data && Object.keys(data).length === 0 && data.constructor === Object) {
            setNextIsNotAvailable(true)
            snackbar.setSnackbar(lang.notifications.helperText.notFound, snackbarStatus.error)
        }
        if (!success) {
            setNextIsNotAvailable(true)
            snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        } 
    }

    const handleNext = () => {
        onSetActiveStep(1)
        setNextIsNotAvailable(true)
    }

    useEffect(() => {
        // setDataOfMerchant({
        //     address: '',
        //     regionName: '',
        //     districtName: ''
        // })
        exisistTinOrPinfl && showComponent && handleGetInfo()
    }, [showComponent])

    useEffect(() => {
        setNextIsNotAvailable(true)
    }, [onSetActiveStep])

    return (
        <div className = {styles.cont}>
            {/* <LinearLoading start = {loading}/> */}
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.order.titles.identification}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        onChange = {handleSetTinOrPinfl}
                                        value = {tinOrPinfl}
                                        standart
                                        disabled = {exisistTinOrPinfl}
                                        label = {lang.forms.order.tinOrPinfl}
                                        onKeyPress = {handlePressEnter}
                                        error = {error}
                                    >
                                        <div className = {styles.helperButtonsCont}>
                                            <Button
                                                // withoutStyle
                                                onClick = {handleValidate}
                                            >
                                                {lang.forms.buttonTitles.checkTinPinfl}
                                                {/* <SearchIcon/> */}
                                            </Button>
                                        </div>
                                    </Input>
                                </div>
                                <div className = {styles.itemChilds}>
                                    <div className = {styles.itemChildsAddressCont}>
                                        <div className = {styles.addressTop}>
                                            <div className = {balanceData.status === 1 ? styles.balancePositive : styles.balanceNegative}>{balanceData.text}</div>
                                            <div name = 'regionName'>{lang.forms.order.region}: {regionName}</div>
                                            <div>{lang.forms.order.city}: {districtName}</div>
                                            <div>{lang.forms.order.companyAddress}: {address}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </Paper>
                        <div className = {`${styles.buttonCont} ${styles.buttonContExt}`}>
                            <Button 
                                // disabled = {nextIsNotAvailable}
                                onClick = {handleNext}
                            >
                                {lang.forms.buttonTitles.next}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})