import { Api, apiUrles } from "../../../../api"
import { localStorageKeys, regexOnlyNumbers } from "../../../../constants"
import { progressForm, snackbar, snackbarStatus, uploadProgress } from "../../../../actions"
import { readLocalStorage } from "../../../../store/localStorage"
import { lang } from "../../../../languages/lang"

export const onSubmit = async values => {

    const response = {}

    // const {
    //     contract,
    //     date_of_contract,
    //     type_of_application,
    //     model,
    //     serial_number,
    //     fiskalMod,
    //     txkmFile,
    //     shopNameOrder,
    //     exampleOneFile,
    //     exampleTwoFile,
    //     file_name_reg,
    //     file_name_pass,
    //     file_name_others,
    //     merchantName,
    //     userMobile,
    //     nickName,
    //     userPasswd,
    //     id
    // } = values
    const {
        contract,
        date_of_contract,
        type_of_application,
        txkmFile,
        // exampleOneFile,
        // exampleTwoFile,
        // file_name_reg,
        file_name_pass,
        // file_name_others,
        // shopNameOrder,
        // service_center,
        // service_employee,
        nickName,
        userMobile,
        userPasswd,
        id
    } = values

    let newData = new FormData()

    newData.append('contract', contract)
    newData.append('date_of_contract', new Date(date_of_contract).getTime() / 1000)
    newData.append('type_of_application', type_of_application)
    // newData.append('model', model)
    // newData.append('serial_number', parseInt(serial_number))
    // newData.append('fiskalMod', fiskalMod)
    // newData.append('shopName', shopNameOrder)
    // newData.append('merchantName', merchantName)
    newData.append('userMobile', parseInt(userMobile))
    newData.append('nickName', nickName)
    newData.append('userPasswd', parseInt(userPasswd))
    newData.append('multi-files', txkmFile)
    // newData.append('multi-files', exampleOneFile)
    // newData.append('multi-files', exampleTwoFile)
    // newData.append('multi-files', file_name_reg)
    newData.append('multi-files', file_name_pass)
    // newData.append('multi-files', file_name_others)

    const token = readLocalStorage('token')

    let url = ''

    if (id) {
        url = apiUrles.ordersUpdate + id
    } else {
        url = apiUrles.ordersAdd
    }

    const res = await Api(url, newData, 'post', token, uploadProgress)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    progressForm.setProgressForm(0)

    return response
}

export const onSubmitMerchant = async values => {
    const {
        file_name_pass,
        nickName,
        userMobile,
        userPasswd,
        tinOrPinfl
    } = values
    console.log('values merhcant add front',values)

    let newData = new FormData()
    const tinOrPinflString = tinOrPinfl.toString()

    newData.append('userMobile', parseInt(userMobile))
    newData.append('nickName', nickName)
    tinOrPinflString.length === 9 && newData.append('tin', parseInt(tinOrPinfl))
    tinOrPinflString.length === 14 && newData.append('pinfl', parseInt(tinOrPinfl))
    // merMapId && newData.append('merMapId', merMapId)
    newData.append('userPasswd', parseInt(userPasswd))
    // newData.append('multi-files', txkmFile)
    newData.append('multi-files', file_name_pass)
    
    const response = {}
    
    const token = readLocalStorage(localStorageKeys.token)
    // value.userMobile = parseInt(value.userMobile)
    const url = (false ? `${apiUrles.updateMerchant}` : apiUrles.addMerchant)
    const res = await Api(url, newData, 'post', token)
    console.log('merhc', res)
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    // progressForm.setProgressForm(0)
    return response
}

export const onSubmitStore = async value => {
    const {
        shopName, 
        cityAddress, 
        regionCode, //regionCode
        regionName, //regionCode
        districtCode, //districtCode
        districtName, //districtCode
        group_of_activities, //activityGroup
        activityGroupName,
        activityType, //activityType
        activityTypeName, //activityType
        nomenclature,
        cadastre, //rentNumber/cadastreNumber
        docCadatsre,
        tinOrPinfl,
        lat,
        lng,
        merchantId,
        merMapId
    } = value
    const formData = new FormData()
    const tinOrPinflString = tinOrPinfl.toString()

    formData.append('shopName', shopName)
    formData.append('cityAddress', cityAddress)
    formData.append('regionCode', regionCode)
    formData.append('regionName', regionName)
    formData.append('districtCode', districtCode)
    formData.append('districtName', districtName)
    formData.append('activityGroup', group_of_activities)
    formData.append('activityGroupName', activityGroupName)
    formData.append('activityType', activityType)
    formData.append('activityTypeName', activityTypeName)
    formData.append('cadastreNumber', cadastre)
    tinOrPinflString.length === 9 && formData.append('tin', parseInt(tinOrPinfl))
    tinOrPinflString.length === 14 && formData.append('pinfl', parseInt(tinOrPinfl))
    formData.append('latitude', lat)
    formData.append('longitude', lng)
    formData.append('merchantId', merchantId)
    formData.append('merMapId', merMapId)
    formData.append('multi-files', docCadatsre)
    formData.append('multi-files', nomenclature)

    console.log(' storeFrontValue',value)
    const response = {}
    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(apiUrles.addStore, formData, 'post', token)
    console.log('backstore', res)
    if (res.status === 200) {
        response.successStore = true
        response.dataStore = res.data
    } else {
        console.log(res)
        response.successStore = false
    }
    // progressForm.setProgressForm(0)
    return response
}

export const onSubmitDevice = async value => {
    const {
        shopId,
        shopMapId,
        merchantId,
        modelNo,
        machineNo,
        fiskalMod,
        contract,
        date_of_contract,
        mfo,
        bankAccount,
        // type_of_application,
        typeOfApplication,
        // service_center,
        // service_employee,
        formCto,
        contractFile,
        applicationFile,
        officeId,
        phone,
        tinOrPinfl,
        others
    } = value
    console.log('Frontdevice',value)
    const formData = new FormData()
    const tinOrPinflString = tinOrPinfl.toString()

    formData.append('ccmCategoryId', modelNo)
    formData.append('shopId', shopId)
    formData.append('shopMapId', shopMapId)
    formData.append('merchantId', merchantId)
    formData.append('ccmSerialNumber', machineNo)
    formData.append('fiskalMod', fiskalMod)
    formData.append('officeId', officeId)
    formData.append('phone', phone)
    // formData.append('service_center', service_center)
    // formData.append('service_employee', service_employee)
    formData.append('contractNumber', contract)
    formData.append('mfo', mfo)
    formData.append('bankAccount', bankAccount)
    formData.append('date', new Date(date_of_contract).getTime() / 1000)
    formData.append('typeOfApplication', typeOfApplication)
    formData.append('multi-files', applicationFile)
    formData.append('multi-files', contractFile)
    formData.append('multi-files', formCto)
    tinOrPinflString.length === 9 && formData.append('tin', parseInt(tinOrPinfl))
    tinOrPinflString.length === 14 && formData.append('pinfl', parseInt(tinOrPinfl))
    // formData.append('multi-files', others)
    const response = {}
    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(apiUrles.addCashDrawer, formData, 'post', token, false, uploadProgress)
    console.log('backdevice', res)
    if (res.status === 200) {
        response.successDevice = true
        response.dataDevice = res.data
    } else {
        console.log(res)
        response.successDevice = false
    }
    // progressForm.setProgressForm(0)
    return response
}

export const onGetInfoByTin = async datum => {
    let url = ''
    let tinPinfl = datum.toString()
    console.log('length of tin/pinfl ', tinPinfl.length)
    if (tinPinfl.length === 9) {
        url = `${apiUrles.checkBalance}?tin=${tinPinfl}`
    } else if (tinPinfl.length === 14) {
        url = `${apiUrles.checkBalance}?pinfl=${tinPinfl}`
    } else {
        console.log('length of tin/pinfl ', tinPinfl.length)
        return { success: false }
    }
    const response = {}
    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(url, null, 'get', token)
    console.log( 'get info by tin',res)
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        response.success = false
    }
    return response
}

export const getOneStore = async id => {
    const response = {}
    const token = readLocalStorage('token')
    const res = await Api(`${apiUrles.oneStore}${id}`, {}, 'get', token)
    console.log(res)
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        response.success = false
    }
    return response
}

export const validateOrderForm = ({
    userMobile,
    nickName,
    userPasswd,
    file_name_pass,
    id
}) => {

    const errors = {}

    // if (!file_name_pass.size) {
    //     errors.file_name_passErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(userPasswd))) {
    //     errors.userPasswdErr = 'onlyNumbers'
    // }
    // if (userPasswd.length < 6 || userPasswd.length > 6) {
    //     errors.userPasswdErr = 'required 6 symb'
    // }

    return errors
}



