import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import EditIcon from '@material-ui/icons/Edit'
import { onSubmit, validateOrderForm } from './helperFunctions'
import { maskUserMobile, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { reserveOrder } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { Api } from '../../../../api'


const modelData = [
    {
        title: 'PAX A-930',
        id: '1'
    }
]

// interface FormikValuesInterface {
//     contract: string,
//     date_of_contract: Date,
//     type_of_application: number,
//     file_name_pass: {
//         name: string,
//     },
//     nickName:  string,
//     userMobile: number,
//     userPasswd:  number,
//     id: string,
//     tin: string

// }

export const OrderForm = inject('lang')(
    ({
        closeForm, 
        data, 
        onUploadingForm, 
        tinOrPinfl,
        // pinfl, 
        onSetActiveStep, 
        onSetOrderValues, 
        lang 
    }) => {

    const handleCloseForm = () => {
        closeForm('cancel')
        onSetActiveStep(0)
    }
    const handleBack = () => onSetActiveStep(-1)

    const handleSubmitOrder = async values => {
        values.tinOrPinfl = tinOrPinfl
        if (data?.merMapId) {
            console.log('update')
        } else {
            onSetActiveStep(1)
            onSetOrderValues(values)
        }
    }

    const primaryOnChange = useCallback(({ target }) => {
        let value = ''
        if (target.files) {
            if (target.files.length > 0) {
                value = target.files[0]
            }
        } else {
            if (target.name === 'userMobile') {
                value = target.value.replace(regexSpace, '')
            } else {
                value = target.value
            }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        // if ((target.files && (target.files.length > 0)) || (!target.files && !value) || (!target.files && value)) {
            formik.handleChange(event)
        // }
    }, [])

    const formik = useFormik({
        initialValues:  {
            file_name_pass: {
                name: lang.forms.order.titles.copyPassport,
            },
            nickName: data.nickName ? data.nickName : '',
            userMobile: data.userMobile ? data.userMobile : '98',
            userPasswd: data.userPasswd ? data.userPasswd : '',
            id: data.id ? data.id : '',
            tinOrPinfl: tinOrPinfl
        },
        onSubmit: handleSubmitOrder,
        validate: validateOrderForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            file_name_pass,
            nickName,
            userMobile,
            userPasswd
        },
        errors: {
            file_name_passErr,
            nickNameErr,
            userMobileErr,
            userPasswdErr,
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.order.titles.personalData}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'nickName'
                                        onChange = {primaryOnChange}
                                        error = {nickNameErr}
                                        value = {nickName}
                                        standart
                                        label = {lang.forms.order.firstMiddleLastName}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <InputMask
                                        onChange = {primaryOnChange}
                                        standart
                                        mask = {maskUserMobile}
                                        name = 'userMobile'
                                        value = {userMobile}
                                    >
                                        {inputProps => <Input 
                                            label = {`${lang.forms.order.telephone}/${lang.forms.order.login}`}
                                            {...inputProps}
                                            error = {userMobileErr}
                                        />}
                                    </InputMask>
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'userPasswd'
                                        onChange = {primaryOnChange}
                                        error = {userPasswdErr}
                                        value = {userPasswd}
                                        standart
                                        label = {lang.forms.signIn.password}
                                        type = 'userPasswd'
                                        maxLength = {6}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'file_name_pass'
                                        onChange = {primaryOnChange}
                                        value = {file_name_pass}
                                        error = {file_name_passErr}
                                        oldFile = {data.file_url_pass ? {
                                            name: data.file_name_pass,
                                            url: data.file_url_pass
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <div className = {styles.buttonCont}>
                            {onSetActiveStep && <Button onClick = {handleBack}>{lang.forms.buttonTitles.back}</Button>}
                            <Button 
                                onClick = {handleSubmit}
                            >
                                {(data && (Object.keys(data).length === 0)) ? lang.forms.buttonTitles.next : lang.forms.buttonTitles.edit}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

OrderForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}
