import { Api, apiUrles } from "../../../../api"
import { localStorageKeys, regexOnlyNumbers } from "../../../../constants"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmitSupportProRepair = async value => {
    const {
        shopMapId,
        modelNo,
        machineNo,
        fisk_number,
        service_center,
        service_employee,
        formCto,
        contractFile,
        others,
        tin
    } = value
    console.log('asdada',value)
    const formData = new FormData()

    formData.append('ccmCategoryId', modelNo)
    formData.append('shopMapId', shopMapId)
    formData.append('ccmSerialNumber', machineNo)
    formData.append('terminalId', fisk_number)
    formData.append('service_center', service_center)
    formData.append('service_employee', service_employee)
    formData.append('multi-files', formCto)
    formData.append('multi-files', contractFile)
    // formData.append('multi-files', others)
    formData.append('tin', tin)
    const response = {}
    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(apiUrles.addCashDrawer, formData, 'post', token)
    console.log('device', res)
    if (res.status === 200) {
        response.successDevice = true
        response.dataDevice = res.data
    } else {
        console.log(res)
        response.successDevice = false
    }
    // progressForm.setProgressForm(0)
    return response
}


export const validateSupportProRepairForm = ({
    ctoEmployee,
    applicationFileRepair,
    breakdownReason,
    modelNo,
    serial_number,
    comment
}) => {

    const errors = {}

    // if (contract.length < 1) {
    //     errors.contractErr = 'required'
    // }
    // if (date_of_contract.length < 1) {
    //     errors.date_of_contractErr = 'required'
    // }
    // if (type_of_application.length < 1) {
    //     errors.type_of_applicationErr = 'required'
    // }
    // if (model.length < 1) {
    //     errors.modelErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(serial_number))) {
    //     errors.serial_numberErr = 'onlyNumbers'
    // }
    // if (serial_number.length < 1) {
    //     errors.serial_numberErr = 'required'
    // }
    // if (fisk_number.length < 1) {
    //     errors.fisk_numberErr = 'required'
    // }
    // if (!txkmFile.size && !id) {
    //     errors.txkmFileErr = 'required'
    // }
    // if (shopNameOrder.length < 1) {
    //     errors.shopNameOrderErr = 'required'
    // }
    // if (service_center.length < 1) {
    //     errors.service_centerErr = 'required'
    // }
    // if (service_employee.length < 1) {
    //     errors.service_employeeErr = 'required'
    // }
    // if (nickName.length < 1) {
    //     errors.nickNameErr = 'required'
    // }
    // if (userMobile.length < 1) {
    //     errors.userMobileErr = 'required'
    // }
    // if (!(regexOnlyNumbers.test(userPasswd))) {
    //     errors.userPasswdErr = 'onlyNumbers'
    // }
    // if (userPasswd.length < 6 || userPasswd.length > 6) {
    //     errors.userPasswdErr = 'required 6 symb'
    // }

    return errors
}



