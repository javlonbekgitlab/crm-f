import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Button } from '../../../button'
import PropTypes from 'prop-types'
import { useFormik } from 'formik'
import InputMask from 'react-input-mask'
import { Input } from '../../../input'
import { Paper } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import EditIcon from '@material-ui/icons/Edit'
import { onSubmit, validateSupportProRepairForm } from './helperFunctions'
import { maskUserMobile, modelNoData, regexOnlyNumbers, regexSpace } from '../../../../constants'
import { RadioButtonGroup } from '../../../radioGroup'
import { DatePicker } from '../../../datePicker'
import { reserveOrder } from '../../../../store/reserve'
import { inject } from 'mobx-react'
import { Api } from '../../../../api'


const ctoEmployeeData = [
    {
        title: 'chuvak',
        id: '1'
    }
]

const breakdownReasonData = [
    {
        title: 'buzgan',
        id: '1'
    }
]

export const SupportProRepairForm = inject('lang', 'supportProRepairList')(
    ({
        closeForm, 
        data, 
        onUploadingForm, 
        lang,
        supportProRepairList
    }
    
    ) => {

    const handleCloseForm = () => {
        closeForm('cancel')
    }

    const handleSubmitForm = values => {
        console.log(values)
        const tmpData = {
            data: [
                ...supportProRepairList.data,
                values
            ]
        }
        supportProRepairList.setSupportProRepairList(tmpData)
        // setLoading(true)
        // onUploadingForm(true)

        // const { success } = await onSubmit(values)

        // if (success) {
        //     onUploadingForm(false)
        //     getOrders()
        // } 
        closeForm({ success: true })
        // setLoading(false)
    }

    const primaryOnChange = useCallback(({ target }) => {
        let value = ''
        if (target.files) {
            if (target.files.length > 0) {
                value = target.files[0]
            }
        } else {
            if (target.name === 'userMobile') {
                value = target.value.replace(regexSpace, '')
            } else {
                value = target.value
            }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        if ((target.files && (target.files.length > 0)) || (!target.files && !value) || (!target.files && value)) {
            formik.handleChange(event)
        }
    }, [])

    const formik = useFormik({
        initialValues: {
            ctoEmployee: data.ctoEmployee ? data.ctoEmployee : '',
            breakdownReason: data.breakdownReason ? data.breakdownReason : '',
            tin: data.tin ? data.tin : '',
            applicationFileRepair: {
                name: lang.forms.supportProRepair.applicationFile,
            },
            serial_number: data.serial_number ? data.serial_number : '',
            comment: data.comment ? data.comment : '',
            modelNo: data.modelNo ? data.modelNo : '',
        },
        onSubmit: handleSubmitForm,
        validate: validateSupportProRepairForm,
        validateOnChange: false,
        validateOnBlur: false,
        
    })

    const {
        values: {
            ctoEmployee,
            applicationFileRepair,
            tin,
            breakdownReason,
            modelNo,
            serial_number,
            comment
        },
        errors: {
            ctoEmployeeErr,
            applicationFileRepairErr,
            tinErr,
            breakdownReasonErr,
            modelNoErr,
            commentErr,
            serial_numberErr,
        },
        handleSubmit
    } = formik


    return (
        <div className = {styles.cont}>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.order.titles.personalData}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.order.deviceModel}
                                        name = 'modelNo'
                                        onChange = {primaryOnChange}
                                        error = {modelNoErr}
                                        value = {modelNo}
                                        data = {modelNoData}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'serial_number'
                                        id = 'serial_number'
                                        onChange = {primaryOnChange}
                                        error = {serial_numberErr}
                                        value = {serial_number}
                                        standart
                                        label = {lang.forms.order.serializeNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.supportProRepair.ctoWorker}
                                        name = 'ctoEmployee'
                                        onChange = {primaryOnChange}
                                        error = {ctoEmployeeErr}
                                        value = {ctoEmployee}
                                        data = {ctoEmployeeData}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        label = {lang.forms.order.tin}
                                        name = 'tin'
                                        standart
                                        onChange = {primaryOnChange}
                                        error = {tinErr}
                                        value = {tin}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.supportProRepair.breakdownReason}
                                        name = 'breakdownReason'
                                        onChange = {primaryOnChange}
                                        error = {breakdownReasonErr}
                                        value = {breakdownReason}
                                        data = {breakdownReasonData}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        standart
                                        name = 'comment'
                                        id = 'comment'
                                        value = {comment}
                                        error = {commentErr}
                                        onChange = {primaryOnChange}
                                        label = {lang.forms.supportProRepair.comment}
                                        multiline
                                        rows = {4}
                                    />
                                </div>
                            </div>
                        </Paper>
                        {(data && (Object.keys(data).length > 0)) && <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'applicationFileRepair'
                                        onChange = {primaryOnChange}
                                        value = {applicationFileRepair}
                                        error = {applicationFileRepairErr}
                                        oldFile = {data.file_url_pass ? {
                                            name: data.applicationFileRepair,
                                            url: data.file_url_pass
                                        } : false}
                                    />
                                </div>
                            </div>
                        </Paper>}
                        <div className = {styles.buttonCont}>
                            {(data && (Object.keys(data).length > 0)) && <Button>
                                {lang.forms.supportProRepair.downloadApplication}
                            </Button>}
                            <Button 
                                onClick = {handleSubmit}
                            >
                                {(data && (Object.keys(data).length > 0)) ? lang.forms.buttonTitles.closeApplication : lang.forms.buttonTitles.add}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})

SupportProRepairForm.propTypes = {
    closeForm: PropTypes.func,
    data: PropTypes.object,
    onClickAddShop: PropTypes.func,
    onUploadingForm: PropTypes.func
}
