import React, { useCallback, useEffect, useState } from 'react'
import { Paper } from '@material-ui/core'
import { useFormik } from 'formik'
import { inject } from 'mobx-react'
import { Button } from '../../../button'
import { Input } from '../../../input'
import { SelectCustom } from '../../../select'
import styles from './index.module.sass'
import { employees, serviceCenter } from '../../../../store'
import { drawDOM, exportPDF } from "@progress/kendo-drawing";
import { PDFExport, savePDF } from "@progress/kendo-react-pdf"
import { getOrders } from '../../../../store/getDataFromServer'
import { LinearLoading } from '../../../loading/linerLoading'
import { Api, apiUrles } from '../../../../api'
import { readLocalStorage } from '../../../../store/localStorage'
import { localStorageKeys, maskBankAccount, maskMFO, modelNoData, regexSpace, typeConnection, typeConnectionSub } from '../../../../constants'
import { onSubmitMerchant, onSubmitStore, onSubmitDevice, onGetInfoByTin } from '../orderForm/helperFunctions'
import { snackbarStatus } from '../../../../actions'
import { UploadFile } from '../../../uploadFile'
import { usePDF} from '@react-pdf/renderer';
import { Pdf } from '../../../pdfRender'
import { ModalForm } from '../../../modalForm'
import { ApplicationNits } from '../../../pdfTemps/applicationNits'
import { RadioButtonGroup } from '../../../radioGroup'
import { DatePicker } from '../../../datePicker'
import { validateFormDevice } from './helperFunctions'
import InputMask from 'react-input-mask'


export const AddDevice = inject('lang', 'snackbar')(
    ({
        tinOrPinfl, 
        lang, 
        data, 
        phone,
        shopId, 
        snackbar, 
        officeId,
        shopMapId,
        storeData, 
        fullSubmit,
        merchantId,
        onCloseForm, 
        merchantData, 
        onSetActiveStep, 
        onUploadingForm
    }) => {

    console.log('tinOrPinfl',tinOrPinfl)
    const [loading, setLoading] = useState(false)
    // useEffect(() => handleGetInfo(),[])
    const [applicationModal, setApplicationModal] = useState(false)

    const [pdf, setPdf] = useState({current: null})

    const [accessPdfGenerate, setAccessPdfGenerate] = useState(false)

    const [appButtonLoading, setAppButtonLoading] = useState(false)

    const [compName, setCompName] = useState('')

    const [accessOpenApplication, setAccessOpenApplication] = useState(false)

    const date = new Date(Date.now()).toISOString().slice(0, 10)

    // const [modelName, setModelName] = useState('')

    const handleBack = () => onSetActiveStep(-1)

    const handleCloseForm = () => {
        onCloseForm('cancel')
        onSetActiveStep && onSetActiveStep(0)
    }

    const handleOpenAppFile = () => {
        if (!appButtonLoading && accessOpenApplication) {
            setApplicationModal(true)
        } else {
            handleGetInfo()
        }
    }

    const handleCloseAppFile = () => setApplicationModal(false)

    const handleGetInfo = async () => {
        setAppButtonLoading(true)
        const { success, data } = await onGetInfoByTin(tinOrPinfl) 
        if (success && data.name) {
            setAccessOpenApplication(true)
            setCompName(data.name)
        } else {
            snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        }
        setAppButtonLoading(false)
    }


    const exportPDFWithComponent = async () => {
        if (pdf.current) {
            // pdf.current.save()
            console.log('save',pdf.current.save('test'))
        }
    }

    // useEffect(() => {
    //     // let element = document.querySelector(".test") || pdf.current.rootElForPDF;
    //     // console.log(element)
    //     // console.log(pdf.current.rootElForPDF)
    //     // drawDOM(element, {
    //     //     paperSize: "A4",
    //     // }).then((group) => {
    //     //     return exportPDF(group);
    //     // })
    //     //     .then((dataUri) => {
    //     //         console.log(dataUri)
    //     //         console.log(dataUri.split(";base64,")[1])
    //     //         // setData(dataUri.split(";base64,")[1])
    //     //     });
    // }, [setPdf, pdf])

    // const generateApplication = () => {
    //     let element = document.querySelector(".test") || pdf.current?.rootElForPDF;
    //     // console.log(element)
    //     // console.log(pdf.current.rootElForPDF)
    //     drawDOM(element, {
    //         paperSize: "A4",
    //     }).then((group) => {
    //         return exportPDF(group);
    //     })
    //         .then((dataUri) => {
    //             console.log(dataUri)
    //             console.log(dataUri.split(";base64,")[1])
    //             formik.handleChange({
    //                 target: {
    //                     name: 'applicationFile',
    //                     value: dataUri.split(";base64,")[1]
    //                 }
    //             })
    //             // setData(dataUri.split(";base64,")[1])
    //         });  
    // };

    const handleSubmitForm = async values => {
        setLoading(true)
        onUploadingForm(true)
        // if (values.type_of_application === '2') {
        //     values.type_of_application = values.type_of_application_sub
        // }
        // console.log(values)
        if (shopId) {
            values.shopId = shopId
            values.shopMapId = shopMapId
            values.merchantId = merchantId
            values.tinOrPinfl = tinOrPinfl
            values.officeId = officeId
            values.phone = phone
            const { successDevice, dataDevice } = await onSubmitDevice(values)
            if (successDevice && dataDevice) {
                snackbar.setSnackbar(lang.notifications.helperText.device + lang.notifications.success.added, snackbarStatus.success)
            } else {
                snackbar.setSnackbar(lang.notifications.helperText.device + lang.notifications.fail.added, snackbarStatus.error)
            }
            onCloseForm({success: successDevice})
            onUploadingForm(false)
            onSetActiveStep(0)
        } else {
            if (fullSubmit) {
                const { success, data } = await onSubmitMerchant(merchantData)
                if (success && data) {
                    console.log('mercha',data)
                    onCloseForm({ success })
                    storeData.merchantId = data._id
                    storeData.tinOrPinfl = tinOrPinfl
                    storeData.merMapId = data.merMapId
                    snackbar.setSnackbar(lang.notifications.helperText.merchant + lang.notifications.success.added, snackbarStatus.success)
                    const { successStore, dataStore } = await onSubmitStore(storeData)
                    if (successStore && dataStore) {
                        setTimeout(() => {
                            snackbar.setSnackbar(lang.notifications.helperText.store + lang.notifications.success.added, snackbarStatus.success)
                        }, 700)
                        values.shopId = dataStore._id
                        values.merchantId = dataStore.merchantId
                        values.officeId = dataStore.officeId
                        values.phone = dataStore.phone
                        values.shopMapId = dataStore.shopMapId
                        values.tinOrPinfl = tinOrPinfl
                        const { successDevice, dataDevice } = await onSubmitDevice(values)
                        if (successDevice && dataDevice) {
                            setTimeout(() => {
                                snackbar.setSnackbar(lang.notifications.helperText.device + lang.notifications.success.added, snackbarStatus.success)
                            }, 1400)
                            onUploadingForm(false)
                        } else {
                            setTimeout(() => {
                                snackbar.setSnackbar(lang.notifications.helperText.device + lang.notifications.fail.added, snackbarStatus.error)
                            }, 1400)
                            onUploadingForm(false)
                        }
                        onSetActiveStep(-3)
                    } else {
                        setTimeout(() => {
                            snackbar.setSnackbar(lang.notifications.helperText.store + lang.notifications.fail.added, snackbarStatus.error)
                        }, 700)
                        onUploadingForm(false)
                    }
                } else {
                    onSetActiveStep(-3)
                    snackbar.setSnackbar(lang.notifications.helperText.merchant + lang.notifications.fail.added, snackbarStatus.error)
                    onUploadingForm(false)
                }
            } else {
                const { successStore, dataStore } = await onSubmitStore(storeData)
                if (successStore && dataStore) {
                    snackbar.setSnackbar(lang.notifications.helperText.store + lang.notifications.success.added, snackbarStatus.success)
                    values.shopId = dataStore._id
                    values.merchantId = dataStore.merchantId
                    values.officeId = dataStore.officeId
                    values.phone = dataStore.phone
                    values.shopMapId = dataStore.shopMapId
                    values.tinOrPinfl = tinOrPinfl
                    const { successDevice, dataDevice } = await onSubmitDevice(values)
                    if (successDevice && dataDevice) {
                        onCloseForm({success: successDevice})
                        setTimeout(() => {
                            snackbar.setSnackbar(lang.notifications.helperText.device + lang.notifications.success.added, snackbarStatus.success)
                        }, 700)
                        onUploadingForm(false)
                    } else {
                        setTimeout(() => {
                            snackbar.setSnackbar(lang.notifications.helperText.device + lang.notifications.fail.added, snackbarStatus.error)
                        }, 700)
                        onUploadingForm(false)
                    }
                } else {
                    onSetActiveStep(-1)
                    snackbar.setSnackbar(lang.notifications.helperText.store + lang.notifications.fail.added, snackbarStatus.error)
                    onUploadingForm(false)
                }
            }
            
        }
        setLoading(false)
    }

    // const primaryOnChange = useCallback(({ target }) => {
    //     let value = ''
    //     if (target.files) {
    //         if (target.files.length > 0) {
    //             value = target.files[0]
    //         }
    //     } else {
    //         if (typeof target.value === 'string' || target.value instanceof String)
    //             value = target.value.replace(regexSpace, '')
    //         else
    //             value = target.value
    //     }
    //     const event = {
    //         target: {
    //             name: target.name,
    //             value
    //         }
    //     }
    //     formik.handleChange(event)
    // }, [])
    const primaryOnChange = useCallback(({ target }) => {
        let value = ''
        if (target.files) {
            if (target.files.length > 0) {
                value = target.files[0]
            }
        } else {
            if (typeof target.value === 'string' || target.value instanceof String) {
                value = target.value.replace(regexSpace, '')
                console.log(parseInt(value))
            } else {
                value = target.value
            }
            if ((target.name === 'type_of_application') && (value === '1')) {
                formik.handleChange({target: {
                    name: 'typeOfApplication',
                    value
                }})
                formik.handleChange({target: {
                    name: 'type_of_application_sub',
                    value: ''
                }})
            }
            if ((target.name === 'type_of_application') && (value === '2')) {
                formik.handleChange({target: {
                    name: 'typeOfApplication',
                    value: ''
                }})
                formik.handleChange({target: {
                    name: 'type_of_application_sub',
                    value: ''
                }})
            }
            if ((target.name === 'type_of_application_sub')) {
                formik.handleChange({target: {
                    name: 'typeOfApplication',
                    value
                }})
            }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        if ((target.files && (target.files.length > 0)) || (!target.files && !value) || (!target.files && value)) {
            formik.handleChange(event)
        }
    }, [applicationModal])

    const formik = useFormik({
        initialValues: {
            modelNo: data?.modelNo ? data.modelNo : '',
            machineNo: data?.machineNo ? data.machineNo : '',
            fiskalMod: '',
            bankAccount: '',
            mfo: '',
            // service_center: data?.service_center ? data.service_center : '',
            // service_employee: data?.service_employee ? data.service_employee : '',
            contract: data?.contract ? data?.contract : '',
            date_of_contract: data?.date_of_contract ? new Date(data.date_of_contract * 1000) : new Date(),
            type_of_application: data?.type_of_application ? data.type_of_application : '',
            type_of_application_sub: data?.type_of_application ? data.type_of_application : '',
            typeOfApplication: data?.type_of_application ? data.type_of_application : '',
            // applicationFile: '',
            contractFile: {
                name: lang.forms.order.contractFile,
            },
            applicationFile: {
                name: lang.forms.terminal.application,
            },
            formCto: {
                name: lang.forms.order.titles.formCentTechService,
            }
        },
        validate: validateFormDevice,
        validateOnChange: false,
        onSubmit: handleSubmitForm
    })

    const { 
        values: {
            modelNo,
            machineNo,
            fiskalMod,
            bankAccount,
            mfo,
            // service_center,
            // service_employee,
            contract,
            date_of_contract,
            type_of_application,
            type_of_application_sub,
            formCto,
            contractFile,
            applicationFile
        },
        errors: {
            modelNoErr,
            machineNoErr,
            fiskalModErr,
            bankAccountErr,
            mfoErr,
            // service_centerErr,
            // service_employeeErr,
            contractErr,
            applicationFileErr,
            formCtoErr,
            type_of_applicationErr,
            type_of_application_subErr,
            contractFileErr
        },
        handleSubmit
    } = formik

    useEffect(() => {
        if (modelNo && machineNo && fiskalMod) {
            setAccessPdfGenerate(true)
        } else {
            setAccessPdfGenerate(false)
        }
    }, [modelNo, machineNo, fiskalMod])

    useEffect(() => {
        accessPdfGenerate && handleGetInfo()
    }, [accessPdfGenerate])

    return (
        <div className = {styles.cont}>
            <LinearLoading start = {loading}/>
            <div className = {styles.formCont}>
                <div className = {styles.formTop}>
                    {accessPdfGenerate && <ModalForm
                        open = {applicationModal}
                        width = {'md'}
                        className = {styles.modal}
                        close = {handleCloseAppFile}
                        // keepMounted = {keepMounted}
                    >
                        <Paper 
                            elevation = {3}
                            className = {styles.appPaper}
                        >
                            <Pdf 
                                getPdfComponent = {setPdf}
                                fileName = {`S.N-${machineNo}, tinOrPinfl-${tinOrPinfl}, Date-${date}`}
                            >
                                <ApplicationNits
                                    data = {{
                                        ...formik.values, 
                                        ...merchantData, 
                                        ...storeData,
                                        modelName: 'PAX A-930', 
                                        compName,
                                        tinOrPinfl
                                    }}
                                />
                            </Pdf>
                        </Paper>
                        <div className = {styles.buttonContApp}>
                            <Button onClick = {exportPDFWithComponent}>{lang.forms.buttonTitles.download}</Button>
                            <Button onClick = {handleCloseAppFile}>{lang.forms.buttonTitles.back}</Button>
                        </div>
                    </ModalForm>}
                    <div className = {styles.topColumn}>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.order.titles.device}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.order.deviceModel}
                                        name = 'modelNo'
                                        onChange = {primaryOnChange}
                                        error = {modelNoErr}
                                        value = {modelNo}
                                        data = {modelNoData}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'machineNo'
                                        id = 'machineNo'
                                        onChange = {primaryOnChange}
                                        error =  {machineNoErr}
                                        value = {machineNo}
                                        standart
                                        label = {lang.forms.order.serializeNumber}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        name = 'fiskalMod'
                                        id = 'fiskalMod'
                                        onChange = {primaryOnChange}
                                        error = {fiskalModErr}
                                        value = {fiskalMod}
                                        standart
                                        label = {lang.forms.order.fiskNumber}
                                    />
                                </div>
                            </div>
                        </Paper>
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <Input
                                        standart
                                        label = {lang.forms.order.contractNumber}
                                        name = 'contract'
                                        id = 'contract'
                                        onChange = {primaryOnChange}
                                        error = {contractErr}
                                        value = {contract}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <DatePicker
                                        name = 'date_of_contract'
                                        onChange = {primaryOnChange}
                                        value = {date_of_contract}
                                        label = {lang.forms.order.dateAgreement}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <InputMask
                                        onChange = {primaryOnChange}
                                        standart
                                        mask = {maskMFO}
                                        name = 'mfo'
                                        value = {mfo}
                                    >
                                        {inputProps => <Input 
                                            label = {lang.table.columnsName.payments.mfo}
                                            {...inputProps}
                                            error = {mfoErr}
                                        />}
                                    </InputMask>
                                    {/* <Input
                                        standart
                                        label = {lang.table.columnsName.payments.mfo}
                                        name = 'mfo'
                                        onChange = {primaryOnChange}
                                        error = {mfoErr}
                                        value = {mfo}
                                    /> */}
                                </div>
                                <div className = {styles.itemChilds}>
                                    <InputMask
                                        onChange = {primaryOnChange}
                                        standart
                                        mask = {maskBankAccount}
                                        name = 'bankAccount'
                                        value = {bankAccount}
                                    >
                                        {inputProps => <Input 
                                            label = {lang.table.columnsName.payments.bankAccount}
                                            {...inputProps}
                                            error = {bankAccountErr}
                                        />}
                                    </InputMask>
                                    {/* <Input
                                        standart
                                        label = {lang.table.columnsName.payments.bankAccount}
                                        name = 'bankAccount'
                                        onChange = {primaryOnChange}
                                        error = {bankAccountErr}
                                        value = {bankAccount}
                                    /> */}
                                </div>
                                <div className = {styles.itemChildsRadioCont}>
                                    <div className = {styles.radioTitle}>{lang.forms.order.bidType}</div>
                                    <RadioButtonGroup
                                        value = {type_of_application}
                                        name = 'type_of_application'
                                        onChange = {primaryOnChange}
                                        error = {type_of_applicationErr}
                                        data = {typeConnection}
                                    />
                                </div> 
                                {(type_of_application === '4') && <div className = {styles.itemChildsRadioCont}>
                                    <RadioButtonGroup
                                        value = {type_of_application_sub}
                                        name = 'type_of_application_sub'
                                        onChange = {primaryOnChange}
                                        error = {type_of_application_subErr}
                                        data = {typeConnectionSub}
                                    />
                                </div>}
                            </div>
                        </Paper>
                    {/* </div> */}
                    {/* <div className = {styles.topColumn}> */}
                        {/* <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.order.titles.service}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.order.serviceCenter}
                                        name = 'service_center'
                                        onChange = {primaryOnChange}
                                        error = {service_centerErr}
                                        value = {service_center}
                                        data = {serviceCenter}
                                    />
                                </div>
                                <div className = {styles.itemChilds}>
                                    <SelectCustom
                                        title = {lang.forms.order.employee}
                                        name = 'service_employee'
                                        onChange = {primaryOnChange}
                                        error = {service_employeeErr}
                                        value = {service_employee}
                                        data = {employees}
                                    />
                                </div>
                            </div>
                        </Paper> */}
                        <Paper 
                            elevation = {3}
                            className = {styles.paper}
                        >
                            <div className = {styles.itemTitle}>{lang.forms.order.titles.agreement}</div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'contractFile'
                                        onChange = {primaryOnChange}
                                        value = {contractFile}
                                        // acceptFile = 'application/pdf,application/vnd.ms-excel'
                                        error = {contractFileErr}
                                        // oldFile = {data.file_url ? {
                                        //     name: data.file_name,
                                        //     url: data.file_url
                                        // } : false}
                                    />
                                </div>
                            </div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'formCto'
                                        onChange = {primaryOnChange}
                                        value = {formCto}
                                        error = {formCtoErr}
                                        // acceptFile = 'application/pdf,application/vnd.ms-excel'
                                        // oldFile = {data.file_url ? {
                                        //     name: data.file_name,
                                        //     url: data.file_url
                                        // } : false}
                                    />
                                </div>
                            </div>
                            <div className = {styles.topColumnItem}>
                                <div className = {styles.itemChilds}>
                                    <UploadFile
                                        name = 'applicationFile'
                                        onChange = {primaryOnChange}
                                        value = {applicationFile}
                                        error = {applicationFileErr}
                                        // acceptFile = 'application/pdf,application/vnd.ms-excel'
                                        // oldFile = {data.file_url ? {
                                        //     name: data.file_name,
                                        //     url: data.file_url
                                        // } : false}
                                    />
                                </div>
                            </div> 
                        </Paper>
                        <div className = {styles.buttonCont}>
                            {onSetActiveStep && <Button
                                onClick = {handleBack}
                            >
                                {lang.forms.buttonTitles.back}
                            </Button>}
                            {accessPdfGenerate && <Button
                                loading = {appButtonLoading}
                                onClick = {handleOpenAppFile}
                            >
                                {lang.forms.buttonTitles.openApplication}
                            </Button>}
                            <Button 
                                onClick = {handleSubmit}
                            >
                                {(data && (Object.keys(data).length > 0)) ? lang.forms.buttonTitles.edit : lang.forms.buttonTitles.add}
                            </Button>
                            <Button 
                                onClick={handleCloseForm}
                            >
                                {lang.forms.buttonTitles.cancel}
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
})