import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Paper } from '@material-ui/core'
import { Input } from '../../../input'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import { Button } from '../../../button'
import { useFormik } from 'formik'
import { onSubmitApplication, onSubmitSendToNits, validateForm } from './helperFunctions'
import { Map } from '../../../map'
import { modelNoData, regexSpace } from '../../../../constants'
import { inject } from 'mobx-react'
import { Api } from '../../../../api'
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined'
import { onGetInfoByTin, onSubmitStore } from '../orderForm/helperFunctions'
import { getDistricts, getRegions, getGroupActivities, getListActivities, getAllApplications } from '../../../../store/getDataFromServer'
import { transformRegions } from '../../../../store/transformers'
import { Marker } from 'react-leaflet'
import { autoSelectNames, snackbarStatus } from '../../../../actions'
import { ApplicationNits } from '../../../pdfTemps/applicationNits'
import { ModalForm } from '../../../modalForm'
import { Pdf } from '../../../pdfRender'


export const ApplicationForm = inject('lang', 'snackbar')(
    ({
        onCloseForm, 
        snackbar,
        data, 
        lang, 
        onUploadingForm
    }) => {

    const [regions, setRegions] = useState([])

    const [districts, setDistricts] = useState([])

    const [groupActivities, setGroupActivities] = useState([])

    const [listActivities, setListActivities] = useState([])

    const [applicationModal, setApplicationModal] = useState(false)

    const [pdf, setPdf] = useState({current: null})

    const [accessPdfGenerate, setAccessPdfGenerate] = useState(false)

    const [appButtonLoading, setAppButtonLoading] = useState(false)

    const [compName, setCompName] = useState('')

    const [accessOpenApplication, setAccessOpenApplication] = useState(false)

    const [disabled, setDisabled] = useState(true)

    const date = new Date(Date.now()).toISOString().slice(0, 10)

    const handleOpenRegions = async () => {
        const regions = await getRegions()
        if (regions) {
            const regionsFromServer = transformRegions(regions, 'name')
            setRegions(regionsFromServer)
        }
    }

    const handleOpenGroupActivity = async () => {
        const resFromServer = await getGroupActivities()
        if (resFromServer) {
            const groupActivitiesFromServer = transformRegions(resFromServer, 'name')
            setGroupActivities(groupActivitiesFromServer)
        }
    }

    const handleSetDistricts = async value => {
        setDistricts([])
        const districtList = await getDistricts(value)
        const districtFromServer = transformRegions(districtList, 'name')
        setDistricts(districtFromServer)
    }

    const handleSetActivityList = async value => {
        setListActivities([])
        const activityList = await getListActivities(value)
        const activityFromServer = transformRegions(activityList, 'name')
        setListActivities(activityFromServer)
    }

    const handleCloseForm = () => {
        onCloseForm('cancel')
    }

    const handleOpenAppFile = () => {
        handleGetInfo()
        if (!appButtonLoading && accessOpenApplication) {
            setApplicationModal(true)
        } else {
            handleGetInfo()
        }
    }

    const handleCloseAppFile = () => setApplicationModal(false)

    const handleGetInfo = async () => {
        setAppButtonLoading(true)
        const { success, data } = await onGetInfoByTin(tin) 
        if (success && data.name) {
            setAccessOpenApplication(true)
            setCompName(data.name)
        } else {
            snackbar.setSnackbar(lang.notifications.fail.noConnection, snackbarStatus.error)
        }
        setAppButtonLoading(false)
    }


    const exportPDFWithComponent = async () => {
        if (pdf.current) {
            // pdf.current.save()
            console.log('save',pdf.current.save('test'))
        }
    }

    // const handleSubmitForm = async values => {
    //     const { shopId } = data
    //     onUploadingForm(true)
    //     if (data.statusDesc) {
    //         snackbar.setSnackbar('', snackbarStatus.error, 0)
    //     }
    //     const { success } = await onSubmitApplication({values, shopId})
    //     if (success) {
    //         onCloseForm({success})
    //     } else {
    //         onCloseForm({success})
    //     }
    //     onUploadingForm(false)
    //     // getAllApplications()
    // }

    // esimda yo nimaga qiganim
    // const handleSubmitForm = async values => {
    //     if (merchantId) {
    //         values.merchantId = merchantId
    //         values.merMapId = merMapId
    //         const { successStore } = await onSubmitStore(values)
    //         onCloseForm({ success: successStore })
    //     } else {
    //         onSetStoreValues(values)
    //     }
    // }

    const handleSubmitForm = async values => {
        console.log('data._id',data._id)
        const dataForSubmit = {
            _id: data._id,
        } 
        values.contractFile.size && (dataForSubmit.contractFile = values.contractFile.size) 
        const { success } = await onSubmitSendToNits(dataForSubmit)
        if (success) {
            onCloseForm({success})
        } else {
            onCloseForm({success})
        }
        onUploadingForm(false)
    }

    const primaryOnChange = useCallback(async({ target }) => {
        let value = ''
        if (target.files) {
            value = target.files[0]
        } else {
            // if (typeof target.value === 'string' || target.value instanceof String)
            //     // value = target.value.replace(regexSpace, '')
            // else
                value = target.value
            if (target.name === 'regionCode') {
                formik.handleChange({
                    target: {
                        name: 'districtCode',
                        value: ''
                    }
                })
                handleSetDistricts(value)
                formik.handleChange(autoSelectNames({
                    name: 'regionName',
                    data: regions,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            if (target.name === 'activityGroup') {
                formik.handleChange({
                    target: {
                        name: 'activityType',
                        value: ''
                    }
                })
                handleSetActivityList(value)
                formik.handleChange(autoSelectNames({
                    name: 'activityGroupName',
                    data: groupActivities,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            if (target.name === 'districtCode') {
                formik.handleChange(autoSelectNames({
                    name: 'districtName',
                    data: districts,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            if (target.name === 'activityType') {
                formik.handleChange(autoSelectNames({
                    name: 'activityTypeName',
                    data: listActivities,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [regions, districts, groupActivities, listActivities])

    const handleSetCoords = coords => {
        Object.keys(coords).map(key => formik.handleChange({target: {
            name: key,
            value: coords[key]
        }}))
    }

    const formik = useFormik({
        initialValues: {
            shopName: data?.shopName ? data.shopName : '', 
            cityAddress: data?.cityAddress ? data.cityAddress : '', 
            tin: data?.tin, 
            regionCode: data?.regionCode ? data.regionCode : '',
            regionName: data?.regionName ? data.regionName : '', 
            districtCode: data?.districtCode ? data.districtCode : '',
            districtName: data?.districtName ? data.districtName : '',
            activityGroup: data?.activityGroup ? data.activityGroup : '', 
            activityGroupName: data?.activityGroupName ? data.activityGroupName : '',
            activityType: data?.activityType ? data.activityType : '',
            activityTypeName: data?.activityTypeName ? data.activityTypeName : '',
            cadastre: data?.cadastreNumber ? data.cadastreNumber : '', 
            lat: data?.latitude ? data.latitude : '',
            lng: data?.longitude ? data.longitude : '',
            docCadatsre: {
                name: lang.forms.storeOrder.docCadastre
            },
            nomenclature: {
                name: lang.forms.order.titles.documentation
            },
            modelNo: data?.modelNo ? data.modelNo : '',
            machineNo: data?.machineNo ? data.machineNo : '',
            fiskalMod: data.fiskalMod ? data.fiskalMod : '',
            nickName: data.nickName,
            userMobile: data.userMobile,
            contractFile: {
                name: lang.forms.order.contractFile,
            },
            applicationFile: {
                name: 'application'
            },
            formCto: {
                name: lang.forms.order.titles.formCentTechService
            },
            registrationCardUrl: {
                name: 'fileFromNits'
            },
            oldData: data ? data : {}
        },
        onSubmit: handleSubmitForm,
        validate: validateForm,
        validateOnChange: false
    })

    const {
        values: {
            shopName, 
            cityAddress, 
            regionCode, 
            districtCode,
            activityGroup, 
            activityType, 
            nomenclature,
            cadastre, 
            lat,
            tin,
            lng,
            docCadatsre,
            modelNo,
            machineNo,
            fiskalMod,
            formCto,
            contractFile,
            applicationFile,
            nickName,
            userMobile,
            registrationCardUrl,
            oldData
        },
        errors : {
            shopNameErr, 
            cityAddressErr, 
            regionCodeErr, 
            districtCodeErr,
            activityGroupErr, 
            activityTypeErr, 
            cadastreErr, 
            latErr,
            lngErr,
            docCadatsreErr,
            modelNoErr,
            machineNoErr,
            fiskalModErr,
            contractFileErr
        },
        handleSubmit
    } = formik

    useEffect(() => {
        (data.status === 4) && setDisabled(false)
        data.statusDesc && snackbar.setSnackbar(`${lang.notifications.helperText.statusDescription} ${data.statusDesc}`, snackbarStatus.error, null)
    }, [])

    useEffect(() => {
        handleOpenRegions()
        handleOpenGroupActivity()
    }, [])

    useEffect(() => {
        handleSetDistricts(regionCode)
    }, [regions])

    useEffect(() => {
        handleSetActivityList(activityGroup)
    }, [groupActivities])

    useEffect(() => {
        if (modelNo && machineNo && fiskalMod) {
            setAccessPdfGenerate(true)
        } else {
            setAccessPdfGenerate(false)
        }
    }, [modelNo, machineNo, fiskalMod])

    useEffect(() => {
        accessPdfGenerate && handleGetInfo()
    }, [accessPdfGenerate])

    return (
        <div className = {styles.cont}>
            <div className = {styles.row}>
                <div className = {styles.item}>
                    <div className = {styles.map}>
                        <Map
                            center = {[lat, lng]}
                            getCoords = {handleSetCoords}
                            disabled = {disabled}
                        >
                            <Marker position = {[lat, lng]}/>
                        </Map>  
                        <div 
                            className = {styles.inputCont}
                        >   
                            <div className = {styles.input}>
                                <Input
                                    name = 'lat'
                                    id = 'lat'
                                    error = {latErr}
                                    value = {lat}
                                    disabled = {disabled}
                                    // onChange = {primaryOnChange}
                                    standart
                                    disabled
                                    label = {lang.forms.storeOrder.latitude}
                                />
                            </div>
                            <div className = {styles.input}>
                                <Input
                                    name = 'lng'
                                    id = 'lng'
                                    error = {lngErr}
                                    value = {lng}
                                    disabled = {disabled}
                                    // onChange = {primaryOnChange}
                                    standart
                                    disabled
                                    label = {lang.forms.storeOrder.longitude}
                                />
                            </div>
                        </div>
                    </div>
                    {accessPdfGenerate && <ModalForm
                        open = {applicationModal}
                        width = {'md'}
                        className = {styles.modal}
                        close = {handleCloseAppFile}
                        // keepMounted = {keepMounted}
                    >
                        <Paper 
                            elevation = {3}
                            className = {styles.appPaper}
                        >
                            <Pdf 
                                getPdfComponent = {setPdf}
                                fileName = {`S.N-${machineNo}, INN-${tin}, Date-${date}`}
                            >
                                <ApplicationNits
                                    data = {{
                                        ...formik.values,
                                        nickName,
                                        userMobile, 
                                        modelName: 'PAX A-930', 
                                        compName,
                                        tin
                                    }}
                                />
                            </Pdf>
                        </Paper>
                        <div className = {styles.buttonContApp}>
                            <Button onClick = {exportPDFWithComponent}>{lang.forms.buttonTitles.download}</Button>
                            <Button onClick = {handleCloseAppFile}>{lang.forms.buttonTitles.back}</Button>
                        </div>
                    </ModalForm>}
                </div>
                <div className = {styles.item}>
                    <Paper
                        elevation = {3} 
                        className = {styles.paper}
                    >
                        <div className = {styles.itemChilds}>
                            <Input
                                name = 'shopName'
                                id = 'shopName'
                                error = {shopNameErr}
                                value = {shopName}
                                disabled = {disabled}
                                onChange = {primaryOnChange}
                                standart
                                label = {lang.forms.storeOrder.name}
                            />
                        </div>
                        <div className = {styles.itemChilds}>
                            <div className = {styles.itemChildsBlock}>
                                <SelectCustom
                                    name = 'regionCode'
                                    error = {regionCodeErr}
                                    value = {regionCode}
                                    disabled = {disabled}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.regions}
                                    data = {regions}
                                    onOpen = {handleOpenRegions}
                                />
                                <SelectCustom
                                    name = 'activityGroup'
                                    error = {activityGroupErr}
                                    value = {activityGroup}
                                    disabled = {disabled}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.groupActivity}
                                    data = {groupActivities}
                                    onOpen = {handleOpenGroupActivity}
                                />
                            </div>
                            <div className = {styles.itemChildsBlock}>
                                <SelectCustom
                                    name = 'districtCode'
                                    error = {districtCodeErr}
                                    value = {districtCode}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.city}
                                    data = {districts}
                                    disabled = {!regions.length || !regionCode || disabled}

                                />
                                <SelectCustom
                                    name = 'activityType'
                                    error = {activityTypeErr}
                                    value = {activityType}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.typeActivity}
                                    data = {listActivities}
                                    disabled = {!groupActivities.length || !activityGroup || disabled}
                                />
                            </div>
                        </div>
                        <div className = {styles.itemChilds}>
                            <Input
                                name = 'cadastre'
                                id = 'cadastre'
                                error = {cadastreErr}
                                value = {cadastre}
                                disabled = {disabled}
                                onChange = {primaryOnChange}
                                standart
                                label = {lang.forms.storeOrder.numberCadastre}
                            />
                        </div>
                        <div className = {styles.itemChilds}>
                            <Input
                                standart
                                name = 'cityAddress'
                                id = 'cityAddress'
                                value = {cityAddress}
                                disabled = {disabled}
                                error = {cityAddressErr}
                                onChange = {primaryOnChange}
                                label = {lang.forms.storeOrder.address}
                                multiline
                                rows = {4}
                            />
                        </div>
                        <div className = {styles.itemChilds}>
                            <div className = {styles.itemChildsBlock}>
                                <UploadFile
                                    name = 'docCadatsre'
                                    value = {docCadatsre}
                                    disabled = {disabled}
                                    onChange = {primaryOnChange}                                                       
                                    error = {docCadatsreErr}
                                    oldFile = {oldData.docCadastre ? {
                                        name: oldData.docCadastre.name,
                                        url: oldData.docCadastre.source
                                    } : false}
                                />
                            </div>
                            <div className = {styles.itemChildsBlock}>
                                <UploadFile
                                    name = 'nomenclature'
                                    value = {nomenclature}
                                    disabled = {disabled}
                                    onChange = {primaryOnChange}                                                       
                                    // error = {nomenclatureErr}
                                    oldFile = {oldData.nomenclature ? {
                                        name: oldData.nomenclature.name,
                                        url: oldData.nomenclature.source
                                    } : false}
                                />
                            </div>
                        </div>
                    </Paper>
                </div>
            </div>
            <div className = {styles.row}>
                <div className = {styles.item}>
                    <Paper 
                        elevation = {3}
                        className = {styles.paper}
                    >
                        <div className = {styles.itemTitle}>{lang.forms.order.titles.agreement}</div>
                        <div className = {styles.topColumnItem}>
                            <div className = {styles.itemChilds}>
                                <UploadFile
                                    name = 'contractFile'
                                    onChange = {primaryOnChange}
                                    value = {contractFile}
                                    disabled = {data.status === 6 ? false : disabled}
                                    // acceptFile = 'application/pdf,application/vnd.ms-excel'
                                    error = {contractFileErr}
                                    oldFile = {oldData.contractFile ? {
                                        name: oldData.contractFile.name,
                                        url: oldData.contractFile.source
                                    } : false}
                                />
                            </div>
                        </div>
                        <div className = {styles.topColumnItem}>
                            <div className = {styles.itemChilds}>
                                <UploadFile
                                    name = 'formCto'
                                    onChange = {primaryOnChange}
                                    value = {formCto}
                                    disabled = {disabled}
                                    // acceptFile = 'application/pdf,application/vnd.ms-excel'
                                    // error = {txkmFileErr}
                                    oldFile = {oldData.formCTO ? {
                                        name: oldData.formCTO.name,
                                        url: oldData.formCTO.source
                                    } : false}
                                />
                            </div>
                        </div>
                        <div className = {styles.topColumnItem}>
                            <div className = {styles.itemChilds}>
                                <UploadFile
                                    name = 'applicationFile'
                                    onChange = {primaryOnChange}
                                    value = {applicationFile}
                                    disabled = {disabled}
                                    // acceptFile = 'application/pdf,application/vnd.ms-excel'
                                    // error = {txkmFileErr}
                                    oldFile = {oldData.applicationFile ? {
                                        name: oldData.applicationFile.name,
                                        url: oldData.applicationFile.source
                                    } : false}
                                />
                            </div>
                        </div> 
                        <div className = {styles.topColumnItem}>
                            <div className = {styles.itemChilds}>
                                <UploadFile
                                    // name = 'nomenclature'
                                    value = {registrationCardUrl}
                                    disabled
                                    // onChange = {primaryOnChange}                                                       
                                    // error = {nomenclatureErr}
                                    oldFile = {oldData.registrationCardUrl ? {
                                        name: 'oldData.registrationCardUrl.name',
                                        url: oldData.registrationCardUrl
                                    } : false}
                                />
                            </div>
                        </div> 
                    </Paper>
                    
                </div>
                <div className = {styles.item}>
                    <Paper 
                        elevation = {3}
                        className = {styles.paper}
                    >
                        <div className = {styles.itemTitle}>{lang.forms.order.titles.device}</div>
                        <div className = {styles.topColumnItem}>
                            <div className = {styles.itemChilds}>
                                <SelectCustom
                                    title = {lang.forms.order.deviceModel}
                                    name = 'modelNo'
                                    onChange = {primaryOnChange}
                                    error = {modelNoErr}
                                    value = {modelNo}
                                    disabled = {disabled}
                                    data = {modelNoData}
                                />
                            </div>
                            <div className = {styles.itemChilds}>
                                <Input
                                    name = 'machineNo'
                                    onChange = {primaryOnChange}
                                    error =  {machineNoErr}
                                    value = {machineNo}
                                    disabled = {disabled}
                                    standart
                                    label = {lang.forms.order.serializeNumber}
                                />
                            </div>
                            <div className = {styles.itemChilds}>
                                <Input
                                    name = 'fiskalMod'
                                    onChange = {primaryOnChange}
                                    error = {fiskalModErr}
                                    value = {fiskalMod}
                                    disabled = {disabled}
                                    standart
                                    label = {lang.forms.order.fiskNumber}
                                />
                            </div>
                        </div>
                    </Paper>
                    <div className = {styles.buttonCont}>
                        {accessPdfGenerate && !disabled && <Button
                            loading = {appButtonLoading}
                            onClick = {handleOpenAppFile}
                        >
                            {lang.forms.buttonTitles.openApplication}
                        </Button>}
                        {!disabled && <Button 
                            onClick = {handleSubmit}
                        >
                            {lang.forms.buttonTitles.edit}
                        </Button>}
                        {data.status === 6 && <Button 
                            onClick = {handleSubmit}
                        >
                            {lang.forms.buttonTitles.sentToNits}
                        </Button>}
                        <Button 
                            onClick = {handleCloseForm}
                        >
                            {lang.forms.buttonTitles.cancel}
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    )
})
