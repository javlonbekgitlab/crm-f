import { uploadProgress } from "../../../../actions"
import { Api, apiUrles } from "../../../../api"
import { localStorageKeys } from "../../../../constants"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmitApplication = async ({values = Object, shopId = String}) => {
    const {
        shopName, 
        cityAddress, 
        regionCode, 
        regionName,
        districtCode,
        districtName,
        activityGroup, 
        activityGroupName,
        activityType, 
        activityTypeName,
        nomenclature,
        cadastre, 
        lat,
        lng,
        docCadatsre,
        modelNo,
        machineNo,
        fiskalMod,
        formCto,
        contractFile,
        applicationFile
    } = values
 
    let binaryArrayForIndicatingFiles = [0, 0, 0, 0, 0]

    docCadatsre.size && (binaryArrayForIndicatingFiles[0] = 1)
    nomenclature.size && (binaryArrayForIndicatingFiles[1] = 1)
    contractFile.size && (binaryArrayForIndicatingFiles[2] = 1) 
    formCto.size && (binaryArrayForIndicatingFiles[3] = 1)
    applicationFile.size && (binaryArrayForIndicatingFiles[4] = 1)

    let newData = new FormData()
    newData.append('shopName', shopName)
    newData.append('cityAddress', cityAddress)
    newData.append('regionCode', regionCode)
    newData.append('regionName', regionName)
    newData.append('districtCode', districtCode)
    newData.append('districtName', districtName)
    newData.append('activityGroup', activityGroup)
    newData.append('activityGroupName', activityGroupName)
    newData.append('activityType', activityType)
    newData.append('activityTypeName', activityTypeName)
    newData.append('cadastreNumber', cadastre)
    newData.append('latitude', lat)
    newData.append('longitude', lng)
    newData.append('ccmCategoryId', modelNo)
    newData.append('ccmSerialNumber', machineNo)
    newData.append('fiskalMod', fiskalMod)
    newData.append('binaryArrayForIndicatingFiles', binaryArrayForIndicatingFiles)
    newData.append('multi-files', docCadatsre)
    newData.append('multi-files', nomenclature)
    newData.append('multi-files', contractFile)
    newData.append('multi-files', formCto)
    newData.append('multi-files', applicationFile)
    
    const response = {}

    const token = readLocalStorage(localStorageKeys.token)
    const res = await Api(`${apiUrles.updateApplication}${shopId}`, newData, 'post', token)
    console.log('update', res)
    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }
    // progressForm.setProgressForm(0)
    return response
}

export const onSubmitSendToNits = async data => {

    const formData = new FormData()

    formData.append('_id', data._id)
    data.contractFile && formData.append('multi-files', data.contractFile)
    const response = {}
    const token = readLocalStorage(localStorageKeys.token)
    console.log(data)
    const res = await Api(apiUrles.sendApplicationToNitsWithId, formData, 'post', token, false, uploadProgress)
    console.log('backdevice', res)
    if (res.status === 200) {
        response.successDevice = true
        response.dataDevice = res.data
    } else {
        console.log(res)
        response.successDevice = false
    }
    // progressForm.setProgressForm(0)
    return response
} 

export const validateForm = ({
    shopName, 
    cityAddress, 
    regionCode, 
    districtCode,
    activityGroup, 
    activityType, 
    cadastre, 
    lat,
    lng,
    modelNo,
    machineNo,
    fiskalMod,
}) => {

    const errors = {}

    if (shopName.length < 1) {
        errors.shopNameErr = 'required'
    }
    if (cityAddress.length < 1) {
        errors.cityAddressErr = 'required'
    }
    if (regionCode.length < 1) {
        errors.regionCodeErr = 'required'
    }
    if (districtCode.length < 1) {
        errors.districtCodeErr = 'required'
    }
    if (activityGroup.length < 1) {
        errors.activityGroupErr = 'required'
    }
    if (activityType.length < 1) {
        errors.activityTypeErr = 'required'
    }
    if (cadastre.length < 1) {
        errors.cadastreErr = 'required'
    }
    if (lat.length < 1) {
        errors.latErr = 'required'
    }
    if (lng.length < 1) {
        errors.lngErr = 'required'
    }
    if (modelNo.length < 1) {
        errors.modelNoErr = 'required'
    }
    if (machineNo.length < 1) {
        errors.machineNoErr = 'required'
    }
    if (fiskalMod.length < 1) {
        errors.fiskalModErr = 'required'
    }

    return errors
}