import React, { useCallback, useEffect, useState } from 'react'
import styles from './index.module.sass'
import { Paper } from '@material-ui/core'
import { Input } from '../../../input'
import { SelectCustom } from '../../../select'
import { UploadFile } from '../../../uploadFile'
import { Button } from '../../../button'
import { useFormik } from 'formik'
import { onSubmitStoreOrder, validateStoreOrderForm } from './helperFunctions'
import { Map } from '../../../map'
import { regexSpace } from '../../../../constants'
import { inject } from 'mobx-react'
import { Api } from '../../../../api'
import LocationOnOutlinedIcon from '@material-ui/icons/LocationOnOutlined'
import { onSubmitStore } from '../orderForm/helperFunctions'
import { getDistricts, getRegions, getGroupActivities, getListActivities } from '../../../../store/getDataFromServer'
import { transformRegions } from '../../../../store/transformers'
import { Marker } from 'react-leaflet'
import { autoSelectNames } from '../../../../actions'
import { CashierForm } from '../../../cashierForm'


export const StoreOrderForm = inject('lang')(
    ({
        onCloseForm, 
        data, 
        tinOrPinfl, 
        lang, 
        onSetActiveStep, 
        onSetStoreValues, 
        merchantId,
        merMapId
    }) => {

    const [regions, setRegions] = useState([])

    const [districts, setDistricts] = useState([])

    const [groupActivities, setGroupActivities] = useState([])

    const [listActivities, setListActivities] = useState([])

    // const handleClickMap = ({lat, lng}) => {
    //     // setCoordinate({lat, lng})
    //     console.log(lat, lng)
    //     formik.handleChange({target: {
    //         name: 'lat',
    //         value: lat
    //     }})
    //     formik.handleChange({target: {
    //         name: 'lng',
    //         value: lng
    //     }})
    // }

    const handleOpenRegions = async () => {
        const regions = await getRegions()
        if (regions) {
            const regionsFromServer = transformRegions(regions, 'name')
            setRegions(regionsFromServer)
        }
    }

    const handleOpenGroupActivity = async () => {
        const resFromServer = await getGroupActivities()
        if (resFromServer) {
            const groupActivitiesFromServer = transformRegions(resFromServer, 'name')
            setGroupActivities(groupActivitiesFromServer)
        }
    }

    const handleSetDistricts = async value => {
        setDistricts([])
        const districtList = await getDistricts(value)
        const districtFromServer = transformRegions(districtList, 'name')
        setDistricts(districtFromServer)
    }

    const handleSetActivityList = async value => {
        setListActivities([])
        const activityList = await getListActivities(value)
        const activityFromServer = transformRegions(activityList, 'name')
        setListActivities(activityFromServer)
    }

    const handleBack = () => onSetActiveStep(-1)

    const handleCloseForm = () => {
        onCloseForm('cancel')
        onSetActiveStep && onSetActiveStep(0)
    }

    const handleSubmitForm = async values => {
        // if (merchantId) {
        //     values.merchantId = merchantId
        //     values.merMapId = merMapId
        //     const { successStore } = await onSubmitStore(values)
        //     onCloseForm({ success: successStore })
        // } else {
            onSetActiveStep(1)
            onSetStoreValues(values)
        // }
    }

   
    const handleSetCoords = coords => {
        Object.keys(coords).map(key => formik.handleChange({target: {
            name: key,
            value: coords[key]
        }}))
    }

    // const handleDeleteItemOfCashier = phoneOfDeleteCashier => {
    //     const newCashiers = formik.values.cashiers.filter(cashier => cashier.phone !== phoneOfDeleteCashier)
    //     formik.handleChange({target: {
    //         name: 'cashiers',
    //         value: newCashiers
    //     }})
    // }

    const formik = useFormik({
        initialValues: {
            shopName: '', 
            cityAddress: '', 
            tinOrPinfl, 
            regionCode: '',
            regionName: '', 
            districtCode: '',
            districtName: '',
            group_of_activities: '', 
            activityGroupName: '',
            activityType: '',
            activityTypeName: '',
            cadastre: '', 
            lat: data?.lat ? data.lat : '',
            lng: data?.lng ? data.lng : '',
            // cashiers: [],
            docCadatsre: {
                name: lang.forms.storeOrder.docCadastre
            },
            nomenclature: {
                name: lang.forms.order.titles.documentation,
            },
        },
        onSubmit: handleSubmitForm,
        validate: validateStoreOrderForm,
        validateOnChange: false
    })

    const {
        values: {
            shopName, 
            cityAddress, 
            regionCode, 
            districtCode,
            group_of_activities, 
            activityType, 
            nomenclature,
            cadastre, 
            lat,
            lng,
            docCadatsre,
            // cashiers
        },
        errors : {
            shopNameErr, 
            cityAddressErr, 
            regionCodeErr, 
            districtCodeErr,
            group_of_activitiesErr, 
            activityTypeErr, 
            cadastreErr, 
            latErr,
            lngErr,
            docCadatsreErr,
            nomenclatureErr,
            mapErr,
            // cashiersErr
        },
        handleSubmit
    } = formik

    const primaryOnChange = useCallback(async({ target }) => {
        let value = ''
        if (target.files) {
            value = target.files[0]
        } else {
            // if (typeof target.value === 'string' || target.value instanceof String)
            //     // value = target.value.replace(regexSpace, '')
            // else
                value = target.value
            if (target.name === 'regionCode') {
                formik.handleChange({
                    target: {
                        name: 'districtCode',
                        value: ''
                    }
                })
                handleSetDistricts(value)
                formik.handleChange(autoSelectNames({
                    name: 'regionName',
                    data: regions,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            if (target.name === 'group_of_activities') {
                formik.handleChange({
                    target: {
                        name: 'activityType',
                        value: ''
                    }
                })
                handleSetActivityList(value)
                formik.handleChange(autoSelectNames({
                    name: 'activityGroupName',
                    data: groupActivities,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            if (target.name === 'districtCode') {
                formik.handleChange(autoSelectNames({
                    name: 'districtName',
                    data: districts,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            if (target.name === 'activityType') {
                formik.handleChange(autoSelectNames({
                    name: 'activityTypeName',
                    data: listActivities,
                    keyOfValue: 'title',
                    value: parseInt(value)
                }))
            }
            // if (target.name === 'cashiers') {
            //     value = [...formik.values.cashiers, target.value]
            // }
        }
        const event = {
            target: {
                name: target.name,
                value
            }
        }
        formik.handleChange(event)
    }, [regions, districts, groupActivities, listActivities])

    return (
        <div className = {styles.cont}>
            <div className = {styles.row}>
                <div className = {styles.item}>
                    <div className = {styles.map}>
                        <Map
                            getCoords = {handleSetCoords}
                            error = {mapErr}
                        >
                            {lat && lng && <Marker position = {[lat, lng]}/>}
                        </Map>  
                        <div 
                            className = {styles.inputCont}
                        >   
                            <div className = {styles.input}>
                                <Input
                                    name = 'lat'
                                    id = 'lat'
                                    error = {latErr}
                                    value = {lat}
                                    // onChange = {primaryOnChange}
                                    standart
                                    disabled
                                    label = {lang.forms.storeOrder.latitude}
                                />
                            </div>
                            <div className = {styles.input}>
                                <Input
                                    name = 'lng'
                                    id = 'lng'
                                    error = {lngErr}
                                    value = {lng}
                                    // onChange = {primaryOnChange}
                                    standart
                                    disabled
                                    label = {lang.forms.storeOrder.longitude}
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div className = {styles.item}>
                    <Paper
                        elevation = {3} 
                        className = {styles.paper}
                    >
                        <div className = {styles.itemChilds}>
                            <Input
                                name = 'shopName'
                                id = 'shopName'
                                error = {shopNameErr}
                                value = {shopName}
                                onChange = {primaryOnChange}
                                standart
                                label = {lang.forms.storeOrder.name}
                            />
                        </div>
                        {/* <div className = {styles.itemChilds}>
                            <div className = {styles.itemChildsTop}>
                                <Input
                                    name = 'inn'
                                    id = 'inn'
                                    error = {innErr}
                                    value = {inn}
                                    onChange = {primaryOnChange}
                                    standart
                                    label = {lang.forms.order.stir}
                                />
                            </div>
                            <div className = {styles.itemChildsTop}>
                                <Input
                                    name = 'shopName'
                                    id = 'shopName'
                                    error = {shopNameErr}
                                    value = {shopName}
                                    onChange = {primaryOnChange}
                                    standart
                                    label = {lang.forms.storeOrder.name}
                                />
                            </div>
                        </div> */}
                        <div className = {styles.itemChilds}>
                            <div className = {styles.itemChildsBlock}>
                                <SelectCustom
                                    name = 'regionCode'
                                    error = {regionCodeErr}
                                    value = {regionCode}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.regions}
                                    data = {regions}
                                    onOpen = {handleOpenRegions}
                                />
                                <SelectCustom
                                    name = 'group_of_activities'
                                    error = {group_of_activitiesErr}
                                    value = {group_of_activities}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.groupActivity}
                                    data = {groupActivities}
                                    onOpen = {handleOpenGroupActivity}
                                />
                            </div>
                            <div className = {styles.itemChildsBlock}>
                                <SelectCustom
                                    name = 'districtCode'
                                    error = {districtCodeErr}
                                    value = {districtCode}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.city}
                                    data = {districts}
                                    disabled = {!regions.length || !regionCode}

                                />
                                <SelectCustom
                                    name = 'activityType'
                                    error = {activityTypeErr}
                                    value = {activityType}
                                    onChange = {primaryOnChange}
                                    title = {lang.forms.storeOrder.typeActivity}
                                    data = {listActivities}
                                    disabled = {!groupActivities.length || !group_of_activities}
                                />
                            </div>
                        </div>
                        <div className = {styles.itemChilds}>
                            <Input
                                name = 'cadastre'
                                id = 'cadastre'
                                error = {cadastreErr}
                                value = {cadastre}
                                onChange = {primaryOnChange}
                                standart
                                label = {lang.forms.storeOrder.numberCadastre}
                            />
                        </div>
                        <div className = {styles.itemChilds}>
                            <Input
                                standart
                                name = 'cityAddress'
                                id = 'cityAddress'
                                value = {cityAddress}
                                error = {cityAddressErr}
                                onChange = {primaryOnChange}
                                label = {lang.forms.storeOrder.address}
                                multiline
                                rows = {4}
                            />
                        </div>
                        <div className = {styles.itemChilds}>
                            <div className = {styles.itemChildsBlock}>
                                <UploadFile
                                    name = 'docCadatsre'
                                    value = {docCadatsre}
                                    onChange = {primaryOnChange}                                                       
                                    error = {docCadatsreErr}
                                    // oldFile = {data.file_url ? {
                                    //     name: data.file_name_store_2,
                                    //     url: data.file_url
                                    // } : false}
                                />
                            </div>
                            <div className = {styles.itemChildsBlock}>
                                <UploadFile
                                    name = 'nomenclature'
                                    value = {nomenclature}
                                    onChange = {primaryOnChange}                                                       
                                    error = {nomenclatureErr}
                                    // oldFile = {data.file_url ? {
                                    //     name: data.file_name_store_2,
                                    //     url: data.file_url
                                    // } : false}
                                />
                            </div>
                        </div>
                    </Paper>
                </div>
                {/* <div className = {styles.item}>
                    <CashierForm
                        onClickAdd = {primaryOnChange}
                        onClickDelete = {handleDeleteItemOfCashier}
                        name = 'cashiers'
                        value = {cashiers}
                        error = {cashiersErr}
                    />
                </div> */}
            </div>
            <div className = {styles.row}>
                <div className = {styles.buttonCont}>
                    {onSetActiveStep && <Button onClick = {handleBack}>{lang.forms.buttonTitles.back}</Button>}
                    <Button onClick = {handleSubmit}>{onSetActiveStep ? lang.forms.buttonTitles.next : lang.forms.buttonTitles.add}</Button>
                    <Button onClick = {handleCloseForm}>{lang.forms.buttonTitles.cancel}</Button>
                </div>
            </div>
        </div>
    )
})
