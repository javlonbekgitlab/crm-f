import { Api, apiUrles } from "../../../../api"
import { readLocalStorage } from "../../../../store/localStorage"

export const onSubmitStoreOrder = async values => {

    const response = {}

    const {
        shopName, 
        cityAddress, 
        inn, 
        region, 
        town,
        group_of_activities, 
        activity, 
        cadastre, 
        rent,
        model,
        serial_number,
        docCadatsre,
        docAgreement
    } = values

    let newData = new FormData()

    newData.append('shopName', shopName)
    newData.append('cityAddress', cityAddress)
    newData.append('inn', parseInt(inn))
    newData.append('region', region)
    newData.append('town', town)
    newData.append('group_of_activities', group_of_activities)
    newData.append('activity', activity)
    newData.append('cadastreNumber', cadastre)
    newData.append('rent', rent)
    newData.append('model', model)
    newData.append('serial_number', parseInt(serial_number))
    newData.append('multi-files', docCadatsre)
    newData.append('multi-files', docAgreement)

    const token = readLocalStorage('token')

    const res = await Api(apiUrles.ordersStoreAdd, newData, 'post', token)

    if (res.status === 200) {
        response.success = true
        response.data = res.data
    } else {
        console.log(res)
        response.success = false
    }

    return response
}

export const validateStoreOrderForm = ({
    shopName, 
    cityAddress, 
    regionCode, 
    districtCode,
    group_of_activities, 
    activityType, 
    nomenclature,
    cadastre, 
    lat,
    lng,
    docCadatsre,
    // cashiers
}) => {

    const errors = {}


    // if (shopName.length < 1) {
    //     errors.shopNameErr = 'required'
    // }
    // if (cityAddress.length < 1) {
    //     errors.cityAddressErr = 'required'
    // }
    // if (regionCode.length < 1) {
    //     errors.regionCodeErr = 'required'
    // }
    // if (districtCode.length < 1) {
    //     errors.districtCodeErr = 'required'
    // }
    // if (group_of_activities.length < 1) {
    //     errors.group_of_activitiesErr = 'required'
    // }
    // if (activityType.length < 1) {
    //     errors.activityTypeErr = 'required'
    // }
    // // if (cashiers.length < 1) {
    // //     errors.cashiersErr = 'required'
    // // }
    // // if (!nomenclature.size) {
    // //     errors.nomenclatureErr = 'required'
    // // }
    // if (cadastre.length < 1) {
    //     errors.cadastreErr = 'required'
    // }
    // if (lat.length < 1) {
    //     errors.latErr = 'required'
    // }
    // if (lng.length < 1) {
    //     errors.lngErr = 'required'
    // }
    // if (!docCadatsre.size) {
    //     errors.docCadatsreErr = 'required'
    // }
    // if (!lat || !lng) {
    //     errors.mapErr = 'set point'
    // }

    return errors
}

// let req  
// (req = () => {
//     const appsecret = '&appsecret=0664a3cc2f08b057704024af64350333'
//     let tmp = ''
//     const data = {
//         opMapId: 10000001,
//         cityCountyId: parseInt(110000),
//         time: Math.floor(new Date().getTime() / 1000),
//     }
//     const ordered = Object.keys(data).sort().reduce((obj, key) => { 
//             obj[key] = data[key] 
//             return obj
//         }, 
//     {})
//     const sign = sha1(querystring.stringify(ordered,  null, null, { encodeURIComponent: querystring.unescape }) + appsecret)
//     data.sign = sign
//     const res = Api(apiUrles.getUrban, data, 'post', null, apiUrles.chinaHosting)
//     console.log(res)
//     // const headers = {
//     //     'Content-Type': 'application/json',
//     // }
//     // const httpreq = http.request({
//     //     host: 'ds.51zzd.com',
//     //     path: '/control/api/mer/queryCity',
//     //     port: 80,
//     //     method: 'POST',
//     //     headers
//     // }, (http_res) => {
//     //     http_res.on('data', function (chunk) {
//     //         tmp += chunk
//     //     })
//     //     http_res.on('end', function() {
//     //         res.status(200).send(tmp)
//     //         const test = JSON.parse(tmp)
//     //         // console.log(test.data)
//     //     })
//     // })
// })()