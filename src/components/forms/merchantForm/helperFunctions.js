export const validate = values => {
    
    const {nickName, opName, userMobile, password} = values

    const errors = {}

    if (nickName.length <= 2) {
        errors.nickNameError = 'min 2 symb required'
    }

    if (opName.length <= 2) {
        errors.opNameError = 'min 2 symb required'
    }

    if (userMobile.length <= 2) {
        errors.userMobileError = 'min 2 symb required'
    }

    if (password.length <= 2) {
        errors.passwordError = 'min 2 symb required'
    }

    return errors
}

