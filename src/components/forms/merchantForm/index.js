// import React from 'react'
// import styles from '../index.module.sass'
// import { Button } from '../../button'
// import PropTypes from 'prop-types'
// import { useFormik } from 'formik'
// import { validate } from './helperFunctions'
// import InputMask from 'react-input-mask'
// import { Api, apiUrles } from '../../../api'
// import { getMerchantList } from '../../../store/getDataFromServer'
// import { Input } from '../../input'
// import { maskUserMobile, regexSpace } from '../../../constants'
// import { inject } from 'mobx-react'

// export const MerchantForm = inject('lang')(({edit, closeForm, data, partnerName, thirdOpMapId, lang}) => {

//     const onSubmit = values => {
//         if (edit) {
//             updateMerchant(values)
//         } else {
//             addMerchant(values)
//         }
//     }

//     const addMerchant = async values => {

//         const {opName, nickName, userMobile, password} = values

//         const newData = {
//             opName,
//             thirdOpMapId,
//             nickName,
//             userMobile: parseInt(userMobile.replace(regexSpace, '')),
//             password
//         }

//         const resMerchant = await Api(apiUrles.merchantAdd, newData, 'post')

//         if (resMerchant.status === 200) {
//             closeForm()
//             getMerchantList()
//         } else {
//             alert(resMerchant.toString()) 
//         }
//     }

//     const updateMerchant = async values => {

//         const {thirdMerMapId} = data

//         const {nickName, userMobile, password} = values

//         const newData = {
//             nickName,
//             userMobile: parseInt(userMobile.replace(regexSpace, '')),
//             password
//         }

//         const resMerchant = await Api(`${apiUrles.merchantUpdate}${thirdMerMapId}`, newData, 'post')

//         if (resMerchant.status === 200) {
//             closeForm()
//             getMerchantList()
//         } else {
//             alert(resMerchant.toString()) 
//         }
//     }

//     const formik = useFormik({
//         initialValues: {
//             opName: partnerName,
//             nickName: edit ? data.nickName : '',
//             password: '',
//             userMobile: edit ? data.userMobile : ''
//         },
//         validate,
//         onSubmit
//     })

//     const { 
//         errors: {opNameError, nickNameError, passwordError, userMobileError},
//         values: {opName, nickName, password, userMobile},
//         handleChange,
//         handleSubmit
//     } = formik

//     return (
//         <div className = {styles.cont}>
//             {!edit && <Input 
//                 name = 'opName'
//                 disabled
//                 label={lang.forms.merchant.opName} 
//                 helperText = {opNameError}
//                 error = {opNameError}
//                 onChange = {handleChange}
//                 value={opName}
//             />}
//             <InputMask
//                 onChange = {handleChange}
//                 value = {userMobile}
//                 mask = {maskUserMobile} 
//                 name = 'userMobile'
//                 helperText = {userMobileError}
//             >
//                 {inputProps => <Input 
//                     label={lang.forms.merchant.userMobile} 
//                     {...inputProps}
//                     error = {userMobileError}
//                 />}
//             </InputMask> 
//             <Input 
//                 name = 'nickName'
//                 label={lang.forms.merchant.nickName} 
//                 helperText = {nickNameError}
//                 error = {nickNameError}
//                 onChange = {handleChange}
//                 value = {nickName}
//             />
//             <Input 
//                 name = 'password'
//                 type = 'password'
//                 label={lang.forms.merchant.password} 
//                 helperText = {passwordError}
//                 error = {passwordError}
//                 onChange = {handleChange}
//                 value = {password}
//             />
//             <div className = {styles.buttonCont}>
//                 <Button 
//                     onClick = {handleSubmit}
//                 >
//                     {edit ? lang.forms.buttonTitles.edit : lang.forms.buttonTitles.add}
//                 </Button>
//                 <Button 
//                     onClick={closeForm}
//                 >
//                     {lang.forms.buttonTitles.cancel}
//                 </Button>
//             </div>
//         </div>
//     )
// })

// MerchantForm.propTypes = {
//     edit: PropTypes.bool,
//     closeForm: PropTypes.func,
//     data: PropTypes.object,
//     partnerName: PropTypes.string,
//     thirdOpMapId: PropTypes.string
// }