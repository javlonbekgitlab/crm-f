import React, { memo, useMemo } from 'react'
import TextField from '@material-ui/core/TextField'

export const Input = memo(({standart, error, maxLength, ...props}) => {

    // const children = useMemo(() => props.children, [])

    console.log('input')

    return (
        <TextField 
            margin = 'dense' 
            size = 'small'
            fullWidth
            variant={standart ? 'standard': 'outlined'}
            helperText = {error}
            error = {error ? true : false}
            inputProps = {{
                step: 100,
                maxLength
            }}
            InputLabelProps={{
                style: {
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                width: '100%',
                fontSize: '0.9em',
                color: '#696969',
                } }}
            InputProps={props.children 
                ? {
                    endAdornment: (
                        props.children
                    ),
                } 
                : {}
            } 
            {...props}
        />
    )
})
