import React, { useState } from 'react'
import MaterialTable from 'material-table'
import PropTypes from 'prop-types'
import styles from './index.module.sass'
import { storeList } from '../../store'
import { AddButton } from '../addButton'
import { observer } from 'mobx-react-lite'
import { StoreTable } from '../storeTable'

export const TreeTable = observer(({title, openAddForm, data, onRowClick}) => { 
    
    const [storeData] = useState(storeList.data) 

    const handleDetailClick = () => ({
        render: rowData => {
            const { thirdMerMapId } = rowData
            const filteredStore = storeData.filter(
                store => store.thirdMerMapId === thirdMerMapId
            )
            return (
                <StoreTable
                    parentId = {thirdMerMapId}
                    data = {filteredStore}
                />
            )
        }
    })

    return (
    <MaterialTable
        components = {{
            Toolbar: props => {
                return (
                    <TableHeader title = {title} addClick = {openAddForm} props = {props}/>
                )
            },
        }}
        data={data}
        columns = {[
            {
                title: 'ID',
                field: 'thirdOpMapId',
                align: 'center',
            },
            {
                title: 'Имя партнера',
                field: 'opName',
                align: 'center',
            },
            {
                title: 'Имя пользователя',
                field: 'nickName',
                align: 'center'
            },
            {
                title: 'Логин ID',
                field: 'userMobile',
                align: 'center'
            },
            {
                title: 'Дата создания',
                field: 'time',
                align: 'center',
            }
        ]}
        detailPanel={[handleDetailClick]}
        onRowClick={onRowClick}
        options={{
            search: false,
            showSelectAllCheckbox: false,
            paging: false,
            sorting: false,
            maxBodyHeight: 400,
            showTitle: false,
            columnsButton: true,
            headerStyle: {
                // fontSize: '1.05em',
                // fontWeight: 'normal',
                // color: '#00000070',
                // alignItems: 'center',
                // justifyContent: 'center',
                // textAlign: 'center'
            },
            rowStyle: {
                alignItems: 'center',
                justifyContent: 'center',
                textAlign: 'center',
            },
        }}
    />
    )
})

TreeTable.propTypes = {
    title: PropTypes.string.isRequired,
    idPartner: PropTypes.string,
    openAddForm: PropTypes.func,
    openNestedAddForm: PropTypes.func
}

const TableHeader = ({title, addClick}) => {
    return (
        <div className = {styles.tableHeaderCont}>
            <div className = {styles.tableHeaderTitle}>{title}</div>
            <div className = {styles.addButtonCont}>
                <AddButton color onClick = {addClick}/>
            </div>
        </div>
    )
}

TableHeader.propTypes = {
    title: PropTypes.string.isRequired,
    addClick: PropTypes.func.isRequired
}
