import React, { memo } from 'react'
import styles from './index.module.sass'
import { MenuItem } from '@material-ui/core'
import { ButtonWithIcon } from '../../../button'
import { inject } from 'mobx-react'
import PropTypes from 'prop-types'

export const Confirmation = inject('lang')(memo(({title, text, lang, onClickAdd, onCloseMenu}) => {
    
    return (
        <div className = {styles.cont}>
            <div className = {styles.title}>{title}</div>
            <div className = {styles.text}>{text}</div>
            <div className = {styles.actionCont}>
                <ButtonWithIcon onClick = {onCloseMenu}>{lang.forms.buttonTitles.add}</ButtonWithIcon>
                <ButtonWithIcon onClick = {onCloseMenu} >{lang.forms.buttonTitles.cancel}</ButtonWithIcon>
            </div>
        </div>
    )
}))

Confirmation.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    onClickAdd: PropTypes.func,
    onCloseMenu: PropTypes.func
}

