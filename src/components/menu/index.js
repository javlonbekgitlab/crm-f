import React, { memo, useState } from 'react'
import Menu from '@material-ui/core/Menu'
import styles from './index.module.sass'
import { MenuItem } from '@material-ui/core'
import Checkbox from '@material-ui/core/Checkbox'
import { useCallback } from 'react'


export const CustomMenu = memo(({children, button, render}) => {
    const [anchorEl, setAnchorEl] = useState(null)

    const handleClick = useCallback((event) => {
        setAnchorEl(event.currentTarget)
    }, [])

    const handleClose = useCallback(() => {
        setAnchorEl(null)
    }, [])

    return (
        <div className = {styles.cont}>
            <div onClick = {handleClick}>
                {button}
            </div>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {children && children}
                {render && <div className = {styles.renderCont}>{render(handleClose)}</div>}
            </Menu>
        </div>
    )
})

export const CustomMenuItem = memo(({checked, children, ...props}) => {
    return (
        <MenuItem 
            className = {styles.menuItem}
            {...props}
        >
            <Checkbox
                checked = {checked}
                color="primary"
            />
            <div className = {styles.label}>{children}</div>
        </MenuItem>
    )
})
