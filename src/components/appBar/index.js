import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'   
import HomeIcon from '@material-ui/icons/Home' 
import { useHistory } from 'react-router-dom'
import logo from '../../assets/logo/whiteArca.svg'
import PropTypes from 'prop-types'
import { routes } from '../../store/routes'

export const AppBarCustom = ({title, children, hideGoBack, logoShow, goBack, goHome}) => {

    const classes = useStyles()

    const history = useHistory()

    const handleBack = () => history.goBack()

    const handleGoHome = () => history.push(routes.menu)

    return (
        <AppBar 
            position="fixed"
            className = {classes.cont}
        >
            <div className = {classes.item}>
                <div className = {classes.itemAbsolute}>
                    <ArrowBackIcon onClick = {goBack ? goBack : handleBack} className = {hideGoBack ? `${classes.icon} ${classes.hide}` : classes.icon}/>
                    {(!goBack && goHome) && <HomeIcon onClick = {handleGoHome} className = {`${classes.icon} ${classes.homeIcon}`}/>}
                </div>
            </div>
            {logoShow && <div className = {classes.logoCont}>
                <img className = {classes.logo} src = {logo} alt = 'logo'/>
            </div>}
            {title && <div className = {classes.titleCont}>
                <div className = {classes.title}>{title}</div>
            </div>}
            {children}
        </AppBar>
    )
}

AppBarCustom.propTypes = {
    title: PropTypes.string,
    logoShow: PropTypes.bool,
    hideGoBack: PropTypes.bool,
    children: PropTypes.element,
    goBack: PropTypes.func,
    goHome: PropTypes.bool
}

const useStyles = makeStyles(() => ({
    cont: {
        display: 'flex',
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        position: 'relative',
        zIndex: 22,
        color: '#fff',
        // padding: '2em'
    },
    item: {
        display: 'flex',
        alignItems: 'center',
        position: 'relative',
    },
    itemAbsolute: {
        display: 'flex',
        position: 'absolute',
        left: '25px'
    },
    logoCont: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: '1em'
    },
    logo: {
        objectFit: 'contain',
        width: '9em'
    },
    hide: {
        opacity: 0,
        pointerEvents: 'none'
    },
    icon: {
        fontSize: '2.2em',
        transition: 'all 0.3s',
        borderRadius: '0.1em',
        // padding: '0.5em',
        color: '#ffffff',
        cursor: 'pointer',
        '&:hover': {
            backgroundColor: '#ffffff10'
        },
        '&:active': {
            transform: 'scale(0.8)'
        }
    },
    homeIcon: {
        position: 'absolute',
        left: '140%'
    },
    titleCont: {
        // width: '100%',
        display: 'flex',
        position: 'absolute',
        left: '50%',
        fontWeight: 'bold',
        fontSize: '1.8em',
        // textTransform: 'capitalize'
    },
    title: {
        position: 'relative',
        left: '-50%'
    }
}))