import React, { useState } from "react"
// import { drawDOM, exportPDF } from "@progress/kendo-drawing";
import { PDFExport, savePDF } from "@progress/kendo-react-pdf"
import styles from './index.module.sass'
// import { transliterateObject } from "../../actions";
// import '../../actions/stringMap.js'

export const Pdf = ({getPdfComponent, children, fileName}) => {

  const pdfExportComponent = React.useRef(null)

   

  // const exportPDFWithMethod = () => {
  //   let element = document.querySelector(".test") || pdfExportComponent.current.rootElForPDF;
  //   console.log(element)
  //   console.log(pdfExportComponent.current.rootElForPDF)
  //   drawDOM(element, {
  //     paperSize: "A4",
  //   }).then((group) => {
  //     return exportPDF(group);
  //   })
  //   .then((dataUri) => {
  //     console.log(dataUri)
  //     console.log(dataUri.split(";base64,")[1])
  //     // setData(dataUri.split(";base64,")[1])
  //   });
  // };

  // const exportPDFWithComponent = () => {
  //   if (pdfExportComponent.current) {
  //     pdfExportComponent.current.save();
  //   }
  //   console.log(pdfExportComponent)
  // };

  return (
    <div className = {styles.mainCont}>
      <PDFExport fileName = {fileName} ref={pdfExportComponent} paperSize="A3">
        {getPdfComponent && getPdfComponent(pdfExportComponent)}
        {children}
      </PDFExport>
    </div>
  );
};

