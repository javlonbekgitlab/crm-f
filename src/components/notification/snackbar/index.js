import React, { memo, useState } from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'

const Alert = props => {
    return <MuiAlert elevation={3} variant="filled" {...props} />
}

export const CustomSnackbar = memo(({message, show, status, setShow, autoHideDuration = 1500}) => {

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return
        }
        setShow(false)
    }
    
    const handleOnClick = () => setShow(false)

    return (
        <Snackbar 
            open={show} 
            autoHideDuration={autoHideDuration}
            anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
            onClose={handleClose}
            onClick = {handleOnClick}
        >
            <Alert
                onClose = {!autoHideDuration && handleOnClick} 
                severity={status}
            >
                {message}
            </Alert>
        </Snackbar>
    )
})