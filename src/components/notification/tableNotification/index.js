import { inject, observer } from 'mobx-react'
import React from 'react'
import { noData } from './constants'
import styles from './index.module.sass'

export const Notification = inject('lang')(observer(({lang}) => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.item}>{lang.table.notifications.noData}</div>
        </div>
    )
}))


