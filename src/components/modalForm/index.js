import React, { memo } from 'react'
import DialogContent from '@material-ui/core/DialogContent'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import PropTypes from 'prop-types'
import styles from './index.module.sass'

export const ModalForm = memo(({open, title, close, children, width, keepMounted, ...props}) => {

  const handleClose = () => {
    close && close(false)
  }

  return (
    <>
      <Dialog  
        open={open} 
        onClose={handleClose} 
        aria-labelledby="form-dialog-title"
        fullWidth = {width && true}
        maxWidth = {width}
        keepMounted = {keepMounted}
        {...props}
      >
        {title && <DialogTitle id="form-dialog-title">{title}</DialogTitle>}
        <DialogContent className = {styles.dialogContent}>
          {children}
        </DialogContent>
      </Dialog>
    </>
  )
}
)
ModalForm.propTypes = {
    open: PropTypes.bool.isRequired,
    title: PropTypes.string,
    close: PropTypes.func,
    width: PropTypes.string,
    keepMounted: PropTypes.bool
}