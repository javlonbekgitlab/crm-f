// import React, { useCallback, useEffect, useState } from 'react'
// import { tableTitle } from '../../pages/admin-page-oldVersion/constants'
// import Dialog from '@material-ui/core/Dialog'
// import DialogContent from '@material-ui/core/DialogContent'
// import DialogContentText from '@material-ui/core/DialogContentText'
// import DialogTitle from '@material-ui/core/DialogTitle'
// import { TreeTable } from '../treeTable'
// import Paper from '@material-ui/core/Paper'
// import Draggable from 'react-draggable'
// import { ModalForm } from '../modalForm'
// import { MerchantForm } from '../forms/merchantForm'
// import { getMerchantList, getStoreList } from '../../store/getDataFromServer'
// import { merchantList } from '../../store'
// import { observer } from 'mobx-react-lite'
// import { inject } from 'mobx-react'

// export const MerchantTable = inject('lang')(observer(({open, currentPartner, closeMerchant, lang}) => {

//     const {thirdOpMapId, opName} = currentPartner

//     useEffect(() => {
//         getMerchantList()
//         getStoreList()
//     }, [])

//     const filteredMerchant = merchantList.data.filter(
//         merchant => merchant.thirdOpMapId === thirdOpMapId
//     ) 

//     filteredMerchant.map(
//         (merchant, index) => filteredMerchant[index].opName = opName
//     )

//     const [merchantEdit, setMerchantEdit] = useState(false)

//     const [merchantForm, setMerchantForm] = useState(false)

//     const [selectedMerchant, setSelectedMerchant] = useState({})

//     const handleOpenMerchantForm = useCallback(() => {
//         setMerchantForm (true)
//         setMerchantEdit(false)
//     },[])

//     const handleCloseMerchantForm  = () => {
//         setMerchantForm (false)
//     }   

//     const handleRowClick = useCallback((e, selectedMerchantFromParam) => {
//         setMerchantEdit(true)
//         setMerchantForm(true)
//         setSelectedMerchant(selectedMerchantFromParam)
//     }, [])  

//     return (
//         <>
//             <Dialog
//                 open={open}
//                 onClose={closeMerchant}
//                 PaperComponent={PaperComponent}
//                 aria-labelledby="draggable-dialog-title"
//                 fullWidth = {true}
//                 maxWidth = 'xl'
//             >
//                 <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">
//                 </DialogTitle>
//                 <DialogContent>
//                     <DialogContentText>
//                         <TreeTable
//                             title = {tableTitle.merchant}
//                             openAddForm = {handleOpenMerchantForm}
//                             onRowClick = {handleRowClick}
//                             data = {filteredMerchant}
//                         />
//                     </DialogContentText>
//                 </DialogContent>
//             </Dialog>
//             <ModalForm
//                 title = {merchantEdit ? lang.forms.merchant.editMerchant : lang.forms.merchant.addMerchant}
//                 open = {merchantForm}
//                 close = {handleCloseMerchantForm}
//             >
//                 <MerchantForm
//                     edit = {merchantEdit}
//                     closeForm = {handleCloseMerchantForm}
//                     data = {selectedMerchant}
//                     partnerName = {opName}
//                     thirdOpMapId = {thirdOpMapId}
//                 />
//             </ModalForm>
//         </>
//     )
// }))

// const PaperComponent = props => {
//     return (
//       <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
//         <Paper {...props} />
//       </Draggable>
//     )
// }


