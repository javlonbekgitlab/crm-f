import React from 'react'
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns'
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers'
import styles from './index.module.sass'


export const DatePicker = ({value, onChange, label, name, disabled}) => {

    const handleChange = date => {
        onChange({
            target: {
                value: date,
                name,
            }
        })
    }

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <KeyboardDatePicker
                disableToolbar
                variant="inline"
                format="dd/MM/yyyy"
                margin="normal"
                // id="date-picker-inline"
                label={label}
                value={value}
                autoOk
                disabled = {disabled}
                onChange={handleChange}
                className = {styles.cont}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            />
        </MuiPickersUtilsProvider>
    )
}