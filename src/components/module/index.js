import React, { forwardRef } from 'react'
import styles from './index.module.sass'
import LockIcon from '@material-ui/icons/Lock'
import locked from '../../assets/icons/lock.svg'

export const Module = forwardRef(({active, id, title, children, notLocked, showTitleModule, onOpen}, ref ) => {

    const handleOpen = e => {
        notLocked && onOpen(e)
    }

    return (
        <div 
            className = {active === id ? `${styles.child} ${styles.fullScreenModule}` : `${notLocked ? `${styles.child}` : `${styles.child} ${styles.monochrome}`}`}
            ref = {ref}
        >
            <div 
                className = {!showTitleModule ? (active === id ? `${styles.layer}` : `${notLocked ? `${styles.layer} ${styles.hoverLayer}` : `${styles.layer}`} `) : `${styles.hide}`}
                id = {id}
                onClick = {handleOpen}
            >
                {notLocked 
                    ? <> 
                        {active !== id && <div className = {styles.moduleHeaderCont}>
                            <div 
                                className = {active === id ? `${styles.title}` : `${styles.title} ${styles.titleShow}`}
                            >
                                {title}
                            </div>
                        </div>}
                        {active === id && <div className = {active === id ? `${styles.submenu} ${styles.submenuShow}` : `${styles.submenu}`}>
                            <div className = {styles.miniHeaderCont}>
                                <div 
                                    className = {styles.title}
                                >
                                    {title}
                                </div>
                            </div>
                            <div className = {styles.childCont}>
                                {children}
                            </div>
                        </div>}
                    </>
                    : <div className = {styles.lockIconCont}>
                        {/* <img src = {locked} alt = 'locked' className = {styles.lockIcon}/> */}
                        {/* <LockIcon className = {styles.lockIcon}/> */}
                    </div>
                }
            </div>
        </div>
    )
})