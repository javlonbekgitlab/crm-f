import React, { useEffect, useState } from 'react'
import { storeTableTitles } from '../../pages/admin-page-oldVersion/constants'
import { CustomTable } from '../table'
import PropTypes from 'prop-types'
import { StoreForm } from '../forms/storeForm'
import { ModalForm } from '../modalForm'
import { getStoreList } from '../../store/getDataFromServer'
import { observer } from 'mobx-react-lite'
import { inject } from 'mobx-react'

export const StoreTable = inject('lang')(observer(({data, parentId, lang}) => {

    useEffect(() => {
        getStoreList()
    }, [])

    const [storeEdit, setStoreEdit] = useState(false)

    const [storeForm, setStoreForm] = useState(false)

    const [selectedStore, setSelectedStore] = useState({})

    const handleAddStore = () => {
        setStoreForm(true)
        setStoreEdit(false)
    } 

    const handleCloseStoreForm = () => {
        setStoreForm(false)
    }

    const handleEdit = store => {
        setStoreForm(true)
        setStoreEdit(true)
        setSelectedStore(store)
    }

    return (
        <>
            <CustomTable
                tableContent = {data}
                tableCellTitles = {storeTableTitles}
                onClickPlusButton = {handleAddStore}
                rowClick = {handleEdit}
            />
            <ModalForm
                title = {storeEdit ? lang.forms.store.editStore : lang.forms.store.addStore}
                open = {storeForm}
                close = {handleCloseStoreForm}
            >
                <StoreForm
                    edit = {storeEdit}
                    closeForm = {handleCloseStoreForm}
                    data = {selectedStore}
                    parentId = {parentId}
                />
            </ModalForm>
        </>
    )
}))

StoreTable.propTypes = {
    data: PropTypes.array,
    parentId: PropTypes.string
}