import React from 'react'
import Add from '@material-ui/icons/Add'
import { makeStyles } from '@material-ui/core/styles'
import { loadCSS } from 'fg-loadcss'
import PropTypes from 'prop-types'
import { IconButton } from '../button'

export const AddButton = ({onClick}) => {

    React.useEffect(() => {
        const node = loadCSS(
            'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
            document.querySelector('#font-awesome-css'),
        )

        return () => {
            node.parentNode.removeChild(node)
        }
    }, [])

    const classes = useStyles()

    return (
        <IconButton>
            <div className = {classes.icons} onClick = {onClick}>
                <Add/>
            </div>
        </IconButton>
    )
}

AddButton.propTypes = {
    onClick: PropTypes.func.isRequired
}



const useStyles = makeStyles((theme) => ({
    iconCont: {
        transition: 'all 0.2s',
        '&:active': {
          transform: 'scale(0.8)'
      }
    },
    icons: {
        display: 'flex',
        alignItems: 'center',
        transition: 'ease-in',
        transitionDuration: '1s',
        color: '#000000',
        margin: '2px',
        scale: 1.7,
        '& > .fa': {
            margin: theme.spacing(2),
        },
        cursor: 'pointer',
    } 
}))
  