import React, { memo, useCallback, useEffect, useMemo, useState } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
// import TablePagination from '@material-ui/core/TablePagination'
import TableHead from '@material-ui/core/TableHead'
import ViewWeekIcon from '@material-ui/icons/ViewWeek'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { Notification } from '../notification/tableNotification'
import styles from './index.module.sass'
import { CustomMenu, CustomMenuItem } from '../menu'
import { Button, IconButton, ButtonWithIcon } from '../button'
import { Checkbox, Collapse, Zoom, Slide, TablePagination } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import DeleteIcon from '@material-ui/icons/Delete'
import GetAppIcon from '@material-ui/icons/GetApp'
import SearchIcon from '@material-ui/icons/Search'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import FilterListIcon from '@material-ui/icons/FilterList'
import { inject } from 'mobx-react'
import { Input } from '../input'
import { MyTable } from './myTable'
import { RadioButtonGroup } from '../radioGroup'
import { getStatuses, maxRowsOnTable, statuses } from '../../constants'
import { TableLoader } from '../loading/tableLoader'

  const EnhancedTableHead = memo(
    ({
      classes, 
      tableTitles, 
      onClickDelete, 
      filterColumn, 
      onClickCheckBox, 
      selectedRows, 
      detailPanel, 
      checkBox,
      actions
    }) => {
    return (
      <TableHead>
        <TableRow className = {classes.headTableRow}>
            {detailPanel && <TableCell
              className = {classes.tableCellItems}
              align='left' 
            >
            </TableCell>}
            {tableTitles.map((item, index) => 
              <TableCell
                key={item.name}
                align={index === 0 ? 'center': 'center'} 
                padding='default'
                className={filterColumn.includes(item.key) ? classes.hide : classes.tableCellItems}
              >
                {item.name}
              </TableCell>
              )}
            {(checkBox || actions) && <TableCell
              align='right' 
              padding='default'
              className={classes.tableCellItems}
            >
              {checkBox && <Checkbox
                color = 'primary'
                onClick = {onClickCheckBox}
                checked = {(selectedRows.length > 0) ? true : false}
              />}
            </TableCell>}
        </TableRow>
      </TableHead>
    )
  })
  
  EnhancedTableHead.propTypes = {
    classes: PropTypes.object.isRequired,
    rightButton: PropTypes.bool
  }
  
  
  export const CustomTable = inject('lang')(({ 
    page, 
    setPage,
    lang,
    total,
    actions,
    getData,
    checkBox,
    searchBar,
    statusBar,
    tableTitle, 
    onRowClick,
    statusValue,
    tableContent, 
    onChangePage,
    renderButtons,
    onClickDelete,
    onChangeStatus,
    tableCellTitles,
    detailPanelTitle,
    onClickPlusButton,
    downloadTableData,
    detailTableColumns,
    defaultHiddenColumns,
    onClickSearchBarButton,
  }) => {

    const [selectedColumn, setSelectedColumn] = useState(defaultHiddenColumns ? defaultHiddenColumns : [])

    const [selectedRows, setSelectedRows] = useState([])

    const [searchValue, setSearchValue] = useState('')

    // const [page, setPage] = useState(0)

    const [status, setStatus] = useState(0)

    const [detailPanelOrdinalNumber, setDetailPanelOrdinalNumber] = useState(undefined)

    const [loading, setLoading] = useState(false)
  
    const getAppIconMemo = useMemo(() => <GetAppIcon/>, [])
    
    const addIconMemo = useMemo(() => <AddIcon/>, [])
    
    const deleteIconMemo = useMemo(() => <DeleteIcon/>, [])

    const statusButton = useMemo(() => <ButtonWithIcon
        variant = 'contained'
        icon = {<FilterListIcon/>}
      >
        {lang.forms.buttonTitles.status}
      </ButtonWithIcon>
    , [lang.activeLang])

    const filterColumnsButton = useMemo(() => <ButtonWithIcon
        variant = 'contained'
        icon = {<ViewWeekIcon/>}
      >
        {lang.forms.buttonTitles.filter}
      </ButtonWithIcon>
    , [lang.activeLang])

  //   const searchBarButtonMemo = useMemo(() => <Button
  //       withoutStyle
  //       onClick = {() => onClickSearchBarButton(searchValue)}
  //     >
  //       <SearchIcon/>
  //     </Button>
  // , [searchValue, setSearchValue])

    const classes = useStyles()

    const handleSelectColumn = useCallback(key => {
      if (selectedColumn.includes(key)) {
        const filteredSelectedColumns = selectedColumn.filter(item => item !== key)
        setSelectedColumn(filteredSelectedColumns)
      } else {
        setSelectedColumn(prevState => [key, ...prevState])
      }
    }, [selectedColumn])

    const handleSelectRow = useCallback(id => {
      if (selectedRows.includes(id)) {
        const filteredSelectedRowss = selectedRows.filter(item => item !== id)
        setSelectedRows(filteredSelectedRowss)
      } else {
        setSelectedRows(prevState => [id, ...prevState])
      }
    }, [selectedRows, setSelectedRows])

    const handleGeneralSelect = useCallback(() => {
      if (selectedRows.length > 0) {
        setSelectedRows([])
      } else {
        const allRowsIds = tableContent.map(item => item._id)
        setSelectedRows(allRowsIds)
      }
    }, [selectedRows, setSelectedRows])

    const handleRowClick = useCallback(partner => {
      onRowClick && onRowClick(partner)
    }, [])

    const handleClickDelete = useCallback(async () => {
      const success = await onClickDelete(selectedRows)
      if (success) {
        setSelectedRows([])
      }
    }, [selectedRows])

    const handleOpenDetailPanel = useCallback(ordinalNumber => {
      if (ordinalNumber === detailPanelOrdinalNumber) {
        setDetailPanelOrdinalNumber(undefined)
      } else {
        setDetailPanelOrdinalNumber(ordinalNumber)
      }
    }, [detailPanelOrdinalNumber])

    const handleChangePage = useCallback((event, page) => {
      // onChangePage(pageNumber)
      setPage(page)
      setLoading(true)
      getData({page, status, search: searchValue})
    }, [page])

    const handleChangeSearchBarValue = ({target}) => {
      setSearchValue(target.value)
    }

    const handleSubmitSearch = () => {
      setLoading(true)
      getData({page, status, search: searchValue})
    }
    // const handleClicSearchBarButton = () => {
    //   // setSearchValue(value)
    //   // onClickSearchBarButton(searchValue)
    // }

    const handleChangeStatus = ({target}) => {
      console.log(searchValue)
      // onChangeStatus(e)
      setStatus(parseInt(target.value))
      setLoading(true)
      getData({page, status: target.value, search: searchValue})
    }

    const handlePressInputSearchBar = ({charCode}) => (charCode === 13 && handleSubmitSearch())

    const handleClickDownload = useCallback(() => console.log('dowload'), [])

    const renderCellTitles = useMemo(() => tableCellTitles.map((item, index) => 
      <CustomMenuItem 
        key = {item.name}
        checked = {selectedColumn.includes(item.key) ? false : true}
        onClick = {() => handleSelectColumn(item.key, index)}
      >
        {item.name}
      </CustomMenuItem>
    ), [selectedColumn, lang.activeLang, tableCellTitles])

    const renderStatuses = useCallback(closeMenu => 
      <RadioButtonGroup
        onClose = {closeMenu}
        data = {getStatuses(lang)}
        value = {status}
        onChange = {handleChangeStatus}
      />
    , [status, searchValue, lang.activeLang])

    useEffect(() => {
      setLoading(false)
    }, [tableContent])

    return (
      <div className={classes.root}>
        <Paper className={styles.tableCont} elevation = {3}>
          <div className = {classes.tableHeadCont}>
            {/* <div className={classes.title}>{tableTitle}</div> */}
            <div className = {classes.headLeftSide}>
              {searchBar && <div className = {classes.leftSideItems}>
                <Input
                  standart
                  placeholder = {lang.forms.buttonTitles.search}
                  onChange = {handleChangeSearchBarValue}
                  value = {searchValue}
                  onKeyPress = {handlePressInputSearchBar}
                >
                  {/* {searchBarButtonMemo} */}
                  <Button
                    withoutStyle
                    onClick = {handleSubmitSearch}
                  >
                    <SearchIcon/>
                  </Button>
                </Input>
              </div>}
              <div className = {classes.leftSideItems}>
                {downloadTableData && <ButtonWithIcon
                  icon = {getAppIconMemo}
                  variant = 'contained'
                  onClick = {handleClickDownload}
                  disabled
                >
                  {lang.forms.buttonTitles.download}
                </ButtonWithIcon>}
              </div>
            </div>
            <div className = {classes.headRightSide}>
              {renderButtons && renderButtons(selectedRows)}
              {statusBar && <CustomMenu
                render = {renderStatuses}
                button = {statusButton}
              />}
              {defaultHiddenColumns && <CustomMenu
                button = {filterColumnsButton}
              >
                {renderCellTitles}
              </CustomMenu>}
              {onClickPlusButton && <ButtonWithIcon
                onClick = {onClickPlusButton}
                icon = {addIconMemo}
                variant = 'contained'
              >
                {lang.forms.buttonTitles.add}
              </ButtonWithIcon>}
              {onClickDelete && <ButtonWithIcon 
                onClick = {handleClickDelete}
                variant = 'contained'
                disabled = {(selectedRows.length > 0) ? false : true}
                icon = {deleteIconMemo}
              >
                {lang.forms.buttonTitles.delete}
              </ButtonWithIcon>}
            </div>
          </div>
          <TableContainer className={styles.container}>
            <Table
              className={classes.table}
              stickyHeader = {true}
              aria-label="sticky table"
              size='medium'
            >
              <EnhancedTableHead
                classes = {classes}
                tableTitles = {tableCellTitles}
                actions = {actions}
                filterColumn = {selectedColumn}
                selectedRows = {selectedRows}
                onClickCheckBox = {handleGeneralSelect}
                detailPanel = {detailPanelTitle}
                onClickDelete = {onClickDelete}
                checkBox = {checkBox}
              />
                <BodyOfTable
                  detailTableColumns = {detailTableColumns}
                  onSelectRow = {handleSelectRow}
                  onRowClick = {handleRowClick}
                  onRowClickParent ={onRowClick}
                  onOpenDetailPanel = {handleOpenDetailPanel}
                  tableContent = {tableContent}
                  classes = {classes}
                  tableCellTitles = {tableCellTitles}
                  selectedColumn = {selectedColumn}
                  actions = {actions}
                  checkBox = {checkBox}
                  selectedRows = {selectedRows}
                  onClickDelete = {onClickDelete}
                  detailPanelTitle = {detailPanelTitle}
                  detailPanelOrdinalNumber = {detailPanelOrdinalNumber}
                />
            </Table>
          </TableContainer>
          <TableLoader start = {loading}/>
          {(total > 10) && <TablePagination
            rowsPerPageOptions={[maxRowsOnTable]}
            component="div"
            count={total}
            page = {page}
            rowsPerPage={maxRowsOnTable}
            labelDisplayedRows={
              ({ from, to, count }) => {
                return '' + from + '-' + to + ` ${lang.table.pagination.of} ` + count
              }
            }
            // labelRowsPerPage = {lang.table.pagination.rowsPerPage}
            className = {styles.pagination}
            onChangePage={handleChangePage}
            // onChangeRowsPerPage={handleChangeRowsPerPage}
          />}
        </Paper>
      </div>
    )
  }
)
  CustomTable.propTypes = {
    tableContent: PropTypes.array.isRequired,
    tableTitle: PropTypes.string,
    tableCellTitles: PropTypes.array.isRequired,
    actions: PropTypes.array,
    onRightButtonClick: PropTypes.func,
    onClickDelete: PropTypes.func,
    childComponent: PropTypes.element,
    onClickPlusButton: PropTypes.func,
    onRowClick: PropTypes.func,
    rightButton: PropTypes.bool,
    defaultHiddenColumns: PropTypes.array,
  }

  const BodyOfTable = memo(
    ({
      detailTableColumns,
      onSelectRow,
      onRowClick,
      onRowClickParent,
      onOpenDetailPanel,
      tableContent,
      classes,
      checkBox,
      tableCellTitles,
      selectedColumn,
      actions,
      selectedRows,
      onClickDelete,
      detailPanelTitle,
      detailPanelOrdinalNumber
    }) => {
    return (
      <TableBody className = {styles.tableBody}>
      {(Array.isArray(tableContent) && (tableContent.length > 0)) ? tableContent
        .map((itemMain, ordinalNumber) => {
          return (
            <>
              <TableRow
                hover = {onRowClickParent ? true : false}
                className = {classes.tableRow}
                tabIndex={-1}
                key = {itemMain[tableCellTitles[0]]}
              >
                {itemMain.detailPanel && <TableCell
                  align='left'
                  // className={selectedColumn.includes(item.key) ? classes.hide : classes.cell}
                >
                  <Button 
                    withoutStyle
                    onClick = {() => onOpenDetailPanel(ordinalNumber)}
                  >
                    {(detailPanelOrdinalNumber === ordinalNumber) ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
                  </Button>
                </TableCell>}
                {tableCellTitles.map(item => 
                  <TableCell
                    key = {item.key}
                    onClick={() => onRowClick(itemMain)} 
                    align='center'
                    className={selectedColumn.includes(item.key) ? classes.hide : classes.cell}
                  >
                    {itemMain[item.key]}
                  </TableCell>
                )}
                {(checkBox || actions) && <TableCell 
                  align = 'right' 
                  className = {classes.cell}
                >
                  {checkBox && <Checkbox
                    checked = {selectedRows.includes(itemMain._id) ? true : false}
                    color="primary"
                    onClick = {() => itemMain._id && onSelectRow(itemMain._id)}
                  />}
                  {actions && actions.map(action =>
                    <Button
                      withoutStyle 
                      className = {classes.iconButton}
                      onClick={() => action.onClick(itemMain)}
                    >
                      {action.icon}
                    </Button>
                  )}
                </TableCell>}
              </TableRow>
              {itemMain.detailPanel && <TableRow>
                <TableCell className = {classes.collapseCont} colSpan = {tableCellTitles.length + 2}>
                  <Collapse in = {detailPanelOrdinalNumber === ordinalNumber}>
                    {itemMain.detailPanel && <MyTable
                      columns = {detailTableColumns}
                      title = {detailPanelTitle}
                      detailPanel = {itemMain.detailPanel}
                    />}
                  </Collapse>
                </TableCell>
              </TableRow>}
            </>)
          }
        ) : <Notification/> }
      </TableBody>
    )
  })


  const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      alignItems: 'center',
      display: 'flex',
      justifyContent: 'center',
      padding: '1em',
      height: '100%'
    },
    paper: {
      width: '100%',
      margin: theme.spacing(2),
      height: '100%'
    },
    
    tableHeadCont: {
      display: 'flex',
      width: '100%',
      padding: '1em',
      justifyContent: 'space-between',
      transition: 'all 0.3s'
    },
    headLeftSide: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
    },
    leftSideItems: {
      display: 'flex',
      marginRight: '1em',
      paddingLeft: '0.2em',
      
    },
    headRightSide: {
      width: '100%',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    title: {
      // width: '100%',
      padding: '1em',
      fontSize: '1.4em',
      fontWeight: 'bold'
    },
    tableCellItems: {
      fontSize: '1.2em',
      color: '#00000070',
      backgroundColor: '#fff'
    },
    table: {
      minWidth: 750,
      height: 'max-content',
    },
    tableRow: {
      position: 'relative',
      cursor: 'pointer',
      // padding: '0 !important',
      },
    cell: {
      // padding: 0,
      // fontSize: '1.2em !important',
    },
    actionBar: {
      display: 'flex',
      justifyContent: 'flex-end'
    },
    collapseCont: {
      padding: '0'
    },
    actionsContMain: {
      display: 'flex',
      alignItems: 'center',
    },
    accordionCont: {
      width: '100%'
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
    hide: {
      display: 'none',
    }
  }))
  
