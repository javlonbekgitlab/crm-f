import React, { memo } from 'react'
import styles from './index.module.sass'
import PropTypes from 'prop-types'
import { Checkbox } from '@material-ui/core'

export const MyTable = memo(({columns, detailPanel, title}) => {
    return (
        <div className = {styles.cont}>
            <div className = {styles.head}>
                <div className = {styles.title}>{title}</div>
                {(!detailPanel.action) && <div className = {styles.columnsCont}>
                    <div className = {styles.row}>
                        {columns.map(item => (
                            <div className = {styles.cell}>{item.name}</div>
                        ))}
                    </div>
                </div>}
            </div>
            <div className = {styles.body}>
                {detailPanel.action  
                    ? <ListWithAction detailPanel = {detailPanel}/>
                    : <TableWithoutAction detailPanel = {detailPanel} columns = {columns}/>
                }
            </div>
        </div>
    )
})

MyTable.proptypes = {
    columns: PropTypes.array,
    data: PropTypes.array,
    title: PropTypes.string
}

const TableWithoutAction = memo(({detailPanel, columns}) => {
    return (
        <div className = {styles.bodyContent}>
            {detailPanel.data.map(item => (
                <div className = {styles.row}>
                    {columns.map(columns => (
                        <div className = {styles.cell}>{item[columns.key]}</div>
                    ))}
                </div>
            ))}
        </div>
    )
})

const ListWithAction = memo(({detailPanel}) => {
    return (
        <div className = {styles.mainCont}>
            <div className = {styles.gridCont}>
                {detailPanel.data.map(item => (
                    <div className = {styles.role}>
                        <Checkbox 
                            color = 'primary'
                            checked = {item.access}
                        />
                        <div className = {styles.roleTitle}>{item.title}</div>
                    </div>
                ))}
            </div>
        </div>
    )
})

