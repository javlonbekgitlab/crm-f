import React, { memo } from 'react'
import IconButton from '@material-ui/core/IconButton'
import DescriptionIcon from '@material-ui/icons/Description'
import styles from './index.module.sass'

export const UploadFile = memo(({name, value, acceptFile, disabled, error, oldFile, ...props }) => {
    return (
        <div className = {error ? `${styles.inputUploadCont} ${styles.errorInput}` : `${disabled ? `${styles.inputUploadCont} ${styles.disabledCont}` : styles.inputUploadCont}`}>
            <div className = {styles.inputCont}>
                <input  
                    accept={acceptFile} 
                    className={styles.input} 
                    id={name} 
                    name = {name}
                    type="file"
                    disabled = {disabled}
                    {...props}
                />
                <div className = {styles.inputUploadTitle}>{value.name}</div>
                <label className = {styles.icon} htmlFor={name}>
                    <IconButton disabled = {disabled} aria-label="upload picture" component="span">
                        <DescriptionIcon/>
                    </IconButton>
                </label>
            </div>
            {error && <div className = {styles.error}>{error}</div>}
            {oldFile && <a className = {styles.fileName} target = 'blank' href = {oldFile.url}>{oldFile.name}</a>}
        </div>
    )
})
