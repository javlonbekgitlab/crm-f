import React from 'react' 
import { inject } from 'mobx-react'
import { Paper } from '@material-ui/core'
import styles from './index.module.sass'
import { Input } from '../input'
import { maskUserMobile, regexSpace } from '../../constants/index'
import ClearIcon from '@material-ui/icons/Clear'
import InputMask from 'react-input-mask';
import { Button, IconButton } from '../button'
import { useFormik } from 'formik'
import { onValidate } from './helperFunctions'

export const CashierForm = inject('lang')(({
    lang, 
    onClickAdd, 
    onClickDelete, 
    name,
    value,
    error
}) => {

    const handleClickAdd = values => {
        onClickAdd({
            target: {
                name,
                value: values
            }
        })
    }

    const handleValidate = values => {
        return onValidate({...values, value})
    }

    const formik = useFormik({
        initialValues: {
            fullName: '',
            phone: ''
        },
        onSubmit: handleClickAdd,
        validate: handleValidate,
        validateOnChange: false
    })

    const primaryOnChange = ({target}) => {
        let value = target.value.replace(regexSpace, '')
        formik.handleChange({
            target: {
                name: target.name,
                value
            }
        })
    }

    const { 
        values: {
            fullName,
            phone
        },
        errors: {
            fullNameErr,
            phoneErr
        },
        handleSubmit
    } = formik
    
    return (
        <div className = {styles.cont}>
            <div className = {styles.paperCont}>
                <Paper
                    className = {error ? `${styles.paper} ${styles.errCont}` : styles.paper}
                    elevation = {3}
                >
                    <div className = {styles.itemTitle}>{lang.forms.storeOrder.cashier}</div>
                    <div className = {styles.topColumnItem}>
                        <div className = {styles.itemChilds}>
                            <Input
                                name = 'fullName'
                                onChange = {primaryOnChange}
                                error = {fullNameErr}
                                value = {fullName}
                                standart
                                label = {lang.forms.order.firstMiddleLastName}
                            />
                        </div>
                        <div className = {styles.itemChilds}>
                            <InputMask
                                onChange = {primaryOnChange}
                                standart
                                mask = {maskUserMobile}
                                name = 'phone'
                                value = {phone}
                            >
                                {inputProps => <Input 
                                    label = {`${lang.forms.order.telephone}/${lang.forms.order.login}`}
                                    {...inputProps}
                                    error = {phoneErr}
                                />}
                            </InputMask>
                        </div>
                    </div>
                    <div className = {styles.buttonCont}>
                        <Button onClick = {handleSubmit}>{lang.forms.buttonTitles.addCashier}</Button>
                    </div>
                </Paper>
            </div>
            {value.length > 0 && <div className = {styles.paperCont}>
                <Paper
                    className = {styles.paper}
                    elevation = {3}
                >
                    <div className = {styles.topColumnItem}>
                        {value.map((item, index) => 
                            <div className = {styles.waiterName} key = {item.fullName + index}>
                                <div>{item.fullName}</div>
                                <IconButton onClick = {() => onClickDelete(item.phone)}>
                                    <ClearIcon/>
                                </IconButton>
                            </div>
                        )}
                    </div>
                </Paper>
            </div>}
        </div>
    )
})