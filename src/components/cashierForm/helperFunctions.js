import { regexSpace } from "../../constants"

export const onValidate = ({fullName, phone, value}) => {
    const errors = {}
    if (!fullName) {
        errors.fullNameErr = 'required'
    }
    if (!phone) {
        errors.phoneErr = 'required'
    }
    for (let index = 0; index < value.length; index ++) {
        if (value[index].phone === phone) {
            errors.phoneErr = 'unique'
        }
    }
    return errors
}